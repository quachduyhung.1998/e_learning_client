module.exports = {
    plugins: [
        [
            'module-resolver',
            {
                root: ['./src'],
                extensions: ['.js', '.ts', '.tsx', '.json', '.css'],
                alias: {
                    '__tests__/*': './__tests__/',
                    'features/*': './src/features/',
                    'helpers/*': './src/helpers/'
                }
            }
        ]
    ]
};