module.exports = {
    parser: '@typescript-eslint/parser', // Specifies the ESLint parser
    parserOptions: {
        ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
        sourceType: 'module', // Allows for the use of imports
        ecmaFeatures: {
            jsx: true // Allows for the parsing of JSX
        },
        project: `./tsconfig.json`
    },
    settings: {
        react: {
            version: 'detect' // Tells eslint-plugin-react to automatically detect the version of React to use
        }
    },
    extends: [
        'plugin:react/recommended',
        'plugin:@typescript-eslint/recommended', // Uses the recommended rules from the @typescript-eslint/eslint-plugin
        'prettier/@typescript-eslint', // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
        'plugin:prettier/recommended' // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
    ],
    rules: {
        'import/no-unresolved': 0,
        'class-methods-use-this': 0,
        '@typescript-eslint/ban-ts-ignore': 0,
        'import/prefer-default-export': 0,
        '@typescript-eslint/no-use-before-define': 0,
        'react/destructuring-assignment': 0,
        'prefer-destructuring': 0,
        '@typescript-eslint/no-empty-interface': 0,
        'no-console': 0,
        'global-require': 0,
        'no-plusplus': 0,
        'no-param-reassign': 0,
        'no-empty': 0,
        'react/prefer-stateless-function': 0,
        'import/no-cycle': 0,
        'no-underscore-dangle': 0,
        'react/no-access-state-in-setstate': 0,
        '@typescript-eslint/ban-ts-comment': 0,
        'typescript-eslint/camelcase': 0,
        '@typescript-eslint/no-non-null-assertion': 0
    }
};
