import { ChangeEventHandler, useState, ChangeEvent } from 'react';

const useInputState = (
    initialValue: string
): [(value: string) => void, ChangeEventHandler, string | undefined] => {
    const [value, setValue] = useState<string>(initialValue);

    const onChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
        setValue(evt.target.value);
    };

    const setInitialValue = (value = '') => {
        setValue(value);
    };

    return [setInitialValue, onChange, value];
};

export default useInputState;
