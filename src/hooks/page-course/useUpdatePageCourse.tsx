import LoadingManager from 'components/loading/LoadingManager';
import { ToastSuccess } from 'helper/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import PageCourse from 'types/PageCourse';
import UpdatePageCourseReq from 'types/requests/UpdatePageCourseReq';
import UpdatePageCourseResponse from 'types/responses/UpdatePageCourseResponse';

function useUpdatePageCourse(): [
    PageCourse | null,
    (data: any) => Promise<void>,
    string
] {
    const { t } = useTranslation();
    const [
        resUpdatePageCourse,
        setResUpdatePageCourse
    ] = useState<PageCourse | null>(null);
    const [messageError, setMessageError] = useState<string>('');
    const history = useHistory();
    const onUpdatePageCourse = async (data: any) => {
        try {
            const res = await ApiHelper.post<
                UpdatePageCourseReq,
                UpdatePageCourseResponse
            >(`/lessons/${data?.course_id}`, data, {
                'Content-Type': 'application/json'
            });
            if (res.status === SUCCESS) {
                setResUpdatePageCourse(res.data);
                history.goBack();
                ToastSuccess({ message: t(`success`) });
            } else {
                setResUpdatePageCourse(null);
                setMessageError(`${t(`messages.${res.code}`)}`);
            }
        } catch (error) {
            setResUpdatePageCourse(null);
            setMessageError(`${t(`fail`)}`);
        }
        LoadingManager.hide();
    };

    return [resUpdatePageCourse, onUpdatePageCourse, messageError];
}

export default useUpdatePageCourse;
