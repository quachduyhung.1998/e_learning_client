import { getUrl } from 'helper/String';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import PageCourse from 'types/PageCourse';
import GetPageCourseReq from 'types/requests/GetPageCourseReq';
import GetPageCourseResponse from 'types/responses/GetPageCourseResponse';

function useGetPageCourse(): [
    PageCourse | null,
    (request: GetPageCourseReq) => Promise<void>,
    React.Dispatch<React.SetStateAction<PageCourse | null>>
] {
    const [resPageCourse, setResPageCourse] = useState<PageCourse | null>(null);

    const onGetPageCourse = async (request: GetPageCourseReq) => {
        try {
            const res = await ApiHelper.fetch<
                GetPageCourseReq,
                GetPageCourseResponse
            >(getUrl(Endpoint.LESSON + `/${request.courseId}`, { ...request }));
            if (res.status === SUCCESS) {
                setResPageCourse(res.data);
            } else {
                setResPageCourse(null);
            }
        } catch (error) {
            setResPageCourse(null);
        }
    };

    return [resPageCourse, onGetPageCourse, setResPageCourse];
}

export default useGetPageCourse;
