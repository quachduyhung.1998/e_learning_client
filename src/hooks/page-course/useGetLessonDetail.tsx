import { getUrl } from 'helper/String';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import PageCourse from 'types/PageCourse';
import GetPageCourseReq from 'types/requests/GetPageCourseReq';
import GetPageCourseResponse from 'types/responses/GetPageCourseResponse';

function useGetLessonDetail(): [
    PageCourse | null,
    (request: GetPageCourseReq) => Promise<void>,
    React.Dispatch<React.SetStateAction<PageCourse | null>>
] {
    const [lesson, setLesson] = useState<PageCourse | null>(null);

    const onGetLesson = async (request: GetPageCourseReq) => {
        try {
            const res = await ApiHelper.fetch<
                GetPageCourseReq,
                GetPageCourseResponse
            >(getUrl(Endpoint.LESSON, { ...request }));
            if (res.status === SUCCESS) {
                setLesson(res.data);
            } else {
                setLesson(null);
            }
        } catch (error) {
            setLesson(null);
        }
    };

    return [lesson, onGetLesson, setLesson];
}

export default useGetLessonDetail;
