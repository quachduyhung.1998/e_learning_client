import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import PageCourse from 'types/PageCourse';
import CreatePageCourseReq from 'types/requests/CreatePageCourseReq';
import CreatePageCourseResponse from 'types/responses/CreatePageCourseResponse';
import { ToastSuccess } from 'helper/Toast';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

function useCreatePageCourse(): [
    PageCourse | null,
    (data: CreatePageCourseReq) => Promise<void>,
    string
] {
    const { t } = useTranslation();
    const history = useHistory();
    const [
        resCreatePageCourse,
        setResCreatePageCourse
    ] = useState<PageCourse | null>(null);
    const [messageError, setMessageError] = useState<string>('');
    const onCreatePageCourse = async (data: CreatePageCourseReq) => {
        try {
            const res = await ApiHelper.post<
                CreatePageCourseReq,
                CreatePageCourseResponse
            >(Endpoint.LESSON, data, {
                'Content-Type': 'application/json'
            });
            if (res.status === SUCCESS || res.status === 201) {
                setResCreatePageCourse(res.data);
                history.goBack();
                ToastSuccess({ message: t(`success`) });
            } else {
                setResCreatePageCourse(null);
            }
        } catch (error) {
            setResCreatePageCourse(null);
        }
    };

    return [resCreatePageCourse, onCreatePageCourse, messageError];
}

export default useCreatePageCourse;
