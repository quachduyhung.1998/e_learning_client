import { ToastError } from 'components/admin/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { GroupUser } from 'types/GroupUser';
import DetailGroupUserRes from 'types/responses/DetailGroupUserRes';

function useDetailGroupUser() {
    const { t } = useTranslation();
    const [resDetailGroupUser, setResDetailGroupUser] = useState<GroupUser>();
    const onDetailGroupUser = async (request: { groupId: string }) => {
        try {
            const res = await ApiHelper.fetch<
                { groupId: string },
                DetailGroupUserRes
            >(Endpoint.DETAIL_GROUP_USER, request);
            if (res.status === SUCCESS) {
                setResDetailGroupUser(res.data.result);
            } else {
                setResDetailGroupUser(undefined);
                ToastError({
                    message: t('fail'),
                    description: t(`messages.${res.code}`)
                });
            }
        } catch (error) {
            setResDetailGroupUser(undefined);
            ToastError({ description: t(`fail`) });
        }
    };
    return { resDetailGroupUser, onDetailGroupUser };
}

export default useDetailGroupUser;
