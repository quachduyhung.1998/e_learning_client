import { ToastError, ToastSuccess } from 'components/admin/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

function useDeleteGroupUser() {
    const { t } = useTranslation();
    const [resDeleteGroupUser, setResDeleteGroupUser] = useState<number>(0);
    const onDeleteGroupUser = async (request: { groupId: string }) => {
        try {
            const res = await ApiHelper.post(
                Endpoint.DELETE_GROUP_USER,
                request
            );
            if (res.status === SUCCESS) {
                setResDeleteGroupUser(moment().unix());
                ToastSuccess({
                    message: t('success'),
                    description: t('delete_success')
                });
            } else {
                setResDeleteGroupUser(0);
                ToastError({
                    message: t('fail'),
                    description: t(`messages.${res.code}`)
                });
            }
        } catch (error) {
            setResDeleteGroupUser(0);
            ToastError({ description: t(`fail`) });
        }
    };
    return { resDeleteGroupUser, onDeleteGroupUser };
}

export default useDeleteGroupUser;
