import { ToastError, ToastSuccess } from 'components/admin/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

function useAddUserGroup() {
    const { t } = useTranslation();
    const [resAddUserGroup, setResAddUserGroup] = useState();
    const onAddUserGroup = async (request: {
        groupId: string;
        userIds: string[];
    }) => {
        try {
            const res = await ApiHelper.post(Endpoint.ADD_USER_GROUP, request);
            if (res.status === SUCCESS) {
                setResAddUserGroup(res.data);
                ToastSuccess({
                    message: t('success'),
                    description: t('add_user_success')
                });
            } else {
                setResAddUserGroup(undefined);
                ToastError({
                    message: t('fail'),
                    description: t(`status_code.${res.status}`)
                });
            }
        } catch (error) {
            setResAddUserGroup(undefined);
            ToastError({ description: t(`fail`) });
        }
    };
    return { resAddUserGroup, onAddUserGroup };
}

export default useAddUserGroup;
