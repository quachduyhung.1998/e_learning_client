import { ToastError, ToastSuccess } from 'components/admin/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

function useCreateGroupUser() {
    const { t } = useTranslation();
    const [resCreateGroupUser, setResCreateGroupUser] = useState();
    const onCreateGroupUser = async (request: {
        fullName: string;
        mail: string;
        description: string;
    }) => {
        try {
            const res = await ApiHelper.post(
                Endpoint.CREATE_GROUP_USER,
                request
            );
            if (res.status === SUCCESS) {
                setResCreateGroupUser(res.data);
                ToastSuccess({
                    message: t('success'),
                    description: t('create_success')
                });
            } else {
                setResCreateGroupUser(undefined);
                ToastError({
                    message: t('fail'),
                    description: t(`messages.${res.code}`)
                });
            }
        } catch (error) {
            setResCreateGroupUser(undefined);
            ToastError({ description: t(`fail`) });
        }
    };
    return { resCreateGroupUser, onCreateGroupUser };
}

export default useCreateGroupUser;
