import { ToastError } from 'components/admin/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { GroupUser } from 'types/GroupUser';

function useListGroupUser() {
    const { t } = useTranslation();
    const [loading, setLoading] = useState(false);
    const [resListGroupUser, setResListGroupUser] = useState<GroupUser[]>([]);
    const [total, setTotal] = useState(0);
    const onListGroupUser = async (request: {
        limit: number;
        page: number;
        search: string;
    }) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch(
                Endpoint.LIST_GROUP_USER,
                request
            );
            if (res.status === SUCCESS) {
                setResListGroupUser(res.data.results);
                setTotal(res.data.total);
            } else {
                setResListGroupUser([]);
                setTotal(0);
                ToastError({
                    message: t('fail'),
                    description: t(`messages.${res.code}`)
                });
            }
        } catch (error) {
            setResListGroupUser([]);
            setTotal(0);
            ToastError({ description: t(`fail`) });
        }
        setLoading(false);
    };
    return { resListGroupUser, total, loading, onListGroupUser };
}

export default useListGroupUser;
