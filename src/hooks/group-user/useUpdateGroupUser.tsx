import { ToastError, ToastSuccess } from 'components/admin/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

function useUpdateGroupUser() {
    const { t } = useTranslation();
    const [resUpdateGroupUser, setResUpdateGroupUser] = useState();
    const onUpdateGroupUser = async (request: {
        groupId: string;
        fullName: string;
        mail: string;
        description: string;
    }) => {
        try {
            const res = await ApiHelper.post(
                Endpoint.UPDATE_GROUP_USER,
                request
            );
            if (res.status === SUCCESS) {
                setResUpdateGroupUser(res.data);
                ToastSuccess({
                    message: t('success'),
                    description: t('update_success')
                });
            } else {
                setResUpdateGroupUser(undefined);
                ToastError({
                    message: t('fail'),
                    description: t(`messages.${res.code}`)
                });
            }
        } catch (error) {
            setResUpdateGroupUser(undefined);
            ToastError({ description: t(`fail`) });
        }
    };
    return { resUpdateGroupUser, onUpdateGroupUser };
}

export default useUpdateGroupUser;
