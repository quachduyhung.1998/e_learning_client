import { ToastError, ToastSuccess } from 'components/admin/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

function useRemoveUserGroup() {
    const { t } = useTranslation();
    const [resRemoveUserGroup, setResRemoveUserGroup] = useState<number>(0);
    const onRemoveUserGroup = async (request: {
        groupId: string;
        userIds: string[];
    }) => {
        try {
            const res = await ApiHelper.post(
                Endpoint.REMOVE_USER_GROUP,
                request
            );
            if (res.status === SUCCESS) {
                setResRemoveUserGroup(moment().unix());
                ToastSuccess({
                    message: t('success'),
                    description: t('remove_user_success')
                });
            } else {
                setResRemoveUserGroup(0);
                ToastError({
                    message: t('fail'),
                    description: t(`messages.${res.code}`)
                });
            }
        } catch (error) {
            setResRemoveUserGroup(0);
            ToastError({ message: t('fail'), description: t(`fail`) });
        }
    };
    return { resRemoveUserGroup, onRemoveUserGroup };
}

export default useRemoveUserGroup;
