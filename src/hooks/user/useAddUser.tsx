import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';

function useAddUser() {
    const [loading, setLoading] = useState(false);
    const onAddUser = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.post(Endpoint.USER, request);
            setLoading(false);
            if (res.status === SUCCESS || res.status === 201) {
                return true;
            }
            return false;
        } catch (error) {
            return false;
        }
    };
    return { onAddUser, loading };
}
export default useAddUser;
