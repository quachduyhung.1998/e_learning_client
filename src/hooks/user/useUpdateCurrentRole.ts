import { ToastError } from 'helper/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { UpdateCurrentRoleReq } from 'types/requests/UpdateCurrentRoleReq';
import { BaseResponse } from 'types/responses/BaseResponse';
import { Role } from 'types/User';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';

const useUpdateCurrentRole = () => {
    const { t } = useTranslation();
    const [resUpdateCurrentRole, setResUpdateCurrentRole] = useState<
        string | null
    >(null);
    const onUpdateCurrentRole = async (roleArchetype: string) => {
        try {
            setResUpdateCurrentRole(roleArchetype);
            LocalStorageHelper.save(StorageKey.CURRENT_ROLE, roleArchetype);
        } catch (error) {
            setResUpdateCurrentRole(null);
            ToastError({ message: t('messages.UPDATE_CURRENT_FAIL') });
        }
    };
    return { resUpdateCurrentRole, onUpdateCurrentRole };
};

export default useUpdateCurrentRole;
