import { ToastError } from 'components/admin/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { GroupUser } from 'types/GroupUser';
import { ListUserReq } from 'types/requests';
import { ListUserFilterRes } from 'types/responses';
import { User } from 'types/User';

function useListUserFilter() {
    const { t } = useTranslation();
    const [resListUserFilter, setResListUserFilter] = useState<User[]>([]);
    const [total, setTotal] = useState(0);
    const onListUserFilter = async (request: ListUserReq) => {
        try {
            const res = await ApiHelper.fetch<ListUserReq, ListUserFilterRes>(
                Endpoint.USERS_LIST,
                request
            );
            if (res.status === SUCCESS) {
                setResListUserFilter(res.data.items);
                setTotal(res.data.total);
            } else {
                setResListUserFilter([]);
                setTotal(0);
                ToastError({
                    message: t('fail'),
                    description: t(`messages.${res.code}`)
                });
            }
        } catch (error) {
            setResListUserFilter([]);
            setTotal(0);
            ToastError({ message: t('fail'), description: t(`fail`) });
        }
    };
    return { resListUserFilter, setResListUserFilter, total, onListUserFilter };
}

export default useListUserFilter;
