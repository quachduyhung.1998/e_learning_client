import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { InforUserReq } from '../../types/requests';
import { InforUserResponse } from '../../types/responses';
import { User } from 'types/User';

function useDetailUser(): [
    (request: InforUserReq) => Promise<void>,
    User | undefined
] {
    const [resUserInfor, setResUserInfor] = useState<User>();
    const onFetchInfoUser = async (request: InforUserReq) => {
        try {
            const res = await ApiHelper.fetch<InforUserReq, InforUserResponse>(
                Endpoint.GET_DETAIL_USER + request?._id,
                request
            );
            if (res.status === SUCCESS) {
                setResUserInfor(res.data);
            }
        } catch (error) {}
    };
    return [onFetchInfoUser, resUserInfor];
}
export default useDetailUser;
