import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { ListUserData, ListUserResponse } from 'types/responses';
import { ListUserReq } from '../../types/requests';

const useListUserPermission = (): [
    (request: any) => Promise<void>,
    any | undefined,
    boolean
] => {
    const [loading, setLoading] = useState<boolean>(false);
    const [users, setUsers] = useState<any>();

    const onFetchUsers = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch<any, any>(
                Endpoint.USERS_PERMISSION + request?.roleId,
                request
            );
            setLoading(false);
            if (res.status === SUCCESS) {
                setUsers(res?.data?.data);
            }
        } catch (error) {
            setLoading(false);
        }
    };

    return [onFetchUsers, users, loading];
};

export default useListUserPermission;
