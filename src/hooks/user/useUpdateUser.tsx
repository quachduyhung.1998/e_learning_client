import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';

function useUpdateUser() {
    const [loading, setLoading] = useState(false);
    const onUpdateUser = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.put(
                `/users/${request?.user_id}/change-permissions`,
                request
            );
            setLoading(false);
            if (res.status === SUCCESS || res.status === 201) {
                return true;
            }
            return false;
        } catch (error) {
            return false;
        }
    };
    return { onUpdateUser, loading };
}
export default useUpdateUser;
