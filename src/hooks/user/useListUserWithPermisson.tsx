import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { ListUserData, ListUserResponse } from 'types/responses';
import { ListUserReq } from '../../types/requests';

const useListUser = () => {
    const [loading, setLoading] = useState<boolean>(false);
    const [users, setUsers] = useState<any>([]);

    const onFetchUsers = async (request: ListUserReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch(
                Endpoint.USERS_PERMISSION + request?.role,
                request
            );
            setLoading(false);
            if (res.status === SUCCESS) {
                setUsers(res.data?.data);
            }
        } catch (error) {
            setLoading(false);
        }
    };

    return [onFetchUsers, users, loading];
};

export default useListUser;
