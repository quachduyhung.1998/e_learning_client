import LoadingManager from 'components/loading/LoadingManager';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { Upload, UploadResponse } from './../types/responses/UploadResponse';
import { useTranslation } from 'react-i18next';

function useUpload(): [any, (request: FormData) => Promise<void>, string] {
    const { t } = useTranslation();
    const [res, setRes] = useState<any>(null);
    const [messageError, setMessageError] = useState<string>('');

    const onUpload = async (request: FormData) => {
        try {
            const res = await ApiHelper.postFormData(Endpoint.UPLOAD, request, {
                'Content-Type': 'multipart/form-data'
            });
            if (res.status === SUCCESS) {
                setRes(res.data);
            } else {
                setMessageError(`${t('upload_fail')}`);
                setRes(null);
            }
        } catch (error) {
            setMessageError(`${t('upload_fail')}`);
            setRes(null);
        }
        LoadingManager.hide();
    };

    return [res, onUpload, messageError];
}

export default useUpload;
