import React, { useState } from 'react';
import { BaseResponse } from 'types/responses/BaseResponse';
import ApiHelper from 'network/ApiClient';
import {
    SUCCESS,
    QUIZ_NOT_FOUND,
    COURSE_ALREADY_APPROVED,
    PAGE_NOT_FOUND
} from 'network/ResponseCode';
import { useTranslation } from 'react-i18next';
import { ToastSuccess } from 'components/admin/Toast';

interface GetQuizReq {
    courseId: number;
    courseModuleId: number | undefined;
    moduleType?: string;
}

interface GetQuizResponse extends BaseResponse {
    data: boolean;
}

function useDeleteQuiz(): [
    boolean,
    (request: GetQuizReq) => Promise<void>,
    string
] {
    const [resQuiz, setResQuiz] = useState<boolean>(false);
    const { t } = useTranslation();
    const [messageError, setMessageError] = useState<string>('');
    const onDeleteQuiz = async (request: GetQuizReq) => {
        try {
            const res = await ApiHelper.delete<
                GetQuizReq | undefined,
                GetQuizResponse
            >(`/${request.moduleType}`, {
                courseId: request.courseId,
                courseModuleId: request.courseModuleId
            });
            if (res.status === SUCCESS) {
                setResQuiz(res.data);
                ToastSuccess({
                    message: t('success'),
                    description: t('delete_success_confirm', {
                        name: t(`delete_${request.moduleType}`)
                    })
                });
            } else if (res.code === QUIZ_NOT_FOUND) {
                setMessageError(`${t('quiz_already_delete')}`);
                setResQuiz(res.data);
            } else if (res.code === PAGE_NOT_FOUND) {
                setMessageError(`${t('quiz_already_delete')}`);
                setResQuiz(res.data);
            } else if (res.code === COURSE_ALREADY_APPROVED) {
                setMessageError(
                    `${t('quiz_COURSE_ALREADY_APPROVEDalready_delete')}`
                );
                setResQuiz(res.data);
            } else {
                setMessageError(
                    `${t('delete_fail_confirm', {
                        name: t(`delete_${request.moduleType}`)
                    })}`
                );
                setResQuiz(false);
            }
        } catch (error) {
            setMessageError(
                `${t('delete_fail_confirm', {
                    name: t(`delete_${request.moduleType}`)
                })}`
            );
            setResQuiz(false);
        }
    };

    return [resQuiz, onDeleteQuiz, messageError];
}

export default useDeleteQuiz;
