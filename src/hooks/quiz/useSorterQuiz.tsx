import { message } from 'antd';
import moment from 'moment';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { BaseResponse } from 'types/responses/BaseResponse';
interface GetTopicReq {
    courseModuleId: number;
    beforeCourseModuleId: number;
    courseId: number;
    section: number;
}

interface GetTopicResponse extends BaseResponse {
    data: boolean;
}

function useSorterQuiz(): {
    resQuiz: number;
    onSorterQuiz: (request: GetTopicReq) => Promise<void>;
    loading: boolean;
} {
    const { t } = useTranslation();
    const [resQuiz, setResQuiz] = useState<number>(0);
    const [loading, setLoading] = useState<boolean>(false);
    const onSorterQuiz = async (request: GetTopicReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.post<
                GetTopicReq | undefined,
                GetTopicResponse
            >(Endpoint.MOVE_QUIZ, request);
            if (res.status === SUCCESS) {
                message.success(t('success'));
                setResQuiz(moment().unix());
            } else {
                if (res.code !== 'RECORD_NOT_FOUND') {
                    message.success(t(`messages.${res.code}`));
                }
                setResQuiz(0);
            }
        } catch (error) {
            message.success(t('fail'));
            setResQuiz(0);
        }
        setLoading(false);
    };

    return { resQuiz, onSorterQuiz, loading };
}

export default useSorterQuiz;
