import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS, ATTEMPT_ALREADY_CLOSE } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DetailQuestion } from 'types/Question';
import { GetQuestionsStartedQuizReq } from 'types/requests/GetQuestionsStartedQuizReq';
import { GetQuestionsStartedQuizRes } from 'types/responses/GetQuestionsStartedQuizRes';
const useGetQuestionStartedQuiz = () => {
    const { t } = useTranslation();
    const [question, setQuestion] = useState<DetailQuestion | null>(null);
    const [is_done_quiz, setIs_done_quiz] = useState(false);
    const [getQuestionStartedQuizError, setMessageError] = useState<string>('');
    const onGetQuestionsStartedQuiz = async (request: {
        attemptId: number;
        uniqueId: number;
        page: number;
    }) => {
        try {
            const res = await ApiHelper.fetch<
                GetQuestionsStartedQuizReq,
                GetQuestionsStartedQuizRes
            >(Endpoint.GET_QUESTION_STARTED_QUIZ, request);
            if (res.status === SUCCESS) {
                setQuestion(res.data);
            } else if (res.code === ATTEMPT_ALREADY_CLOSE) {
                setMessageError(`${t('quiz_already_done')}`);
                setIs_done_quiz(true);
            } else {
                setMessageError(`${t(`messages.${res.code}`)}`);
            }
            return;
        } catch (error) {}
        setQuestion(null);
        setMessageError(`${t(`fail`)}`);
        return;
    };
    return {
        question,
        onGetQuestionsStartedQuiz,
        is_done_quiz,
        getQuestionStartedQuizError
    };
};

export default useGetQuestionStartedQuiz;
