import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import GetHistoryStartedQuizReq from 'types/requests/GetHistoryStartedQuizReq';
import GetHistoryStartedQuizRes, {
    HistoryStaredQuiz
} from 'types/responses/GetHistoryStartedQuizRes';

const useHistoryStartedQuiz = () => {
    const [resGetHistoryStartedQuiz, setResGetHistoryStartedQuiz] = useState<
        HistoryStaredQuiz[]
    >([]);

    const onGetHistoryStartedQuiz = async (
        request: GetHistoryStartedQuizReq
    ) => {
        try {
            const res = await ApiHelper.fetch<
                GetHistoryStartedQuizReq,
                GetHistoryStartedQuizRes
            >(Endpoint.GET_HISTORY_STARTED_QUIZ, request);
            if (res.status === SUCCESS) {
                setResGetHistoryStartedQuiz(res.data);
            } else {
                setResGetHistoryStartedQuiz([]);
            }
        } catch (error) {
            setResGetHistoryStartedQuiz([]);
        }
    };

    return { resGetHistoryStartedQuiz, onGetHistoryStartedQuiz };
};

export default useHistoryStartedQuiz;
