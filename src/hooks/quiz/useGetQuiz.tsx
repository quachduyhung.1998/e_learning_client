import { getUrl } from 'helper/String';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import Quiz from 'types/Quiz';
import GetQuizReq from 'types/requests/GetQuizReq';
import GetQuizResponse from 'types/responses/GetQuizResponse';

function useGetQuiz(): [
    Quiz | null,
    (request: GetQuizReq) => Promise<void>,
    React.Dispatch<React.SetStateAction<Quiz | null>>
] {
    const [resQuiz, setResQuiz] = useState<Quiz | null>(null);

    const onGetQuiz = async (request: GetQuizReq) => {
        try {
            const res = await ApiHelper.fetch<GetQuizReq, GetQuizResponse>(
                getUrl(Endpoint.GET_QUIZ, { ...request }),
                request
            );
            if (res.status === SUCCESS) {
                setResQuiz(res.data);
            } else {
                setResQuiz(null);
            }
        } catch (error) {
            setResQuiz(null);
        }
    };

    return [resQuiz, onGetQuiz, setResQuiz];
}

export default useGetQuiz;
