import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS, ATTEMPT_ALREADY_CLOSE } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { UpdateAnswerStartedQuizReq } from 'types/requests/UpdateAnswerStartedQuizReq';
import { UpdateAnswerStartedQuizRes } from 'types/responses/UpdateAnswerStartedQuizRes';

const useUpdateAnswerStartedQuiz = () => {
    const { t } = useTranslation();
    const [updateAnswerStartedQuizError, setMessageError] = useState<string>(
        ''
    );
    const [resUpdateAnswer, setResUpdateAnswer] = useState<any>();

    const onUpdateAnswerStartedQuiz = async (
        request: UpdateAnswerStartedQuizReq
    ) => {
        try {
            const res = await ApiHelper.post<
                UpdateAnswerStartedQuizReq,
                UpdateAnswerStartedQuizRes
            >(Endpoint.UPDATE_ANSWER_STARTED_QUIZ, request);
            if (res.status === SUCCESS) {
                setResUpdateAnswer(res.data);
            } else if (res.code === ATTEMPT_ALREADY_CLOSE) {
                return;
            } else {
                setMessageError(`${t(`messages.${res.code}`)}`);
            }
            return;
        } catch (error) {}
        setResUpdateAnswer(null);
        setMessageError(`${t(`fail`)}`);
        return;
    };

    return {
        resUpdateAnswer,
        onUpdateAnswerStartedQuiz,
        updateAnswerStartedQuizError
    };
};

export default useUpdateAnswerStartedQuiz;
