import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetDetailHistoryStartedQuizReq from 'types/requests/GetDetailHistoryStartedQuizReq';
import GetDetailHistoryStartedQuizRes from 'types/responses/GetDetailHistoryStartedQuizRes';

const useDetailHistoryStartedQuiz = () => {
    const [
        resGetDetailHistoryStartedQuiz,
        setResGetDetailHistoryStartedQuiz
    ] = useState<DetailQuestion[]>();

    const onGetDetailHistoryStartedQuiz = async (
        request: GetDetailHistoryStartedQuizReq
    ) => {
        try {
            const res = await ApiHelper.fetch<
                GetDetailHistoryStartedQuizReq,
                GetDetailHistoryStartedQuizRes
            >(Endpoint.GET_DETAIL_HISTORY_STARTED_QUIZ, request);
            if (res.status === SUCCESS) {
                setResGetDetailHistoryStartedQuiz(res.data);
            } else {
                setResGetDetailHistoryStartedQuiz([]);
            }
        } catch (error) {
            setResGetDetailHistoryStartedQuiz([]);
        }
    };

    return {
        resGetDetailHistoryStartedQuiz,
        setResGetDetailHistoryStartedQuiz,
        onGetDetailHistoryStartedQuiz
    };
};

export default useDetailHistoryStartedQuiz;
