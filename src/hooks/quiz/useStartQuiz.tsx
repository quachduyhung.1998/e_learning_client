import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { StartQuizReq } from 'types/requests/StartQuizReq';
import { StartQuizRes } from 'types/responses/StartQuizRes';
import { useTranslation } from 'react-i18next';
import { ResultStartQuiz } from 'types/Quiz';

const useStartQuiz = () => {
    const { t } = useTranslation();
    const [startQuizError, setMessageError] = useState<string>('');

    const [resStartQuiz, setResStartQuiz] = useState<ResultStartQuiz | null>(
        null
    );
    const onStartQuiz = async (request: StartQuizReq) => {
        try {
            const res = await ApiHelper.post<StartQuizReq, StartQuizRes>(
                Endpoint.START_QUIZ,
                request
            );
            if (res.status === SUCCESS) {
                setResStartQuiz(res.data);
            } else {
                setMessageError(`${t(`messages.${res.code}`)}`);
            }
            return;
        } catch (error) {}
        setResStartQuiz(null);
        setMessageError(`${t(`fail`)}`);
        return;
    };

    return { resStartQuiz, setResStartQuiz, onStartQuiz, startQuizError };
};

export default useStartQuiz;
