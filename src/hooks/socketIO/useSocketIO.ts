import LocalStorageHelper from 'helper/LocalStorageHelper';
import { logout, getRoleByRoute } from 'helper/Utils';
import socketIOClient from 'network/SocketIO';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { updateUserRoles } from 'reducer/authReducer';
import { User } from 'types/User';
import events from './Event';

function useSocket() {
    const history = useHistory();
    const dispatch = useDispatch();
    useEffect(() => {
        socketIOClient.on('connect', () => {
            console.log('Socket is connected');
        });
        socketIOClient.on(events.CHANGE_ROLE, (roles: string[]) => {
            const user = LocalStorageHelper.getUserStorage();
            const currentRole = getRoleByRoute();

            if (!user || (currentRole && !roles.includes(currentRole))) {
                logout();
            }
            const actionUpdateUserRole = updateUserRoles(roles);
            dispatch(actionUpdateUserRole);
            LocalStorageHelper.updateUserStorage({
                ...user,
                userRoles: roles
            } as User);
        });

        return () => {
            socketIOClient.disconnect();
        };
    }, []);
    return;
}

export default useSocket;
