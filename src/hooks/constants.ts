export const SubscriptionChanel = {
    comment: {
        listenName: 'NewLiveStreamingComment',
        channel: 'live_streaming_comment_{idLiveStream}_LIVE_STREAMING'
    },
    chat: {
        listenName: 'NkGroupMessagePosted',
        channel: 'nk_group_{conversationId}'
    },
    reaction: {
        listenName: 'NewLiveStreamingReaction',
        channel: 'live_streaming_reaction_{idLiveStream}_LIVE_STREAMING'
    }
};
