import { message } from 'antd';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { BaseResponse } from 'types/responses/BaseResponse';
interface EditTopicReq {
    name: string;
    sectionId: number;
    courseId: number;
}

function useEditTopic(): [
    boolean,
    (request: EditTopicReq) => Promise<void>,
    boolean,
    string
] {
    const [resTopic, setResTopic] = useState<boolean>(false);
    const { t } = useTranslation();
    const [useEditNameError, setMessageError] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const onEditTopic = async (request: EditTopicReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.put<
                EditTopicReq | undefined,
                BaseResponse
            >(Endpoint.ADD_TOPIC, request);
            if (res.status === SUCCESS) {
                setLoading(false);
                setResTopic(true);

                message.success(t('success'));
            } else {
                setResTopic(false);
                setMessageError(`${t(`messages.${res.code}`)}`);
            }
        } catch (error) {
            setResTopic(false);
            setMessageError(`${t('fail')}`);
        }
    };

    return [resTopic, onEditTopic, loading, useEditNameError];
}

export default useEditTopic;
