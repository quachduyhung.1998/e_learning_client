import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { BaseResponse } from 'types/responses/BaseResponse';
import moment from 'moment';
import { message } from 'antd';
interface GetTopicReq {
    courseId: number;
    section: number;
    targetSection: number;
}

interface GetTopicResponse extends BaseResponse {
    data: boolean;
}

function useSorterTopic() {
    const { t } = useTranslation();
    const [resTopic, setResTopic] = useState<number>(0);
    const [loading, setLoading] = useState<boolean>(false);
    const onSorterTopic = async (request: GetTopicReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.post<
                GetTopicReq | undefined,
                GetTopicResponse
            >(Endpoint.MOVE_TOPIC, request);
            if (res.status === SUCCESS) {
                message.success(t('success'));
                setResTopic(moment().unix());
            } else {
                if (res.code !== 'RECORD_NOT_FOUND') {
                    message.success(t(`messages.${res.code}`));
                }
                setResTopic(0);
            }
        } catch (error) {
            message.success(t('fail'));
            setResTopic(0);
        }
        setLoading(false);
    };

    return { resTopic, onSorterTopic, loading };
}

export default useSorterTopic;
