import React, { useState } from 'react';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import {
    SUCCESS,
    COURSE_ALREADY_APPROVED,
    TOPIC_NOT_FOUND
} from 'network/ResponseCode';
import { getUrl } from 'helper/String';
import { BaseResponse } from 'types/responses/BaseResponse';
import { useTranslation } from 'react-i18next';
import { ToastSuccess } from 'components/admin/Toast';
interface GetTopicReq {
    courseId: number;
    sectionId: number | undefined;
}

interface GetTopicResponse extends BaseResponse {
    data: boolean;
}
function useDeleteTopic(): [
    boolean,
    (request: GetTopicReq) => Promise<void>,
    boolean,
    string
] {
    const { t } = useTranslation();
    const [resTopic, setResTopic] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);
    const [messageError, setMessageError] = useState<string>('');
    const onDeleteTopic = async (request: GetTopicReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.delete<
                GetTopicReq | undefined,
                GetTopicResponse
            >(getUrl(Endpoint.ADD_TOPIC, { ...request }), request);
            if (res.status === SUCCESS) {
                setResTopic(res.data);
                ToastSuccess({
                    message: t('success'),
                    description: t('delete_success_confirm', {
                        name: t('delete_lesson')
                    })
                });
            } else if (res.code === COURSE_ALREADY_APPROVED) {
                setResTopic(res.data);
                setMessageError(`${t('delete_fail_course_already_approved')}`);
            } else if (res.code === TOPIC_NOT_FOUND) {
                setResTopic(res.data);
                setMessageError(`${t('topic_already_delete')}`);
            }
        } catch (error) {
            setResTopic(false);
            setMessageError(
                `${t('delete_fail_confirm', {
                    name: t('delete_lesson')
                })}`
            );
        }
        setLoading(false);
    };

    return [resTopic, onDeleteTopic, loading, messageError];
}

export default useDeleteTopic;
