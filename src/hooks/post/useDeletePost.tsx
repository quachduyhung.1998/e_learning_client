import ApiHelper from 'network/ApiClient';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import Endpoint from 'network/Endpoint';
import { BaseResponse } from 'types/responses/BaseResponse';
import { message } from 'antd';
import { useTranslation } from 'react-i18next';
import { ToastSuccess } from 'components/admin/Toast';

function useDeletePost() {
    const [loading, setLoading] = useState(false);
    const { t } = useTranslation();
    const onDeletePost = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.delete(`/posts/${request?.id}/delete`);
            if (res.status === SUCCESS) {
                message.success('success');
                return true;
            } else {
                message.success('fail');
                return false;
            }
        } catch (error) {}
        setLoading(false);
    };

    return { onDeletePost, loading };
}

export default useDeletePost;
