import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import LoadingManager from 'components/loading/LoadingManager';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS, COURSE_ALREADY_APPROVED } from 'network/ResponseCode';
import { ToastSuccess } from 'helper/Toast';
import { getUrl } from 'helper/String';
import { useHistory } from 'react-router-dom';
import { QUESTIONS_TAB } from 'pages/add-exam/AddExamTopicPage';
import { EXAM_TOPIC_DETAIL } from 'routes/web/paths';

function useAddPost() {
    const { t } = useTranslation();
    const [loading, setLoading] = useState<boolean>(false);
    const history = useHistory();
    const [res, setRes] = useState<any | undefined>();
    const onAddPost = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.post<any, any>(
                '/posts/create',
                request
            );
            if (res.status === SUCCESS || res.status === 201) {
                setRes(res.data);
                setLoading(false);
                history.goBack();
                ToastSuccess({ message: t('success') });
                return true;
            } else {
                setLoading(false);
                return false;
            }
        } catch (error) {
            setLoading(false);
        }
    };
    return { onAddPost, res, loading };
}

export default useAddPost;
