import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetQuestionsOfQuizReq from 'types/requests/GetQuestionsOfQuizReq';
import GetQuestionsOfQuizResponse from 'types/responses/GetQuestionsOfQuizResponse';

function useFetchPostInClass() {
    const [listPost, setListPost] = useState<any[] | null>([]);
    const [loading, setLoading] = useState(false);
    const onFetchPostInClass = async (request: any) => {
        setLoading(true);
        const res = await ApiHelper.fetch(
            `/classes/${request.id}/get-all-post`,
            request
        );
        setLoading(false);
        if (res.status === SUCCESS) {
            setListPost(res?.data || []);
        } else {
            setListPost([]);
        }
    };

    return {
        listPost,
        loading,
        onFetchPostInClass
    };
}

export default useFetchPostInClass;
