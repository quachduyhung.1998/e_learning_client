import ApiHelper from 'network/ApiClient';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';

function useDetailPost() {
    const [loading, setLoading] = useState<boolean>(false);
    const [res, setRes] = useState<any>();

    const onDetailPost = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch(
                '/posts' + `/${request.id}`,
                request
            );
            setLoading(false);
            if (res.status === SUCCESS) {
                setRes(res.data);
            }
        } catch (error) {
            setLoading(false);
        }
    };

    return { onDetailPost, res, loading };
}

export default useDetailPost;
