import LoadingManager from 'components/loading/LoadingManager';
import { ToastSuccess } from 'helper/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

function useUpdatePost() {
    const { t } = useTranslation();
    const [loading, setLoading] = useState<boolean>(false);
    const history = useHistory();
    const [res, setRes] = useState<any | undefined>();
    const onUpdatePost = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.put<any, any>(
                `/posts/${request.id}/update`,
                request
            );
            if (res.status === SUCCESS) {
                setLoading(false);
                setRes(res.data);
                history.goBack();
                ToastSuccess({ message: t('success') });
            } else {
                setLoading(false);
            }
        } catch (error) {
            setLoading(false);
        }
    };
    return { onUpdatePost, res, loading };
}

export default useUpdatePost;
