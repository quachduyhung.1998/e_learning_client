import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetQuestionsOfQuizReq from 'types/requests/GetQuestionsOfQuizReq';
import GetQuestionsOfQuizResponse from 'types/responses/GetQuestionsOfQuizResponse';

function useFetchListLesson() {
    const [resGetQuestionsOfQuiz, setResGetQuestionsOfQuiz] = useState<
        DetailQuestion[] | null
    >(null);
    const [totalLesson, setTotalLesson] = useState<number>(0);
    const [listLesson, setListLesson] = useState<DetailQuestion[] | null>(null);
    const onFetchListLesson = async (request: GetQuestionsOfQuizReq) => {
        try {
            const res = await ApiHelper.fetch<GetQuestionsOfQuizReq, any>(
                `/course-category/${request.category}/get-all-question`,
                request
            );
            if (res.status === SUCCESS) {
                setTotalLesson(res.data.total);

                if (res.data) {
                    setListLesson(res.data);
                }
            }
        } catch (error) {
            setResGetQuestionsOfQuiz(null);
        }
    };

    return {
        listLesson,
        totalLesson,
        onFetchListLesson
    };
}

export default useFetchListLesson;
