import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import LoadingManager from 'components/loading/LoadingManager';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS, COURSE_ALREADY_APPROVED } from 'network/ResponseCode';
import { ToastSuccess } from 'helper/Toast';
import { getUrl } from 'helper/String';
import { useHistory } from 'react-router-dom';
import { QUESTIONS_TAB } from 'pages/add-exam/AddExamTopicPage';
import { EXAM_TOPIC_DETAIL } from 'routes/web/paths';
interface PostQuestionReq {
    id?: number;
    name: string;
    answers: string;
    type_question: number;
    course_category_id: number;
    mark: number | string;
}

interface PostQuestionRes {
    statusCode: number;
    status: number;
    message: string;
    data: PostQuestionReq;
    code?: string;
}
function usePostQuestion(): [
    (request: PostQuestionReq) => Promise<void>,
    PostQuestionReq | undefined,
    boolean,
    string
] {
    const { t } = useTranslation();
    const history = useHistory();
    const [loading, setLoading] = useState<boolean>(false);
    const [res, setRes] = useState<PostQuestionReq | undefined>();
    const [messageError, setMessageError] = useState<string>('');
    const onAddQuestion = async (request: PostQuestionReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.post<PostQuestionReq, PostQuestionRes>(
                Endpoint.QUESTION,
                request
            );
            if (res.status === SUCCESS) {
                setRes(res.data);
                setLoading(false);
                ToastSuccess({ message: t('success') });
                history.goBack();
                // history.push(
                //     `${getUrl(EXAM_TOPIC_DETAIL, {
                //         courseId: request.courseId,
                //         quizId: request.quizId
                //     })}${QUESTIONS_TAB}`
                // );
            } else if (res.code === COURSE_ALREADY_APPROVED) {
                setLoading(false);
                setMessageError(`${t('messages.COURSE_ALREADY_APPROVED')}`);
            } else {
                setLoading(false);
                setMessageError(`${t('fail')}`);
            }
        } catch (error) {
            setLoading(false);
            setMessageError(`${t('fail')}`);
        }
        LoadingManager.hide();
    };
    return [onAddQuestion, res, loading, messageError];
}

export default usePostQuestion;
