import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetQuestionsOfQuizReq from 'types/requests/GetQuestionsOfQuizReq';
import GetQuestionsOfQuizResponse from 'types/responses/GetQuestionsOfQuizResponse';

function useGetQuestionsOfQuiz() {
    const [resGetQuestionsOfQuiz, setResGetQuestionsOfQuiz] = useState<
        DetailQuestion[] | null
    >(null);
    const [resTotalQuestion, setResTotalQuestion] = useState<number>(0);
    const [resGetQuestionsNewPage, setResGetQuestionsNewPage] = useState<
        DetailQuestion[] | null
    >(null);
    const onGetQuestionsOfQuiz = async (request: GetQuestionsOfQuizReq) => {
        try {
            const res = await ApiHelper.fetch<GetQuestionsOfQuizReq, any>(
                `/course-category/${request.category}/get-all-question`,
                request
            );
            if (res.status === SUCCESS) {
                setResTotalQuestion(res.data.total);
                let newResGetQuestionsOfQuiz: DetailQuestion[] = [];
                // if (resGetQuestionsOfQuiz) {
                //     newResGetQuestionsOfQuiz = [...resGetQuestionsOfQuiz];
                // }
                if (res.data) {
                    setResGetQuestionsNewPage(res.data);
                }
                setResGetQuestionsOfQuiz(newResGetQuestionsOfQuiz);
            } else {
                setResTotalQuestion(0);
                setResGetQuestionsOfQuiz(null);
            }
        } catch (error) {
            setResTotalQuestion(0);
            setResGetQuestionsOfQuiz(null);
        }
    };

    return {
        resGetQuestionsNewPage,
        resGetQuestionsOfQuiz,
        setResGetQuestionsOfQuiz,
        resTotalQuestion,
        onGetQuestionsOfQuiz
    };
}

export default useGetQuestionsOfQuiz;
