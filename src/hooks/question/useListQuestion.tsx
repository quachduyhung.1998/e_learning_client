import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { ListQuestionResponse } from './../../types/responses/QuestionResponse';
import { ListQuestionReq } from './../../types/requests/QuestionReq';
import { Question } from './../../types/Question';

function useListQuestion(): [
    (request: ListQuestionReq) => Promise<void>,
    Question[] | undefined,
    boolean
] {
    const [loading, setLoading] = useState<boolean>(false);
    const [res, setRes] = useState<Question[]>();

    const onFetchQuestions = async (request: ListQuestionReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch<
                ListQuestionReq,
                ListQuestionResponse
            >(Endpoint.LIST_QUESTION, request);
            setLoading(false);
            if (res.status === SUCCESS) {
                setRes(res.data);
            }
        } catch (error) {
            setLoading(false);
        }
    };

    return [onFetchQuestions, res, loading];
}

export default useListQuestion;
