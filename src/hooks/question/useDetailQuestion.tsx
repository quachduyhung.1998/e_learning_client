import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { QuestionResponse } from './../../types/responses/QuestionResponse';
import { DetailQuestionReq } from './../../types/requests/QuestionReq';
import { DetailQuestion } from './../../types/Question';

function useDetailQuestion(): [
    (request: DetailQuestionReq) => Promise<void>,
    DetailQuestion | undefined,
    boolean
] {
    const [loading, setLoading] = useState<boolean>(false);
    const [res, setRes] = useState<DetailQuestion>();

    const onDetailQuestion = async (request: DetailQuestionReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch<
                DetailQuestionReq,
                QuestionResponse
            >(Endpoint.QUESTION + `/${request.id}`, request);
            setLoading(false);
            if (res.status === SUCCESS) {
                setRes(res.data);
            }
        } catch (error) {
            setLoading(false);
        }
    };

    return [onDetailQuestion, res, loading];
}

export default useDetailQuestion;
