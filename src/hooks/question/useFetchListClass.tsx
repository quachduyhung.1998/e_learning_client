import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetQuestionsOfQuizReq from 'types/requests/GetQuestionsOfQuizReq';
import GetQuestionsOfQuizResponse from 'types/responses/GetQuestionsOfQuizResponse';

function useFetchListClass() {
    const [listClass, setListClass] = useState<any[] | null>([]);
    const [loading, setLoading] = useState(false);
    const onFetchListClass = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch('/classes', request);
            setLoading(false);
            if (res.status === SUCCESS) {
                setListClass(res?.data?.data);
            }
        } catch (error) {}
    };

    return {
        listClass,
        loading,
        onFetchListClass
    };
}

export default useFetchListClass;
