import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetQuestionsOfQuizReq from 'types/requests/GetQuestionsOfQuizReq';
import GetQuestionsOfQuizResponse from 'types/responses/GetQuestionsOfQuizResponse';

function useFetchTestResult() {
    const [listResultTest, setList] = useState<any[] | null>([]);
    const [loading, setLoading] = useState(false);
    const onFetchListResultTest = async (request: any) => {
        setLoading(true);
        const res = await ApiHelper.fetch(
            `/courses/${request?.courseId}/test-worked`,
            request
        );
        setLoading(false);
        if (res.status === SUCCESS) {
            setList(res?.data || []);
        } else {
            setList([]);
        }
    };

    return {
        listResultTest,
        loading,
        onFetchListResultTest
    };
}

export default useFetchTestResult;
