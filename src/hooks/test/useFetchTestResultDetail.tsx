import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetQuestionsOfQuizReq from 'types/requests/GetQuestionsOfQuizReq';
import GetQuestionsOfQuizResponse from 'types/responses/GetQuestionsOfQuizResponse';

function useFetchTestResultDetail() {
    const [listResultTest, setList] = useState<any>();
    const [loading, setLoading] = useState(false);
    const onFetchListResultTestDetail = async (request: any) => {
        setLoading(true);
        const res = await ApiHelper.fetch(
            `/tests/${request?.test_id}/test-result`,
            request
        );
        setLoading(false);
        if (res.status === SUCCESS) {
            setList(res?.data);
        }
    };

    return {
        listResultTest,
        loading,
        onFetchListResultTestDetail
    };
}

export default useFetchTestResultDetail;
