export enum ConversationType {
    CHAT = 'CHAT',
    GROUP = 'GROUP',
    ONE_TO_ONE = 'ONE_TO_ONE'
}

export enum MessageType {
    FILE = 'FILE',
    TEXT = 'TEXT'
}

export enum ConversationTypeSend {
    TYPE_SEND_EMPLOYEES_TO_EMPLOYEES = 'TYPE_SEND_EMPLOYEES_TO_EMPLOYEES',
    TYPE_SEND_EMPLOYEES_TO_CUSTOMER = 'TYPE_SEND_EMPLOYEES_TO_CUSTOMER',
    TYPE_SEND_CUSTOMER_TO_EMPLOYEES = 'TYPE_SEND_CUSTOMER_TO_EMPLOYEES'
}
export interface Conversation {
    avatar: string;
    check_unique: string;
    created_at: string;
    id: number;
    last_message: string;
    message_id: number;
    message_type: string;
    name: string;
    status: string;
    type: ConversationType;
    type_send: ConversationTypeSend;
}

export interface GetMessageConversationParam {
    conversationId: number;
    type: ConversationType;
}

export interface ActionSendMessageParams {
    message_type: MessageType;
    message: string;
    type: ConversationType;
    receiver_id: number;
    type_send: ConversationTypeSend;
}

export interface Message {
    created_at: string;
    direction: MessageDirection;
    id: number;
    message: string;
    message_type: MessageType;
    receiver_id: number;
    sender: { id: number; name: string; avatar: string };
    sender_id: number;
    status: string;
    type: ConversationType;
    type_user: number | null;
    updated_at: string;
}
export enum MessageDirection {
    IN = 'in',
    OUT = 'out'
}
