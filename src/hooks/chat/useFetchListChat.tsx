import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetQuestionsOfQuizReq from 'types/requests/GetQuestionsOfQuizReq';
import GetQuestionsOfQuizResponse from 'types/responses/GetQuestionsOfQuizResponse';

function useFetchListChat() {
    const [listChat, setListChat] = useState<any[] | null>([]);
    const [loading, setLoading] = useState(false);
    const onFetchListChat = async (request?: any) => {
        setLoading(true);
        const res = await ApiHelper.fetch('/chats/get-all', request);
        setLoading(false);
        if (res.status === SUCCESS) {
            setListChat(res?.data || []);
        } else {
            setListChat([]);
        }
    };

    return {
        listChat,
        loading,
        onFetchListChat
    };
}

export default useFetchListChat;
