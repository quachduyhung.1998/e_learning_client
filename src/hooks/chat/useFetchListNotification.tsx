import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetQuestionsOfQuizReq from 'types/requests/GetQuestionsOfQuizReq';
import GetQuestionsOfQuizResponse from 'types/responses/GetQuestionsOfQuizResponse';

function useFetchListNotification() {
    const [listNotification, setListNotification] = useState<any[] | null>([]);
    const [loading, setLoading] = useState(false);
    const onFetchListNotification = async (request?: any) => {
        setLoading(true);
        const res = await ApiHelper.fetch('/notifications/get-all', request);
        setLoading(false);
        if (res.status === SUCCESS) {
            setListNotification(res?.data?.data || []);
        } else {
            setListNotification([]);
        }
    };

    return {
        listNotification,
        loading,
        onFetchListNotification
    };
}

export default useFetchListNotification;
