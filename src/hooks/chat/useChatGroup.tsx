import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import LoadingManager from 'components/loading/LoadingManager';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS, COURSE_ALREADY_APPROVED } from 'network/ResponseCode';
import { ToastSuccess } from 'helper/Toast';
import { getUrl } from 'helper/String';
import { useHistory } from 'react-router-dom';
import { QUESTIONS_TAB } from 'pages/add-exam/AddExamTopicPage';
import { EXAM_TOPIC_DETAIL } from 'routes/web/paths';
interface PostQuestionReq {
    id?: number;
    name: string;
    answers: string;
    type_question: number;
    course_category_id: number;
    mark: number | string;
}

interface PostQuestionRes {
    statusCode: number;
    status: number;
    message: string;
    data: PostQuestionReq;
    code?: string;
}
function useChatGroup() {
    const { t } = useTranslation();
    const history = useHistory();
    const [loading, setLoading] = useState<boolean>(false);
    const [res, setRes] = useState<PostQuestionReq | undefined>();
    const [messageError, setMessageError] = useState<string>('');
    const onCreateChatGroup = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.post<any, any>('/chats/group', request);
            if (res.status === SUCCESS || res.status === 201) {
                setRes(res);
                setLoading(false);
            } else {
                setLoading(false);
                setMessageError(`${t('fail')}`);
            }
        } catch (error) {
            setLoading(false);
            setMessageError(`${t('fail')}`);
        }
    };
    return { onCreateChatGroup, res, loading, messageError };
}

export default useChatGroup;
