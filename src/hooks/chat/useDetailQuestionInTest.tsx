import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { QuestionResponse } from '../../types/responses/QuestionResponse';
import { DetailQuestionReq } from '../../types/requests/QuestionReq';
import { DetailQuestion } from '../../types/Question';

function useDetailQuestionInTest(): [
    (request: DetailQuestionReq) => Promise<void>,
    any,
    boolean
] {
    const [loading, setLoading] = useState<boolean>(false);
    const [res, setRes] = useState<any>();

    const onDetailQuestionInTest = async (request: DetailQuestionReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch(
                '/tests' + `/${request.id}` + '/get-all-question',
                request
            );
            setLoading(false);
            if (res.status === SUCCESS) {
                setRes(res.data);
            }
        } catch (error) {
            setLoading(false);
        }
    };

    return [onDetailQuestionInTest, res, loading];
}

export default useDetailQuestionInTest;
