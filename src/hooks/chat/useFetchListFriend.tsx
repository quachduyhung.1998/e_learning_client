import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetQuestionsOfQuizReq from 'types/requests/GetQuestionsOfQuizReq';
import GetQuestionsOfQuizResponse from 'types/responses/GetQuestionsOfQuizResponse';

function useFetchListFriend() {
    const [listFriend, setListFriend] = useState<any[] | null>([]);
    const [loading, setLoading] = useState(false);
    const onFetchListFriend = async (request?: any) => {
        setLoading(true);
        const res = await ApiHelper.fetch(
            '/chats/get-friend-suggestion',
            request
        );
        setLoading(false);
        if (res.status === SUCCESS) {
            setListFriend(res?.data || []);
        } else {
            setListFriend([]);
        }
    };

    return {
        listFriend,
        loading,
        onFetchListFriend
    };
}

export default useFetchListFriend;
