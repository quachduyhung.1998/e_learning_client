import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetQuestionsOfQuizReq from 'types/requests/GetQuestionsOfQuizReq';
import GetQuestionsOfQuizResponse from 'types/responses/GetQuestionsOfQuizResponse';

function useFetchListMessage() {
    const [listMessage, setListMessage] = useState<any | null>();
    const [loading, setLoading] = useState(false);
    const onFetchListMessage = async (request?: any) => {
        setLoading(true);
        const res = await ApiHelper.fetch(
            `/chats/${request.room_id}/show-message`,
            request
        );
        setLoading(false);
        if (res.status === SUCCESS) {
            setListMessage(res?.data);
        } else {
            setListMessage(null);
        }
    };

    return {
        listMessage,
        loading,
        onFetchListMessage
    };
}

export default useFetchListMessage;
