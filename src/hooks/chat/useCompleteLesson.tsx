import LoadingManager from 'components/loading/LoadingManager';
import { ToastSuccess } from 'helper/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

interface PutQuestionReq {
    id?: number;
    name: string;
    answers: string;
    type_question: number;
    course_category_id: number;
    mark: number | string;
}

interface PutQuestionRes {
    statusCode: number;
    status: number;
    message: string;
    data: PutQuestionReq;
}
function useCompleteLesson(): [
    (request: any) => Promise<void>,
    any | undefined,
    boolean,
    string
] {
    const { t } = useTranslation();
    const [loading, setLoading] = useState<boolean>(false);
    const history = useHistory();
    const [res, setRes] = useState<any | undefined>();
    const [messageError, setMessageError] = useState<string>('');
    const onCompleteLesson = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.post<any, PutQuestionRes>(
                '/lessons/student-complete',
                request
            );
            if (res.status === SUCCESS) {
                setLoading(false);
                setRes(res.data);
            } else {
                setLoading(false);
                setMessageError(`${t('fail')}`);
            }
        } catch (error) {
            setLoading(false);
            setMessageError(`${t('fail')}`);
        }
    };
    return [onCompleteLesson, res, loading, messageError];
}

export default useCompleteLesson;
