import React, { useEffect, useState } from 'react';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { TopicListResponse } from 'types/responses';
import { Topic } from 'types/Topic';
import { SUCCESS } from 'network/ResponseCode';

function useFetchTopic(
    courseId: string,
    userId?: string
): [
    any,
    React.Dispatch<React.SetStateAction<any>>,
    (courseId: number, userId?: string) => void
] {
    const [topics, setTopics] = useState<any>();
    const onFetchTopics = async (courseId: number, userId?: string) => {
        try {
            const res = await ApiHelper.fetch<any, TopicListResponse>(
                `${Endpoint.COURSE}/${courseId}`,
                userId ? { userId: userId } : {}
            );
            if (res.status === SUCCESS) {
                setTopics(res.data);
            }
        } catch (error) {}
    };

    useEffect(() => {
        onFetchTopics(Number(courseId), userId);
    }, [courseId]);

    return [topics, setTopics, onFetchTopics];
}

export default useFetchTopic;
