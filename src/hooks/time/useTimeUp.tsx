import { useEffect, useRef, useState } from 'react';

const useTimeUp = (mSeconds: number) => {
    const [millisecond, setMillisecond] = useState<number>(mSeconds);
    const setIntervalRef = useRef<number>();
    useEffect(() => {
        setIntervalRef.current = setInterval(() => {
            setMillisecond(millisecond + 1);
        }, 1);
        return () => clearInterval(setIntervalRef.current);
    }, [millisecond]);

    return { millisecond, setMillisecond };
};

export default useTimeUp;
