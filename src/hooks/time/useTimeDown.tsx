import { useEffect, useRef, useState } from 'react';

interface Props {
    seconds: number;
    secondsStart?: number;
}

const useTimeDown = (props: Props) => {
    const [seconds, setSeconds] = useState<number>(
        props?.secondsStart || props.seconds
    );
    const [minuteString, setMinuteString] = useState<string>('');
    const [running, setRunning] = useState<boolean>(true);
    const [percent, setPercent] = useState<number>(0);
    const setIntervalRef = useRef<number>();
    useEffect(() => {
        if (seconds >= 0) {
            setIntervalRef.current = setInterval(() => {
                setSeconds(seconds - 1);
                const s = `0${seconds % 60}`.slice(-2);
                const m = `0${Math.floor(seconds / 60)}`.slice(-2);
                setMinuteString(`${m}:${s}`);
                setPercent(((props.seconds - seconds) / props.seconds) * 100);
            }, 1000);
        } else {
            setMinuteString(`00:00`);
            setRunning(false);
            setPercent(100);
        }

        return () => clearInterval(setIntervalRef.current);
    }, [seconds]);

    useEffect(() => {
        if (!running) {
            setSeconds(-1);
        }
    }, [running]);

    return { running, setSeconds, setRunning, percent, minuteString };
};

export default useTimeDown;
