import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetQuestionsOfQuizReq from 'types/requests/GetQuestionsOfQuizReq';
import GetQuestionsOfQuizResponse from 'types/responses/GetQuestionsOfQuizResponse';

function useFetchListUserInClass() {
    const [listUser, setListUserClass] = useState<any[] | null>([]);
    const [loading, setLoading] = useState(false);
    const onFetchListUserInClass = async (request: any) => {
        setLoading(true);
        const res = await ApiHelper.fetch(
            '/get-student-of-class/' + request?.id
        );
        setLoading(false);
        if (res.status === SUCCESS) {
            setListUserClass(res?.data || []);
        } else {
            setListUserClass([]);
        }
    };

    return {
        listUser,
        loading,
        onFetchListUserInClass
    };
}

export default useFetchListUserInClass;
