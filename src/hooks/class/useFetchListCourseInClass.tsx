import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { DetailQuestion } from 'types/Question';
import GetQuestionsOfQuizReq from 'types/requests/GetQuestionsOfQuizReq';
import GetQuestionsOfQuizResponse from 'types/responses/GetQuestionsOfQuizResponse';

function useFetchListCourseInClass() {
    const [listCourseInClass, setListCourseClass] = useState<any[] | null>([]);
    const [loading, setLoading] = useState(false);
    const onFetchListCourseInClass = async (request: any) => {
        setLoading(true);
        const res = await ApiHelper.fetch('get-course-of-class/' + request?.id);
        setLoading(false);
        if (res.status === SUCCESS) {
            setListCourseClass(res?.data || []);
        } else {
            setListCourseClass([]);
        }
    };

    return {
        listCourseInClass,
        loading,
        onFetchListCourseInClass
    };
}

export default useFetchListCourseInClass;
