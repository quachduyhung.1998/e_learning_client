import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { QuestionResponse } from '../../types/responses/QuestionResponse';
import { DetailQuestionReq } from '../../types/requests/QuestionReq';
import { DetailQuestion } from '../../types/Question';

function useDetailClass(): [
    (request: DetailQuestionReq) => Promise<void>,
    any,
    boolean
] {
    const [loading, setLoading] = useState<boolean>(false);
    const [res, setRes] = useState<any>();

    const onDetailClass = async (request: DetailQuestionReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch(
                '/classes' + `/${request.id}`,
                request
            );
            setLoading(false);
            if (res.status === SUCCESS) {
                setRes(res.data);
            }
        } catch (error) {
            setLoading(false);
        }
    };

    return [onDetailClass, res, loading];
}

export default useDetailClass;
