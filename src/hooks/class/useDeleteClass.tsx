import ApiHelper from 'network/ApiClient';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import Endpoint from 'network/Endpoint';
import { BaseResponse } from 'types/responses/BaseResponse';
import { message } from 'antd';
import { useTranslation } from 'react-i18next';
import { ToastSuccess } from 'components/admin/Toast';

interface GetQuestionReq {
    courseId: number;
    quizId: number;
    questionId: number;
    id?: string;
}

interface GetQuizResponse extends BaseResponse {
    data: boolean;
}

function useDeleteClass(): [
    boolean,
    (request: GetQuestionReq) => Promise<void>,
    boolean,
    string
] {
    const [resQuestion, setResQuestion] = useState<boolean>(false);
    const [loading, setLoading] = useState(false);
    const { t } = useTranslation();
    const [messageError, setMessageError] = useState<string>('');
    const onDeleteClass = async (request: GetQuestionReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.delete<
                GetQuestionReq | undefined,
                GetQuizResponse
            >('/classes' + `/${request.id}`, request);
            if (res.status === SUCCESS) {
                setResQuestion(res.data);
                message.success('Xóa lớp học thành công');
            } else {
                setMessageError(res?.message);
                setResQuestion(false);
            }
        } catch (error) {
            setMessageError(
                `${t('delete_fail_confirm', {
                    name: t('delete_question')
                })}`
            );
            setResQuestion(false);
        }
        setLoading(false);
    };

    return [resQuestion, onDeleteClass, loading, messageError];
}

export default useDeleteClass;
