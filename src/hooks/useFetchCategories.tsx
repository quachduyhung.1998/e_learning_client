import { useState } from 'react';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from '../network/ResponseCode';
import { Category } from 'types/Category';
import { CategoryListResponse } from 'types/responses/CategoryRes';

interface CategoryListReq {}

function useFetchCategories(): [() => Promise<void>, Category[], boolean] {
    const [loading, setLoading] = useState<boolean>(false);
    const [res, setRes] = useState<Category[]>([]);

    const onFetchCategories = async () => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch<
                CategoryListReq,
                CategoryListResponse
            >(Endpoint.CATEGORIES);
            if (!res) return;
            setLoading(false);
            if (res.status === SUCCESS) {
                setRes(res.data);
            }
        } catch (error) {
            setLoading(false);
        }
    };

    return [onFetchCategories, res, loading];
}

export default useFetchCategories;
