import LoadingManager from 'components/loading/LoadingManager';
import { StorageKey } from 'helper/LocalStorageHelper';
import { ToastSuccess } from 'helper/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AddCourseData } from 'types/responses/CourseRes';
import { AddCourseReq } from '../../types/requests';
import { AddCourseResponse } from '../../types/responses';
import { useHistory } from 'react-router-dom';
import { LECTURERS_COURSE } from 'routes/web/paths';

function useAddCourse(): [
    (request: AddCourseReq) => Promise<void>,
    AddCourseData | undefined,
    string
] {
    const { t } = useTranslation();
    const history = useHistory();
    const [messageError, setMessageError] = useState<string>('');
    const [resAddCourse, setResAddCourse] = useState<AddCourseData>();

    const onAddCourse = async (request: AddCourseReq) => {
        try {
            const res = await ApiHelper.post<AddCourseReq, AddCourseResponse>(
                Endpoint.COURSE,
                request
            );
            if (res.status === SUCCESS) {
                setResAddCourse(res.data);
                ToastSuccess({ message: t('success') });
                //reset cache
                localStorage.setItem(StorageKey.CACHE_FORM_CREATE_COURSE, '{}');
                history.push(`${LECTURERS_COURSE}/${res.data.id}`);
            } else {
                setResAddCourse(undefined);
                setMessageError(`${t(`messages.${res.code}`)}`);
            }
        } catch (error) {
            setResAddCourse(undefined);
            setMessageError(`${t('fail')}`);
        }
        LoadingManager.hide();
    };
    return [onAddCourse, resAddCourse, messageError];
}

export default useAddCourse;
