import LoadingManager from 'components/loading/LoadingManager';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import {
    SUCCESS,
    COURSE_ALREADY_APPROVED,
    COURSE_NOT_FOUND
} from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DeleteCourseReq } from '../../types/requests';
import { DeleteCourseResponse } from '../../types/responses';
import moment from 'moment';
import { ToastSuccess } from 'components/admin/Toast';

function useDeleteCourse(): [
    (request: DeleteCourseReq) => Promise<void>,
    number,
    string
] {
    const { t } = useTranslation();

    const [resDeleteCourse, setResDeleteCourse] = useState<number>(0);
    const [messageError, setMessageError] = useState<string>('');

    const onDeleteCourse = async (request: DeleteCourseReq) => {
        try {
            const res = await ApiHelper.delete<
                DeleteCourseReq,
                DeleteCourseResponse
            >(Endpoint.COURSE + `/${request.courseId}`, request);

            if (res.status === SUCCESS) {
                setResDeleteCourse(moment().unix());
                ToastSuccess({
                    message: t('success'),
                    description: t('delete_success_confirm', {
                        name: t('delete_course')
                    })
                });
            } else if (res.code === COURSE_ALREADY_APPROVED) {
                setResDeleteCourse(moment().unix());
                setMessageError(`${t('delete_fail_course_already_approved')}`);
            } else if (res.code === COURSE_NOT_FOUND) {
                setResDeleteCourse(moment().unix());
                setMessageError(`${t('course_already_delete')}`);
            } else {
                setResDeleteCourse(0);
                setMessageError(`${t(`messages.${res.code}`)}`);
            }
        } catch (error) {
            setResDeleteCourse(0);
            setMessageError(`${t('delete_fail')}`);
        }
        LoadingManager.hide();
    };
    return [onDeleteCourse, resDeleteCourse, messageError];
}

export default useDeleteCourse;
