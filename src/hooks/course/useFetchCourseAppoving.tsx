import { notification } from 'antd';
import ApiHelper from 'network/ApiClient';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';

function useFetchCourseApproving() {
    const [loading, setLoading] = useState(false);
    const [params, setParams] = useState<any>({
        page: 1,
        limit: 20
    });
    const [list, setList] = useState<any[]>([]);

    const onFetchCourseApproving = async (params?: any) => {
        setParams(params || {});
        setLoading(true);
        const res = await ApiHelper.fetch('/courses-pending-approval', {
            ...params
        });
        setLoading(false);
        if (res.status === SUCCESS) {
            setList([...res?.data]);

            return res;
        }

        notification.error({ message: res.message });
        return false;
    };

    return { loading, list, params, setList, onFetchCourseApproving };
}

export default useFetchCourseApproving;
