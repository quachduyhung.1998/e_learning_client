import { notification } from 'antd';
import ApiHelper from 'network/ApiClient';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';

function useFetchCoursePublic() {
    const [loading, setLoading] = useState(false);
    const [params, setParams] = useState<any>({
        page: 1,
        limit: 20
    });
    const [list, setList] = useState<any[]>([]);

    const onFetchCoursePublic = async (params?: any) => {
        setParams(params || {});
        setLoading(true);
        const res = await ApiHelper.fetch('/courses', {
            ...params
        });
        setLoading(false);
        if (res.status === SUCCESS) {
            setList(res?.data?.data);

            return res;
        }

        notification.error({ message: res.message });
        return false;
    };

    return { loading, list, onFetchCoursePublic };
}

export default useFetchCoursePublic;
