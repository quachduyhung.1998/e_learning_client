import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import GetCourseRecommendationReq from 'types/requests/GetCourseRecommendationReq';
import { ListCourseData, ListCourseResponse } from 'types/responses';

const useGetCourseRecommendation = () => {
    const [
        resGetCourseRecommendation,
        setResGetCourseRecommendation
    ] = useState<ListCourseData>();

    const onGetCourseRecommendation = async (
        request: GetCourseRecommendationReq
    ) => {
        try {
            const res = await ApiHelper.fetch<
                GetCourseRecommendationReq,
                ListCourseResponse
            >(Endpoint.RECOMMENDATION, request);
            if (res.status === SUCCESS) {
                setResGetCourseRecommendation(res.data);
            } else {
                setResGetCourseRecommendation(undefined);
            }
        } catch (error) {
            setResGetCourseRecommendation(undefined);
        }
    };

    return {
        resGetCourseRecommendation,
        onGetCourseRecommendation,
        setResGetCourseRecommendation
    };
};

export default useGetCourseRecommendation;
