import React, { useState } from 'react';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { BAD_REQUEST } from 'network/ResponseCode';
interface ListCourseReq {
    courseId: number;
}

interface ListCourseData {
    statusCode: number;
    message: string;
    code: string;
    data: undefined;
}

interface ResponseCourse {
    code: string;
}
function useCourseCanApprove(): [
    (request: ListCourseReq) => Promise<void>,
    ListCourseData | undefined,
    boolean
] {
    const [loading, setLoading] = useState<boolean>(false);
    const [resCourses, setResCourses] = useState<ListCourseData>();

    const onFetchCourses = async (request: ListCourseReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch<ListCourseReq, ListCourseData>(
                Endpoint.CAN_APPROVE_COURSE,
                request
            );
            setLoading(false);
            if (res.statusCode === 200) {
                setResCourses(res);
            } else {
                setResCourses(undefined);
            }
        } catch (error) {
            setResCourses(undefined);
            setLoading(false);
        }
    };
    return [onFetchCourses, resCourses, loading];
}

export default useCourseCanApprove;
