import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { UpdateInteractiveReq } from 'types/requests';
import { message } from 'antd';
import { useTranslation } from 'react-i18next';
interface UpdateInteractiveRes {
    statusCode: number;
    message: string;
    data: UpdateInteractiveReq;
}
const useUpdateInteractiveCourse = () => {
    const { t } = useTranslation();
    const [updateInteractiveCourseError, setMessageError] = useState<string>(
        ''
    );
    const [resUpdateInteractive, setResUpdateInteractive] = useState<
        UpdateInteractiveReq
    >();
    const onUpdateInteractiveCourse = async (request: UpdateInteractiveReq) => {
        try {
            const res = await ApiHelper.post<
                UpdateInteractiveReq,
                UpdateInteractiveRes
            >(Endpoint.UPDATE_INTERACTIVE_COURSE, request);
            if (res.statusCode === SUCCESS) {
                setResUpdateInteractive(res.data);
            } else {
                setMessageError(`${t('update_interative_course_fail')}`);
            }
            return;
        } catch (error) {
            setMessageError(`${t('update_interative_course_fail')}`);
        }
        return;
    };
    return {
        resUpdateInteractive,
        onUpdateInteractiveCourse,
        updateInteractiveCourseError
    };
};
export default useUpdateInteractiveCourse;
