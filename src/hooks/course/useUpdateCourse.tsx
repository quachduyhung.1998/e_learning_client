import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { UpdateCourseReq } from '../../types/requests';
import { UpdateCourseResponse } from '../../types/responses';
import { useTranslation } from 'react-i18next';
import { ToastSuccess } from 'helper/Toast';
import LoadingManager from 'components/loading/LoadingManager';
import { useState } from 'react';

function useUpdateCourse(): [
    (request: UpdateCourseReq) => Promise<void>,
    string
] {
    const { t } = useTranslation();
    const [messageError, setMessageError] = useState<string>('');
    const onUpdateCourse = async (request: UpdateCourseReq) => {
        try {
            const res = await ApiHelper.post<
                UpdateCourseReq,
                UpdateCourseResponse
            >(Endpoint.COURSE + `/${request.id}`, request);
            if (res.status === SUCCESS) {
                ToastSuccess({ message: t('success') });
            } else {
                setMessageError(`${t(`messages.${res.code}`)}`);
            }
        } catch (error) {
            setMessageError(`${t('fail')}`);
        }
        LoadingManager.hide();
    };
    return [onUpdateCourse, messageError];
}

export default useUpdateCourse;
