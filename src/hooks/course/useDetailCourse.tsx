import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS, COURSE_NOT_FOUND } from 'network/ResponseCode';
import { useState } from 'react';
import { InfoCourseReq } from 'types/requests';
import { InfoCourseResponse } from 'types/responses';
import { Course } from 'types/Course';
import { HOME } from 'routes/web/paths';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

function useDetailCourse(): [
    (request: InfoCourseReq) => Promise<void>,
    Course | undefined,
    boolean,
    string
] {
    const [loading, setLoading] = useState<boolean>(false);
    const [resCourseInfo, setResCourseInfo] = useState<Course>();
    const [detailCourseError, setMessageError] = useState<string>('');
    const history = useHistory();
    const { t } = useTranslation();
    const onFetchInfoCourse = async (request: InfoCourseReq) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch<
                InfoCourseReq,
                InfoCourseResponse
            >(Endpoint.COURSE + `/${request.id}`, request);
            setLoading(false);
            if (res.status === SUCCESS) {
                if (res.data) {
                    setResCourseInfo(res.data);
                }
            } else if (res.code === COURSE_NOT_FOUND) {
                setMessageError(`${t('course_not_found')}`);
                history.push(HOME);
            }
        } catch (error) {
            setLoading(false);
        }
    };

    return [onFetchInfoCourse, resCourseInfo, loading, detailCourseError];
}

export default useDetailCourse;
