import { ToastSuccess } from 'helper/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Course } from 'types/Course';
import { ApproveCourseReq } from 'types/requests/ApproveCourseReq';
import ApproveCourseRes from 'types/responses/ApproveCourseRes';
import LoadingManager from 'components/loading/LoadingManager';

function useApproveCourse(): [
    Course | null,
    (request: ApproveCourseReq) => Promise<void>,
    string
] {
    const { t } = useTranslation();
    const [resApproveCourse, setResApproveCourse] = useState<Course | null>(
        null
    );
    const [courseApproveError, setMessageError] = useState<string>('');

    const onApproveCourse = async (request: ApproveCourseReq) => {
        try {
            const res = await ApiHelper.put<ApproveCourseReq, ApproveCourseRes>(
                `/courses/${request.courseId}/public`,
                { ...request }
            );
            if (res.status === SUCCESS) {
                setResApproveCourse(res.data);
                ToastSuccess({ message: t('success') });
            } else {
                setResApproveCourse(null);
            }
        } catch (error) {
            setResApproveCourse(null);
        }
    };

    return [resApproveCourse, onApproveCourse, courseApproveError];
}

export default useApproveCourse;
