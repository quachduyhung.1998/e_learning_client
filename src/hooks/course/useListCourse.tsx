import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { ListCourseData, ListCourseResponse } from 'types/responses';
import { ListCourseReq } from '../../types/requests';

function useListCourse(): [
    (request: ListCourseReq) => Promise<void>,
    ListCourseData | any | undefined,
    boolean
] {
    const [loading, setLoading] = useState<boolean>(false);
    const [resCourses, setResCourses] = useState<any[] | any>([]);

    const onFetchCourses = async (request: ListCourseReq) => {
        try {
            const newRequest = { ...request };
            if (newRequest.status === '') delete newRequest.status;
            setLoading(true);
            const res = await ApiHelper.fetch<
                ListCourseReq,
                ListCourseResponse
            >(Endpoint.LECTURER_COURSE, newRequest);
            setLoading(false);
            if (res.status === SUCCESS) {
                setResCourses(res.data?.data);
            } else {
                setResCourses([]);
            }
        } catch (error) {
            setResCourses([]);
            setLoading(false);
        }
    };

    return [onFetchCourses, resCourses, loading];
}

export default useListCourse;
