import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import GetCourseInJoinReq from 'types/requests/GetCourseInJoinReq';
import { ListCourseData, ListCourseResponse } from 'types/responses';

const useGetCourseInJoin = () => {
    const [resGetCourseInJoin, setResGetCourseInJoin] = useState<any>();

    const onGetCourseInJoin = async (request: GetCourseInJoinReq) => {
        try {
            const res = await ApiHelper.fetch(
                Endpoint.LECTURER_COURSE,
                request
            );
            if (res.status === SUCCESS) {
                setResGetCourseInJoin(res.data);
            } else {
                setResGetCourseInJoin(undefined);
            }
        } catch (error) {
            setResGetCourseInJoin(undefined);
        }
    };

    return { resGetCourseInJoin, onGetCourseInJoin, setResGetCourseInJoin };
};

export default useGetCourseInJoin;
