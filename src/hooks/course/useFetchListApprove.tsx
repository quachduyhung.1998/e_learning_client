import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { ListCourseData, ListCourseResponse } from 'types/responses';
import { ListCourseReq } from '../../types/requests';

function useFetchListApprove(): [
    (request: ListCourseReq) => Promise<void>,
    ListCourseData | any | undefined,
    boolean
] {
    const [loading, setLoading] = useState<boolean>(false);
    const [resCoursesApprove, setResCourses] = useState<any[] | any>([]);

    const onFetchApproveCourse = async (request: ListCourseReq) => {
        try {
            const newRequest = { ...request };
            if (newRequest.status === '') delete newRequest.status;
            setLoading(true);
            const res = await ApiHelper.fetch<
                ListCourseReq,
                ListCourseResponse
            >('/courses-pending-approval', newRequest);
            setLoading(false);
            if (res.status === SUCCESS) {
                setResCourses(res.data);
            } else {
                setResCourses([]);
            }
        } catch (error) {
            setResCourses([]);
            setLoading(false);
        }
    };

    return [onFetchApproveCourse, resCoursesApprove, loading];
}

export default useFetchListApprove;
