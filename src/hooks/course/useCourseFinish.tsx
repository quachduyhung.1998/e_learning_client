import { ToastSuccess } from 'helper/Toast';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Course } from 'types/Course';
import { CourseFinishReq } from 'types/requests/CourseFinishReq';
import CourseFinishRes from 'types/responses/CourseFinishRes';
import LoadingManager from 'components/loading/LoadingManager';

function useCourseFinish(): [
    Course | null,
    (request: CourseFinishReq) => Promise<void>,
    string
] {
    const { t } = useTranslation();
    const [resCourseFinish, setResCourseFinish] = useState<Course | null>(null);
    const [courseFinishError, setMessageError] = useState<string>('');
    const onCourseFinish = async (request: CourseFinishReq) => {
        try {
            const res = await ApiHelper.put<CourseFinishReq, CourseFinishRes>(
                `/courses/${request.id}/complete`,
                { ...request, status: 1 }
            );
            if (res.status === SUCCESS) {
                setResCourseFinish(res.data);
                ToastSuccess({ message: t('success') });
            } else {
                setResCourseFinish(null);
                setMessageError(`${t(`messages.${res.code}`)}`);
            }
        } catch (error) {
            setResCourseFinish(null);
            setMessageError(`${t(`messages.${error}`)}`);
        }
        LoadingManager.hide();
    };

    return [resCourseFinish, onCourseFinish, courseFinishError];
}

export default useCourseFinish;
