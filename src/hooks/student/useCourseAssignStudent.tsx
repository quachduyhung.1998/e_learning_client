import React, { useState } from 'react';
import { AssignStudentReq } from 'types/requests';
import { BaseResponse } from 'types/responses/BaseResponse';
import Endpoint from 'network/Endpoint';
import ApiHelper from 'network/ApiClient';
// import { AssignStudentResponse } from 'types/responses/UserResponse';
import { SUCCESS, ALREADY_ASSIGNED } from 'network/ResponseCode';
import { ToastSuccess } from 'components/admin/Toast';
import { useTranslation } from 'react-i18next';
import { message } from 'antd';

function useCourseAssignStudent(): [
    (request: any) => Promise<void>,
    boolean | undefined,
    boolean,
    string
] {
    const { t } = useTranslation();
    const [loading, setLoading] = useState<boolean>(false);
    const [resUser, setResUser] = useState<boolean>(false);
    const [messageError, setMessageError] = useState<string>('');
    const onAssignStudent = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.post('/student-class', request);
            if (res.status === SUCCESS || res.status === 201) {
                setResUser(res?.data);
                ToastSuccess({
                    message: t('success'),
                    description: t('add_success')
                });
            } else if (res.status === ALREADY_ASSIGNED) {
                setMessageError(`${res.data.results} ${t('already_assign')}`);
            } else {
                setMessageError(`${res.message}`);
            }
        } catch (error) {
            setMessageError(`${t('adding_students_course_failed')}`);
        }
        setLoading(false);
    };
    return [onAssignStudent, resUser, loading, messageError];
}

export default useCourseAssignStudent;
