import ApiHelper from 'network/ApiClient';
import { useState } from 'react';
import { SUCCESS } from './../../network/ResponseCode';

function useFetchUser(): [(request: any) => Promise<void>, any, boolean] {
    const [loading, setLoading] = useState<boolean>(false);
    const [resUser, setResUser] = useState<any>();
    const onFetchUsers = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch(
                `/courses/${request?.courseId}/all-user-progress`,
                request
            );
            if (res.status === SUCCESS) {
                setResUser(res.data);
            }
        } catch (error) {}
        setLoading(false);
    };
    return [onFetchUsers, resUser, loading];
}

export default useFetchUser;
