import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import moment from 'moment';

const useViewedQuizCourse = () => {
    const [resOnViewedQuizCourse, setOnViewedQuizCourse] = useState<number>();

    const onViewedQuizCourse = async (quizId: number) => {
        try {
            const res = await ApiHelper.post(
                Endpoint.STUDENT_VIEWED_QUIZ_COURSE,
                {
                    quizId: quizId
                }
            );
            if (res.status === SUCCESS) {
                setOnViewedQuizCourse(moment().unix());
                return;
            }
        } catch (error) {}
        setOnViewedQuizCourse(0);
        return;
    };

    return { resOnViewedQuizCourse, onViewedQuizCourse };
};

export default useViewedQuizCourse;
