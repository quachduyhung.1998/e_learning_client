import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { useState } from 'react';
import { ListUserSearchUnerolledReq } from 'types/requests';
import { SUCCESS } from './../../network/ResponseCode';
import {
    ListUserData,
    ListUserResponse
} from './../../types/responses/UserResponse';

function useFetchSearchUnerolled(): [
    (request: ListUserSearchUnerolledReq, mergeData?: boolean) => Promise<void>,
    ListUserData | undefined,
    boolean,
    React.Dispatch<React.SetStateAction<ListUserData | undefined>>
] {
    const [loading, setLoading] = useState<boolean>(false);
    const [resUser, setResUser] = useState<ListUserData>();
    const onFetchData = async (
        request: ListUserSearchUnerolledReq,
        mergeData?: boolean
    ) => {
        try {
            setLoading(true);
            const res = await ApiHelper.fetch<
                ListUserSearchUnerolledReq,
                ListUserResponse
            >(Endpoint.USERS_SEARCH_UNEROLLED, request);
            if (res.status === SUCCESS) {
                if (mergeData && resUser) {
                    const newUser = [...resUser.results, ...res.data.results];
                    setResUser({ ...resUser, results: newUser });
                } else {
                    setResUser(res.data);
                }
            }
        } catch (error) {}
        setLoading(false);
    };
    return [onFetchData, resUser, loading, setResUser];
}

export default useFetchSearchUnerolled;
