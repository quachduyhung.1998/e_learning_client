import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { RemoveStudentsCourseReq } from './../../types/requests/UserReq';
import ApiHelper from 'network/ApiClient';
import { BaseResponse } from 'types/responses/BaseResponse';
import Endpoint from 'network/Endpoint';
import { ToastSuccess } from 'components/admin/Toast';
import { SUCCESS, NOT_ASSIGNED } from 'network/ResponseCode';
import moment from 'moment';
function useRemoveStudentsCourse(): [
    (request: RemoveStudentsCourseReq) => Promise<void>,
    boolean | undefined,
    number,
    string
] {
    const { t } = useTranslation();
    const [loading, setLoading] = useState<number>(0);
    const [resRemoveUser, setResUser] = useState<boolean>(false);
    const [messageError, setMessageError] = useState<string>('');
    const onRemoveFetchData = async (request: RemoveStudentsCourseReq) => {
        try {
            const res = await ApiHelper.post<
                RemoveStudentsCourseReq,
                BaseResponse
            >(Endpoint.COURSE_UNASSIGN_STUDENT, request);
            if (res.status === SUCCESS) {
                setLoading(moment().unix());
                setResUser(res?.data);
                ToastSuccess({
                    message: t('success'),
                    description: t('remove_students_course_successfully')
                });
            } else if (res.status === NOT_ASSIGNED) {
                setMessageError(`
                    ${res.data.results} ${t('not_assigned')}
                `);
            } else {
                setMessageError(`${res.message}`);
            }
        } catch (error) {
            setMessageError(`${error}`);
        }
    };
    return [onRemoveFetchData, resRemoveUser, loading, messageError];
}

export default useRemoveStudentsCourse;
