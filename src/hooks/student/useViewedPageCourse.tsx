import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import moment from 'moment';

const useViewedPageCourse = () => {
    const [resOnViewedPageCourse, setOnViewedPageCourse] = useState<number>();

    const onViewedPageCourse = async (pageId: number) => {
        try {
            const res = await ApiHelper.post(
                Endpoint.STUDENT_VIEWED_PAGE_COURSE,
                {
                    pageId: pageId
                }
            );
            if (res.status === SUCCESS) {
                setOnViewedPageCourse(moment().unix());
                return;
            }
        } catch (error) {}
        setOnViewedPageCourse(0);
        return;
    };

    return { resOnViewedPageCourse, onViewedPageCourse };
};

export default useViewedPageCourse;
