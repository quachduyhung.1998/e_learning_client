import LoadingManager from 'components/loading/LoadingManager';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DetailQuestion } from 'types/Question';
import { GetQuestionRevisionReq } from 'types/requests';
import { GetQuestionRevisionRes } from 'types/responses';

const useGetQuestionRevision = () => {
    const { t } = useTranslation();
    const [resGetQuestionRevision, setResGetQuestionRevision] = useState<
        DetailQuestion
    >();
    const [messageError, setMessageError] = useState<string>('');
    const [
        loadingGetQuestionRevision,
        setLoadingGetQuestionRevision
    ] = useState<boolean>(false);
    const onGetQuestionRevision = async (request: GetQuestionRevisionReq) => {
        try {
            setLoadingGetQuestionRevision(true);
            const res = await ApiHelper.fetch<
                GetQuestionRevisionReq,
                GetQuestionRevisionRes
            >(Endpoint.GET_QUESTION_REVISION, request);
            if (res.status === SUCCESS) {
                setResGetQuestionRevision(res.data);
            } else {
                setMessageError(`${t(`messages.${res.code}`)}`);
            }
        } catch (error) {
            setMessageError(`${t('fail')}`);
        }
        LoadingManager.hide();
        setLoadingGetQuestionRevision(false);
    };
    return {
        resGetQuestionRevision,
        onGetQuestionRevision,
        loadingGetQuestionRevision,
        messageError
    };
};

export default useGetQuestionRevision;
