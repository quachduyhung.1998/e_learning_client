import LoadingManager from 'components/loading/LoadingManager';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Revision } from 'types/Revision';
import { CreateRevisionReq } from 'types/requests';
import { CreateRevisionRes } from 'types/responses';

const useCreateRevision = () => {
    const { t } = useTranslation();
    const [createRevisionError, setMessageError] = useState<string>('');
    const [resCreateRevision, setResCreateRevision] = useState<Revision>();
    const onCreateRevision = async (request: CreateRevisionReq) => {
        try {
            if (request.categoryIds && request.categoryIds?.length === 0)
                delete request?.categoryIds;
            const res = await ApiHelper.post<
                CreateRevisionReq,
                CreateRevisionRes
            >(Endpoint.CREATE_REVISION, request);
            if (res.status === SUCCESS) {
                setResCreateRevision(res.data);
            } else {
                setMessageError(`${t(`messages.${res.code}`)}`);
            }
        } catch (error) {
            setMessageError(`${t('fail')}`);
        }
        LoadingManager.hide();
    };
    return { resCreateRevision, onCreateRevision, createRevisionError };
};

export default useCreateRevision;
