import LoadingManager from 'components/loading/LoadingManager';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { UpdateAnswerQuestionRevisionReq } from 'types/requests';
import {
    ResultRevision,
    UpdateAnswerQuestionRevisionRes
} from 'types/responses';

const useUpdateAnswerQuestionRevision = () => {
    const { t } = useTranslation();
    const [updateAnswerQuestionRevisionError, setMessageError] = useState<
        string
    >('');
    const [
        resUpdateAnswerQuestionRevision,
        setResUpdateAnswerQuestionRevision
    ] = useState<ResultRevision | any>();
    const [
        loadingUpdateAnswerQuestionRevision,
        setLoadingUpdateAnswerQuestionRevision
    ] = useState<boolean>(false);
    const onUpdateAnswerQuestionRevision = async (
        request: UpdateAnswerQuestionRevisionReq
    ) => {
        try {
            setLoadingUpdateAnswerQuestionRevision(true);
            const res = await ApiHelper.post<
                UpdateAnswerQuestionRevisionReq,
                UpdateAnswerQuestionRevisionRes
            >(Endpoint.UPDATE_ANSWER_QUESTION_REVISION, request);
            if (res.status === SUCCESS) {
                setResUpdateAnswerQuestionRevision(res.data);
            } else {
                setMessageError(`${t(`messages.${res.code}`)}`);
            }
        } catch (error) {
            setMessageError(`${t('fail')}`);
        }
        LoadingManager.hide();
        setLoadingUpdateAnswerQuestionRevision(false);
    };
    return {
        resUpdateAnswerQuestionRevision,
        onUpdateAnswerQuestionRevision,
        loadingUpdateAnswerQuestionRevision,
        updateAnswerQuestionRevisionError
    };
};

export default useUpdateAnswerQuestionRevision;
