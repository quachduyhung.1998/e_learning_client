import LoadingManager from 'components/loading/LoadingManager';
import { ToastSuccess } from 'helper/Toast';
import ApiHelper from 'network/ApiClient';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

function useCreateComment() {
    const { t } = useTranslation();
    const history = useHistory();
    const [loading, setLoading] = useState<boolean>(false);
    const [res, setRes] = useState<any>();
    const onCreateComment = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.post<any, any>(
                '/comments/create',
                request
            );
            setLoading(false);
            if (res.status === SUCCESS || res.status === 201) {
                setRes(res.data);
                setLoading(false);
                return true;
            } else {
                return false;
            }
        } catch (error) {
            setLoading(false);
        }
    };
    return { onCreateComment, res, loading };
}

export default useCreateComment;
