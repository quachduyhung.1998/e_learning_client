import LoadingManager from 'components/loading/LoadingManager';
import { ToastSuccess } from 'helper/Toast';
import ApiHelper from 'network/ApiClient';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

function useUpdateComment() {
    const { t } = useTranslation();
    const [loading, setLoading] = useState<boolean>(false);
    const [res, setRes] = useState<any>();
    const onUpdateComment = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.put<any, any>(
                `/comments/${request?.id}/udpate`,
                request
            );
            setLoading(false);

            if (res.status === SUCCESS || res.status === 201) {
                setRes(res.data);
                setLoading(false);
                return true;
            } else {
                return false;
            }
        } catch (error) {
            setLoading(false);
        }
    };
    return { onUpdateComment, res, loading };
}

export default useUpdateComment;
