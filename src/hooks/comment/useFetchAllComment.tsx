import ApiHelper from 'network/ApiClient';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';

function useFetchListComment() {
    const [listComment, setListComment] = useState<any | null>();
    const [loading, setLoading] = useState(false);
    const onFetchListComment = async (request?: any) => {
        setLoading(true);
        const res = await ApiHelper.fetch(
            `/posts/${request?.id}/get-all-comment`,
            request
        );
        setLoading(false);
        if (res.status === SUCCESS) {
            setListComment(res?.data);
        } else {
            setListComment(null);
        }
    };

    return {
        listComment,
        loading,
        onFetchListComment
    };
}

export default useFetchListComment;
