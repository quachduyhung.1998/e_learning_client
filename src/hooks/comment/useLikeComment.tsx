import { ToastSuccess } from 'helper/Toast';
import ApiHelper from 'network/ApiClient';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

function useLikeComment() {
    const { t } = useTranslation();
    const [loading, setLoading] = useState<boolean>(false);
    const [res, setRes] = useState<any>();
    const onLikeComment = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.post<any, any>(
                '/like-comment',
                request
            );
            setLoading(false);
            if (res.status === SUCCESS || res.status === 201) {
                setRes(res.data);
                setLoading(false);
                return true;
            } else {
                return false;
            }
        } catch (error) {
            setLoading(false);
        }
    };
    return { onLikeComment, res, loading };
}

export default useLikeComment;
