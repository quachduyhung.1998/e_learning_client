import LoadingManager from 'components/loading/LoadingManager';
import { ToastSuccess } from 'helper/Toast';
import ApiHelper from 'network/ApiClient';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

function useDeleteComment() {
    const { t } = useTranslation();
    const [loading, setLoading] = useState<boolean>(false);
    const [res, setRes] = useState<any>();
    const onDeleteComment = async (request: any) => {
        try {
            setLoading(true);
            const res = await ApiHelper.delete<any, any>(
                `/comments/${request?.id}/delete`,
                request
            );
            setLoading(false);

            if (res.status === SUCCESS || res.status === 201) {
                setRes(res.data);
                setLoading(false);
                ToastSuccess({ message: t('success') });
                return true;
            } else {
                return false;
            }
        } catch (error) {
            setLoading(false);
        }
    };
    return { onDeleteComment, res, loading };
}

export default useDeleteComment;
