import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { LoginReq } from 'types/requests';
import LoginResponse from 'types/responses/LoginResponse';
import { User } from 'types/User';

const useLoginApi = (): [
    boolean,
    (request: LoginReq, rememberAccount: boolean) => Promise<void>,

    string,
    User?
] => {
    const [isLoading, setIsLoading] = useState(false);

    const [user, setUser] = useState<User | undefined>(undefined);
    const history = useHistory();
    const { t } = useTranslation();
    const [messageError, setMessageError] = useState<string>('');

    /**
     * Gọi API đăng nhập
     * @param request Thông tin param gửi lên server.
     * @param rememberAccount Option ghi nhớ mật khẩu.
     */
    const onLogin = async (request: LoginReq, rememberAccount: boolean) => {
        try {
            setIsLoading(true);
            const res = await ApiHelper.post<LoginReq, LoginResponse>(
                Endpoint.LOGIN,
                request
            );

            setIsLoading(false);

            if (res.status === SUCCESS) {
                onLoginSuccess(request, res, rememberAccount);
            } else {
                onLoginFail();
            }
        } catch (error) {
            onLoginFail();
            setIsLoading(false);
        }
    };

    /**
     * Xử lý khi đăng nhập thành công
     * Lưu lại thông tin user vào LocalStorage
     * Nếu người dùng ghi nhớ mật khẩu thì lưu lại thông tin account
     * @param req Thông tin gửi tên server từ phía client.abs
     * @param res Response trả về sau khi đăng nhập thành công
     * @param rememberAccount Option ghi nhớ mật khẩu.
     */
    const onLoginSuccess = (
        req: LoginReq,
        res: LoginResponse,
        rememberAccount: boolean
    ) => {
        if (rememberAccount) {
            LocalStorageHelper.save(
                StorageKey.USER_ACCOUNT,
                JSON.stringify(req)
            );
        } else {
            LocalStorageHelper.remove(StorageKey.USER_ACCOUNT);
        }
        LocalStorageHelper.save(StorageKey.TOKEN, res?.access_token);
        LocalStorageHelper.save(
            StorageKey.USER_SESSION,
            JSON.stringify(res.user)
        );
        if (res?.user?.role === 1) {
            history.push('/teacher');
        } else if (res?.user?.role === 2) {
            history.push('/admin');
        } else {
            history.push('/');
        }

        setUser(res.user);
    };

    const onLoginFail = () => {
        // Show popup here
        setMessageError(`${t('account_is_invalid')}`);
    };

    return [isLoading, onLogin, messageError, user];
};

export default useLoginApi;
