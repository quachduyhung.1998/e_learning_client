import ApiHelper from 'network/ApiClient';
import { SUCCESS } from 'network/ResponseCode';
import { useState } from 'react';

function useStatistic() {
    const [listData, setData] = useState<any | null>();
    const [loading, setLoading] = useState(false);
    const onStatistic = async (request?: any) => {
        setLoading(true);
        const res = await ApiHelper.fetch(`/admin/dashboard`, request);
        setLoading(false);
        if (res.status === SUCCESS) {
            setData(res?.data);
        } else {
            setData(null);
        }
    };

    return {
        listData,
        loading,
        onStatistic
    };
}

export default useStatistic;
