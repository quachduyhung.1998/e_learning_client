/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React, { ComponentType } from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { User } from 'types/User';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';

interface Props extends RouteProps {
    component: React.LazyExoticComponent<ComponentType<any>>;
}

const ProtectedRoute = (props: Props) => {
    const { component: Component, ...rest } = props;
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    return (
        <Route
            {...rest}
            render={(props) => {
                if (user) {
                    return <Component {...rest} {...props} />;
                } else {
                    return <Redirect to="/login" />;
                }
            }}
        />
    );
};

export default ProtectedRoute;
