/**
 * Màn hình trang chủ
 * Nếu là học viên thì sẽ show thông tin danh sách khóa học
 * Nếu là giảng viên thì show danh sách khóa học của giảng viên
 */
export const HOME = '/';
export const HOME_TEACHER = '/teacher';
export const LOGIN = '/login';
export const UPDATE_PROFILE = '/update-profile';

// Màn hình kiểm tra đánh giá năng lực
export const KNOWLEDGE_TEST = '/knowledge-test';

// Root route các màn hình của giảng viên
export const LECTURERS_COURSE = '/teacher/course';

// Tạo khóa học
export const LECTURERS_COURSE_CREATE = '/teacher/course/create';

// Cập nhật khóa học
export const LECTURERS_COURSE_DETAIL = '/teacher/course/update';

// Tạo bài học
export const LECTURERS_LESSON_CREATE = '/teacher/lesson/create';
export const LECTURERS_TEST_DETAIL = '/teacher/test-detail';

// Cập nhật bài học
export const LECTURERS_LESSON_UPDATE = '/teacher/lesson/update';

// Root route màn hình của giảng viên
export const MY_COURSE = '/teacher/course';

// Màn danh sách học viên
export const STUDENT = '/teacher/course/:courseId/students';
export const CHAT_LIST = '/chats/list';
export const POST_LIST = '/post';
export const POST_ACTION = '/post-detail';
//  Màn hình chi tiết học viên
export const STUDENT_DETAIL = '/student-detail/:id/';

export const STUDENT_COURSE_DETAIL = '/student/course';
// Giảng viên thêm mới 1 khóa học
export const ADD_COURSE = '/teacher/add-course';

// Màn hình danh sách câu hỏi trong bài kiểm tra
export const LIST_QUESTION = '/teacher/course/:courseId/quiz/:quizId/questions';
export const DETAIL_QUESTION = '/teacher/detail-question';
export const CREATE_QUESTION = '/teacher/create-question';
export const CLASS = '/teacher/class';
export const ACTION_ADD_CLASS = '/teacher/class/add-user';

// Màn hình thêm mới 1 nội dung bài học trong 1 topic
export const ADD_CONTENT_TOPIC =
    '/teacher/course/:courseId/topic/:topicId/add-content';
// Màn hình thêm mới 1 bài học
export const ADD_PAGE_COURSE =
    '/teacher/course/:courseId/topic/:sectionId/create-page-course';

// Màn hình cập nhật page
export const UPDATE_PAGE_COURSE =
    '/teacher/course/:courseId/update-page-course';
export const TEST_OVERVIEW_DETAIL = '/teacher/course/overview/test-detail';
// Màn hình nội dung bài học của học sinh
export const STUDENT_COURSE_CONTENT = '/student/course/:courseId/content';
// Tạo mới bài kiểm tra trong 1 topic
export const ADD_EXAM_TOPIC = '/teacher/course/:courseId/add-exam';
export const DETAIL_EXAM = '/teacher/course/:courseId/detail';
//Màn hình tìm kiếm khóa học của giảng viên
export const TEACHER_SEARCH_COURSE = '/teacher/search';
// Màn hình tìm kiếm khóa học của học sinh
export const SEARCH_COURSE = '/course/search';
// Review 1 khóa học
export const REVIEW_COURSE = '/teacher/course/:courseId/review';
// Tạo mới bài kiểm tra trong 1 topic
export const EXAM_TOPIC_DETAIL = '/teacher/course/:courseId/quiz/:quizId';

// Revision Over View
export const REVISION_START = '/revision/start';
// Giảng viên xem kết quả bài kiểm tra của học sinh
export const COURSE_QUIZ_RESULT =
    '/teacher/course/:courseId/student/quiz-result/over-view';
// Học viên : khoá học của tôi
export const STUDENT_COURSE = '/student-course';
// Học viên : Có thể bạn quan tâm khoá học này
export const RECOMMENDATION_COURSE = '/recommendation-course';
