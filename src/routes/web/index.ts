import { lazy } from 'react';
import { Route } from 'react-router-dom';
import * as paths from 'routes/web/paths';
import { IRoute } from 'types/Route';
import ProtectedRoute from 'routes/ProtectedRoute';

const routes: IRoute[] = [
    {
        route: Route,
        path: paths.LOGIN,
        component: lazy(() => import('pages/user/LoginPage'))
    },
    {
        // route: ProtectedRoute,
        route: Route,
        path: paths.HOME,
        exact: true,
        component: lazy(() => import('pages/home/HomePage'))
    },
    {
        route: ProtectedRoute,
        path: paths.TEACHER_SEARCH_COURSE,
        exact: true,
        component: lazy(() => import('pages/search/SearchCoursePage'))
    },
    {
        route: ProtectedRoute,
        path: paths.HOME_TEACHER,
        exact: true,
        component: lazy(() => import('pages/home/HomeTeacherPage'))
    },
    {
        route: ProtectedRoute,
        path: paths.KNOWLEDGE_TEST,
        exact: true,
        component: lazy(() => import('pages/knowledge-test/KnowledgeTestPage'))
    },
    {
        route: ProtectedRoute,
        path: paths.ADD_COURSE,
        exact: true,
        component: lazy(() => import('pages/teacher-course/CreateCourse'))
    },
    {
        route: ProtectedRoute,
        path: paths.STUDENT,
        exact: true,
        component: lazy(() => import('pages/student/StudentListPage'))
    },
    {
        route: ProtectedRoute,
        path: `${paths.STUDENT_COURSE_DETAIL}/:id`,
        exact: true,
        component: lazy(() => import('pages/student/StudentCourseDetailPage'))
    },
    {
        route: ProtectedRoute,
        path: `${paths.LECTURERS_COURSE}/:id`,
        exact: true,
        component: lazy(() =>
            import('pages/teacher-course/TeacherCourseDetailPage')
        )
    },
    {
        route: ProtectedRoute,
        path: paths.ADD_PAGE_COURSE,
        exact: true,
        component: lazy(() => import('pages/page-course/CreatePageCoursePage'))
    },
    {
        route: ProtectedRoute,
        path: paths.UPDATE_PAGE_COURSE,
        exact: true,
        component: lazy(() => import('pages/page-course/UpdatePageCoursePage'))
    },
    {
        route: ProtectedRoute,
        path: paths.TEST_OVERVIEW_DETAIL,
        exact: true,
        component: lazy(() => import('pages/quiz-result/HistoryDetailTest'))
    },
    {
        route: ProtectedRoute,
        path: paths.ADD_EXAM_TOPIC,
        exact: true,
        component: lazy(() => import('pages/add-exam/AddExamTopicPage'))
    },
    {
        route: ProtectedRoute,
        path: paths.POST_LIST,
        exact: true,
        component: lazy(() => import('pages/class/ClassHome'))
    },
    {
        route: ProtectedRoute,
        path: paths.POST_ACTION,
        exact: true,
        component: lazy(() => import('pages/class/ActionClassHome'))
    },
    {
        route: ProtectedRoute,
        path: paths.CHAT_LIST,
        exact: true,
        component: lazy(() => import('pages/chats/list'))
    },
    {
        route: ProtectedRoute,
        path: paths.DETAIL_EXAM,
        exact: true,
        component: lazy(() => import('pages/add-exam/AddExamTopicPage'))
    },
    {
        route: ProtectedRoute,
        path: paths.REVIEW_COURSE,
        exact: true,
        component: lazy(() => import('pages/review-course/ReviewCoursePage'))
    },
    {
        route: ProtectedRoute,
        path: paths.EXAM_TOPIC_DETAIL,
        exact: true,
        component: lazy(() => import('pages/add-exam/AddExamTopicPage'))
    },
    {
        route: ProtectedRoute,
        path: paths.LIST_QUESTION,
        exact: true,
        component: lazy(() => import('pages/exam-question/ExamQuestionPage'))
    },
    {
        route: ProtectedRoute,
        path: paths.CREATE_QUESTION,
        exact: true,
        component: lazy(() => import('pages/exam-question/CreateQuestionPage'))
    },
    {
        route: ProtectedRoute,
        path: paths.CLASS,
        exact: true,
        component: lazy(() => import('pages/admin/class/action'))
    },
    {
        route: ProtectedRoute,
        path: paths.ACTION_ADD_CLASS,
        exact: true,
        component: lazy(() => import('pages/admin/class/actionAddUser'))
    },
    {
        route: ProtectedRoute,
        path: paths.DETAIL_QUESTION,
        exact: true,
        component: lazy(() => import('pages/exam-question/DetailQuestionPage'))
    },
    {
        route: ProtectedRoute,
        path: paths.SEARCH_COURSE,
        exact: true,
        component: lazy(() => import('pages/search/SearchCoursePage'))
    },

    {
        route: ProtectedRoute,
        path: paths.UPDATE_PROFILE,
        exact: true,
        component: lazy(() => import('pages/update-profile/UpdateProfilePage'))
    },
    {
        route: ProtectedRoute,
        path: paths.REVISION_START,
        exact: true,
        component: lazy(() => import('pages/revision/RevisionStart'))
    },
    {
        route: ProtectedRoute,
        path: paths.COURSE_QUIZ_RESULT,
        exact: true,
        component: lazy(() => import('pages/quiz-result/OverView'))
    },
    {
        route: ProtectedRoute,
        path: paths.STUDENT_COURSE,
        exact: true,
        component: lazy(() => import('pages/student-course/CourseListPage'))
    },
    {
        route: ProtectedRoute,
        path: paths.RECOMMENDATION_COURSE,
        exact: true,
        component: lazy(() =>
            import('pages/recommendation-course/RecommendationCourse')
        )
    },
    {
        route: ProtectedRoute,
        path: paths.STUDENT_COURSE_CONTENT,
        exact: true,
        component: lazy(() =>
            import('pages/student/page-courses/StudentCourseContentPage')
        )
    },
    {
        route: ProtectedRoute,
        path: paths.STUDENT_DETAIL,
        exact: true,
        component: lazy(() => import('pages/student/StudentDetailPage'))
    }
];

export default routes;
