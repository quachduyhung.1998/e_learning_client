import * as paths from 'routes/admin/paths';
import { Route } from 'react-router-dom';
import { lazy } from 'react';
import { IRoute } from 'types/Route';
import ProtectedRoute from 'routes/ProtectedRoute';

const routes: IRoute[] = [
    {
        route: ProtectedRoute,
        path: paths.DASHBOARD,
        exact: true,
        component: lazy(() => import('pages/admin/dashboard/Index'))
    },
    {
        route: ProtectedRoute,
        path: paths.REVIEWING_COURSES,
        component: lazy(() => import('pages/admin/course/ReviewingCourse'))
    },
    {
        route: ProtectedRoute,
        path: paths.USER_LIST,
        exact: true,
        component: lazy(() => import('pages/admin/user/List'))
    },
    {
        route: ProtectedRoute,
        path: paths.USER_ACTION,
        exact: true,
        component: lazy(() => import('pages/admin/user/actionUser'))
    },
    {
        route: ProtectedRoute,
        path: paths.CLASS_LIST,
        exact: true,
        component: lazy(() => import('pages/admin/class/List'))
    },
    {
        route: ProtectedRoute,
        path: paths.CLASS_ACTION,
        exact: true,
        component: lazy(() => import('pages/admin/class/action'))
    },
    {
        route: ProtectedRoute,
        path: paths.GROUP_USER_LIST,
        exact: true,
        component: lazy(() => import('pages/admin/group-user/List'))
    },
    {
        route: ProtectedRoute,
        path: paths.GROUP_USER_CREATE,
        exact: true,
        component: lazy(() => import('pages/admin/group-user/Create'))
    },
    {
        route: ProtectedRoute,
        path: paths.GROUP_USER_UPDATE,
        exact: true,
        component: lazy(() => import('pages/admin/group-user/Edit'))
    },
    {
        route: ProtectedRoute,
        path: paths.GROUP_USER_LIST_USER,
        exact: true,
        component: lazy(() => import('pages/admin/group-user/ListUser'))
    }
];

export default routes;
