// Dashboard Admin
export const DASHBOARD = '/admin';

// Danh sách khoá học cần review
export const REVIEWING_COURSES = '/admin/reviewing-courses';

// Danh sách user
export const USER_LIST = '/admin/users';
export const USER_ACTION = '/admin/users/action';
export const CLASS_ACTION = '/admin/class/action';
export const CLASS_LIST = '/admin/class';

// Group user
export const GROUP_USER_LIST = '/admin/group-user';
export const GROUP_USER_CREATE = '/admin/group-user/create';
export const GROUP_USER_UPDATE = '/admin/group-user/:groupId/edit';
export const GROUP_USER_LIST_USER = '/admin/group-user/:groupId/user';
