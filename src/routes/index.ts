import { lazy } from 'react';
import { Route } from 'react-router-dom';
import { IRoute } from 'types/Route';

export const paths = {
    admin: {
        index: '/admin'
    },
    web: {
        login: '/login',
        index: '/'
    },
    all: '*'
};

const routes: IRoute[] = [
    {
        route: Route,
        path: paths.admin.index,
        component: lazy(() => import('layout/admin/MasterLayout'))
    },
    {
        route: Route,
        path: paths.web.login,
        component: lazy(() => import('pages/user/LoginPage'))
    },
    {
        route: Route,
        path: paths.web.index,
        component: lazy(() => import('layout/web/MasterLayout'))
    },
    {
        route: Route,
        path: paths.all,
        component: lazy(() => import('pages/common/NotFoundPage'))
    }
];

export default routes;
