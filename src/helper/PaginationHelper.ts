export const handleTotalPage = ({
    total,
    limit
}: {
    total: number;
    limit: number;
}) => (total > limit ? Math.ceil(total / limit) : 0);
