import Echo from 'laravel-echo';
import ioClient from 'socket.io-client';
import Pusher from 'pusher-js';

const options = {
    broadcaster: 'pusher',
    key: '8a09307a347bebcc29e4',
    cluster: 'ap1',
    forceTLS: true
};
const echoClient = new Echo(options);

export default echoClient;
