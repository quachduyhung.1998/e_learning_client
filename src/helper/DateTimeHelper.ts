export const SIMPLE_DATE = 'YYYY-MM-DD';
export const SIMPLE_TIME = 'HH:mm:ss';
export const DATE_TIME_VIEW = 'HH:mm DD/MM/YYYY';
export const ISO_DATE_TIME = 'YYYY-MM-DDTHH:mm:ss.sssZ';
