import axios, { AxiosResponse, AxiosRequestConfig } from 'axios';
import { API_DOMAIN } from 'constants/App';

async function postApi(url: string, data: any) {
    const response: AxiosResponse = await axios.post(`${API_DOMAIN}/${url}`, {
        ...data
    });
    return response;
}

export { postApi };
