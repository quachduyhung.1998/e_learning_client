import moment from 'moment';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { DASHBOARD } from 'routes/admin/paths';
import {
    HOME,
    KNOWLEDGE_TEST,
    LOGIN,
    UPDATE_PROFILE,
    HOME_TEACHER
} from 'routes/web/paths';
import { Role, RoleType, User, RouteType } from 'types/User';
import { Answer } from 'types/Answer';
import { DATE_TIME_VIEW } from './DateTimeHelper';
import LocalStorageHelper, { StorageKey } from './LocalStorageHelper';
/**
 * Chuyển đổi từ role type sang role name
 * @param role Role cần để lấy thông tin về tên
 * @returns Tên của role đó
 */
export const getRoleName = (role: string): string => {
    switch (role) {
        case RoleType.ADMIN:
            return 'admin_role';

        case RoleType.STUDENT:
            return 'student_role';

        case RoleType.TEACHER:
            return 'teacher_role';

        default:
            return '';
    }
};

/**
 *
 * @param route Route hiện tại
 */
export const getRoleByRoute = (): string => {
    const route = window.location.pathname;
    const CURRENT_ROLE = route.split('/')[1] ? route.split('/')[1] : '';
    switch (CURRENT_ROLE) {
        case RouteType.ADMIN:
            return RoleType.ADMIN;
        case RouteType.TEACHER:
            return RoleType.TEACHER;
        default:
            return RoleType.STUDENT;
    }
};
/**
 * Điều hướng người dùng đến màn hình phù hợp sau khi người dùng thực hiện thao tác login.
 * @param user Thông tin người dùng.
 * @param role Quyền đang được lựa chọn để thực hiện việc điều hướng.
 * @param history Router history để điều hướng.
 * @param skipUpdateProfile Bỏ qua bước update profile.
 * @param skipKnowledgeTest Bỏ qua bước làm bài kiểm tra đầu vào.
 */
export const loginNavigation = (
    user: User,
    role: string,
    history: any,
    skipUpdateProfile?: boolean,
    skipKnowledgeTest?: boolean
): void => {
    if (
        role === RoleType.STUDENT &&
        user?.needUpdateProfile &&
        !skipUpdateProfile
    ) {
        history.push(UPDATE_PROFILE, {
            user,
            selectedRole: role
        });
        return;
    }
    if (
        role === RoleType.STUDENT &&
        user?.needPlacementExam &&
        !skipKnowledgeTest
    ) {
        history.push(KNOWLEDGE_TEST, {
            user,
            selectedRole: role
        });
        return;
    }
    // Kiểm tra nếu role = manager thì login vào màn Dashboard
    if (role === RoleType.ADMIN) {
        history.push(DASHBOARD);
    } else if (role === RoleType.TEACHER) {
        history.push(HOME_TEACHER);
    } else {
        history.push(HOME);
    }
};

export const onKeyPressByRegex = (e: React.KeyboardEvent, regex?: string) => {
    const _regex = regex ? regex : '^[0-9]*$';
    const specialCharRegex = new RegExp(_regex);
    const pressedKey = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (!specialCharRegex.test(pressedKey)) {
        e.preventDefault();
        return false;
    }
};
export const category_course: any[] = [
    { id: 1, name: 'Toán' },
    { id: 2, name: 'Tiếng anh' }
];
export const logout = (): void => {
    const refreshToken = LocalStorageHelper.getRefreshToken();
    if (refreshToken) {
        ApiHelper.post(Endpoint.LOGOUT, { refreshToken });
    }

    LocalStorageHelper.remove(StorageKey.USER_SESSION);
    LocalStorageHelper.remove(StorageKey.CURRENT_ROLE);
    window.location.href = LOGIN;
};

export const answerValidation = (
    answers: Answer[]
): { valid: boolean; message?: string } => {
    if (answers.length < 2) {
        return { valid: false, message: 'answer_not_empty' };
    }

    let answerExists = false;
    let contentEmpty = false;
    answers.forEach((answer: Answer) => {
        if (answer.correct) {
            answerExists = true;
        }

        if (!answer.value) {
            contentEmpty = true;
        }
    });

    if (!answerExists) {
        return { valid: false, message: 'at_least_one_correct_answer' };
    }

    if (contentEmpty) {
        return { valid: false, message: 'answer_cannot_empty' };
    }

    return {
        valid: true
    };
};

/**
 * Valid thời gian hoạt động của bài kiểm tra
 * @param dateStart Thời gian bắt đầu của bài kiểm tra
 * @param dateEnd Thời gian kết thúc của bài kiểm tra
 */
export const validateDateTime = (
    timeOpen?: moment.Moment,
    timeClose?: moment.Moment
): { valid: boolean; message?: string } => {
    const currentDate = moment(new Date(), DATE_TIME_VIEW);

    if (timeClose) {
        if (timeClose.isBefore(currentDate)) {
            return {
                valid: false,
                message: 'date_select_must_greater_than_current'
            };
        }

        if (timeOpen) {
            if (timeClose.isBefore(timeOpen)) {
                return {
                    valid: false,
                    message: 'date_end_must_greater_than_start'
                };
            }
        }
    }

    return { valid: true };
};
