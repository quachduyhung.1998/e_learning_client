import React from 'react';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

interface ToastProps {
    title?: string | null;
    message: string;
}

interface Content {
    title: string;
    message: string;
}

const MessageCompoment = (props: Content) => {
    const { title, message } = props;
    return (
        <div>
            <p className="m-0">
                <b>{title}</b>
            </p>
            <p className="m-0">{message}</p>
        </div>
    );
};

export const ToastSuccess = (content: ToastProps): React.ReactText =>
    toast.success(
        <MessageCompoment
            title={content?.title || ''}
            message={content?.message || ''}
        />,
        {
            position: 'top-right',
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
        }
    );

export const ToastError = (content: ToastProps): any => {
    toast.error(
        <MessageCompoment
            title={content?.title || ''}
            message={content?.message || ''}
        />,
        {
            position: 'top-right',
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
        }
    );
};
