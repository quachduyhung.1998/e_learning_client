import { Role } from 'types/User';
import { User } from 'types/User';
import { Session } from 'types/Session';

export enum StorageKey {
    TOKEN = 'TOKEN',
    USER_SESSION = 'USER_SESSION',
    USER_ACCOUNT = 'USER_ACCOUNT',
    CURRENT_ROLE = 'CURRENT_ROLE',
    CACHE_FORM_CREATE_COURSE = 'CACHE_FORM_CREATE_COURSE',
    CACHE_FORM_CREATE_PAGE_COURSE = 'CACHE_FORM_CREATE_PAGE_COURSE',
    REVISION_INFO = 'REVISION_INFO',
    CACHE_FORM_REVISION_REQ = 'CACHE_FORM_REVISION_REQ',
    CACHE_FORM_CREATE_EXAM = 'CACHE_FORM_CREATE_EXAM'
}

function save(key: StorageKey, value: string): void {
    localStorage.setItem(key, value);
}

function get(key: StorageKey): string | null {
    return localStorage.getItem(key);
}

function saveObject(key: StorageKey, object: any): boolean {
    try {
        localStorage.setItem(key, JSON.stringify(object));
        return true;
    } catch (error) {
        return false;
    }
}

function getObject(key: StorageKey): any {
    try {
        return JSON.parse(localStorage.getItem(key)?.toString() || '{}');
    } catch (error) {
        return {};
    }
}

function remove(key: StorageKey): void {
    localStorage.removeItem(key);
}

function clear(): void {
    localStorage.clear();
}

function updateUserStorage(user: User): void {
    const sessionStr = localStorage.getItem(StorageKey.USER_SESSION);
    if (sessionStr) {
        let session: Session = JSON.parse(sessionStr);
        session = {
            ...session,
            idTokenPayload: user
        };

        localStorage.setItem(StorageKey.USER_SESSION, JSON.stringify(session));
    }
}

function updateAccessToken(idToken: string): void {
    const sessionStr = localStorage.getItem(StorageKey.USER_SESSION);
    if (sessionStr) {
        let session: Session = JSON.parse(sessionStr);
        session = {
            ...session,
            idToken
        };

        localStorage.setItem(StorageKey.USER_SESSION, JSON.stringify(session));
    }
}

function getAccessToken(): string | undefined {
    const sessionStr = localStorage.getItem(StorageKey.USER_SESSION) || '';
    if (!sessionStr) return undefined;
    const session: Session = JSON.parse(sessionStr);
    return session.idToken;
}

function getRefreshToken(): string | undefined {
    const sessionStr = localStorage.getItem(StorageKey.USER_SESSION) || '';
    if (!sessionStr) return undefined;
    const session: Session = JSON.parse(sessionStr);
    return session.refreshToken;
}

function getUserStorage(): User | undefined {
    const sessionStr = localStorage.getItem(StorageKey.USER_SESSION);
    if (!sessionStr) return undefined;
    const session: Session = JSON.parse(sessionStr);
    return session.idTokenPayload;
}

function getCurrentRole(): string | undefined {
    return localStorage.getItem(StorageKey.CURRENT_ROLE) || undefined;
}

export default {
    save,
    get,
    saveObject,
    getObject,
    remove,
    clear,
    updateUserStorage,
    getAccessToken,
    getRefreshToken,
    updateAccessToken,
    getUserStorage,
    getCurrentRole
};
