import { CourseStatus } from 'types/Course';

export const getStatusFormatCourse = (status: CourseStatus): string => {
    switch (status) {
        case CourseStatus.STATUS_DRAFT:
            return 'status_draft';
        case CourseStatus.STATUS_REVIEWING:
            return 'status_review';
        case CourseStatus.STATUS_APPROVED:
            return 'status_approved';
        default:
            return 'unknown';
    }
};
