import { indexOf } from 'lodash';

interface DataReplace {
    [key: string]: string | number | undefined;
}

export const __ = (str: string, data: DataReplace): string => {
    for (const [key, value] of Object.entries(data)) {
        str = str.replace(new RegExp(`{${key}}`, 'g'), `${value?.toString()}`);
    }
    return str;
};
export const sliceString = (text: string, length: number) => {
    return text?.length > length ? `${text.substring(0, length)}...` : text;
};
export const replaceThenAddCommas = (number: string | number) =>
    (number &&
        number
            .toString()
            .replace(/^0+/, '')
            .replace(/\./g, '')
            .replace(/[^0-9\.]+/g, '')
            .replace(/\B(?=(\d{3})+(?!\d))/g, ',')) ||
    number;

export const format = {
    time: {
        vi: 'HH:mm',
        HH_mm_dd_mm_yyyy: 'HH:mm DD/MM/YYYY',
        HH_mm_ss_dd_mm_yyyy: 'HH:mm:ss DD/MM/YYYY',
        yyyy_mm_dd_hh_mm_ss: 'YYYY/MM/DD HH:mm:ss',
        HH_mm_mm_dd_yyyy: 'HH:mm MM/DD/YYYY',
        HH_mm_yyyy_mm_dd: 'HH:mm YYYY/MM/DD',
        dd_mm_yyyy: 'DD/MM/YYYY',
        mm_dd_yyyy: 'MM/DD/YYYY',
        yyyy_mm_dd: 'YYYY/MM/DD',
        dddd: 'dddd',
        yyyy: 'YYYY',
        mm_yyyy: 'MM/YYYY',
        q_yyyy: 'Q,YYYY'
    }
};
export const getUrl = (str: string, data: DataReplace): string => {
    for (const [key, value] of Object.entries(data)) {
        str = str.replace(new RegExp(`/:${key}`, 'g'), `/${value?.toString()}`);
    }
    return str;
};

export const subString = (str: string, len: number, add = '') => {
    const s = (str || '').trim();
    if (s.length <= len) {
        return s;
    }
    const temp = s.substring(0, len);
    if (temp.slice(-1) === ' ') {
        return temp.trim() + add;
    }
    const indexBlankLast = temp.lastIndexOf(' ');
    if (indexBlankLast !== -1) {
        return temp.substring(0, indexBlankLast).trim() + add;
    }
    temp.slice(-1);
    return temp + add;
};

export const keySplit = (key: string) => {
    const arr = key.split('_');
    if (arr.length !== 2) {
        throw new Error('Key not valid');
    }
    const data: { userId: string; collectionType: string } = {
        userId: arr[0],
        collectionType: arr[1]
    };

    return data;
};
