export default function getImageServer(uri: string): string {
    return `${process.env.REACT_APP_DOMAIN}${uri}`;
}
