import { USER } from '../constants/LocalStorageKey';
import LocalStorageHelper, { StorageKey } from './LocalStorageHelper';

export function getUserByToken() {
    const user = localStorage.getItem(USER);
    if (user) {
        return JSON.parse(user);
    }
    return '';
}

export function getStatusRemember() {
    const account = LocalStorageHelper.get(StorageKey.USER_ACCOUNT);
    if (account) {
        return JSON.parse(account);
    }
    return undefined;
}
