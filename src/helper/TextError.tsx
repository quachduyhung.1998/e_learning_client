import React, { ReactNode } from 'react';

interface IPROPS {
    message: string | ReactNode | undefined;
}

function TextError(props: IPROPS) {
    return <span className="small text-danger">{props.message}</span>;
}

export default TextError;
