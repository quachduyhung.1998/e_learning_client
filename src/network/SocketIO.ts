import SocketIOClient from 'socket.io-client';
import LocalStorageHelper from 'helper/LocalStorageHelper';

const token = LocalStorageHelper.getAccessToken();

const socketIOClient = SocketIOClient(
    process.env.REACT_APP_API_DOMAIN_SOCKET || 'ws://localhost:4000',
    { transports: ['polling'], query: { token: token } }
);

export default socketIOClient;
