/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import EventBus from 'events/EventBus';
import { EventBusName, LogoutPayloadType } from 'events/EventBusType';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import Endpoint from 'network/Endpoint';
import { BaseResponse } from 'types/responses/BaseResponse';
import { Session } from 'types/Session';
import { UNAUTHENTICATED } from './ResponseCode';
import { logout, getRoleByRoute } from 'helper/Utils';

const REQ_TIMEOUT = 60 * 1000;
const instance = axios.create({
    baseURL: process.env.REACT_APP_API_DOMAIN,
    timeout: REQ_TIMEOUT
});

instance.interceptors.request.use((_config) => requestHandler(_config));

const requestHandler = (request: AxiosRequestConfig) => {
    console.log(
        `Request API ===> ${request.url}`,
        request.params,
        request.data
    );

    return request;
};

instance.interceptors.response.use(
    (response) => successHandler(response),
    (error) => errorHandler(error)
);

const errorHandler = (error: any) => {
    const originalRequest = error.config;
    const refreshToken = LocalStorageHelper.getRefreshToken();

    if (
        error.response.status === UNAUTHENTICATED &&
        !originalRequest._retry &&
        refreshToken !== ''
    ) {
        originalRequest._retry = true;
        return instance
            .post(Endpoint.REFRESH_TOKEN, {
                refreshToken
            })
            .then((res) => {
                // Save new token to local storage
                LocalStorageHelper.updateAccessToken(res.data.idToken);

                // Change Authorization header
                originalRequest.headers = {
                    ...originalRequest.headers,
                    Authorization: `Bearer ${res.data.idToken}`
                };
                // Return originalRequest object with Axios
                return instance(originalRequest);
            })
            .catch((error) => {
                EventBus.getInstance().post<LogoutPayloadType>({
                    type: EventBusName.LOGOUT_EVENT
                });
            });
    }

    return Promise.reject({ ...error });
};

const successHandler = (response: AxiosResponse) => {
    console.log(`Response API <=== ${response.config.url}`, response.data);
    if (response.data.code === 'CURRENT_ROLE_NOT_FOUND') {
        logout();
    }
    return response.data;
};

async function fetch<ReqType, ResType extends BaseResponse>(
    url: string,
    params?: ReqType,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.get(url, { params, headers });
}

async function post<ReqType, ResType extends BaseResponse>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.post(url, { ...data }, { headers });
}

async function postFormData<ResType extends BaseResponse>(
    url: string,
    data?: FormData,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.post(url, data, { headers });
}

async function put<ReqType, ResType extends BaseResponse>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.put(url, { ...data }, { headers });
}
async function remove<ReqType, ResType extends BaseResponse>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.delete(url, { data, headers });
}

function getHeader(customHeaders: any = {}): any {
    const sessionStr: string | null = LocalStorageHelper.get(StorageKey.TOKEN);
    // const currentRole = getRoleByRoute();
    if (sessionStr) {
        // const session: Session = JSON.parse(sessionStr);
        // const token = session.idToken;
        if (sessionStr) {
            return {
                ...customHeaders,
                // role: currentRole,
                Authorization: `Bearer ${sessionStr}`
            };
        }
    }

    return { ...customHeaders };
}
const ApiHelper = { fetch, post, postFormData, put, delete: remove };

export default ApiHelper;
