export default {
    LOGIN: '/login',
    // Lấy danh sách chủ đề khi tạo mới khóa học
    CATEGORIES: '/course-category/2',
    UPLOAD: '/upload-image',
    COURSE: '/courses',
    LESSON: '/lessons',
    UPDATE_COURSE: '/course/update',
    DELETE_COURSE: '/course',
    UPDATE_USER_PROFILE: '/user/profile',
    REFRESH_TOKEN: '/refresh-token',
    PLACEMENT_EXAM: '/placement-exam',
    KNOWLEDGE_TEST: '/knowledge-test',
    GET_PAGE_COURSE: '/course/:courseId/page/:pageId',
    PAGE: '/page',
    UPDATE_PAGE: '/page/update',
    LOGOUT: '/logout',
    LECTURER_COURSE: '/my-courses',
    LECTURER_COURSE_DETAIL: '/course-infomation',
    // Tạo mới topic
    ADD_TOPIC: '/course/topic',
    // Hoàn thành khóa học
    COURSE_FINISH_UPDATING: '/complete',
    // Get quiz info
    GET_QUIZ: '/course/:courseId/quiz/:instanceId',
    // Tạo mới bài kiểm tra
    ADD_QUIZ: '/quiz/create',
    // Danh sách câu hỏi trong bài kiểm tra
    LIST_QUESTION: '/questions',
    // Thêm câu hỏi
    QUESTION: '/questions',
    // Get câu hỏi của 1 bài kiểm tra
    QUESTIONS_OF_QUIZ: '/list_questions/',
    // Tạo bài kiểm tra
    QUIZ_CREATE: '/tests',
    // Cập nhật bài kiểm tra
    QUIZ_UPDATE: '/quiz/update',
    // Chi tiết bài kiểm tra
    SHOW_COURSE_QUIZ: '/course/:courseId/quiz/:quizId',
    // Approve course
    APPROVE_COURSE: '/course/approve',
    // List User of Course
    LIST_USER_OF_COURSE: '/course/users',
    // Danh sách users theo roles
    USERS_LIST: '/users/list',
    // Gán giảng viên vào approve khoá học
    ASSIGN_TEACHER_TO_APPROVE: '/teacher-approval-course',
    // Tìm kiếm users
    USERS: '/users',
    USERS_PERMISSION: '/users/permissions/',
    // Gán role cho nhiều user
    ASSIGN_ROLE: '/users/assign-role',
    // Gán nhiều role cho user
    ASSIGN_ROLES: '/users/assign-roles',
    // Gỡ role cho nhiều user
    UN_ASSIGN_ROLE: '/users/unassign-role',
    // Gỡ nhiều role cho user
    UN_ASSIGN_ROLES: '/users/unassign-roles',
    // Danh sách user/group không được gán vào khoá học theo role
    USERS_SEARCH_UNEROLLED: '/users/searchUnerolled',
    // Xoá học viên vào khoá học
    COURSE_UNASSIGN_STUDENT: '/course/unassign-student',
    // Gán học viên vào khoá học
    COURSE_ASSIGN_STUDENT: '/course/assign-student',
    // Gán giáo viên vào review khoá học
    ASSIGN_TEACHER_TO_REVIEW: '/course/assign-user-to-review',
    // Danh sách khóa học đề xuất
    RECOMMENDATION: 'course/recommendations',
    // Update current role
    UPDATE_CURRENT_ROLE: '/users/update-current-role',
    // HV tạo một bài ôn tập
    CREATE_REVISION: '/revision/create',
    // Get question revision
    GET_QUESTION_REVISION: '/revision/question',
    // Update question revision
    UPDATE_ANSWER_QUESTION_REVISION: '/revision/question',
    // Remove giáo viên ra khỏi danh sách được gán để duyệt khoá học
    REMOVE_USER_FROM_ASSIGNED_LIST: '/course/remove-user-from-assigned-list',
    // student check viewed page course
    STUDENT_VIEWED_PAGE_COURSE: '/page/markAsViewed',
    // student check viewed page course
    STUDENT_VIEWED_QUIZ_COURSE: '/quiz/markAsViewed',
    // student start quiz
    START_QUIZ: '/quiz/start',
    // student get question started quiz
    GET_QUESTION_STARTED_QUIZ: '/quiz/question',
    // student update answer question started quiz
    UPDATE_ANSWER_STARTED_QUIZ: '/quiz/update-answer',
    // Get history started quiz
    GET_HISTORY_STARTED_QUIZ: '/quiz/attempts',
    // Get detail history started quiz
    GET_DETAIL_HISTORY_STARTED_QUIZ: '/quiz/attempt',
    // Get approve Course
    CAN_APPROVE_COURSE: '/course/can-approve',
    // List group user
    LIST_GROUP_USER: '/groups',
    // Create group user
    CREATE_GROUP_USER: '/group/create',
    // Update group user
    UPDATE_GROUP_USER: '/group/update',
    // Detail group user
    DETAIL_GROUP_USER: '/group/detail',
    // Delete group user
    DELETE_GROUP_USER: '/group/delete',
    // Add group user
    ADD_USER_GROUP: '/group/add-users',
    // Remove group user
    REMOVE_USER_GROUP: '/group/remove-users',
    //get user detail
    GET_DETAIL_USER: '/users/',
    USER: '/users',
    // Move Topic
    MOVE_TOPIC: '/course/move-topic',
    // Move Module
    MOVE_QUIZ: '/page/move-module',
    // Update interative course
    UPDATE_INTERACTIVE_COURSE: '/course/update-interactive'
};
