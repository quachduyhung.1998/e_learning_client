import Modal from 'antd/lib/modal/Modal';
import Exam from 'components/shares/Exam';
import InfoExam from 'components/shares/InfoExam';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import useGetQuestionRevision from 'hooks/revision/useGetQuestionRevision';
import useUpdateAnswerQuestionRevision from 'hooks/revision/useUpdateAnswerQuestionRevision';
import useTimeUp from 'hooks/time/useTimeUp';
import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { Container } from 'reactstrap';
import { HOME } from 'routes/web/paths';
import { Revision } from 'types/Revision';
import { Button, Row, Col } from 'antd';
import moment from 'moment';
import images from 'images';
import '../student/page-courses/StudentCourseContentPage.scss';
import ModalError from 'components/shares/modal/ModalError';

const RevisionStart = (): JSX.Element | null => {
    const { t } = useTranslation();
    const history = useHistory();

    const revisionInfo: Revision = LocalStorageHelper.getObject(
        StorageKey.REVISION_INFO
    );
    const [overView, setOverView] = useState(
        revisionInfo?.timeStart ? false : true
    );

    const [showModal, setShowModal] = useState(false);
    const [isStart, setIsStart] = useState(
        revisionInfo?.questionIndex ? true : false
    );

    //thời gian để hoàn thành câu hỏi;
    const { millisecond, setMillisecond } = useTimeUp(0);

    const {
        resGetQuestionRevision,
        onGetQuestionRevision,
        messageError
    } = useGetQuestionRevision();
    const {
        resUpdateAnswerQuestionRevision,
        onUpdateAnswerQuestionRevision,
        updateAnswerQuestionRevisionError
    } = useUpdateAnswerQuestionRevision();

    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (messageError || updateAnswerQuestionRevisionError) {
            setShowMessageError(true);
        }
    }, [messageError, updateAnswerQuestionRevisionError]);

    const getQuestion = (index: number) => {
        if (isStart) {
            onGetQuestionRevision({
                revisionId: revisionInfo._id,
                page: index
            });
        } else {
            onGetQuestionRevision({ revisionId: revisionInfo._id });
            setIsStart(true);
        }
        LocalStorageHelper.saveObject(StorageKey.REVISION_INFO, {
            ...revisionInfo,
            questionIndex: index
        });
        setMillisecond(0);
    };

    const updateAnswer = (id: number, index: number, answers: number[]) => {
        if (answers !== resGetQuestionRevision?.answerCorrect) {
            onUpdateAnswerQuestionRevision({
                answerCorrect: answers,
                moodleQuestionId: id,
                thinkingDuration: millisecond,
                revisionId: revisionInfo._id
            });
        }
    };
    const finishExam = (id: number, index: number, answers: number[]) => {
        onUpdateAnswerQuestionRevision({
            answerCorrect: answers,
            moodleQuestionId: id,
            thinkingDuration: millisecond,
            revisionId: revisionInfo._id,
            type: 'FINISH'
        });
        setShowModal(true);
    };

    const onCancel = () => {
        LocalStorageHelper.remove(StorageKey.REVISION_INFO);
        history.push(HOME);
    };

    const onStart = () => {
        setOverView(false);
        setMillisecond(0);
        LocalStorageHelper.saveObject(StorageKey.REVISION_INFO, {
            ...revisionInfo,
            timeStart: moment()
        });
    };

    if (!revisionInfo._id && !showModal) {
        return (
            <div style={{ textAlign: 'center' }}>
                <Button type="primary" onClick={() => history.push(HOME)}>
                    {t('home')}
                </Button>
            </div>
        );
    }

    return (
        <Container>
            {overView && (
                <div className="container-course-content">
                    <Row className="header-information">
                        <Col sm={18}>
                            <h3>{t('revision')}</h3>
                        </Col>
                        <Col sm={6} style={{ textAlign: 'right' }}>
                            <Button
                                className="btn-style"
                                onClick={() => {
                                    history.push('/');
                                }}
                            >
                                <img src={images.ic_down} alt="" />
                                {t('back')}
                            </Button>
                        </Col>
                    </Row>
                    <div style={{ padding: '0.5em', backgroundColor: 'white' }}>
                        <InfoExam
                            name={t('revision')}
                            typeQuestion={t('multiple_choice')}
                            totalQuestion={
                                revisionInfo.currentTotalQuestion || 0
                            }
                            timeLimit={revisionInfo.maxDuration * 60}
                            onStart={onStart}
                            onCancel={onCancel}
                        />
                    </div>
                </div>
            )}
            {!overView && (
                <div style={{ padding: '0.5em', backgroundColor: 'white' }}>
                    {/* {revisionInfo._id && (
                        <Exam
                            initIndex={revisionInfo.questionIndex}
                            getQuestion={getQuestion}
                            updateAnswer={updateAnswer}
                            finishExam={finishExam}
                            timeLimit={revisionInfo.maxDuration * 60}
                            timeStart={
                                revisionInfo?.timeStart
                                    ? revisionInfo.maxDuration * 60 -
                                      (moment().unix() -
                                          moment(
                                              revisionInfo?.timeStart
                                          ).unix())
                                    : undefined
                            }
                            totalQuestion={revisionInfo.currentTotalQuestion}
                            question={resGetQuestionRevision}
                            answers={resGetQuestionRevision?.answerCorrect}
                        />
                    )} */}
                    <Modal
                        title={t('result_exam')}
                        visible={showModal}
                        onOk={onCancel}
                        onCancel={onCancel}
                        footer={null}
                    >
                        <p style={{ fontSize: '3em', textAlign: 'center' }}>
                            {
                                resUpdateAnswerQuestionRevision?.totalQuestionCorrect
                            }
                            /{resUpdateAnswerQuestionRevision?.totalQuestion}
                        </p>
                        <div style={{ textAlign: 'center' }}>
                            <Button type="primary" onClick={onCancel}>
                                {t('confirm')}
                            </Button>
                        </div>
                    </Modal>
                </div>
            )}
            {showMessageError && (
                <ModalError
                    messageError={
                        messageError || updateAnswerQuestionRevisionError
                    }
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </Container>
    );
};

export default RevisionStart;
