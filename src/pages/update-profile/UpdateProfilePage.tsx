import UpdateProfileContainer from 'components/update-profile/UpdateProfileContainer';
import React from 'react';
interface Props {}

const UpdateProfilePage = (props: Props): JSX.Element => {
    return <UpdateProfileContainer />;
};

export default UpdateProfilePage;
