import { Button, Row, Space, Tabs, Popconfirm } from 'antd';
import ContentCourse from 'components/review-course/ContentCourse';
import InfoCourse from 'components/review-course/InfoCourse';
import useApproveCourse from 'hooks/course/useApproveCourse';
import useDetailCourse from 'hooks/course/useDetailCourse';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useParams } from 'react-router-dom';
import { Container } from 'reactstrap';
import { CourseStatus } from 'types/Course';
import ModalError from 'components/shares/modal/ModalError';

const { TabPane } = Tabs;

interface Params {
    courseId: string;
}

const ReviewCoursePage = (): JSX.Element => {
    const { t } = useTranslation();
    const params = useParams<Params>();
    const history = useHistory();
    const [onGetCourse, course, loading, detailCourseError] = useDetailCourse();

    useEffect(() => {
        onGetCourse({ id: params?.courseId });
    }, []);

    const [
        resApproveCourse,
        onApproveCourse,
        courseApproveError
    ] = useApproveCourse();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (courseApproveError || detailCourseError) {
            setShowMessageError(true);
        }
    }, [courseApproveError, detailCourseError]);
    useEffect(() => {
        if (resApproveCourse) {
            onGetCourse({ id: params?.courseId });
        }
    }, [resApproveCourse]);

    return (
        <Container>
            <Row justify="end">
                <Space>
                    <Button onClick={() => history.push('/')} danger>
                        {t('back')}
                    </Button>{' '}
                    {course && course?.status !== CourseStatus.STATUS_APPROVED && (
                        <Popconfirm
                            placement="topRight"
                            title={t('question_approve_course')}
                            onConfirm={() => {
                                course &&
                                    onApproveCourse({ courseId: course.id });
                            }}
                            okText={t('yes')}
                            cancelText={t('no')}
                        >
                            <Button type="primary">{t('approve')}</Button>
                        </Popconfirm>
                    )}
                </Space>
            </Row>
            <Tabs defaultActiveKey="1">
                <TabPane tab={t('info')} key="1">
                    <InfoCourse course={course} />
                </TabPane>
                <TabPane tab={t('content')} key="2">
                    <ContentCourse courseId={parseInt(params?.courseId)} />
                </TabPane>
            </Tabs>
            {showMessageError && (
                <ModalError
                    messageError={courseApproveError || detailCourseError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </Container>
    );
};

export default ReviewCoursePage;
