import CKEditor from 'components/ckeditor/ckEditor';
import {
    Button,
    Col,
    Form,
    Input,
    Row,
    Select,
    message,
    notification
} from 'antd';
import { Store } from 'antd/lib/form/interface';
import React, { MouseEvent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import usePostQuestion from './../../hooks/question/usePostQuestion';
import { DetailQuestion, QuestionTypeAnswer } from './../../types/Question';
import usePutQuestion from 'hooks/question/usePutQuestion';
import { property, map } from 'lodash';
import images from 'images';
import LabelRequired from 'components/shares/LabelRequired';
import TitleComponent from 'components/text/TitleComponent';
import constStyle from 'constants/Style';
import useAddPost from 'hooks/post/useAddPost';
import * as qs from 'query-string';
import useUpdatePost from 'hooks/post/useUpdatePost';
import useDetailPost from 'hooks/post/useDetailPost';

function ActionClassHome(props: any): JSX.Element {
    const { t } = useTranslation();
    const { question, courseId, quizId } = props;
    const query = qs.parse(window.location.search);
    const [form] = Form.useForm();
    const { onAddPost, res, loading } = useAddPost();
    const {
        onUpdatePost,
        res: resUpdate,
        loading: loadingUpdate
    } = useUpdatePost();
    const {
        onDetailPost,
        res: detailPost,
        loading: loadingDetail
    } = useDetailPost();
    useEffect(() => {
        if (query?.id) {
            onDetailPost({ id: query?.id });
        }
    }, [query?.id]);

    useEffect(() => {
        if (detailPost) {
            form.setFieldsValue({ content: detailPost?.content });
        }
    }, [detailPost]);
    /**
     * Xử lý lưu dữ liệu form
     * @param value
     */
    const onSubmit = (value: Store) => {
        form.validateFields().then((values) => {
            let data: any = {
                content: values.content,
                class_id: query?.class_id
            };

            if (query?.id) {
                data.id = query?.id;
                onUpdatePost(data);
            } else {
                onAddPost(data);
            }
        });
    };
    const history = useHistory();

    /**
     * Trở về trang danh sách
     * @param e
     */
    const handleCancel = (e: MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        history.goBack();
    };

    const handleChangeDesc = (event: any, editor: any) => {
        if (form) {
            form.setFieldsValue({
                content: editor.getData()
            });
        }
    };
    return (
        <div className="question-form bg-white">
            <Form
                form={form}
                layout="horizontal"
                onFinish={onSubmit}
                onFinishFailed={(err) => err}
                hideRequiredMark={true}
            >
                <Row justify="center" gutter={[16, 16]}>
                    <Col sm={20}>
                        <Row className="d-flex" justify="space-between">
                            <TitleComponent
                                title="Bài viết"
                                color={constStyle.COLOR_MAIN}
                            />
                            <div className="list-action">
                                <Button
                                    className="style-btn"
                                    style={{ marginRight: '10px' }}
                                    type="default"
                                    onClick={handleCancel}
                                >
                                    <img src={images.ic_down} alt="" />
                                    {t('back')}
                                </Button>
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    className="style-btn"
                                >
                                    <img src={images.ic_upload} alt="" />
                                    {query?.id ? t('update') : t('create')}
                                </Button>
                            </div>
                        </Row>
                    </Col>

                    <Col span={20}>
                        <Form.Item
                            labelAlign="left"
                            className="ckh-200"
                            label={t('content')}
                            name="content"
                            labelCol={{ md: { span: 4 }, sm: { span: 8 } }}
                            wrapperCol={{ md: { span: 20 }, sm: { span: 16 } }}
                            // rules={validateForm.description}
                        >
                            <Input hidden />
                            <CKEditor
                                onChange={handleChangeDesc}
                                data={detailPost?.content || ''}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </div>
    );
}

export default React.memo(ActionClassHome);
