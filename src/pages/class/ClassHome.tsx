import {
    CommentOutlined,
    DashOutlined,
    DislikeFilled,
    DislikeOutlined,
    InfoCircleTwoTone,
    LikeFilled,
    LikeOutlined,
    MessageTwoTone,
    SendOutlined
} from '@ant-design/icons';
import { useDebounceFn } from 'ahooks';
import {
    Avatar,
    Button,
    Card,
    Col,
    Comment,
    Divider,
    Drawer,
    Dropdown,
    Form,
    Image,
    Input,
    List,
    Menu,
    Popconfirm,
    Row,
    Tooltip,
    Upload
} from 'antd';
import moment from 'antd/node_modules/moment';
import TitleComponent from 'components/text/TitleComponent';
import constStyle from 'constants/Style';
import { Picker } from 'emoji-mart';
import 'emoji-mart/css/emoji-mart.css';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import useFetchListCourseInClass from 'hooks/class/useFetchListCourseInClass';
import useFetchListUserInClass from 'hooks/class/useFetchListUserInClass';
import useCreateComment from 'hooks/comment/useCreateComment';
import useDeleteComment from 'hooks/comment/useDeleteComment';
import useFetchListComment from 'hooks/comment/useFetchAllComment';
import useLikeComment from 'hooks/comment/useLikeComment';
import useLikePost from 'hooks/comment/useLikePost';
import useUpdateComment from 'hooks/comment/useUpdateComment';
import useDeletePost from 'hooks/post/useDeletePost';
import useFetchPostInClass from 'hooks/post/useFetchPostInClass';
import useUpload from 'hooks/useUpload';
import images from 'images';
import _ from 'lodash';
import ModalCreateNewChat from 'pages/chats/ModalCreateNewChat';
import * as qs from 'query-string';
import React, { createElement, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
function ClassHome(props: any) {
    const query = qs.parse(window.location.search);
    const { listPost, loading, onFetchPostInClass } = useFetchPostInClass();
    const [visible, setVisible] = useState(false);
    const [emoijiSelected, setEmojiSelected] = useState<any>();
    const {
        listUser,
        loading: loadingListUser,
        onFetchListUserInClass
    } = useFetchListUserInClass();
    const showDrawer = () => {
        setVisible(true);
    };

    const onClose = () => {
        setVisible(false);
    };
    const DescriptionItem = ({
        title,
        content
    }: {
        title: string;
        content: any;
    }) => (
        <div className="site-description-item-profile-wrapper">
            <p className="site-description-item-profile-p-label">{title}:</p>
            {content}
        </div>
    );
    const { onCreateComment, res, loading: loadingCreate } = useCreateComment();
    const { onUpdateComment } = useUpdateComment();
    const { onDeleteComment } = useDeleteComment();
    const { onLikePost, res: resLikePost } = useLikePost();
    const { onLikeComment } = useLikeComment();
    const [resImages, onUpload, messageError] = useUpload();
    const {
        listComment,
        loading: loadingComment,
        onFetchListComment
    } = useFetchListComment();
    const { onDeletePost, loading: loadingDelete } = useDeletePost();
    const {
        listCourseInClass,
        loading: loadingListCourse,
        onFetchListCourseInClass
    } = useFetchListCourseInClass();
    const history = useHistory();
    const [form] = Form.useForm();
    const fnFetchListPost = () => {
        onFetchPostInClass({ id: query?.classId });
    };

    useEffect(() => {
        if (query?.classId) {
            fnFetchListPost();
            onFetchListCourseInClass({ id: query?.classId });
            onFetchListUserInClass({ id: query?.classId });
        }
    }, [query?.classId]);
    useEffect(() => {
        if (resLikePost) fnFetchListPost();
    }, [resLikePost]);
    const [ImageFile, setImageFile] = useState<any[]>([]);
    const [imageUpload, setImageUpload] = useState<any>([]);
    const [visibleIcon, setVisibleIcon] = useState<boolean>(false);
    // const [messageText, setMessageText] = useState<string>('');
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    useEffect(() => {
        if (ImageFile && ImageFile?.length > 0) {
            run('imageChange');
        }
    }, [ImageFile]);
    const { run } = useDebounceFn(
        (flag: string) => {
            if (flag === 'imageChange') {
                if (ImageFile && ImageFile.length > 0) {
                    const data = new FormData();
                    let flagNewImage = false;
                    ImageFile.forEach((e: any, index: number) => {
                        if (e.originFileObj) {
                            flagNewImage = true;
                            data.append(`image`, e.originFileObj);
                        }
                    });
                    if (flagNewImage) onUpload(data);
                }
            }
        },

        {
            wait: 500
        }
    );
    useEffect(() => {
        let imageTemp: any[] = [];
        if (resImages) setImageUpload([...imageUpload, resImages?.url]);
    }, [resImages]);
    const handleChange = ({ fileList }: any) => {
        if (fileList && fileList.length > 0) {
            fileList.map((e: any, index: number) => {
                e.status = 'done';
                return e;
            });
            form.setFieldsValue({ image_multi: fileList });
            setImageFile(fileList);
        }
    };

    const [likes, setLikes] = useState(0);
    const [dislikes, setDislikes] = useState(0);
    const [action, setAction] = useState<any>(null);

    const like = () => {
        setLikes(1);
        setDislikes(0);
        setAction('liked');
    };

    const dislike = () => {
        setLikes(0);
        setDislikes(1);
        setAction('disliked');
    };
    const [commentId, setCommentId] = useState<any>();
    const [postId, setPostId] = useState<any>();
    const onSubmit = async (data: any) => {
        let res: any;
        if (!commentId) {
            res = await onCreateComment(data);
        } else {
            res = await onUpdateComment(data);
        }
        setPostId(data.post_id);
        if (res) onFetchListComment({ id: data.post_id });
    };
    const handleSubmit = (id: any) => {
        const field = `message-${id}`;
        const message = form.getFieldValue(field);

        let datas: any = {};
        if (imageUpload && imageUpload.length > 0) {
            datas = {
                type: 1,
                post_id: id,
                content: JSON.stringify(imageUpload)
            };
            onSubmit(datas);
        }
        if (message?.trim()) {
            datas = {
                type: 0,
                post_id: id,
                content: message
            };
            onSubmit(datas);
        }
        // setMessageText('');
        setVisibleIcon(false);
        form.setFieldsValue({ [`message-${id}`]: undefined });
        setImageFile([]);
        setImageUpload([]);
    };
    const onRedirectCourse = (courseId: number) => {
        if (user?.role === 0) {
            history.push({ pathname: `/student/course/${courseId}` });
        } else {
            history.push({ pathname: `/teacher/course/${courseId}#content` });
        }
    };
    const addEmoji = (e: any, fields: any) => {
        let emoji = e.native;
        const messageTemp = form.getFieldValue([fields]);

        form.setFieldsValue({
            [fields]: messageTemp ? messageTemp + emoji : emoji
        });
        // setMessageText(messageTemp ? messageTemp + emoji : emoji);
    };
    const onRemoveImage = (file: any) => {
        const images = [...ImageFile];
        const imageFile = imageUpload?.filter((e: any) => e !== file?.uid);
        setImageUpload(imageFile);
        _.remove(images, (image) => image === file);
        setImageFile(images);
    };
    const onChangeInput = (e: any, fields: any) => {
        form.setFieldsValue({ [fields]: e.target.value });
    };

    const actions = [
        <Tooltip key="comment-basic-like" title="Like">
            <span onClick={like}>
                {createElement(action === 'liked' ? LikeFilled : LikeOutlined)}
                <span className="comment-action">{likes}</span>
            </span>
        </Tooltip>
        // <Tooltip key="comment-basic-dislike" title="Dislike">
        //     <span onClick={dislike}>
        //         {React.createElement(
        //             action === 'disliked' ? DislikeFilled : DislikeOutlined
        //         )}
        //         <span className="comment-action">{dislikes}</span>
        //     </span>
        // </Tooltip>,
        // <span key="comment-basic-reply-to">Reply to</span>
    ];
    const [visibleModalCreateChat, setVisibleModalCreateChat] = useState(false);
    const [userSelectedNewChat, setUserSelectedNewChat] = useState(0);
    const toggleModalCreateChat = () => {
        setVisibleModalCreateChat(!visibleModalCreateChat);
    };
    const onRedirectChat = (item: any) => {
        if (item?.room_id) {
            history.push({
                pathname: '/chats/list',
                search: `room_id=${item.room_id}`
            });
        } else {
            setUserSelectedNewChat(item?.id);
            toggleModalCreateChat();
        }
    };
    return (
        <Row justify="center">
            <ModalCreateNewChat
                defaultUser={userSelectedNewChat}
                visible={visibleModalCreateChat}
                toggleModal={toggleModalCreateChat}
            />
            <Col md={12} className="mx-4">
                {listPost?.map((item: any) => {
                    let listCommentInPost = item?.last_10_comment || [];
                    if (listComment?.list_comment?.length > 0) {
                        if (listComment?.list_comment[0].post_id === postId)
                            listCommentInPost = listComment?.list_comment;
                    }
                    return (
                        <Card
                            key={item?.id}
                            className="mb-5"
                            title={
                                <div
                                    style={{
                                        display: 'flex',
                                        gap: '8px'
                                    }}
                                >
                                    <img
                                        src={
                                            item?.created_by?.avatar ||
                                            images.ic_avatar
                                        }
                                        style={{
                                            width: '2.5em',
                                            height: '2.5em'
                                        }}
                                    />
                                    <div>
                                        <strong>
                                            {item?.created_by?.name}
                                        </strong>
                                        <div
                                            style={{
                                                color: 'rgba(0, 0, 0, 0.45)',
                                                fontSize: '13px'
                                            }}
                                        >
                                            {moment(item?.created_at).fromNow()}
                                        </div>
                                    </div>
                                </div>
                            }
                            extra={
                                item?.created_by?.id === user?.id ? (
                                    <Dropdown
                                        overlay={
                                            <Menu>
                                                <Menu.Item
                                                    onClick={() =>
                                                        history.push({
                                                            pathname:
                                                                '/post-detail',
                                                            search: `class_id=${query?.classId}&id=${item.id}`
                                                        })
                                                    }
                                                >
                                                    <span>Cập nhật</span>
                                                </Menu.Item>
                                                <Menu.Item>
                                                    <Popconfirm
                                                        title="Bạn có chắc chắn muốn xóa bài viết này ？"
                                                        okText="Đồng ý"
                                                        cancelText="Hủy"
                                                        onConfirm={async () => {
                                                            const res = await onDeletePost(
                                                                {
                                                                    id: item.id
                                                                }
                                                            );
                                                            if (res)
                                                                fnFetchListPost();
                                                        }}
                                                    >
                                                        <span>Xóa</span>
                                                    </Popconfirm>
                                                </Menu.Item>
                                            </Menu>
                                        }
                                        placement="bottomCenter"
                                    >
                                        <DashOutlined />
                                    </Dropdown>
                                ) : (
                                    ''
                                )
                            }
                        >
                            <Row>
                                <Col
                                    md={24}
                                    dangerouslySetInnerHTML={{
                                        __html:
                                            item?.content.replace(
                                                /<img/g,
                                                '<img width="100%"'
                                            ) || ''
                                    }}
                                    style={{
                                        fontSize: '16px',
                                        maxWidth: '100%',
                                        overflow: 'hidden'
                                    }}
                                ></Col>
                                <Col md={24}>
                                    <Form
                                        className="input"
                                        form={form}
                                        onFinish={() => handleSubmit(item.id)}
                                    >
                                        <Row
                                            justify="center"
                                            className="bg-white"
                                            style={{ borderRadius: '.5em' }}
                                        >
                                            <Col md={24}>
                                                {ImageFile &&
                                                    ImageFile?.length > 0 && (
                                                        <Upload
                                                            listType="picture-card"
                                                            // onPreview={handlePreview}
                                                            onRemove={
                                                                onRemoveImage
                                                            }
                                                            fileList={ImageFile}
                                                        ></Upload>
                                                    )}
                                            </Col>
                                            <Col md={24}>
                                                <Row gutter={[20, 0]}>
                                                    <Col md={2}>
                                                        {item?.liked ? (
                                                            <LikeFilled
                                                                onClick={async () => {
                                                                    const res = await onLikePost(
                                                                        {
                                                                            post_id:
                                                                                item?.id
                                                                        }
                                                                    );
                                                                    fnFetchListPost();
                                                                }}
                                                                style={{
                                                                    cursor:
                                                                        'pointer',
                                                                    fontSize:
                                                                        '1.3em'
                                                                }}
                                                            />
                                                        ) : (
                                                            <LikeOutlined
                                                                onClick={() => {
                                                                    onLikePost({
                                                                        post_id:
                                                                            item?.id
                                                                    });
                                                                }}
                                                                style={{
                                                                    cursor:
                                                                        'pointer',
                                                                    fontSize:
                                                                        '1.3em'
                                                                }}
                                                            />
                                                        )}
                                                        <span
                                                            style={{
                                                                fontSize:
                                                                    ' 1em',
                                                                textAlign:
                                                                    'center',
                                                                verticalAlign:
                                                                    '-.3em',
                                                                paddingLeft:
                                                                    ' .3em',
                                                                fontWeight:
                                                                    'bold'
                                                            }}
                                                        >
                                                            {item?.total_like}
                                                        </span>
                                                    </Col>
                                                    <Col md={2}>
                                                        <CommentOutlined
                                                            style={{
                                                                fontSize:
                                                                    '1.3em'
                                                            }}
                                                        />
                                                        <span
                                                            style={{
                                                                fontSize:
                                                                    ' 1em',
                                                                textAlign:
                                                                    'center',
                                                                verticalAlign:
                                                                    '-.3em',
                                                                paddingLeft:
                                                                    ' .3em',
                                                                fontWeight:
                                                                    'bold'
                                                            }}
                                                        >
                                                            {
                                                                item?.total_comment
                                                            }{' '}
                                                        </span>
                                                    </Col>
                                                </Row>
                                                <Divider dashed />
                                            </Col>

                                            <Col md={24}>
                                                {visibleIcon &&
                                                emoijiSelected === item.id ? (
                                                    <div className="picker-style">
                                                        <Picker
                                                            onSelect={(
                                                                e: any
                                                            ) =>
                                                                addEmoji(
                                                                    e,
                                                                    `message-${item?.id}`
                                                                )
                                                            }
                                                        />
                                                    </div>
                                                ) : (
                                                    ''
                                                )}
                                                <Form.Item
                                                    name={`message-${item?.id}`}
                                                >
                                                    <Input
                                                        onChange={(e: any) =>
                                                            onChangeInput(
                                                                e,
                                                                `message-${item?.id}`
                                                            )
                                                        }
                                                        style={{
                                                            width: '100%'
                                                        }}
                                                        prefix={
                                                            <div>
                                                                <Upload
                                                                    multiple
                                                                    accept="image/*"
                                                                    fileList={
                                                                        ImageFile
                                                                    }
                                                                    showUploadList={
                                                                        false
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                >
                                                                    <div>
                                                                        <img
                                                                            style={{
                                                                                maxHeight:
                                                                                    '2em'
                                                                            }}
                                                                            alt=""
                                                                            src={
                                                                                images.ic_img
                                                                            }
                                                                        />
                                                                    </div>
                                                                </Upload>
                                                                <img
                                                                    alt=""
                                                                    src={
                                                                        images.ic_smile
                                                                    }
                                                                    style={{
                                                                        maxHeight:
                                                                            '1.5em'
                                                                    }}
                                                                    // className="img-smile"
                                                                    onClick={() => {
                                                                        if (
                                                                            !visibleIcon
                                                                        )
                                                                            setEmojiSelected(
                                                                                item.id
                                                                            );
                                                                        setVisibleIcon(
                                                                            !visibleIcon
                                                                        );
                                                                    }}
                                                                />
                                                            </div>
                                                        }
                                                        addonAfter={
                                                            <SendOutlined
                                                                onClick={() =>
                                                                    handleSubmit(
                                                                        item.id
                                                                    )
                                                                }
                                                                style={{
                                                                    paddingBottom:
                                                                        '0.5em',
                                                                    color:
                                                                        'blue',
                                                                    cursor:
                                                                        'pointer'
                                                                }}
                                                            />
                                                        }
                                                    />
                                                </Form.Item>
                                            </Col>
                                            {listCommentInPost?.map(
                                                (comment: any) => {
                                                    const urlImages: any = [];
                                                    if (comment?.type === 1) {
                                                        const arr =
                                                            JSON.parse(
                                                                comment?.content
                                                            ) || [];
                                                        arr?.forEach(
                                                            (media: any) => {
                                                                urlImages.push(
                                                                    media
                                                                );
                                                            }
                                                        );
                                                    }
                                                    return (
                                                        <Col md={24}>
                                                            <Comment
                                                                actions={[
                                                                    <div>
                                                                        {comment?.liked ? (
                                                                            <LikeFilled
                                                                                onClick={async () => {
                                                                                    const res = await onLikeComment(
                                                                                        {
                                                                                            comment_id:
                                                                                                comment?.id
                                                                                        }
                                                                                    );
                                                                                    if (
                                                                                        res
                                                                                    )
                                                                                        onFetchListComment(
                                                                                            {
                                                                                                id:
                                                                                                    item.id
                                                                                            }
                                                                                        );
                                                                                }}
                                                                                style={{
                                                                                    cursor:
                                                                                        'pointer',
                                                                                    fontSize:
                                                                                        '1em'
                                                                                }}
                                                                            />
                                                                        ) : (
                                                                            <LikeOutlined
                                                                                onClick={async () => {
                                                                                    const res = await onLikeComment(
                                                                                        {
                                                                                            comment_id:
                                                                                                comment?.id
                                                                                        }
                                                                                    );
                                                                                    if (
                                                                                        res
                                                                                    )
                                                                                        onFetchListComment(
                                                                                            {
                                                                                                id:
                                                                                                    item.id
                                                                                            }
                                                                                        );
                                                                                }}
                                                                                style={{
                                                                                    cursor:
                                                                                        'pointer',
                                                                                    fontSize:
                                                                                        '1em'
                                                                                }}
                                                                            />
                                                                        )}
                                                                        <span
                                                                            style={{
                                                                                fontSize:
                                                                                    ' 1em',
                                                                                textAlign:
                                                                                    'center',
                                                                                verticalAlign:
                                                                                    '-.3em',
                                                                                paddingLeft:
                                                                                    ' .3em',
                                                                                fontWeight:
                                                                                    'bold'
                                                                            }}
                                                                        >
                                                                            {
                                                                                comment?.total_like
                                                                            }
                                                                        </span>
                                                                    </div>
                                                                ]}
                                                                author={
                                                                    <a>
                                                                        {
                                                                            comment
                                                                                ?.created_by
                                                                                ?.name
                                                                        }
                                                                    </a>
                                                                }
                                                                avatar={
                                                                    <Avatar
                                                                        src={
                                                                            comment
                                                                                ?.created_by
                                                                                ?.avatar
                                                                                ? comment
                                                                                      ?.created_by
                                                                                      ?.avatar
                                                                                : images.ic_avatar
                                                                        }
                                                                        alt="Han Solo"
                                                                    />
                                                                }
                                                                content={
                                                                    <p>
                                                                        {comment?.type ===
                                                                        1
                                                                            ? urlImages?.map(
                                                                                  (
                                                                                      url: any
                                                                                  ) => {
                                                                                      return (
                                                                                          <Image
                                                                                              style={{
                                                                                                  maxWidth:
                                                                                                      '10em'
                                                                                              }}
                                                                                              src={
                                                                                                  url
                                                                                              }
                                                                                              alt=""
                                                                                          />
                                                                                      );
                                                                                  }
                                                                              )
                                                                            : comment?.content}
                                                                    </p>
                                                                }
                                                                datetime={
                                                                    <Tooltip
                                                                        title={moment().format(
                                                                            'HH:mm:ss DD/MM/YYYY'
                                                                        )}
                                                                    >
                                                                        <span>
                                                                            {moment(
                                                                                comment?.created_at
                                                                            ).fromNow()}
                                                                        </span>
                                                                    </Tooltip>
                                                                }
                                                            />
                                                        </Col>
                                                    );
                                                }
                                            )}
                                        </Row>
                                    </Form>
                                </Col>
                            </Row>
                        </Card>
                    );
                })}
            </Col>
            <Col md={5}>
                <Row>
                    <Col md={24}>
                        <Button
                            onClick={() =>
                                history.push({
                                    pathname: '/post-detail',
                                    search: `class_id=${query?.classId}`
                                })
                            }
                        >
                            Thêm bài viết
                        </Button>
                    </Col>
                    <Col md={24}>
                        <TitleComponent
                            className="ml-1 mb-2 mt-4"
                            color={constStyle.COLOR_MAIN}
                            title="Thành viên"
                        />
                        <List
                            className="bg-white mb-4"
                            dataSource={listUser || []}
                            bordered
                            renderItem={(item: any) => (
                                <List.Item
                                    key={item?.id}
                                    actions={[
                                        <a
                                            onClick={showDrawer}
                                            key={`a-${item?.id}`}
                                        >
                                            <InfoCircleTwoTone />
                                        </a>,
                                        <a
                                            onClick={() => onRedirectChat(item)}
                                            key={`a-${item?.id}`}
                                        >
                                            <MessageTwoTone />
                                        </a>
                                    ]}
                                >
                                    <List.Item.Meta
                                        avatar={
                                            <Avatar
                                                src={
                                                    item?.avatar ||
                                                    images.ic_avatar
                                                }
                                            />
                                        }
                                        title={<a>{item.name}</a>}
                                        description={item.email}
                                    />
                                </List.Item>
                            )}
                        />
                        <TitleComponent
                            className="ml-1 my-2"
                            color={constStyle.COLOR_MAIN}
                            title="Khóa học"
                        />
                        <List
                            className="bg-white"
                            dataSource={listCourseInClass || []}
                            bordered
                            renderItem={(item: any) => (
                                <List.Item
                                    key={item?.id}
                                    actions={[
                                        <a
                                            onClick={() =>
                                                onRedirectCourse(item?.id)
                                            }
                                            key={`a-${item?.id}`}
                                        >
                                            <InfoCircleTwoTone />
                                        </a>
                                    ]}
                                >
                                    <List.Item.Meta
                                        avatar={
                                            <Avatar
                                                src={
                                                    item?.image ||
                                                    images.ic_book
                                                }
                                            />
                                        }
                                        title={
                                            <a
                                                onClick={() =>
                                                    onRedirectCourse(item?.id)
                                                }
                                            >
                                                {item.name}
                                            </a>
                                        }
                                        description={`Giáo viên: ${item.created_by.name}`}
                                    />
                                </List.Item>
                            )}
                        />
                        <Drawer
                            width={640}
                            placement="right"
                            closable={false}
                            onClose={onClose}
                            visible={visible}
                        >
                            <p
                                className="site-description-item-profile-p"
                                style={{ marginBottom: 24 }}
                            >
                                User Profile
                            </p>
                            <p className="site-description-item-profile-p">
                                Personal
                            </p>
                            <Row>
                                <Col span={12}>
                                    <DescriptionItem
                                        title="Full Name"
                                        content="Lily"
                                    />
                                </Col>
                                <Col span={12}>
                                    <DescriptionItem
                                        title="Account"
                                        content="AntDesign@example.com"
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col span={12}>
                                    <DescriptionItem
                                        title="City"
                                        content="HangZhou"
                                    />
                                </Col>
                                <Col span={12}>
                                    <DescriptionItem
                                        title="Country"
                                        content="China🇨🇳"
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col span={12}>
                                    <DescriptionItem
                                        title="Birthday"
                                        content="February 2,1900"
                                    />
                                </Col>
                                <Col span={12}>
                                    <DescriptionItem
                                        title="Website"
                                        content="-"
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col span={24}>
                                    <DescriptionItem
                                        title="Message"
                                        content="Make things as simple as possible but no simpler."
                                    />
                                </Col>
                            </Row>
                            <Divider />
                            <p className="site-description-item-profile-p">
                                Company
                            </p>
                            <Row>
                                <Col span={12}>
                                    <DescriptionItem
                                        title="Position"
                                        content="Programmer"
                                    />
                                </Col>
                                <Col span={12}>
                                    <DescriptionItem
                                        title="Responsibilities"
                                        content="Coding"
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col span={12}>
                                    <DescriptionItem
                                        title="Department"
                                        content="XTech"
                                    />
                                </Col>
                                <Col span={12}>
                                    <DescriptionItem
                                        title="Supervisor"
                                        content={<a>Lin</a>}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col span={24}>
                                    <DescriptionItem
                                        title="Skills"
                                        content="C / C + +, data structures, software engineering, operating systems, computer networks, databases, compiler theory, computer architecture, Microcomputer Principle and Interface Technology, Computer English, Java, ASP, etc."
                                    />
                                </Col>
                            </Row>
                            <Divider />
                            <p className="site-description-item-profile-p">
                                Contacts
                            </p>
                            <Row>
                                <Col span={12}>
                                    <DescriptionItem
                                        title="Email"
                                        content="AntDesign@example.com"
                                    />
                                </Col>
                                <Col span={12}>
                                    <DescriptionItem
                                        title="Phone Number"
                                        content="+86 181 0000 0000"
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col span={24}>
                                    <DescriptionItem
                                        title="Github"
                                        content={
                                            <a href="http://github.com/ant-design/ant-design/">
                                                github.com/ant-design/ant-design/
                                            </a>
                                        }
                                    />
                                </Col>
                            </Row>
                        </Drawer>
                    </Col>
                    <Col md={24}></Col>
                </Row>
            </Col>
        </Row>
    );
}

export default ClassHome;
