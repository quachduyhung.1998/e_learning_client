import { DoubleRightOutlined } from '@ant-design/icons';
import { Col, Divider, Row, Card } from 'antd';
import ListCourse from 'components/list-course/ListCourse';
import TitleComponent from 'components/text/TitleComponent';
import constPaginate from 'constants/Paginate';
import constStyle from 'constants/Style';
import useGetCourseInJoin from 'hooks/course/useGetCourseInJoin';
import useGetCourseRecommendation from 'hooks/course/useGetCourseRecommendation';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Container } from 'reactstrap';
import './MyCoursePage.scss';
import { useHistory } from 'react-router-dom';
import ItemCourse from 'components/list-course/ItemCourse';
import useFetchListClass from 'hooks/class/useFetchListClass';

function MyCoursePage(props: any): JSX.Element {
    const { t } = useTranslation();
    const { Meta } = Card;
    const history = useHistory();
    const [typeListCourse, setTypeListCourse] = useState<string | null>(null);
    const { listClass, loading, onFetchListClass } = useFetchListClass();
    const { resGetCourseInJoin, onGetCourseInJoin } = useGetCourseInJoin();
    const {
        resGetCourseRecommendation,
        onGetCourseRecommendation
    } = useGetCourseRecommendation();

    useEffect(() => {
        onGetCourseInJoin({
            page: constPaginate.PAGE_DEFAULT,
            limit: 8
        });
        onFetchListClass({});
    }, []);

    useEffect(() => {
        onGetCourseRecommendation({
            page: constPaginate.PAGE_DEFAULT,
            limit: 8
        });
    }, []);
    return (
        <div id="student-home">
            <Container>
                <div style={{ padding: '40px 50px', background: 'white' }}>
                    {typeListCourse && (
                        <ListCourse
                            typeList={typeListCourse}
                            onBack={() => setTypeListCourse(null)}
                        />
                    )}
                    {!typeListCourse && (
                        <>
                            <Row>
                                <Col span={24}>
                                    <Row>
                                        <Col
                                            md={{ span: 12 }}
                                            xs={{ span: 16 }}
                                            sm={{ span: 24 }}
                                        >
                                            <TitleComponent
                                                color={constStyle.COLOR_MAIN}
                                                title={t('my_course')}
                                            />
                                        </Col>
                                    </Row>
                                    <div className="position-relative box-slider">
                                        <Row>
                                            {resGetCourseInJoin?.data &&
                                                resGetCourseInJoin?.data?.map(
                                                    (course: any) => {
                                                        return (
                                                            <Col
                                                                md={6}
                                                                key={course.id}
                                                            >
                                                                <ItemCourse
                                                                    course={
                                                                        course
                                                                    }
                                                                />
                                                            </Col>
                                                        );
                                                    }
                                                )}
                                        </Row>
                                    </div>
                                </Col>
                            </Row>
                            <Divider />
                            <Row className="mb-3">
                                <Col
                                    md={{ span: 12 }}
                                    xs={{ span: 16 }}
                                    sm={{ span: 24 }}
                                >
                                    <TitleComponent
                                        color={constStyle.COLOR_MAIN}
                                        title="Lớp học của tôi"
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col span={24}>
                                    <div
                                        className="position-relative box-slider"
                                        style={{ cursor: 'pointer' }}
                                    >
                                        <Row gutter={[32, 32]}>
                                            {listClass &&
                                                listClass?.length > 0 &&
                                                listClass?.map((item) => {
                                                    return (
                                                        <Col
                                                            sm={6}
                                                            key={item.id}
                                                        >
                                                            <Card
                                                                onClick={() =>
                                                                    history.push(
                                                                        {
                                                                            pathname:
                                                                                '/post',
                                                                            search: `classId=${item.id}`
                                                                        }
                                                                    )
                                                                }
                                                            >
                                                                <Meta
                                                                    // avatar={
                                                                    //     <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                                                    // }
                                                                    title={
                                                                        item?.name
                                                                    }
                                                                    description={`Giáo viên: 
                                                                    ${item?.teacher?.name}
                                                                `}
                                                                />
                                                            </Card>
                                                        </Col>
                                                    );
                                                })}
                                        </Row>
                                    </div>
                                </Col>
                            </Row>
                        </>
                    )}
                </div>
            </Container>
        </div>
    );
}

export default MyCoursePage;
