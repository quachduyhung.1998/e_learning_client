import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useParams } from 'react-router-dom';
import { Container } from 'reactstrap';
import QuestionForm from './../../components/lecturers-question/QuestionForm';
import useDetailQuestion from './../../hooks/question/useDetailQuestion';
import { Answer } from './../../types/Answer';
import { DetailQuestion } from './../../types/Question';
import * as qs from 'query-string';
interface FormData {
    name: string;
    categoryId: number;
    content: string;
    type: number;
}

interface Params {
    courseId: string;
    quizId: string;
    id: string;
}

function DetailQuestionPage(): JSX.Element {
    const params: Params = useParams();
    const query = qs.parse(window.location.search);
    const [question, setQuesion] = useState<DetailQuestion>();

    const [onDetailQuestion, resQuestion] = useDetailQuestion();

    useEffect(() => {
        if (query?.id) onDetailQuestion({ id: query.id.toString() });
    }, [query?.id]);

    useEffect(() => {
        if (resQuestion) {
            setQuesion(resQuestion);
        }
    }, [resQuestion]);

    return (
        <Container>
            {question ? (
                <QuestionForm
                    courseId={params.courseId}
                    quizId={params.quizId}
                    question={question}
                />
            ) : undefined}
        </Container>
    );
}

export default React.memo(DetailQuestionPage);
