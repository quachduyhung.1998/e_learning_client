import { Pagination } from 'antd';
import ListQuestion from 'components/lecturers-question/ListQuestion';
import QuestionHeader from 'components/lecturers-question/QuestionHeader';
import constPaginate from 'constants/Paginate';
import useDetailCourse from 'hooks/course/useDetailCourse';
import useGetQuestionsOfQuiz from 'hooks/question/useGetQuestionsOfQuiz';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import { Container } from 'reactstrap';
import { CourseStatus } from 'types/Course';
import ModalError from 'components/shares/modal/ModalError';

interface Params {
    courseId: string;
    quizId: string;
}

function ExamQuestionPage(): JSX.Element {
    const { t } = useTranslation();
    const params: Params = useParams();
    const [page, setPage] = useState<number>(constPaginate.PAGE_DEFAULT);
    const [limit, setLimit] = useState<number>(constPaginate.LIMIT_DEFAULT);
    const {
        resGetQuestionsNewPage,
        resTotalQuestion,
        onGetQuestionsOfQuiz
    } = useGetQuestionsOfQuiz();
    const [
        onFetchInfoCourse,
        resCourseInfo,
        loading,
        detailCourseError
    ] = useDetailCourse();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (detailCourseError) {
            setShowMessageError(true);
        }
    }, [detailCourseError]);
    const handleNextPage = (selectedItem: { selected: number }): void => {
        setPage(selectedItem.selected + 1);
    };
    const onPageChange = (page: number) => {
        setPage(page);
    };

    useEffect(() => {
        onFetchInfoCourse({ id: params.courseId });
    }, []);

    useEffect(() => {
        onGetQuestionsOfQuiz({ quizId: parseInt(params?.quizId), page, limit });
    }, [page]);

    const handleReloadPage = () => {
        onGetQuestionsOfQuiz({
            quizId: Number(params?.quizId),
            page,
            limit
        });
    };
    return (
        <Container>
            {resCourseInfo?.status !== CourseStatus.STATUS_APPROVED && (
                <QuestionHeader />
            )}
            {resGetQuestionsNewPage ? (
                <ListQuestion
                    questions={resGetQuestionsNewPage}
                    onReloadQuestion={handleReloadPage}
                    actions={
                        resCourseInfo?.status !== CourseStatus.STATUS_APPROVED
                    }
                />
            ) : (
                <div className="">{t('no_content')}</div>
            )}
            <div className="d-flex justify-content-end">
                {resTotalQuestion > limit && (
                    <Pagination
                        defaultCurrent={page | 0}
                        total={resTotalQuestion}
                        defaultPageSize={limit | 10}
                        onChange={onPageChange}
                    ></Pagination>
                )}
            </div>
            {showMessageError && (
                <ModalError
                    messageError={detailCourseError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </Container>
    );
}

export default ExamQuestionPage;
