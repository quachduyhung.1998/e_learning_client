import React from 'react';
import { useParams } from 'react-router-dom';
import { Container } from 'reactstrap';
import QuestionForm from './../../components/lecturers-question/QuestionForm';
interface FormData {
    name: string;
    categoryId: number;
    content: string;
    type: number;
}

interface Params {
    courseId: string;
    quizId: string;
}

function CreateQuestionPage(): JSX.Element {
    return (
        <Container>
            <QuestionForm />
        </Container>
    );
}

export default React.memo(CreateQuestionPage);
