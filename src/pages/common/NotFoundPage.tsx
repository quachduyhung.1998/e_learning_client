import React from 'react';
import ElementCenter from 'layout/common/ElementCenter';
import { Button } from 'antd';
import { useTranslation } from 'react-i18next';
import Title from 'antd/lib/typography/Title';
import { Link } from 'react-router-dom';
import { HomeOutlined } from '@ant-design/icons';

const NotFoundPage = (): JSX.Element => {
    const { t } = useTranslation();
    return (
        <ElementCenter>
            <div style={{ textAlign: 'center' }}>
                <Title>404 | Not found</Title>
                <Button type="primary">
                    <Link to="/">
                        {<HomeOutlined />} {t('back_home')}
                    </Link>
                </Button>
            </div>
        </ElementCenter>
    );
};

export default NotFoundPage;
