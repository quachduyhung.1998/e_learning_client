import { Button, Col, Row } from 'antd';
import useDetailCourse from 'hooks/course/useDetailCourse';
import images from 'images';
import React, { useEffect, useState, MouseEvent } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams, useHistory } from 'react-router-dom';
import { Container } from 'reactstrap';
import StudentContentCourse from './StudentContentCourse';
import './StudentCourseContentPage.scss';
import ModalError from 'components/shares/modal/ModalError';
interface Params {
    courseId: string;
    // topic;
}
function CourseContent(): JSX.Element {
    const { t } = useTranslation();
    const params: Params = useParams();
    const history = useHistory();
    const courseId = params.courseId;
    const [showHeaderInformation, setShowHeaderInformation] = useState(true);
    const [viewContentModule, setViewContentModule] = useState<{
        instanceId: number;
        type: string;
    }>();
    const [
        onFetchInfoCourse,
        resCourseInfo,
        loading,
        detailCourseError
    ] = useDetailCourse();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (detailCourseError) {
            setShowMessageError(true);
        }
    }, [detailCourseError]);
    useEffect(() => {
        onFetchInfoCourse({ id: courseId });
    }, []);
    const renderHeaderInformation = (statusScreen: boolean) => {
        setShowHeaderInformation(!statusScreen);
    };

    /**
     * Trở về trang danh sách
     * @param e
     */
    const handleCancel = (e: MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        history.goBack();
    };

    return (
        <div>
            <Container className="container-course-content">
                {showHeaderInformation && (
                    <Row className="header-information">
                        <Col sm={18}>
                            <h3>{resCourseInfo?.fullName}</h3>
                        </Col>
                        <Col sm={6} style={{ textAlign: 'right' }}>
                            <Button
                                className="btn-style"
                                onClick={handleCancel}
                            >
                                <img src={images.ic_down} alt="" />
                                {t('back')}
                            </Button>
                        </Col>
                    </Row>
                )}
                <Row className="container-information ">
                    <Col sm={24}>
                        <StudentContentCourse
                            courseId={parseInt(courseId)}
                            activeModule={viewContentModule}
                            onHandleFullScreen={renderHeaderInformation}
                        />
                    </Col>
                </Row>
            </Container>
            {showMessageError && (
                <ModalError
                    messageError={detailCourseError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
}

export default CourseContent;
