import InfoExam from 'components/shares/InfoExam';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Quiz from 'types/Quiz';
import ListHistory from 'components/quiz-result/ListHistory';
import useHistoryStartedQuiz from 'hooks/quiz/useHistoryStartedQuiz';
import LocalStorageHelper from 'helper/LocalStorageHelper';
import { Divider } from 'antd';
import moment from 'antd/node_modules/moment';

interface Props {
    quiz: Quiz | null;
}
const StudentContentQuiz = (props: Props) => {
    const { t } = useTranslation();
    const { quiz } = props;
    const {
        resGetHistoryStartedQuiz,
        onGetHistoryStartedQuiz
    } = useHistoryStartedQuiz();
    useEffect(() => {
        const user = LocalStorageHelper.getUserStorage();
        setTimeout(() => {
            if (props.quiz && user) {
                onGetHistoryStartedQuiz({
                    quizId: props?.quiz.id,
                    userId: Number(user.moodleUserId)
                });
            }
        }, 500);
    }, [props.quiz]);
    if (quiz) {
        return (
            <>
                <InfoExam
                    time_start={
                        quiz?.time_start ? moment(quiz?.time_start) : undefined
                    }
                    time_end={
                        quiz?.time_end ? moment(quiz?.time_end) : undefined
                    }
                    name={quiz.name}
                    typeQuestion={t('multiple_choice')}
                    totalQuestion={quiz?.total_question || 0}
                    timeLimit={quiz?.time_work || 0}
                    grade={quiz.grade}
                    gradePass={quiz.gradePass}
                />
                <Divider dashed />
                <ListHistory
                    labelNoData={<></>}
                    history={resGetHistoryStartedQuiz}
                />
            </>
        );
    }
    return <div></div>;
};

export default StudentContentQuiz;
