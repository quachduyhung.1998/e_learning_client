import { Button, Col, message, Row } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import ContentPageCourse from 'components/review-course/ContentPageCourse';
import Exam from 'components/shares/Exam';
import ModalError from 'components/shares/modal/ModalError';
import LocalStorageHelper from 'helper/LocalStorageHelper';
import useUpdateInteractiveCourse from 'hooks/course/useUpdateInteractiveCourse';
import useGetQuiz from 'hooks/quiz/useGetQuiz';
import useHistoryStartedQuiz from 'hooks/quiz/useHistoryStartedQuiz';
import useUpdateAnswerStartedQuiz from 'hooks/quiz/useUpdateAnswerStartedQuiz';
import useQuery from 'hooks/route/useQuery';
import useViewedPageCourse from 'hooks/student/useViewedPageCourse';
import useViewedQuizCourse from 'hooks/student/useViewedQuizCourse';
import useCompleteLesson from 'hooks/test/useCompleteLesson';
import useDetailQuestionInTest from 'hooks/test/useDetailQuestionInTest';
import useSubmitTest from 'hooks/test/useSubmitTest';
import useFetchTopic from 'hooks/useFetchTopic';
import images from 'images';
import moment from 'moment';
import queryString from 'query-string';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import Module from 'types/Module';
import PageCourse from 'types/PageCourse';
import StudentSidebar from '../StudentSidebar';
import StudentContentQuiz from './StudentContentQuiz';

interface ActiveModule {
    instanceId: number;
    type: string;
}
interface Props {
    pageCourse?: PageCourse | null;
    courseId: number;
    activeModule?: ActiveModule;
    onHandleFullScreen?: (statusScreen: boolean) => void;
}

function StudentContentCourse(props: Props) {
    const userSession = JSON.parse(window.localStorage.USER_SESSION);
    const userId = ['1'];
    const { courseId, onHandleFullScreen } = props;
    const { t } = useTranslation();
    const history = useHistory();
    const [disableBtn, setDisableBtn] = useState(false);
    const query = useQuery();
    const [checkedFirst, setCheckedFirst] = useState(false);
    const [showSidebar, setShowSidebar] = useState(true);
    const [topics, setTopics, onFetchTopics] = useFetchTopic(
        courseId.toString()
    );

    // const [onDetailTest, resTest, loading] = useDetailTest();
    const [
        onDetailQuestionInTest,
        resTest,
        loadingAll
    ] = useDetailQuestionInTest();
    const [contentQuiz, onGetQuiz] = useGetQuiz();
    const [columnContent, setColumContent] = useState(18);
    const [arrowIcon, setArrowIcon] = useState(false);
    const [fullScreenIcon, setFullScreenIcon] = useState(true);
    const [stateTemp, setStateTemp] = useState('');
    const [activeModule, setActiveModule] = useState<ActiveModule>();
    const [contentPage, setContentPage] = useState<any>();
    const { onSubmitTest, res, loading, messageError } = useSubmitTest();
    const [
        onCompleteLesson,
        resComplete,
        loadingComplete,
        messageErrorComplete
    ] = useCompleteLesson();
    const { resOnViewedPageCourse, onViewedPageCourse } = useViewedPageCourse();
    const { resOnViewedQuizCourse, onViewedQuizCourse } = useViewedQuizCourse();
    const {
        resUpdateAnswer,
        onUpdateAnswerStartedQuiz,
        updateAnswerStartedQuizError
    } = useUpdateAnswerStartedQuiz();
    const [showResultExam, setShowResultExam] = useState<boolean>(false);

    const {
        resGetHistoryStartedQuiz,
        onGetHistoryStartedQuiz
    } = useHistoryStartedQuiz();
    const [hiddenBtnStart, setHiddenBtnStart] = useState(true);
    const [hiddenBtnIgnore, setHiddenBtnIgnore] = useState(true);
    const user = LocalStorageHelper.getUserStorage();
    const flagDone = useRef(false);
    const {
        resUpdateInteractive,
        onUpdateInteractiveCourse,
        updateInteractiveCourseError
    } = useUpdateInteractiveCourse();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (
            // getQuestionStartedQuizError ||
            updateAnswerStartedQuizError ||
            updateInteractiveCourseError
        ) {
            setShowMessageError(true);
        }
    }, [
        // getQuestionStartedQuizError,
        updateAnswerStartedQuizError,
        updateInteractiveCourseError
    ]);
    useEffect(() => {
        if (query.get('instanceId')) {
            setActiveModule({
                instanceId: Number(query.get('instanceId')),
                type: query.get('type') || ''
            });
        }
        if (props?.activeModule) {
            setActiveModule(props.activeModule);
        }
    }, [props?.activeModule]);

    // useEffect(() => {
    //     if (is_done_quiz) {
    //         renderContent();
    //     }
    // }, [is_done_quiz]);
    useEffect(() => {
        if (contentPage) {
            if (contentPage?.time_start && contentPage?.time_end) {
                setDisableBtn(
                    !moment().isBetween(
                        contentPage?.time_start,
                        contentPage?.time_end
                    )
                );
            } else if (contentPage?.time_start && !contentPage?.time_end) {
                setDisableBtn(!moment().isAfter(contentPage?.time_start));
            } else if (contentPage?.time_start && contentPage?.time_end) {
                setDisableBtn(!moment().isBefore(contentPage?.time_end));
            } else {
                setDisableBtn(false);
            }
        }
    }, [contentPage]);
    useEffect(() => {
        if (!activeModule && topics?.lessons?.length) {
            // setCheckedFirst(true);
            const topicsTemp = [...topics.lessons];
            let modules: Module[] = [];

            const moduleFirstNotCompleted = topicsTemp.find(
                (x) => x.completed === false
            );
            if (moduleFirstNotCompleted) {
                setContentPage(moduleFirstNotCompleted);
                setActiveModule({
                    instanceId: moduleFirstNotCompleted.id,
                    type: moduleFirstNotCompleted.name
                });
            } else {
                setContentPage(
                    topics?.lessons?.length > 0 ? topicsTemp[0] : {}
                );
                setActiveModule({
                    instanceId:
                        topics?.lessons?.length > 0
                            ? Number(topicsTemp[0].id)
                            : 0,
                    type: topics?.length > 0 ? topicsTemp[0].name : ''
                });
            }
        }
    }, [topics]);

    const onChangeContentModule = async (
        instanceId: number,
        type: string,
        force: boolean = false
    ): Promise<void> => {
        setActiveModule({ instanceId, type });
    };

    useEffect(() => {
        history.replace({
            search: `?${queryString.stringify(activeModule || {})}`
        });
        if (
            activeModule &&
            topics?.lessons?.length &&
            activeModule.instanceId
        ) {
            const lesson =
                topics?.lessons?.length > 0
                    ? topics?.lessons.find(
                          (item: any) => item.id === activeModule.instanceId
                      )
                    : {};
            setContentPage(
                activeModule.type !== 'quiz' ? lesson : lesson?.test
            );
            const topicsTemp = [...topics.lessons];
            let modules: Module[] = [];
            topicsTemp.map(
                (topic) => (modules = [...modules, ...(topic.modules || [])])
            );

            const lastItemTopicTemp =
                topicsTemp.length > 0 ? topicsTemp[topicsTemp.length - 1] : '';

            if (
                lastItemTopicTemp &&
                lastItemTopicTemp?.id !== activeModule.instanceId
            ) {
                setHiddenBtnIgnore(false);
            } else {
                setHiddenBtnIgnore(true);
            }
        }
    }, [activeModule, topics]);

    const renderArrowIcon = () => {
        return (
            <div
                style={{
                    textAlign: 'right',
                    cursor: 'pointer',
                    position: 'absolute',
                    right: 0,
                    zIndex: 100
                }}
                onClick={handleRenderSideBar}
            >
                {arrowIcon && <img src={images.ic_double_left_arrows} alt="" />}
            </div>
        );
    };

    const handleRenderSideBar = () => {
        setShowSidebar(!showSidebar);
        setArrowIcon(!arrowIcon);
        columnContent === 24 ? setColumContent(18) : setColumContent(24);
    };

    const onCheckView = useCallback(() => {
        if (activeModule) {
            onFetchTopics(Number(courseId));
        }
    }, [activeModule]);

    const updateAnswer = (id: number, index: number, answers: number[]) => {
        if (resTest && answers !== resTest?.answerCorrect) {
            const value = answers.findIndex((x) => x === 1);
            onUpdateAnswerStartedQuiz({
                attemptId: Number(resTest && resTest.attemptId),
                uniqueId: Number(resTest && resTest.uniqueId),
                number: resTest.number,
                answerCorrect: answers,
                finishAttempt: 0,
                timeUp: 0,
                single: resTest.single,
                sequencecheck: resTest.sequencecheck
            });
        }
    };
    const finishExam = (
        answer: any[],
        timeStart: string,
        timeFinish: string
    ) => {
        if (resTest) {
            const arrAnswerTemp: any = [];
            answer.forEach((item: any) => {
                const arrAnserTemp2: any = [];
                item?.answer?.forEach((_e: any) => {
                    _e?.checked && arrAnserTemp2.push(_e?.key);
                });
                arrAnswerTemp.push({
                    question_id: item.question,
                    key: JSON.stringify(arrAnserTemp2)
                });
            });
            onSubmitTest({
                test_id: contentPage.id,
                answers: JSON.stringify(arrAnswerTemp),
                time_start: timeStart,
                time_finish: timeFinish,
                total_time_work: 10
            });
        }
    };

    useEffect(() => {
        if (showResultExam && resTest && resTest.attemptId) {
            onGetHistoryStartedQuiz({
                quizId: resTest.quizId,
                userId: Number(user?.moodleUserId)
            });
        }
    }, [resUpdateAnswer]);

    const handleFullScreen = () => {
        showSidebar === true
            ? setStateTemp('showSideBar')
            : setStateTemp('showArrowIcon');
        setShowSidebar(false);
        setFullScreenIcon(!fullScreenIcon);
        setArrowIcon(false);
        setColumContent(24);
        onHandleFullScreen && onHandleFullScreen(true);
    };
    const handleMinimizeScreen = () => {
        setFullScreenIcon(!fullScreenIcon);
        onHandleFullScreen && onHandleFullScreen(false);
        if (stateTemp === 'showSideBar') {
            setShowSidebar(true);
            setColumContent(18);
        } else {
            setArrowIcon(true);
        }
    };

    const handleContentNext = () => {
        if (topics) {
            let isBreak = false;
            let isLast = false;
            const lesson: any[] = topics.lessons;
            let activeLesson: any = {};
            let beforeActiveLesson: any = {};
            for (const key in lesson) {
                if (isBreak) {
                    activeLesson = lesson[key];

                    break;
                }
                if (lesson[key]?.id === activeModule?.instanceId) {
                    isBreak = true;
                    beforeActiveLesson = lesson[key];
                }
            }
            if (lesson[lesson.length - 1].id === beforeActiveLesson.id) {
                onCompleteLesson({ lesson_id: beforeActiveLesson.id });
                history.push('/');
            }
            if (activeLesson) {
                onCompleteLesson({ lesson_id: beforeActiveLesson.id });
                onCheckView();
                onChangeContentModule(activeLesson?.id, activeLesson.type);
            }
        }
    };
    const handleOnStartQuiz = () => {
        onDetailQuestionInTest({ id: contentPage.id });
    };
    useEffect(() => {
        if (contentQuiz) {
            if (
                (contentQuiz?.timeOpen &&
                    moment(new Date()).isBefore(
                        new Date(contentQuiz?.timeOpen)
                    )) ||
                (contentQuiz?.timeOpen &&
                    contentQuiz?.timeClose &&
                    !moment(new Date()).isBetween(
                        new Date(contentQuiz?.timeOpen),
                        new Date(contentQuiz?.timeClose)
                    )) ||
                (contentQuiz?.timeClose &&
                    moment(new Date()).isAfter(
                        new Date(contentQuiz?.timeClose)
                    )) ||
                (resGetHistoryStartedQuiz &&
                    contentQuiz?.attempts &&
                    resGetHistoryStartedQuiz.length >= contentQuiz?.attempts)
            ) {
                setHiddenBtnStart(true);
            } else {
                setHiddenBtnStart(false);
            }
        }
    }, [contentQuiz, resGetHistoryStartedQuiz]);
    const renderContent = () => {
        // if (is_done_quiz && resTest) {
        //     return <StudentContentQuiz quiz={contentQuiz} />;
        // } else

        if (resTest) {
            return (
                <Exam
                    initIndex={0}
                    // getQuestion={getQuestion}
                    updateAnswer={updateAnswer}
                    finishExam={finishExam}
                    // timeStart={moment().unix() / 1000}
                    timeLimit={Number(contentPage?.time_work) * 60}
                    totalQuestion={contentPage?.totalQuestions || 0}
                    question={resTest || undefined}
                    answers={resTest?.answerCorrect}
                />
            );
        }
        if (activeModule && activeModule.type === 'quiz') {
            return <StudentContentQuiz quiz={contentPage} />;
        }
        if (activeModule) {
            return (
                <ContentPageCourse
                    pageCourse={contentPage}
                    onCheckView={onCheckView}
                />
            );
        }

        return <div></div>;
    };

    const confirmResultExam = () => {
        activeModule &&
            onChangeContentModule(
                activeModule.instanceId,
                activeModule.type,
                true
            );
        setShowResultExam(false);
    };

    return (
        <div>
            {/* <Prompt
                when={resTest ? true : false}
                message={t('question_change_content_when_exam')}
            /> */}
            <Row>
                <Col md={columnContent}>
                    <Row className="bg-white content-course">
                        <Col
                            sm={24}
                            style={{ position: 'relative', minHeight: '70vh' }}
                        >
                            {renderArrowIcon()}
                            {renderContent()}
                        </Col>
                    </Row>
                    <Row className="bottom-content-course">
                        <Col className="col-style">
                            {activeModule &&
                                activeModule.type !== 'quiz' &&
                                (!hiddenBtnIgnore ? (
                                    <Button
                                        className="btn-style btn-dan"
                                        onClick={() => {
                                            if (
                                                topics?.is_sequence &&
                                                contentPage?.test
                                                    ?.is_learned === 0
                                            ) {
                                                message.error(
                                                    'Bạn phải hoàn thành bài kiểm tra của bài học trước'
                                                );
                                                setActiveModule({
                                                    instanceId: contentPage?.id,
                                                    type: 'quiz'
                                                });
                                            } else {
                                                handleContentNext();
                                            }
                                        }}
                                    >
                                        {t('content_next')}
                                    </Button>
                                ) : (
                                    <Button
                                        className="btn-style btn-dan"
                                        onClick={() => {
                                            flagDone.current = true;
                                            if (
                                                topics?.is_sequence &&
                                                contentPage?.test
                                                    ?.is_learned === 0
                                            ) {
                                                message.error(
                                                    'Bạn phải hoàn thành bài kiểm tra của bài học trước'
                                                );
                                                setActiveModule({
                                                    instanceId: contentPage?.id,
                                                    type: 'quiz'
                                                });
                                            } else {
                                                handleContentNext();
                                            }
                                        }}
                                    >
                                        {t('done')}
                                    </Button>
                                ))}
                            {activeModule && activeModule.type === 'quiz' && (
                                <>
                                    {!hiddenBtnIgnore && (
                                        <Button
                                            onClick={() => handleContentNext}
                                            className="btn-style btn-pri"
                                        >
                                            {t('ignore')}
                                        </Button>
                                    )}

                                    {/* {!hiddenBtnStart && ( */}

                                    <Button
                                        disabled={disableBtn}
                                        onClick={handleOnStartQuiz}
                                        className="btn-style btn-dan"
                                    >
                                        {t('begin')}
                                    </Button>
                                    {/* )} */}
                                </>
                            )}

                            {!hiddenBtnStart && fullScreenIcon && (
                                <img
                                    onClick={handleFullScreen}
                                    src={images.ic_zoom_in}
                                    alt=""
                                />
                            )}
                            {!hiddenBtnStart && !fullScreenIcon && (
                                <img
                                    onClick={handleMinimizeScreen}
                                    src={images.ic_zoom_out}
                                    alt=""
                                />
                            )}
                        </Col>
                    </Row>
                </Col>
                {showSidebar && (
                    <Col
                        xs={{ span: 24 }}
                        sm={{ span: 24 }}
                        md={{ span: 6 }}
                        className=" size-bar"
                    >
                        <StudentSidebar
                            confirmChange={resTest ? true : false}
                            onHandleRenderSideBar={handleRenderSideBar}
                            topics={topics}
                            onChangeContentModule={onChangeContentModule}
                            activeModule={activeModule}
                        />
                    </Col>
                )}
            </Row>

            <Modal
                title={t('result_exam')}
                visible={showResultExam}
                onOk={() => confirmResultExam()}
                onCancel={() => confirmResultExam()}
                footer={null}
            >
                <p style={{ fontSize: '3em', textAlign: 'center' }}>
                    {resGetHistoryStartedQuiz[0]?.correct}/
                    {contentQuiz?.totalQuestions}
                </p>
                <div style={{ textAlign: 'center' }}>
                    <Button type="primary" onClick={() => confirmResultExam()}>
                        {t('confirm')}
                    </Button>
                </div>
            </Modal>
            {showMessageError && (
                <ModalError
                    messageError={
                        // getQuestionStartedQuizError ||
                        updateAnswerStartedQuizError ||
                        updateInteractiveCourseError
                    }
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
}

export default StudentContentCourse;
