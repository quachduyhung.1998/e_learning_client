import { Button, Col, Divider, Row, Space } from 'antd';
import { SEP_API_DOMAIN } from 'constants/App';
import { getUrl } from 'helper/String';
import userDetailCourse from 'hooks/course/useDetailCourse';
import useFetchTopic from 'hooks/useFetchTopic';
import images from 'images';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useParams } from 'react-router-dom';
import { Container } from 'reactstrap';
import { STUDENT_COURSE_CONTENT } from 'routes/web/paths';
import { Course } from 'types/Course';
import constStyle from '../../constants/Style';
import './StudentListPage.less';
import ModalError from 'components/shares/modal/ModalError';
import { HistoryOutlined } from '@ant-design/icons';

interface Props {
    detailCourse?: Course;
}
interface Params {
    id: string;
}
function StudentCourseDetailPage(props: Props): JSX.Element {
    const [detailCourse, setDetailCourse] = useState<Course>();
    const { t } = useTranslation();
    const params: Params = useParams();
    const history = useHistory();
    const courseId = params.id;
    const [topics, setTopics] = useFetchTopic(courseId);
    const [isViewContent, setViewContent] = useState(true);
    const [isViewSummary, setViewSummary] = useState(true);
    const [
        onFetchInfoCourse,
        resCourseInfo,
        loading,
        detailCourseError
    ] = userDetailCourse();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (detailCourseError) {
            setShowMessageError(true);
        }
    }, [detailCourseError]);
    useEffect(() => {
        onFetchInfoCourse({ id: courseId });
    }, []);
    useEffect(() => {
        setDetailCourse(resCourseInfo);
    }, [resCourseInfo]);
    const renderSummaryArrow = () => {
        return (
            <div
                className="img-sumary"
                onClick={() => {
                    setViewSummary(!isViewSummary);
                }}
            >
                {isViewSummary && <img src={images.ic_down_arrow} alt="" />}
                {!isViewSummary && <img src={images.ic_up_arrow} alt="" />}
            </div>
        );
    };
    const renderContentArrow = () => {
        return (
            <div
                className="img-sumary"
                onClick={() => {
                    setViewContent(!isViewContent);
                }}
            >
                {isViewContent && <img src={images.ic_down_arrow} alt="" />}
                {!isViewContent && <img src={images.ic_up_arrow} alt="" />}
            </div>
        );
    };
    return (
        <Container>
            <div id="student-course-detail" className="">
                <div>
                    <Row>
                        <Col span={18}>
                            <Row>
                                <Col sm={4}>
                                    <div className="mgr-20">
                                        <img
                                            src={detailCourse?.image}
                                            alt=""
                                            className="full-width"
                                        />
                                    </div>
                                </Col>
                                <Col sm={19} className="mgr-20">
                                    <div
                                        style={{
                                            color: constStyle.COLOR_MAIN,
                                            fontSize: 30,
                                            fontWeight: 'bold'
                                        }}
                                    >
                                        {detailCourse?.name}
                                    </div>
                                    <Row className="mgb-10">
                                        <Col sm={3}>{t('category')} :</Col>
                                        <Col sm={20}>
                                            {detailCourse?.course_category}
                                        </Col>
                                    </Row>
                                    <Row className="mgb-10">
                                        <Col sm={3}>{t('author')} :</Col>
                                        <Col sm={20}>
                                            <span>
                                                {detailCourse?.created_by?.name}{' '}
                                            </span>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>

                        <Col sm={6} className="text-right">
                            <Space>
                                <Button
                                    className="btn-start-learning"
                                    onClick={() =>
                                        history.push({
                                            pathname: `/teacher/course/${params?.id}/student/quiz-result/over-view`
                                        })
                                    }
                                >
                                    <HistoryOutlined />
                                    Lịch sử làm bài
                                </Button>
                                <Button
                                    style={{ float: 'right' }}
                                    className="btn-start-learning"
                                    onClick={() =>
                                        history.push(
                                            getUrl(STUDENT_COURSE_CONTENT, {
                                                courseId:
                                                    detailCourse?.id ||
                                                    params.id
                                            })
                                        )
                                    }
                                >
                                    <img src={images.ic_book} alt="" />
                                    {t('start_learning')}
                                </Button>
                            </Space>
                        </Col>
                        <Divider className="divider-style" />
                    </Row>
                </div>

                <div className="summary">
                    <Row>
                        <Col sm={24}>
                            <div>
                                <h5 style={{ float: 'left' }}>
                                    {t('summary')}
                                </h5>
                                <Row
                                    style={{ marginBottom: '1em' }}
                                    justify="end"
                                >
                                    {' '}
                                    {renderSummaryArrow()}
                                </Row>
                            </div>
                        </Col>
                        {isViewSummary && (
                            <Col sm={24}>
                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: detailCourse?.description || ''
                                    }}
                                ></div>
                            </Col>
                        )}

                        <Divider className="divider-style" />
                    </Row>
                </div>
                <div className="content">
                    <div>
                        <h5 style={{ float: 'left' }}>{t('content')}</h5>
                        <Row style={{ marginBottom: '1em' }} justify="end">
                            {renderContentArrow()}
                        </Row>
                    </div>
                    {isViewContent && (
                        <div>
                            {topics?.lessons?.map((topic: any) => {
                                return (
                                    <div
                                        className="topic-items"
                                        key={topic.id}
                                        style={{ fontWeight: 'bold' }}
                                    >
                                        <h6>{topic?.name}</h6>
                                        {/* {topic?.map((module: any) => {
                                            return (
                                                <div
                                                    className="topic-items"
                                                    key={module.id}
                                                >
                                                    - {module.name}
                                                </div>
                                            );
                                        })} */}
                                    </div>
                                );
                            })}
                        </div>
                    )}
                </div>
            </div>
            {showMessageError && (
                <ModalError
                    messageError={detailCourseError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </Container>
    );
}

export default StudentCourseDetailPage;
