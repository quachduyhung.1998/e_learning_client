import ButtonBack from 'components/shares/button/ButtonBack';
import ModalError from 'components/shares/modal/ModalError';
import List from 'components/student/List';
import PopupAddStudent from 'components/student/PopupAddStudent';
import Search from 'components/student/Search';
import useRemoveStudentsCourse from 'hooks/student/useRemoveStudentsCourse';
import qs from 'query-string';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useParams } from 'react-router-dom';
import { Col, Container, Row } from 'reactstrap';
import { User } from 'types/User';
import useFetchUser from './../../hooks/student/useFetchUser';
import './StudentListPage.less';

interface Params {
    courseId: string;
}
/**
 * Danh sách học viên của một khoá học
 */
function StudentListPage(): JSX.Element {
    const { t } = useTranslation();
    const params: Params = useParams();
    const [firstFilter, setFirstFilter] = useState(true);
    const history = useHistory();
    const [modal, setModal] = useState<boolean>(false);
    const handleStatus = (status: boolean) => {
        setModal(status);
    };
    const [listUser, setListUser] = useState<User[]>([]);
    const [total, setTotal] = useState<number>(0);
    const [totalPage, setTotalPage] = useState<number>(0);

    const [filter, setFilter] = useState({
        courseId: params.courseId
    });

    const [onFetchUsers, resUser] = useFetchUser();
    useEffect(() => {
        onFetchUsers({
            ...filter
        });
    }, [filter]);

    useEffect(() => {
        if (resUser) {
            setListUser(resUser);
            // setTotal(resUser?.total);
            // setTotalPage(resUser.totalPage);
        }
    }, [resUser]);

    /**
     * Sau khi gán user vào khoá học thành công
     * thì load lại danh sách học viên
     */
    // const handleReloadListUser = () => {
    //     onFetchUsers({ ...filter });
    // };

    /**
     * Tìm kiếm tên màn danh sách học viên của khoá học
     * @param text
     */
    const handleSearchStudent = (text: string) => {
        // setFilter({
        //     ...filter,
        //     search: text
        // });
    };

    /**
     * Xử lý màn tìm kiếm danh sách học viên trong khoá học
     * @param pageNumber
     */
    const handelPageChange = (page: number) => {
        // setFilter({ ...filter, page: page });
    };

    const [
        onRemoveFetchData,
        resRemoveUser,
        loading,
        removeStudentCourseError
    ] = useRemoveStudentsCourse();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (removeStudentCourseError) {
            setShowMessageError(true);
        }
    }, [removeStudentCourseError]);
    /**
     * Xử lý xoá học viên trong khoá học
     */
    const handleRemoveStudent = (userId: string, collectionType: string) => {
        onRemoveFetchData({
            courseId: Number(params.courseId),
            userId,
            collectionType
        });
    };

    const onCancelClicked = () => {
        history.goBack();
    };

    return (
        <Container>
            <div id="student">
                <Row className="mb-3">
                    <Col sm={12}>
                        <h2 className="float-left title">
                            {t('list_student')}
                        </h2>
                        <div className="float-right">
                            <ButtonBack onClick={onCancelClicked} />
                        </div>
                    </Col>
                </Row>
                <Search
                    onShowModal={handleStatus}
                    onSearchListStudent={handleSearchStudent}
                />
                <List
                    listUser={listUser}
                    limit={10}
                    page={1}
                    total={total}
                    totalPage={totalPage}
                    onPageChange={handelPageChange}
                    onRemoveCourseStudent={handleRemoveStudent}
                    courseId={Number(params.courseId)}
                />
            </div>
            {showMessageError && (
                <ModalError
                    messageError={removeStudentCourseError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </Container>
    );
}

export default React.memo(StudentListPage);
