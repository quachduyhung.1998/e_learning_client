import {
    Button,
    Checkbox,
    Col,
    Collapse,
    Modal,
    Row,
    Space,
    Tooltip,
    Alert
} from 'antd';
import Confirm from 'components/shares/modal/Confirm';
import constStyle from 'constants/Style';
import images from 'images';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Topic } from 'types/Topic';
import { MAX_LENGTH_NUMBER } from '../../constants/Constants';
const { Panel } = Collapse;

interface Props {
    topics?: any;
    onChangeContentModule: (instance: number, moduleName: string) => void;
    activeModule?: {
        instanceId: number;
        type: string;
    };
    onHandleRenderSideBar?: () => void;
    confirmChange?: boolean;
}

interface IndexActive {
    index: number;
    parent: number;
}

function StudentSidebar(props: Props): JSX.Element {
    const { t } = useTranslation();
    const {
        onChangeContentModule,
        onHandleRenderSideBar,
        confirmChange
    } = props;
    const [resultConfirmChange, setResultConfirmChange] = useState(false);
    const [showModalConfirmChange, setShowModalConfirmChange] = useState(false);
    const [showConditionPopup, setShowConditionPopup] = useState(false);
    const [indexActive, setIndexActive] = useState<IndexActive>({
        index: -1,
        parent: -1
    });
    const [labelConfirmContent, setLabelConfirmContent] = useState('');
    const [
        handleChangeIndexActiveParamTemp,
        setHandleChangeIndexActiveParamTemp
    ] = useState<{
        index: number;
        parent: number;
        instanceId: number;
        moduleName: string;
        conditions?: number[];
    }>();
    useEffect(() => {
        if (resultConfirmChange && handleChangeIndexActiveParamTemp) {
            handleChangeIndexActive(
                handleChangeIndexActiveParamTemp.index,
                handleChangeIndexActiveParamTemp.parent,
                handleChangeIndexActiveParamTemp.instanceId,
                handleChangeIndexActiveParamTemp.moduleName,
                handleChangeIndexActiveParamTemp.conditions
            );
            setResultConfirmChange(false);
            setHandleChangeIndexActiveParamTemp(undefined);
        }
    }, [resultConfirmChange]);
    const onChangeContentPage = () => {};
    const handleChangeIndexActive = (
        index: number,
        parent: number,
        instanceId: number,
        moduleName: string,
        conditions?: number[]
    ) => {
        // if (confirmChange && !resultConfirmChange) {
        //     return;
        // }
        // conditions && conditions?.map((condition) => Number(condition) || []);
        // let flag = false;
        // if (conditions && conditions.length > 0) {
        props.topics?.lesson?.forEach((topic: any) => {
            if (topic.completed === true) {
                setIndexActive({ index: topic?.id?.toString(), parent });
                onChangeContentModule(topic?.id, moduleName);
                // } else if (topic?.completed === false) {
                //     setLabelConfirmContent(topic?.name);
                // }
                // }
                // });
                // });
                //     if (!flag) {
                //         setShowConditionPopup(true);
                //         return;
                //     }
            }
        });
        // if (!flag) {
        //     setIndexActive({ index, parent });
        //     onChangeContentModule(instanceId, moduleName);
        // }
    };
    const handleClickRenderSidebar = () => {
        onHandleRenderSideBar && onHandleRenderSideBar();
    };
    const renderConditionPopup = () => {
        if (showConditionPopup)
            return (
                <Modal
                    centered
                    visible
                    title={
                        <div style={{ textAlign: 'center' }}>
                            <img
                                style={{ width: '40px' }}
                                src={images.ic_error}
                            />
                        </div>
                    }
                    footer={null}
                    closable={false}
                >
                    <div style={{ textAlign: 'center' }}>
                        <Alert
                            message={t('need_to_complete_lesson_content', {
                                content: labelConfirmContent
                            })}
                            type="warning"
                        ></Alert>
                        <h4></h4>
                        <h5>{t('no_access_module_leaning')}</h5>
                        <Button
                            style={{ alignItems: 'center', marginTop: '20px' }}
                            shape="round"
                            danger
                            onClick={() => {
                                setShowConditionPopup(false);
                            }}
                        >
                            {t('close')}
                        </Button>
                    </div>
                </Modal>
            );
    };

    // Độ tài tối đa của tab nội dung
    const MAX_LENGTH: number = MAX_LENGTH_NUMBER.MAX_LENGTH_CONTENT_SIDEBAR;
    // useEffect(() => {
    //     const { activeModule, topics } = props;
    //     if (topics?.length) {
    //         if (activeModule) {
    //             if (topics?.length) {
    //                 let isBreak = false;
    //                 for (let i = 0; i < topics.length; i++) {
    //                     if (isBreak) {
    //                         break;
    //                     }
    //                     const modulesTemp = topics[i]?.modules;
    //                     if (modulesTemp) {
    //                         for (let j = 0; j < modulesTemp.length; j++) {
    //                             if (
    //                                 modulesTemp[j].instanceId ===
    //                                     activeModule.instanceId &&
    //                                 modulesTemp[j].moduleName ===
    //                                     activeModule.type
    //                             ) {
    //                                 handleChangeIndexActive(
    //                                     modulesTemp[j].id,
    //                                     parseInt(topics[i].id),
    //                                     activeModule.instanceId,
    //                                     activeModule.type
    //                                 );
    //                                 isBreak = true;
    //                                 break;
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
    //         } else {
    //             const topicFirst = topics.find((x) => x);
    //             if (topicFirst) {
    //                 const moduleFirst = topicFirst.modules?.find((x) => x);
    //                 if (moduleFirst) {
    //                     handleChangeIndexActive(
    //                         moduleFirst.id,
    //                         parseInt(topicFirst.id),
    //                         moduleFirst.instanceId,
    //                         moduleFirst.moduleName
    //                     );
    //                 }
    //             }
    //         }
    //     }
    // }, [props.activeModule, props?.topics]);
    if (props?.topics) {
        return (
            <div>
                {renderConditionPopup()}

                <Row style={{ background: constStyle.COLOR_MAIN }}>
                    <Col xs={2} onClick={handleClickRenderSidebar}>
                        <img
                            src={images.ic_double_right_arrows}
                            alt=""
                            style={{ cursor: 'pointer' }}
                        />
                    </Col>
                    <Col xs={22}>
                        {' '}
                        <h5
                            className="text-center "
                            style={{ marginTop: '6px' }}
                        >
                            <span style={{ color: 'white' }}>
                                {t('content')}
                            </span>
                        </h5>
                    </Col>
                </Row>
                <Collapse
                    // activeKey={[indexActive?.parent]}
                    expandIconPosition="right"
                    accordion
                >
                    {props?.topics?.lessons?.map(
                        (topic: any, index: number) => {
                            let flag: any = true;
                            if (index > 0) {
                                flag =
                                    props?.topics?.lessons[index - 1]
                                        ?.is_learned;
                            }

                            return (
                                <Panel
                                    header={
                                        <div
                                            className={`animation-hover ${
                                                indexActive.parent ===
                                                Number(topic.id)
                                                    ? 'text-active'
                                                    : ''
                                            }`}
                                            onClick={() => {
                                                if (
                                                    !props?.topics
                                                        ?.is_sequence ||
                                                    flag
                                                ) {
                                                    onChangeContentModule(
                                                        topic.id,
                                                        'lesson'
                                                    );
                                                    setIndexActive({
                                                        ...indexActive,
                                                        parent:
                                                            Number(topic.id) ===
                                                            indexActive.parent
                                                                ? -1
                                                                : Number(
                                                                      topic.id
                                                                  )
                                                    });
                                                }
                                            }}
                                        >
                                            <Checkbox
                                                checked={topic?.is_learned}
                                            />{' '}
                                            <Tooltip
                                                placement="topLeft"
                                                title={topic?.name}
                                            >
                                                {topic.name.length >
                                                MAX_LENGTH ? (
                                                    <span>
                                                        {`${topic.name.substring(
                                                            0,
                                                            MAX_LENGTH
                                                        )} ...`}
                                                    </span>
                                                ) : (
                                                    <span>{topic.name}</span>
                                                )}
                                            </Tooltip>
                                        </div>
                                    }
                                    key={topic.id}
                                >
                                    <Space
                                        direction="vertical"
                                        style={{ width: '100%' }}
                                    >
                                        {topic?.test &&
                                            (!props?.topics?.is_sequence ||
                                                flag) && (
                                                <Row
                                                    key={topic?.test.id}
                                                    style={{
                                                        paddingLeft: '1em'
                                                    }}
                                                >
                                                    <Col xs={2} sm={4}>
                                                        <Checkbox
                                                            checked={
                                                                topic?.test
                                                                    ?.is_learned
                                                            }
                                                        ></Checkbox>
                                                    </Col>
                                                    <Col xs={22} sm={20}>
                                                        <div
                                                            className={`cursor-pointer animation-hover ${
                                                                indexActive.index ===
                                                                topic?.test.id
                                                                    ? 'text-active'
                                                                    : ''
                                                            }`}
                                                            onClick={() => {
                                                                onChangeContentModule(
                                                                    topic.id,
                                                                    'quiz'
                                                                );
                                                            }}
                                                        >
                                                            <Tooltip
                                                                placement="topLeft"
                                                                title={
                                                                    topic?.test
                                                                        ?.name
                                                                }
                                                            >
                                                                {topic?.test
                                                                    ?.name
                                                                    .length >
                                                                MAX_LENGTH ? (
                                                                    <div>
                                                                        <span>
                                                                            {`${topic?.test?.name.substring(
                                                                                0,
                                                                                MAX_LENGTH
                                                                            )} ...`}
                                                                        </span>
                                                                        <p
                                                                            style={{
                                                                                fontStyle:
                                                                                    'italic',
                                                                                fontSize:
                                                                                    '12px',
                                                                                fontWeight:
                                                                                    'lighter'
                                                                            }}
                                                                        >
                                                                            {topic
                                                                                ?.test
                                                                                ?.timeLimit
                                                                                ? topic
                                                                                      ?.test
                                                                                      ?.timeLimit /
                                                                                      60 +
                                                                                  ' ' +
                                                                                  t(
                                                                                      'minute'
                                                                                  )
                                                                                : ''}
                                                                        </p>
                                                                    </div>
                                                                ) : (
                                                                    <div>
                                                                        <span>
                                                                            {
                                                                                topic
                                                                                    ?.test
                                                                                    ?.name
                                                                            }
                                                                        </span>
                                                                        <p
                                                                            style={{
                                                                                fontStyle:
                                                                                    'italic',
                                                                                fontSize:
                                                                                    '12px',
                                                                                fontWeight:
                                                                                    'lighter'
                                                                            }}
                                                                        >
                                                                            {topic
                                                                                ?.test
                                                                                ?.timeLimit
                                                                                ? topic
                                                                                      ?.test
                                                                                      ?.timeLimit /
                                                                                      60 +
                                                                                  ' ' +
                                                                                  t(
                                                                                      'minute'
                                                                                  )
                                                                                : ''}
                                                                        </p>
                                                                    </div>
                                                                )}
                                                            </Tooltip>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            )}
                                    </Space>
                                </Panel>
                            );
                        }
                    )}
                </Collapse>
                <Confirm
                    title={t('change_content')}
                    content={t('question_change_content_when_exam')}
                    onVisible={showModalConfirmChange}
                    onOk={() => {
                        setResultConfirmChange(true);
                        setShowModalConfirmChange(false);
                    }}
                    onCancel={() => {
                        setShowModalConfirmChange(false);
                        setHandleChangeIndexActiveParamTemp(undefined);
                        setResultConfirmChange(false);
                    }}
                    cancelText={t('no')}
                    okText={t('yes')}
                />
            </div>
        );
    }
    return <div></div>;
}

export default React.memo(StudentSidebar);
