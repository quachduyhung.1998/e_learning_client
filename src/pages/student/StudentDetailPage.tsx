import { UserOutlined } from '@ant-design/icons';
import { Avatar, Button, Col, List, Row, Table, Tooltip } from 'antd';
import { IMG_DOMAIN, MAX_LENGTH_NUMBER } from 'constants/Constants';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import useDetailUser from 'hooks/user/useDetailUser';
import images from 'images';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { Container } from 'reactstrap';
import { UPDATE_PROFILE } from 'routes/web/paths';
import { User } from 'types/User';
import './StudentDetailPage.less';
interface Props {
    detailUser?: User;
}

function StudentCourseDetailPage(props: Props): JSX.Element {
    const { t } = useTranslation();
    const history = useHistory();
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    const [onFetchInfoUser, detailUser] = useDetailUser();

    const achievements_student = detailUser ? detailUser.courses : [];
    const userInfor: any = {
        first_and_last_name: user?.name || '',
        email: user?.email || ''
    };
    let list = [];
    if (achievements_student.length > 0) {
        for (const val of achievements_student) {
            list.push({
                done: `"${val.fullName}"`
            });
        }
    }
    const maxLength = MAX_LENGTH_NUMBER.MAX_LENGTH_TABLE_MY_PROFILE;

    const columns = [
        {
            title: '',
            dataIndex: 'image',
            width: 50,
            render: () => {
                return <img src={images.icons8_prize_2} />;
            }
        },
        {
            title: '',
            dataIndex: 'done',
            render: (done: string) => {
                return (
                    <Tooltip placement="topLeft" title={done}>
                        <span style={{ color: 'black' }}>
                            {t('done')}{' '}
                            <span style={{ fontWeight: 'bold' }}>
                                {done.length > maxLength
                                    ? `${done.substring(0, maxLength)} ..."`
                                    : done}
                            </span>
                        </span>
                    </Tooltip>
                );
            }
        }
    ];

    const onSubmit = () => {
        history.push(`${UPDATE_PROFILE}/?query=${detailUser?.fullName}`);
    };

    return (
        <Container>
            <div id="student-detail" className="">
                <Row gutter={[32, 32]} style={{ backgroundColor: '#e1e2e4' }}>
                    <Col
                        md={{ span: 12 }}
                        sm={{ span: 24 }}
                        style={{ width: '100%' }}
                    >
                        <Row>
                            <div className="titleDetailStudent">
                                <UserOutlined
                                    style={{ marginRight: '0.5em' }}
                                />
                                {t('informationUser')}
                            </div>
                        </Row>
                        <Row
                            style={{
                                padding: '0.5em',
                                backgroundColor: 'white'
                            }}
                        >
                            <Col span={10}>
                                <Row
                                    justify="center"
                                    style={{ marginTop: '3em' }}
                                >
                                    <Avatar
                                        size={102}
                                        shape="circle"
                                        src={user?.avatar || images.ic_avatar}
                                    />
                                </Row>

                                <Row
                                    justify="center"
                                    style={{ marginTop: '1em' }}
                                >
                                    <Button
                                        type="default"
                                        style={{ color: ' rgb(39, 54, 121)' }}
                                        onClick={onSubmit}
                                        htmlType="submit"
                                    >
                                        {t('edit_student')}
                                    </Button>
                                </Row>
                                <Row style={{ marginTop: '6.5em' }}></Row>
                            </Col>
                            <Col
                                span={14}
                                style={{ padding: '0.5em', marginTop: '4em' }}
                            >
                                <div
                                    style={{ borderLeft: '1px solid #bab6b6' }}
                                >
                                    {Object.keys(userInfor).map((key) => {
                                        return (
                                            <List.Item
                                                style={{
                                                    color: 'black',
                                                    wordWrap: 'break-word',
                                                    marginLeft: '0.5em'
                                                }}
                                            >
                                                {t(key)}:{' '}
                                                <span
                                                    style={{
                                                        fontWeight: 'bold',
                                                        color: 'black'
                                                    }}
                                                >
                                                    {userInfor[key]}
                                                </span>
                                            </List.Item>
                                        );
                                    })}
                                </div>
                            </Col>
                        </Row>
                    </Col>

                    <Col md={{ span: 12 }} sm={{ span: 24 }}>
                        <Row>
                            <div className="titleDetailStudent">
                                <img
                                    style={{ marginRight: '0.5em' }}
                                    src={images.icons8_prize}
                                    alt=""
                                />
                                {t('achievements_student')}
                            </div>
                        </Row>
                        <Row style={{ backgroundColor: 'white' }}>
                            <Table
                                pagination={false}
                                scroll={{ x: true, y: 240 }}
                                columns={columns}
                                dataSource={list}
                            />
                        </Row>
                    </Col>
                </Row>
            </div>
        </Container>
    );
}

export default StudentCourseDetailPage;
