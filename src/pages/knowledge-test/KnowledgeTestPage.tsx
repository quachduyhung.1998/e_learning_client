import LocalStorageHelper from 'helper/LocalStorageHelper';
import { loginNavigation } from 'helper/Utils';
import ApiHelper from 'network/ApiClient';
import React, { useEffect, useState, CSSProperties } from 'react';
import { useTranslation } from 'react-i18next';
import { Prompt, useHistory, useLocation } from 'react-router-dom';
import { Container } from 'reactstrap';
import { Button, Row, Col, Space } from 'antd';
import { Role, User } from 'types/User';
import Endpoint from 'network/Endpoint';
import { RatedCapacityResponse } from 'types/responses/RatedCapacityResponse';
import TestSuggestion from 'pages/academy/TestSuggestion';
import TitleComponent from 'components/text/TitleComponent';
import constStyle from 'constants/Style';
interface Request {}

const nameNormal: CSSProperties = {
    fontWeight: 'normal'
};

function KnowledgeTestPage(): JSX.Element {
    const { state } = useLocation<{ user: User; selectedRole: string }>();
    const history = useHistory();
    const { t } = useTranslation();
    const [showTask, setTask] = useState(false);
    const handleListTask = () => {
        setIsBlock(true);
        setTask(true);
    };
    const [getQuestion, setQuestion] = useState<RatedCapacityResponse>();
    const [isBlock, setIsBlock] = useState(false);
    useEffect(() => {
        window.onbeforeunload = () => true;
        return () => {
            window.onbeforeunload = () => false;
        };
    }, []);

    const fetchData = async () => {
        try {
            const res = await ApiHelper.fetch<Request, RatedCapacityResponse>(
                Endpoint.PLACEMENT_EXAM,
                {}
            );
            if (res && res.status === 200) {
                setQuestion(res);
            }
        } catch (error) {}
    };

    const user = state?.user || LocalStorageHelper.getUserStorage();

    // if(!user) {
    // }

    const roleSelected =
        state?.selectedRole || LocalStorageHelper.getCurrentRole();

    useEffect(() => {
        fetchData();
    }, []);

    const onSkipClicked = () => {
        loginNavigation(user, String(roleSelected), history, true, true);
    };

    const showTaskFalse = (
        <div>
            <Space size={16} direction="vertical">
                <div>
                    <TitleComponent
                        title={t('aptitude_test')}
                        color={constStyle.COLOR_MAIN}
                    ></TitleComponent>
                    <h1></h1>
                    <h6>
                        <span style={{ fontWeight: 'bold' }}>
                            {t('intro_exam')}
                        </span>
                    </h6>
                    <Row>
                        <Col xs={{ span: 24 }} sm={{ span: 4 }}>
                            <span className="font-weight-bold">
                                {t('question_form')}{' '}
                            </span>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 20 }}>
                            <b style={nameNormal}>{t('multiple_choice')}</b>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 4 }}>
                            <span className="font-weight-bold">
                                {t('number_of_questions')}{' '}
                            </span>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 20 }}>
                            <b style={nameNormal}>
                                {getQuestion?.data.questions
                                    ? getQuestion.data.questions.length
                                    : null}
                            </b>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 4 }}>
                            <span className="font-weight-bold">
                                {t('time')}
                            </span>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 20 }}>
                            <b style={nameNormal}>
                                {getQuestion?.data?.timeLimit
                                    ? getQuestion.data.timeLimit / 60
                                    : 0}{' '}
                                {t('minute')}{' '}
                            </b>
                        </Col>
                    </Row>
                </div>
                <div>
                    <Button
                        type="primary"
                        onClick={handleListTask}
                        className="mr-2 pl-3 pr-3"
                    >
                        {t('begin')}
                    </Button>
                    <Button onClick={onSkipClicked} className="pl-3 pr-3">
                        {t('ignore')}
                    </Button>
                </div>
            </Space>
        </div>
    );
    return (
        <Container>
            <Prompt when={isBlock} message={t('leave_page')} />
            <div className="">
                {showTask === false ? (
                    showTaskFalse
                ) : (
                    <TestSuggestion
                        user={user}
                        role={String(roleSelected)}
                        list={
                            getQuestion?.data?.questions
                                ? getQuestion.data.questions
                                : []
                        }
                        // totalQuestion={getQuestion?.data}
                        timeDown={
                            getQuestion?.data?.timeLimit
                                ? getQuestion.data.timeLimit
                                : 0
                        }
                        onFinish={() => {
                            setIsBlock(false);
                            window.onbeforeunload = () => false;
                        }}
                    />
                )}
            </div>
        </Container>
    );
}

export default KnowledgeTestPage;
