import { makeStyles } from '@material-ui/styles';

export const RatedCapacityStyle = makeStyles({
    boderGray: {
        border: '1px solid gray'
    },
    boderGrayBottom: {
        borderBottom: '1px solid gray'
    },
    bgWhite: {
        backgroundColor: 'white'
    },
    bgGray: {
        backgroundColor: '#ccc'
    },
    answersImage: {
        height: '100px'
    },
    overflowSroll: {
        height: '500px',
        overflowY: 'auto'
    }
});
