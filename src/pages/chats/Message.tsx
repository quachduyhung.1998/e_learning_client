import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { Avatar } from 'antd';
import images from 'images';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
function Message(props: any) {
    const { chat } = props;
    const { t } = useTranslation();
    const urlImages: string[] = [];
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    const message = useRef();
    if (chat) {
        message.current = chat?.message;
        if (chat?.type === 1) {
            const arr = JSON.parse(chat?.message) || [];
            arr?.forEach((media: any) => {
                urlImages.push(media);
            });
        }
    }
    return (
        <li>
            {' '}
            <div
                className={`mx-4 ${chat?.sender?.id !== user?.id && 'd-flex'}`}
            >
                {chat?.sender?.id !== user?.id && (
                    <Avatar src={chat?.sender?.avatar || images.ic_avatar} />
                )}
                <div>
                    <span
                        hidden={chat?.sender?.id === user?.id}
                        className={`creator ${
                            chat?.sender?.id === user?.id
                                ? 'right-msg'
                                : 'left-msg'
                        }`}
                    >
                        {chat?.sender?.name}
                    </span>
                    <div
                        style={{ minWidth: '7em' }}
                        className={`${
                            urlImages.length > 0 ? 'chat-image' : 'chat'
                        } ${
                            chat?.sender?.id === user?.id
                                ? 'right-msg'
                                : 'left-msg'
                        }`}
                    >
                        {urlImages.length > 0
                            ? urlImages.map((url: any) => {
                                  return <img src={url} alt="" />;
                              })
                            : message.current}
                        <p className="label-chat">
                            {chat?.created_at
                                ? moment(chat?.created_at).format(
                                      'hh:mm DD-MM-YYYY'
                                  )
                                : ''}
                        </p>
                    </div>
                </div>
            </div>
        </li>
    );
}

export default Message;
