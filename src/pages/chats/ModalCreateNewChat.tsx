import React, { useEffect, useState } from 'react';
import { Select, Modal, Form, Input } from 'antd';
import useFetchListFriend from 'hooks/chat/useFetchListFriend';
import useCreatePrivateMessage from 'hooks/chat/useCreatePrivateMessage';
import useCreateGroupMessage from 'hooks/chat/useCreateGroupMessage';
import { useHistory } from 'react-router-dom';
function ModalCreateNewChat(props: any) {
    const { listFriend, loading, onFetchListFriend } = useFetchListFriend();
    const history = useHistory();
    const [form] = Form.useForm();
    const {
        onCreatePrivateMessage,
        res,
        messageError
    } = useCreatePrivateMessage();

    const { onCreateGroupMessage, res: resGroup } = useCreateGroupMessage();
    useEffect(() => {
        onFetchListFriend();
    }, []);
    const [userSelected, setUserSelected] = useState<any>([]);

    useEffect(() => {
        if (props?.defaultUser) {
            setUserSelected([props.defaultUser]);
        }
    }, [props?.defaultUser]);
    const onChangeListFriend = (value: any) => {
        setUserSelected(value);
    };
    const onSubmit = () => {
        form.validateFields().then(async (values) => {
            let res: any;
            if (userSelected?.length === 1) {
                res = await onCreatePrivateMessage({
                    send_to_user_id: userSelected[0],
                    message: values?.message,
                    type: 0
                });
            } else {
                res = await onCreateGroupMessage({
                    list_member: JSON.stringify(userSelected),
                    name: values.name
                });
            }
            if (res) {
                props.toggleModal();
                history.push(`/chats/list?room_id=${res?.id}`);
            }
        });
    };
    return (
        <Modal
            visible={props?.visible}
            onCancel={() => props.toggleModal()}
            okText="Tạo mới"
            onOk={onSubmit}
        >
            <Form form={form}>
                {!props?.defaultUser && (
                    <Form.Item
                        hidden={props?.defaultUser}
                        name="list_user"
                        rules={[
                            {
                                required: true,
                                message: 'Chọn người bạn muốn chat'
                            }
                        ]}
                        label="Người dùng"
                        labelCol={{ span: 6 }}
                    >
                        <Select
                            onChange={(value: any) => onChangeListFriend(value)}
                            className="w-100"
                            mode="multiple"
                            value={userSelected}
                        >
                            {listFriend?.map((item: any) => {
                                return (
                                    <Select.Option value={item?.id}>
                                        {item?.name}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                )}

                {userSelected?.length > 1 ? (
                    <Form.Item
                        labelCol={{ span: 6 }}
                        name="name"
                        label="Tên nhóm chat"
                        rules={[
                            {
                                required: true,
                                message: 'Tên nhóm không được để trống'
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                ) : (
                    <Form.Item
                        labelCol={{ span: 6 }}
                        name="message"
                        label="Tin nhắn"
                        rules={[
                            {
                                required: true,
                                message: 'Tin nhắn là bắt buộc'
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                )}
            </Form>
        </Modal>
    );
}

export default ModalCreateNewChat;
