import { useDebounceFn } from 'ahooks';
import { Avatar, Card, Col, Divider, Form, Input, Row, Upload } from 'antd';
import confirm from 'antd/lib/modal/confirm';
import { Picker } from 'emoji-mart';
import 'emoji-mart/css/emoji-mart.css';
import echoClient from 'helper/echoClient';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import useChatGroup from 'hooks/chat/useChatGroup';
import useCreatePrivateMessage from 'hooks/chat/useCreatePrivateMessage';
import useFetchListChat from 'hooks/chat/useFetchListChat';
import useFetchListMessage from 'hooks/chat/useFetchListMessage';
import useUpload from 'hooks/useUpload';
import images from 'images';
import Echo from 'laravel-echo';
import _ from 'lodash';
import FlatList from 'pages/components/blocks/FlatList';
import * as qs from 'query-string';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import Message from './Message';
import './Message.scss';
(window as any).Pusher = require('pusher-js');
function list(props: any) {
    const query = qs.parse(window.location.search);
    const { t } = useTranslation();
    const [resImages, onUpload, messageError] = useUpload();
    useEffect(() => {
        if (resImages) setImageUpload([...imageUpload, resImages?.url]);
    }, [resImages]);
    const { Meta } = Card;
    const [form] = Form.useForm();
    const history = useHistory();
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    const {
        listMessage,
        loading: loadingList,
        onFetchListMessage
    } = useFetchListMessage();
    const [dataMessage, setDataMessage] = useState<any>([]);

    useEffect(() => {
        if (listMessage)
            setDataMessage(listMessage?.list_message?.reverse() || []);
    }, [listMessage]);
    const { onCreatePrivateMessage, res } = useCreatePrivateMessage();
    const { onCreateChatGroup, res: resGroup } = useChatGroup();

    useEffect(() => {
        if (query?.room_id && user?.id) {
            onFetchListMessage({ room_id: query?.room_id });
            onFetchListChat();
            echoClient
                .private(`user-${user?.id}`)
                .listen('.e-learning-chat', (data: any) => {});
            (window as any).Echo = new Echo({
                broadcaster: 'pusher',
                key: '8a09307a347bebcc29e4',
                cluster: 'ap1',
                forceTLS: true
            });
            const channel = (window as any).Echo.channel(`user-${user.id}`);
            channel.listen('.e-learning-chat', function (data: any) {
                onFetchListChat();
                onFetchListMessage({ room_id: query?.room_id });

                // code here ...
            });
        }
    }, [query?.room_id, user?.id, res, resGroup]);
    const { listChat, loading, onFetchListChat } = useFetchListChat();
    const [messageText, setMessageText] = useState<string>('');
    useEffect(() => {
        onFetchListChat();
    }, []);
    const [ImageFile, setImageFile] = useState<any[]>([]);
    const [imageUpload, setImageUpload] = useState<any>([]);
    const [visibleIcon, setVisibleIcon] = useState<boolean>(false);
    const handleChange = ({ fileList }: any) => {
        if (fileList && fileList.length > 0) {
            fileList.map((e: any, index: number) => {
                e.status = 'done';
                return e;
            });
            form.setFieldsValue({ image_multi: fileList });
            setImageFile(fileList);
        }
    };
    useEffect(() => {
        if (ImageFile && ImageFile?.length > 0) {
            run('imageChange');
        }
    }, [ImageFile]);
    const { run } = useDebounceFn(
        (flag: string) => {
            if (flag === 'imageChange') {
                if (ImageFile && ImageFile.length > 0) {
                    const data = new FormData();
                    let flagNewImage = false;
                    ImageFile.forEach((e: any, index: number) => {
                        if (e.originFileObj) {
                            flagNewImage = true;
                            data.append(`image`, e.originFileObj);
                        }
                    });
                    if (flagNewImage) onUpload(data);
                }
            }
        },

        {
            wait: 500
        }
    );

    const onRemoveImage = (file: any) => {
        confirm({
            title: t('delete_unit'),
            onOk: () => {
                const images = [...ImageFile];
                const imageFile = imageUpload?.filter(
                    (e: any) => e !== file?.uid
                );
                setImageUpload(imageFile);
                _.remove(images, (image) => image === file);
                setImageFile(images);
            }
        });
    };
    const onChangeInput = (e: any) => {
        setMessageText(e.target.value);
        form.setFieldsValue({ message: e.target.value });
    };
    const onSubmit = (data: any) => {
        if (listMessage?.is_group) {
            onCreateChatGroup(data);
        } else {
            onCreatePrivateMessage(data);
        }
    };
    const handleSubmit = () => {
        const message = form.getFieldValue('message');
        let datas: any = {};
        if (imageUpload && imageUpload.length > 0) {
            datas = {
                type: 1,
                message: JSON.stringify(imageUpload),
                send_to_user_id: listMessage?.send_to_user?.id
                    ? listMessage?.send_to_user?.id
                    : 3,
                room_id: listMessage?.room_id
            };
            onSubmit(datas);
        }
        if (messageText?.trim()) {
            datas = {
                type: 0,
                message: messageText,
                send_to_user_id: listMessage?.send_to_user?.id
                    ? listMessage?.send_to_user?.id
                    : 3,
                room_id: listMessage?.room_id
            };
            onSubmit(datas);
        }
        setMessageText('');
        setVisibleIcon(false);
        form.setFieldsValue({ message: undefined });
        setImageFile([]);
        setImageUpload([]);
    };
    const addEmoji = (e: any) => {
        let emoji = e.native;
        const messageTemp = form.getFieldValue('message');

        form.setFieldsValue({
            message: messageTemp ? messageTemp + emoji : emoji
        });
        setMessageText(messageTemp ? messageTemp + emoji : emoji);
    };
    return (
        <Row>
            <Col md={6}>
                <p className="font-weight-bold text-center">Chat</p>
                <FlatList>
                    <Row>
                        {listChat?.map((item: any) => {
                            return (
                                <Col
                                    md={23}
                                    offset={1}
                                    onClick={() => {
                                        history.push({
                                            pathname: '/chats/list',
                                            search: `room_id=${item?.room_id}`
                                        });
                                    }}
                                >
                                    <Card style={{ width: 300 }}>
                                        <Meta
                                            avatar={
                                                <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                            }
                                            title={
                                                item?.room_name ||
                                                item?.send_to_user?.name
                                            }
                                            description={
                                                item?.last_message?.message
                                            }
                                        />
                                    </Card>
                                </Col>
                            );
                        })}
                    </Row>
                </FlatList>
            </Col>
            <Col md={18}>
                <Form className="input" form={form} onFinish={handleSubmit}>
                    <Row
                        justify="center"
                        className="bg-white"
                        style={{ borderRadius: '.5em' }}
                    >
                        <Col md={24}>
                            <Row>
                                <Col className="pt-3 pb-0 pl-3 font-weight-bold">
                                    <span>
                                        {listMessage?.send_to_user?.name}
                                    </span>
                                </Col>
                            </Row>
                        </Col>
                        <Divider />
                        <Col md={24}>
                            <FlatList>
                                {dataMessage?.map((item: any) => {
                                    return (
                                        <Row>
                                            <Col md={24}>
                                                <Message chat={item} />
                                            </Col>
                                        </Row>
                                    );
                                })}
                            </FlatList>
                        </Col>
                        <Col md={24}>
                            {ImageFile && ImageFile?.length > 0 && (
                                <Upload
                                    listType="picture-card"
                                    // onPreview={handlePreview}
                                    onRemove={onRemoveImage}
                                    fileList={ImageFile}
                                ></Upload>
                            )}
                        </Col>
                        <Col md={16}>
                            <Form.Item name="message">
                                {visibleIcon && (
                                    <div className="picker-style">
                                        <Picker onSelect={addEmoji} />
                                    </div>
                                )}

                                <Input
                                    onChange={onChangeInput}
                                    value={messageText}
                                    style={{ width: '100%' }}
                                    prefix={
                                        <div>
                                            <Upload
                                                multiple
                                                accept="image/*"
                                                fileList={ImageFile}
                                                showUploadList={false}
                                                onChange={handleChange}
                                            >
                                                <div>
                                                    <img
                                                        style={{
                                                            maxHeight: '2em'
                                                        }}
                                                        alt=""
                                                        src={images.ic_img}
                                                    />
                                                </div>
                                            </Upload>
                                        </div>
                                    }
                                    addonAfter={
                                        <img
                                            alt=""
                                            src={images.ic_smile}
                                            style={{ maxHeight: '1.5em' }}
                                            // className="img-smile"
                                            onClick={() =>
                                                setVisibleIcon(!visibleIcon)
                                            }
                                        />
                                    }
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Col>
        </Row>
    );
}

export default list;
