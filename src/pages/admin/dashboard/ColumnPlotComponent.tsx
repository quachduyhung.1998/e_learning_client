import { Column, ColumnOptions, Datum } from '@antv/g2plot';
import { Col, Row } from 'antd';
import { format, replaceThenAddCommas } from 'helper/String';
import moment from 'moment';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
// import { translate } from 'res/languages';
// import { FeedbackCountByDate } from '../types/analyst-overview.type';
export interface FeedbackCountByDate {
    _id: string;
    date: string;
    total: number;
}
interface Props {
    data: FeedbackCountByDate[];
}
function ColumnPlotComponent(props: Props) {
    const { data } = props;
    const { t } = useTranslation();
    const columnPlot = useRef<any>();
    const [attributeColumnPlot, setAttributeColumnPlot] = useState<
        ColumnOptions
    >({
        data: [],
        xField: 'name',
        yField: 'value',
        columnWidthRatio: 0.8,
        maxColumnWidth: 100,
        xAxis: {
            label: {
                autoHide: false,
                autoRotate: true
            }
        },
        tooltip: {
            formatter: (datum: Datum) => {
                return {
                    name: 'Số lượng',
                    value: datum?.value
                };
            }
        },
        meta: {
            type: {
                alias: '类别'
            },
            sales: {
                alias: '销售额'
            }
        }
    });
    useEffect(() => {
        columnPlot.current = new Column(
            'user_column_plot',
            attributeColumnPlot
        );
        columnPlot.current.render();
    }, []);

    useEffect(() => {
        if (data) {
            const dataTemp: any = [];
            data.forEach((item: any) => {
                dataTemp.push({
                    name: item?.name,
                    value: item?.value
                });
            });
            setAttributeColumnPlot({ ...attributeColumnPlot, data: dataTemp });
        }
    }, [data]);
    useEffect(() => {
        columnPlot.current.update(attributeColumnPlot);
    }, [attributeColumnPlot]);
    return (
        <Row gutter={[32, 32]}>
            <Col md={24}>
                <div id="user_column_plot" />
            </Col>
            {/* <Col md={24}>
                <span className="font-weight-bold">
                    {' '}
                    {t('statistic.totalOrder')}: {replaceThenAddCommas(total)}
                </span>
            </Col> */}
        </Row>
    );
}

export default ColumnPlotComponent;
