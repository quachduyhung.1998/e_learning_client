import { Datum, Line, LineOptions } from '@antv/g2plot';
import { Col, Row } from 'antd';
import { format, replaceThenAddCommas } from 'helper/String';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
// import { translate } from 'res/languages';
// import { UserOnline } from '../types/analyst-overview.type';
interface Props {
    data: any[];
    filterTime?: string;
}

function LinePlotComponent(props: Props) {
    const { data, filterTime } = props;

    const { t } = useTranslation();
    const linePlot = useRef<any>();
    const [attributeLinePlot, setAttributeLinePlot] = useState<LineOptions>({
        data: [],
        padding: 'auto',
        xField: 'time',
        yField: 'value',
        seriesField: 'type',
        xAxis: {
            // type: 'timeCat',
            tickCount: 5
        },
        yAxis: {
            label: {
                formatter: (text: string, item: any, index: number) => {
                    return replaceThenAddCommas(text);
                }
            }
        },
        tooltip: {
            formatter: (datum: Datum) => {
                return {
                    name: 'Số học sinh',
                    value: `${replaceThenAddCommas(datum?.value)} `
                };
            }
        }
        // smooth: true
    });
    useEffect(() => {
        linePlot.current = new Line('line_plot', attributeLinePlot);
        linePlot.current.render();
    }, []);

    useEffect(() => {
        if (data)
            setAttributeLinePlot({
                ...attributeLinePlot,
                data: data
            });
    }, [data]);
    useEffect(() => {
        linePlot.current.update(attributeLinePlot);
    }, [attributeLinePlot]);
    return (
        <Row>
            <Col md={24} className="mb-3">
                <div id="line_plot" />
            </Col>
        </Row>
    );
}

export default LinePlotComponent;
