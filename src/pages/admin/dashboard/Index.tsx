import useStatistic from 'hooks/statistic/useStatistic';
import React, { useEffect } from 'react';
import { Row, Col } from 'antd';
import { useTranslation } from 'react-i18next';
import { replaceThenAddCommas } from 'helper/String';
import LinePlotComponent from './LinePlotComponent';
import ColumnPlotComponent from './ColumnPlotComponent';

function Dashboard(): JSX.Element {
    const { listData, loading, onStatistic } = useStatistic();
    const { t } = useTranslation();
    useEffect(() => {
        onStatistic();
    }, []);
    return (
        <div className="px-3 w-100">
            {' '}
            <Row className="my-3" gutter={[32, 32]}>
                <Col span={6}>
                    <Row className="container-style">
                        <span className="font-weight-bold ">
                            Tổng khóa học: {listData?.total_course | 0}
                        </span>
                    </Row>
                </Col>
                <Col span={6}>
                    <Row className="container-style">
                        <span className="font-weight-bold">
                            Tổng số lớp: {listData?.total_class | 0}
                        </span>
                    </Row>
                </Col>
                <Col span={6}>
                    <Row className="container-style">
                        <span className="font-weight-bold">
                            Tổng số học sinh:{' '}
                            {replaceThenAddCommas(listData?.total_student | 0)}{' '}
                        </span>
                    </Row>
                </Col>
                <Col span={6}>
                    <Row className="container-style">
                        <span className="font-weight-bold">
                            Tổng số giáo viên:{' '}
                            {replaceThenAddCommas(listData?.total_teacher | 0)}{' '}
                        </span>
                    </Row>
                </Col>
            </Row>
            <Row gutter={[32, 32]} className=" my-3">
                <Col span={24}>
                    <Row className="container-style">
                        <Col span={24} className="mb-3">
                            <Row justify="space-between">
                                <Col md={12} sm={24}>
                                    <span className="font-weight-bold">
                                        Top khóa học thịnh hành
                                    </span>
                                </Col>
                                <Col md={8} sm={24}>
                                    {/* <FilterAnalystOverView
                                        onChangeDate={onChangeRevenueTime}
                                    /> */}
                                </Col>
                            </Row>
                        </Col>

                        <Col span={24}>
                            <ColumnPlotComponent
                                data={listData?.course_most_popular || []}
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row gutter={[32, 32]} className=" my-3">
                <Col span={24}>
                    <Row className="container-style">
                        <Col span={24} className="mb-3">
                            <Row justify="space-between">
                                <Col md={12} sm={24}>
                                    <span className="font-weight-bold">
                                        Khối lượng học sinh học tập
                                    </span>
                                </Col>
                                <Col md={8} sm={24}>
                                    {/* <FilterAnalystOverView
                                        onChangeDate={onChangeRevenueTime}
                                    /> */}
                                </Col>
                            </Row>
                        </Col>

                        <Col span={24}>
                            <LinePlotComponent data={listData?.statisc || []} />
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    );
}

export default Dashboard;
