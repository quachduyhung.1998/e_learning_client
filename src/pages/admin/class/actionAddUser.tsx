import QuestionForm from 'components/lecturers-question/QuestionForm';
import useQuery from 'hooks/route/useQuery';
import React from 'react';
import { useParams } from 'react-router-dom';
import { Container } from 'reactstrap';
import ClassForm from './FormAddUser';
interface FormData {
    name: string;
    categoryId: number;
    content: string;
    type: number;
}

interface Params {
    courseId: string;
    quizId: string;
}

function actionAddUser(): JSX.Element {
    const query = useQuery();
    return (
        <Container>
            <ClassForm />
        </Container>
    );
}

export default React.memo(actionAddUser);
