import {
    Button,
    Dropdown,
    Menu,
    Select,
    Col,
    Row,
    Card,
    Breadcrumb
} from 'antd';
import Confirm from 'components/shares/modal/Confirm';
import { getUrl } from 'helper/String';
import useDeleteQuestion from 'hooks/question/useDeleteQuestion';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useParams } from 'react-router-dom';
import { Table } from 'reactstrap';
import { CLASS, CREATE_QUESTION, DETAIL_QUESTION } from 'routes/web/paths';
import { DetailQuestion } from 'types/Question';
// import './scss/ListQuestion.scss';
import ModalError from 'components/shares/modal/ModalError';
import useGetQuestionsOfQuiz from 'hooks/question/useGetQuestionsOfQuiz';
import { paths } from 'routes';
import { category_course, onKeyPressByRegex } from 'helper/Utils';
import { PlusOutlined, SearchOutlined } from '@ant-design/icons';
import useFetchListClass from 'hooks/question/useFetchListClass';
import SectionHeader from 'layout/admin/SectionHeader';
import Search from 'antd/lib/input/Search';
import { CLASS_ACTION, USER_ACTION } from 'routes/admin/paths';
import useDeleteClass from 'hooks/class/useDeleteClass';

interface Props {
    questions?: DetailQuestion[];
    onReloadQuestion?: () => void;
    actions?: boolean;
}

interface Params {
    courseId: string;
    quizId: string;
}
interface Options {
    data: DetailQuestion;
}

function ListClass(props: Props): JSX.Element {
    const { t } = useTranslation();
    const params: Params = useParams();
    const history = useHistory();
    const { questions } = props;
    const [category, setCategory] = useState(1);

    const {
        listClass,
        loading: loadingListClass,
        onFetchListClass
    } = useFetchListClass();

    const [
        resQuestion,
        onDeleteClass,
        loading,
        messageError
    ] = useDeleteClass();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    const onReloadQuestion = () => {
        onFetchListClass({});
    };

    useEffect(() => {
        if (!loading) {
            setObject(undefined);
            setVisible(false);
            onFetchListClass({});
        }
        if (messageError) {
            setShowMessageError(true);
        }
    }, [loading, messageError, category]);
    const [menu] = useState(() => (option: Options) => {
        return (
            <Menu>
                <Menu.Item>
                    <span
                        onClick={() =>
                            history.push({
                                pathname: CLASS_ACTION,
                                hash: '#detail',
                                search: `id=${option.data.id}`
                            })
                        }
                    >
                        {t('edit')}
                    </span>
                </Menu.Item>
                <Menu.Item>
                    <Link
                        to="#"
                        onClick={(e) => {
                            e.preventDefault();
                            setVisible(true);
                            setObject({
                                courseId: Number(params.courseId),
                                quizId: Number(params.quizId),
                                questionId: option.data.id,
                                id: option.data.id.toString()
                            });
                        }}
                    >
                        {t('delete')}
                    </Link>
                </Menu.Item>
            </Menu>
        );
    });
    const [visible, setVisible] = useState(false);
    const [object, setObject] = useState<{
        courseId: number;
        quizId: number;
        questionId: number;
        id?: string;
    }>();
    /**
     * Xóa Câu hỏi
     */
    const onOkDeleteConfirm = () => {
        setVisible(false);
        if (object) onDeleteClass(object);
    };
    return (
        <>
            <Card>
                <SectionHeader
                    contentLeft={
                        <Breadcrumb>
                            <Breadcrumb.Item>{t('users')}</Breadcrumb.Item>
                            <Breadcrumb.Item>{t('list')}</Breadcrumb.Item>
                        </Breadcrumb>
                    }
                />
                <Row gutter={[8, 8]} align="middle" justify="space-between">
                    <Col md={{ span: 6 }} sm={{ span: 8 }} xs={{ span: 24 }}>
                        <Search
                            placeholder={t('enter_text_to_search')}
                            enterButton={<SearchOutlined />}
                            size="middle"
                            // onSearch={(value) => onSearching(value)}
                        />
                    </Col>
                    <Col
                        md={{ span: 6 }}
                        sm={{ span: 8 }}
                        xs={{ span: 24 }}
                        style={{ textAlign: 'right' }}
                    >
                        <Button
                            type="primary"
                            onClick={() =>
                                history.push({
                                    pathname: CLASS_ACTION,
                                    hash: '#create'
                                })
                            }
                            className="style-btn"
                        >
                            {t('add_new')}
                        </Button>
                    </Col>
                </Row>

                <Row>
                    <Col sm={24}>
                        <Table bordered>
                            <thead>
                                <tr className="text-center">
                                    <th style={{ width: '10%' }}>
                                        {t('num_order')}
                                    </th>
                                    <th>{t('class')}</th>
                                    <th>{t('teacher_manager')}</th>
                                    <th>{t('total_student')}</th>
                                    {(props.actions === undefined ||
                                        props.actions) && (
                                        <th>{t('action')}</th>
                                    )}
                                </tr>
                            </thead>
                            <tbody>
                                {listClass?.map(
                                    (item: DetailQuestion, index: number) => {
                                        return (
                                            <tr key={item.id} className="">
                                                <th
                                                    scope="row"
                                                    className="text-center"
                                                >
                                                    {index + 1}
                                                </th>
                                                <td className="text-center">
                                                    {item.name}
                                                </td>
                                                <td className="text-center">
                                                    {item.teacher.name}
                                                </td>
                                                <td className="text-center">
                                                    {item.total_student}
                                                </td>
                                                {(props.actions === undefined ||
                                                    props.actions) && (
                                                    <td
                                                        style={{ width: '20%' }}
                                                        className="text-center"
                                                    >
                                                        <Dropdown
                                                            overlay={() =>
                                                                menu({
                                                                    data: item
                                                                })
                                                            }
                                                            placement="bottomCenter"
                                                        >
                                                            <Button>
                                                                {t('action')}
                                                            </Button>
                                                        </Dropdown>
                                                    </td>
                                                )}
                                            </tr>
                                        );
                                    }
                                )}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                {showMessageError && (
                    <ModalError
                        messageError={messageError}
                        handleCancel={handleCancelMessage}
                    ></ModalError>
                )}
                <Confirm
                    title={`Xóa lớp học ?`}
                    content={t('delete_confirmation_content', {
                        name: 'lớp học'
                    })}
                    onVisible={visible}
                    onOk={onOkDeleteConfirm}
                    onCancel={() => setVisible(false)}
                />
            </Card>
        </>
    );
}

export default React.memo(ListClass);
