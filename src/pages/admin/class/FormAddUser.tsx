import { Button, Col, Form, Input, Row, Select } from 'antd';
import { Store } from 'antd/lib/form/interface';
import LabelRequired from 'components/shares/LabelRequired';
import ModalError from 'components/shares/modal/ModalError';
import TitleComponent from 'components/text/TitleComponent';
import constStyle from 'constants/Style';
import useDetailClass from 'hooks/class/useDetailClass';
import usePostClass from 'hooks/class/usePostClass';
import usePutClass from 'hooks/class/usePutClass';
import useListUserPermission from 'hooks/user/useListUserPermission';
import images from 'images';
import * as qs from 'query-string';
import React, { MouseEvent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { DetailQuestion } from 'types/Question';
interface Props {
    question?: DetailQuestion;
    courseId?: string;
    quizId?: string;
}
interface Data {
    id?: number;
    name: string;
    answers: string;
    type_question: number;
    course_category_id: number;
    mark: number | string;
}
function ClassForm(props: Props): JSX.Element {
    const { t } = useTranslation();
    const query = qs.parse(window.location.search);
    const { question, courseId, quizId } = props;
    const [onFetchUsers, users, loading] = useListUserPermission();
    useEffect(() => {
        if (query?.id) {
            onDetailClass({ id: query.id.toString() });
        }
    }, [query?.id]);
    useEffect(() => {
        onFetchUsers({ roleId: 1 });
    }, []);
    const [form] = Form.useForm();
    const validateForm = {
        name: [
            {
                required: true,
                message: t('required', { name: t('quiz_name') })
            },
            {
                max: 500,
                message: t('question_name_up_to_500_characters')
            }
        ]
    };
    const {
        onAddClass,
        res,
        loading: loadingAdd,
        messageError: postQuestionError
    } = usePostClass();
    const [onDetailClass, detailClass, loadingDetail] = useDetailClass();
    const [
        onUpdateClass,
        resUpdate,
        loadingUpdate,
        putClassError
    ] = usePutClass();
    const [messageError, setMessageError] = useState<string>('');
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (messageError || postQuestionError || putClassError) {
            setShowMessageError(true);
        }
    }, [messageError, postQuestionError, putClassError]);
    const [questionAnswer, setQuestionAnswer] = useState<
        any | { answerCorrect: number[]; answer: string[] }
    >();
    useEffect(() => {
        if (detailClass) {
            form.setFieldsValue({
                name: detailClass.name,
                user_id: detailClass.teacher?.id
            });
        }
    }, [detailClass]);

    /**
     * Xử lý lưu dữ liệu form
     * @param value
     */
    const onSubmit = (value: Store) => {
        let data: any = {};
        data = {
            name: value.name,
            user_id: value.user_id
        };

        if (detailClass) {
            data.id = detailClass?.id;
            onUpdateClass(data);
        } else {
            onAddClass(data);
        }
    };
    const history = useHistory();

    const [checkType, SetType] = useState(0);
    const [selectQuestionType, setSelectQuestionType] = useState(false);
    /**
     * Chọn kiểu : 1 => chọn một , 0 => chọn nhiều , 2 => chọn đúng sai
     * @param value
     */

    /**
     * Trở về trang danh sách
     * @param e
     */
    const handleCancel = (e: MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        history.goBack();
    };

    return (
        <div className="question-form bg-white p-3">
            <Form
                form={form}
                layout="horizontal"
                onFinish={onSubmit}
                onFinishFailed={(err) => err}
                hideRequiredMark={true}
            >
                <Row style={{ marginBottom: '20px' }}>
                    <Col sm={24}>
                        <Row className="d-flex" justify="space-between">
                            <TitleComponent
                                title={t('add_class')}
                                color={constStyle.COLOR_MAIN}
                            />
                            <div className="list-action">
                                <Button
                                    className="style-btn"
                                    style={{ marginRight: '10px' }}
                                    type="default"
                                    onClick={handleCancel}
                                >
                                    <img src={images.ic_down} alt="" />
                                    {t('back')}
                                </Button>
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    className="style-btn"
                                >
                                    <img src={images.ic_upload} alt="" />
                                    {detailClass ? t('update') : t('create')}
                                </Button>
                            </div>
                        </Row>
                    </Col>
                </Row>
                <Form.Item
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 20 }}
                    label={<LabelRequired label={t('class_name')} />}
                    name="name"
                    labelAlign="left"
                    rules={validateForm.name}
                >
                    <Input
                        onBlur={(e) => {
                            form.setFieldsValue({
                                name: e.target.value.trim()
                            });
                        }}
                    />
                </Form.Item>
                <Form.Item
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 20 }}
                    label={<LabelRequired label={t('teacher')} />}
                    name="user_id"
                    labelAlign="left"
                    rules={[]}
                >
                    <Select>
                        {users?.map((item: any) => {
                            return (
                                <Select.Option key={item?.id} value={item?.id}>
                                    {item?.name}
                                </Select.Option>
                            );
                        })}
                    </Select>
                </Form.Item>
            </Form>
            {showMessageError && (
                <ModalError
                    messageError={
                        messageError || postQuestionError || putClassError
                    }
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
}

export default React.memo(ClassForm);
