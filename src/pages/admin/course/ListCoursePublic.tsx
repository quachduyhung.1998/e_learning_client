import ListCourseAction from 'components/lecturers_course/ListCourseAction';
import TableMyCourse from 'components/lecturers_course/TableMyCourse';
import Pagination from 'components/pagination/PaginationComponent';
import constPaginate from 'constants/Paginate';
import useListCourse from 'hooks/course/useListCourse';
import userDeleteCourse from 'hooks/course/userDeleteCourse';
import useFetchCategories from 'hooks/useFetchCategories';
import debounce from 'lodash/debounce';
import React, { useCallback, useEffect, useState } from 'react';
import { Category } from 'types/Category';
import ModalError from 'components/shares/modal/ModalError';
import useFetchCoursePublic from 'hooks/course/useFetchCoursePublic';
import { Filter } from 'pages/teacher-course/CourseListPage';

/**
 * Màn hình danh sách khóa học được tạo bởi giảng viên
 */

interface Props {
    filter: Filter;
    changeFilter: (newFilter: Filter) => void;
    change: boolean;
}

function ListCoursePublic(props: Props): JSX.Element {
    const { loading, list, onFetchCoursePublic } = useFetchCoursePublic();
    const [
        onDeleteCourse,
        resDeleteCourse,
        deleteCourseError
    ] = userDeleteCourse();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (deleteCourseError) {
            setShowMessageError(true);
        }
    }, [deleteCourseError]);
    // useEffect(() => {
    //     props.change &&
    //         onFetchCourses({
    //             search: props.filter.search,
    //             limit: props.filter.limit,
    //             categoryId: props.filter.categoryId
    //                 ? props.filter.categoryId
    //                 : 0,
    //             status: props.filter.status,
    //             page: props.filter.page
    //         });
    // }, [props.filter, resDeleteCourse]);

    useEffect(() => {
        onFetchCoursePublic();
    }, []);

    const handleSearch = (search: string) => {
        props.changeFilter({ ...props.filter, search: search });
    };

    const debouncedSearch = useCallback(debounce(handleSearch, 350), []);

    const handleChangeCategory = (categoryId: number) => {
        props.changeFilter({
            ...props.filter,
            categoryId: categoryId !== 0 ? categoryId : 0
        });
    };

    const handleNextPage = (selectedItem: { selected: number }): void => {
        props.changeFilter({
            ...props.filter,
            page: selectedItem.selected + 1
        });
    };

    const handleChangeStatus = (status: number | '') => {
        props.changeFilter({
            ...props.filter,
            status: status
        });
    };

    return (
        <div>
            <TableMyCourse
                onDeleteCourse={(id) => onDeleteCourse({ courseId: id })}
                page={props.filter.page}
                listCourse={list || []}
            />

            {showMessageError && (
                <ModalError
                    messageError={deleteCourseError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
}

export default React.memo(ListCoursePublic);
