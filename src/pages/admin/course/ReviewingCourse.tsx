import {
    PlusCircleOutlined,
    SearchOutlined,
    LoadingOutlined,
    BookOutlined
} from '@ant-design/icons';
import {
    Avatar,
    Breadcrumb,
    Card,
    Col,
    Input,
    Layout,
    Pagination,
    Row,
    Skeleton,
    Table
} from 'antd';
import { ColumnsType } from 'antd/lib/table';
import getImageServer from 'helper/ImageHelper';
import useQuery from 'hooks/route/useQuery';
import SectionHeader from 'layout/admin/SectionHeader';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { REVIEWING_COURSES } from 'routes/admin/paths';
import { CourseHandle } from 'types/Base';
import { Course } from 'types/Course';
import ModalAddTeacher from 'components/admin/course/ModalAddTeacher';
import ReadMoreAndReadLess from 'components/admin/course/ReadMoreAndReadLess';
import useListCourse from 'hooks/course/useListCourse';
import { SearchType } from 'types/Base';
import { CourseStatus } from 'types/Course';
import useFetchCourseApproving from 'hooks/course/useFetchCourseAppoving';
import { Tabs } from 'antd';
import MyCourseListPage from 'pages/teacher-course/MyCourseListPage';
import ListCoursePublic from './ListCoursePublic';
const { TabPane } = Tabs;
const { Content } = Layout;
const { Search } = Input;

const ReviewingCourses = () => {
    const { t } = useTranslation();
    const query = useQuery();
    const history = useHistory();
    const [isLoading, setLoading] = useState(true);
    const [listCourse, setListCourse] = useState<Course[]>([]);
    const [dataLoaded, setDataLoaded] = useState([]);
    const [total, setTotal] = useState<number>(0);
    const [search, setSearch] = useState('');
    const {
        loading,
        list,
        params: par,
        setList,
        onFetchCourseApproving
    } = useFetchCourseApproving();
    // const [onFetchCourses, resCourses, loading] = useListCourse();
    const [page, setPage] = useState<string>(query.get('page') || '1');
    const [columns, setColumns] = useState<ColumnsType<Course>>([]);
    const MY_COURSE_TAB = '#my-course';
    const LIST_COURCE_APPROVAL = '#approval-course';
    const [tabKey, setTabKey] = useState(
        history.location.hash || MY_COURSE_TAB
    );
    const [showModal, setModal] = useState<{
        id: number;
        visible: boolean;
    }>({
        id: 0,
        visible: false
    });
    const [params, setParams] = useState({
        search: '',
        page,
        limit: '10',
        status: CourseStatus.STATUS_REVIEWING || '0',
        searchType: SearchType.GLOBAL_CASE
    });

    useEffect(() => {
        onFetchCourseApproving({
            search,
            limit: params.limit,
            status: params.status,
            page: params.page
        });
        history.push(`${REVIEWING_COURSES}?page=${params.page}`);
    }, [params]);
    const onSearching = (q: string) => {
        setSearch(q);
    };

    useEffect(() => {
        setParams({
            ...params,
            search
        });
    }, [search]);

    useEffect(() => {
        if (list) {
            setListCourse(list);
            // setTotal(resCourses.total);
            setColumns([
                {
                    title: t('ID'),
                    width: 20,
                    dataIndex: 'id'
                },
                {
                    title: t('image'),
                    dataIndex: 'course_image',
                    width: 150,
                    // eslint-disable-next-line react/display-name
                    render: (image: string) => {
                        return <Avatar shape="square" size={100} src={image} />;
                    }
                },
                // {
                //     title: t('course_code'),
                //     dataIndex: 'code'
                // },
                {
                    title: t('category'),
                    dataIndex: 'course_category'
                },
                {
                    title: t('course_name'),
                    dataIndex: 'course_name'
                },
                {
                    title: t('author'),
                    dataIndex: 'created_by',
                    // eslint-disable-next-line react/display-name
                    render: (creator: any) => {
                        return <div>{creator ? creator.name : ''}</div>;
                    }
                },

                {
                    title: 'Giáo viên phê duyệt',
                    width: 250,
                    dataIndex: 'teacher_approval',
                    // eslint-disable-next-line react/display-name
                    render: (record: any) => {
                        return <span>{record?.name || ''}</span>;
                    }
                },
                {
                    title: t('option'),
                    dataIndex: 'course_id',
                    // eslint-disable-next-line react/display-name
                    render: (id: number) => {
                        return (
                            <div>
                                <button
                                    className="btn btn-danger d-flex align-items-center"
                                    onClick={() =>
                                        handleShowPopupAddTeacher(id)
                                    }
                                >
                                    <PlusCircleOutlined />
                                    {/* thêm mới giảng viên */}
                                </button>
                            </div>
                        );
                    }
                }
            ]);
        }
    }, [list]);
    const [filter, setFilter] = useState<any>({
        search: String(search?.search || ''),
        categoryId: 0,
        page: 1,
        limit: 1,
        status: '',
        createdBy: '',
        searchType: ''
    });
    const handleFetchCourses = () => {
        return onFetchCourseApproving(params);
    };

    const handleShowPopupAddTeacher = (id: number) => {
        setModal({
            id,
            visible: true
        });
    };
    const handleHiddenModal = () => {
        setModal({
            ...showModal,
            visible: false
        });
    };
    const onChangeCourse = (_page: any) => {
        setParams({ ...params, page: _page });
        setPage(_page);
    };
    const changeFilter = (filter: any) => {
        setFilter(filter);
    };
    const onShowSizeChange = (current: number, pageSize: number) => {
        const cOffset = total / pageSize;
        const cOffsetUpward = Math.ceil(cOffset);
        let nOffset = parseInt(params.page);
        if (cOffsetUpward < parseInt(params.page)) {
            nOffset = cOffsetUpward;
        }
        setParams({
            ...params,
            limit: pageSize.toString(),
            page: nOffset.toString()
        });
    };

    // useEffect(() => {
    //     if (loading) {
    //         setLoading(true);
    //     }

    //     if (!loading) {
    //         setLoading(false);
    //         // @ts-ignore
    //         if (resCourses && resCourses.results) {
    //             // @ts-ignore
    //             dataLoaded.push(resCourses.results);
    //             setDataLoaded(dataLoaded);
    //         }
    //     }
    // }, [loading]);

    return (
        <Card>
            <SectionHeader
                contentLeft={
                    <Breadcrumb>
                        <Breadcrumb.Item>
                            {t('reviewing_courses')}
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>{t('list')}</Breadcrumb.Item>
                    </Breadcrumb>
                }
            />
            <Tabs
                type="card"
                onChange={(key: string) => {
                    setTabKey(key);
                }}
                activeKey={tabKey}
            >
                <TabPane
                    tab={
                        <div style={{ fontSize: 16 }}>
                            <BookOutlined />
                            Danh sách khóa học
                        </div>
                    }
                    key={MY_COURSE_TAB}
                >
                    <ListCoursePublic
                        filter={filter}
                        changeFilter={changeFilter}
                        change={tabKey === MY_COURSE_TAB}
                    />
                </TabPane>
                <TabPane
                    tab={
                        <div style={{ fontSize: 16 }}>
                            <BookOutlined />
                            Khóa học chờ duyệt
                        </div>
                    }
                    key={LIST_COURCE_APPROVAL}
                >
                    {/* <Row gutter={[8, 8]}>
                        <Col
                            md={{ span: 6 }}
                            sm={{ span: 8 }}
                            xs={{ span: 24 }}
                        >
                            <Search
                                placeholder={t('enter_text_to_search')}
                                enterButton={<SearchOutlined />}
                                size="middle"
                                onSearch={(value) => onSearching(value)}
                            />
                        </Col>
                    </Row> */}
                    <Table
                        rowKey="id"
                        columns={columns}
                        dataSource={listCourse || []}
                        pagination={false}
                        // loading={{
                        //     spinning: isLoading,
                        //     indicator: <LoadingOutlined spin />
                        // }}
                        scroll={{ x: 'auto' }}
                    />
                </TabPane>
            </Tabs>

            {/* <div style={{ marginTop: '2em', textAlign: 'right' }}>
                <Pagination
                    showSizeChanger
                    onShowSizeChange={onShowSizeChange}
                    defaultCurrent={parseInt(params.page)s}
                    // total={list?.total}
                    defaultPageSize={parseInt(params.limit)}
                    onChange={onChangeCourse}
                    showQuickJumper
                />
            </div> */}
            <ModalAddTeacher
                hiddenModal={handleHiddenModal}
                visible={showModal.visible}
                id={showModal.id}
                form={''}
                onFetchCourses={handleFetchCourses}
            />
        </Card>
    );
};

export default ReviewingCourses;
