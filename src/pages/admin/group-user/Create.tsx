import { Breadcrumb, Button, Card, Form, Space } from 'antd';
import useCreateGroupUser from 'hooks/group-user/useCreateGroupUser';
import SectionHeader from 'layout/admin/SectionHeader';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { GROUP_USER_LIST } from 'routes/admin/paths';
import FormData from './Form';
function Create() {
    const { t } = useTranslation();
    const history = useHistory();
    const [form] = Form.useForm();
    const { resCreateGroupUser, onCreateGroupUser } = useCreateGroupUser();

    const onSubmit = () => {
        form.validateFields()
            .then((values) => {
                onCreateGroupUser({
                    fullName: values?.fullName?.trim(),
                    mail: values?.mail?.trim(),
                    description: values?.description
                });
            })
            .catch((err) => console.log(err));
    };

    useEffect(() => {
        if (resCreateGroupUser) {
            history.push(GROUP_USER_LIST);
        }
    }, [resCreateGroupUser]);

    return (
        <Card>
            <SectionHeader
                contentLeft={
                    <Breadcrumb>
                        <Breadcrumb.Item
                            onClick={() => history.push(GROUP_USER_LIST)}
                        >
                            {t('group_user')}
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>{t('add_new')}</Breadcrumb.Item>
                    </Breadcrumb>
                }
                contentRight={
                    <Space>
                        <Button onClick={() => history.goBack()}>
                            {t('cancel')}
                        </Button>
                        <Button onClick={() => onSubmit()} type="primary">
                            {t('create')}
                        </Button>
                    </Space>
                }
            />
            <FormData form={form} />
        </Card>
    );
}

export default Create;
