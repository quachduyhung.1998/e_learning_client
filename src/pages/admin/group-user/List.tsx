import {
    DeleteOutlined,
    EditOutlined,
    LoadingOutlined,
    TeamOutlined
} from '@ant-design/icons';
import {
    Breadcrumb,
    Button,
    Card,
    Col,
    Input,
    Pagination,
    Row,
    Space,
    Table,
    Tooltip
} from 'antd';
import Modal from 'antd/lib/modal/Modal';
import constPaginate from 'constants/Paginate';
import constStyle from 'constants/Style';
import { getUrl, subString } from 'helper/String';
import useDeleteGroupUser from 'hooks/group-user/useDeleteGroupUser';
import useListGroupUser from 'hooks/group-user/useListGroupUser';
import useQuery from 'hooks/route/useQuery';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import {
    GROUP_USER_CREATE,
    GROUP_USER_LIST,
    GROUP_USER_LIST_USER,
    GROUP_USER_UPDATE
} from 'routes/admin/paths';
import { AlignType, FixedType } from 'types/Common';
import { GroupUser } from 'types/GroupUser';
import { async } from 'rxjs/internal/scheduler/async';
import SectionHeader from 'layout/admin/SectionHeader';

function List() {
    const { t } = useTranslation();
    const history = useHistory();
    const query = useQuery();
    const [filter, setFilter] = useState<{
        limit: number;
        page: number;
        search: string;
    }>({
        limit: Number(query.get('limit')) || constPaginate.LIMIT_DEFAULT,
        page: Number(query.get('page')) || 1,
        search: query.get('search') || ''
    });
    const {
        resListGroupUser,
        total,
        loading,
        onListGroupUser
    } = useListGroupUser();
    const { resDeleteGroupUser, onDeleteGroupUser } = useDeleteGroupUser();
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [groupUserDelete, setGroupUserDelete] = useState<GroupUser>();

    useEffect(() => {
        onListGroupUser(filter);
        history.push({
            search: `?page=${filter.page}&limit=${filter.limit}&search=${filter.search}`
        });
    }, [filter]);

    useEffect(() => {
        if (!resListGroupUser?.length && filter.page > 1) {
            setFilter({ ...filter, page: filter.page - 1 });
        }
    }, [resListGroupUser]);

    useEffect(() => {
        setFilter({ ...filter });
    }, [resDeleteGroupUser]);

    const timeOutRef = useRef(0);
    useEffect(() => {
        clearTimeout(timeOutRef.current);
    }, []);
    const onSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        timeOutRef.current && clearTimeout(timeOutRef.current);
        const value = event.target.value;
        timeOutRef.current = setTimeout(() => {
            onListGroupUser({
                ...filter,
                search: value
            });
            timeOutRef.current = 0;
        }, 300);
    };

    const columns = [
        {
            title: t('name'),
            dataIndex: 'fullName',
            key: 'fullName',
            render: (fullName: string) => {
                return (
                    <Tooltip placement="topLeft" title={fullName}>
                        <span>{subString(fullName, 20, '...')}</span>
                    </Tooltip>
                );
            }
        },
        {
            title: t('email'),
            dataIndex: 'mail',
            key: 'mail',
            render: (mail: string) => {
                return (
                    <Tooltip placement="topLeft" title={mail}>
                        <span>{subString(mail, 30, '...')}</span>
                    </Tooltip>
                );
            }
        },
        {
            title: t('action'),
            dataIndex: '_id',
            key: '_id',
            align: 'center' as AlignType,
            fixed: 'true' as FixedType,
            width: 100,
            render: (_id: string, record: any) => (
                <Space size="middle">
                    <Link to={getUrl(GROUP_USER_LIST_USER, { groupId: _id })}>
                        <TeamOutlined />
                    </Link>
                    <Link to={getUrl(GROUP_USER_UPDATE, { groupId: _id })}>
                        <EditOutlined />
                    </Link>
                    <Link
                        to="#"
                        onClick={() => {
                            setGroupUserDelete(record);
                            setShowModalDelete(true);
                        }}
                    >
                        <DeleteOutlined
                            style={{ color: constStyle.COLOR_MAIN_2 }}
                        />
                    </Link>
                </Space>
            )
        }
    ];

    return (
        <Card>
            <SectionHeader
                contentLeft={
                    <Breadcrumb>
                        <Breadcrumb.Item
                            onClick={() => history.push(GROUP_USER_LIST)}
                        >
                            {t('group_user')}
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>{t('list')}</Breadcrumb.Item>
                    </Breadcrumb>
                }
                contentRight={
                    <Button
                        onClick={() => history.push(GROUP_USER_CREATE)}
                        type="primary"
                    >
                        {t('add_new')}
                    </Button>
                }
            />
            <Row gutter={[8, 8]}>
                <Col md={{ span: 6 }} sm={{ span: 12 }} xs={{ span: 24 }}>
                    <Input
                        onChange={onSearch}
                        placeholder={t('enter_text_to_search')}
                    />
                </Col>
            </Row>
            <Table
                columns={columns}
                dataSource={resListGroupUser}
                rowKey="_id"
                pagination={false}
                loading={{
                    spinning: loading,
                    indicator: <LoadingOutlined spin />
                }}
                scroll={{ x: 'auto' }}
            />
            <div style={{ marginTop: '2em', textAlign: 'right' }}>
                <Pagination
                    showQuickJumper
                    current={filter.page}
                    total={total}
                    pageSize={filter.limit}
                    onChange={(p) => {
                        setFilter({ ...filter, page: p });
                    }}
                    showSizeChanger
                />
            </div>

            <Modal
                centered
                title={t('remove_group_user')}
                visible={showModalDelete}
                onOk={async () => {
                    await (groupUserDelete &&
                        onDeleteGroupUser({
                            groupId: groupUserDelete?._id
                        }));
                    onListGroupUser(filter);
                    setGroupUserDelete(undefined);
                    setShowModalDelete(false);
                }}
                okText={t('accept')}
                onCancel={() => {
                    setGroupUserDelete(undefined);
                    setShowModalDelete(false);
                }}
                cancelText={t('cancel')}
            >
                <p>
                    {t('question_delete_group_user', {
                        name: groupUserDelete?.fullName
                    })}
                </p>
            </Modal>
        </Card>
    );
}

export default List;
