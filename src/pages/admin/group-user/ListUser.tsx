import { DeleteOutlined } from '@ant-design/icons';
import {
    Breadcrumb,
    Button,
    Card,
    Col,
    Divider,
    Row,
    Select,
    Space,
    Table,
    Tooltip
} from 'antd';
import Modal from 'antd/lib/modal/Modal';
import constStyle from 'constants/Style';
import { getUrl, subString } from 'helper/String';
import useAddUserGroup from 'hooks/group-user/useAddUserGroup';
import useDetailGroupUser from 'hooks/group-user/useDetailGroupUser';
import useRemoveUserGroup from 'hooks/group-user/useRemoveUserGroup';
import useListUserFilter from 'hooks/user/useListUserFilter';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useParams } from 'react-router-dom';
import { GROUP_USER_LIST, GROUP_USER_UPDATE } from 'routes/admin/paths';
import { AlignType, FixedType } from 'types/Common';
import { User } from 'types/User';
import SectionHeader from 'layout/admin/SectionHeader';

function List() {
    const { t } = useTranslation();
    const history = useHistory();
    const params = useParams<{
        groupId: string;
    }>();
    const timeOutRef = useRef(0);
    const [search, setSearch] = useState('');
    const [idsSelected, setIdsSelected] = useState<string[]>([]);
    const {
        resListUserFilter,
        setResListUserFilter,
        total,
        onListUserFilter
    } = useListUserFilter();
    const [showModalAdd, setShowModalAdd] = useState(false);
    const [showModalRemove, setShowModalRemove] = useState(false);
    const { resDetailGroupUser, onDetailGroupUser } = useDetailGroupUser();
    const { resAddUserGroup, onAddUserGroup } = useAddUserGroup();
    const { resRemoveUserGroup, onRemoveUserGroup } = useRemoveUserGroup();
    const [userDelete, setUserDelete] = useState<User>();

    useEffect(() => {
        if (params.groupId) {
            onDetailGroupUser({ groupId: params.groupId });
        }
    }, [params.groupId]);

    useEffect(() => {
        onDetailGroupUser({ groupId: params.groupId });
        setIdsSelected([]);
        setResListUserFilter([]);
    }, [resAddUserGroup]);

    useEffect(() => {
        return () => clearTimeout(timeOutRef.current);
    }, []);

    const onSearchModal = (value: string) => {
        timeOutRef.current && clearTimeout(timeOutRef.current);
        timeOutRef.current = setTimeout(() => {
            onListUserFilter({
                search: value,
                groupId: params.groupId,
                excludedUserIds: [
                    ...idsSelected,
                    ...(resDetailGroupUser?.userIds || [])
                ]
            });
            setSearch(value);
        }, 500);
    };

    const columns = [
        {
            title: t('name'),
            dataIndex: 'fullName',
            key: 'fullName',
            render: (fullName: string) => {
                return (
                    <Tooltip placement="topLeft" title={fullName}>
                        <span>{subString(fullName, 20, '...')}</span>
                    </Tooltip>
                );
            }
        },
        {
            title: t('email'),
            dataIndex: 'mail',
            key: 'mail',
            render: (mail: string) => {
                return (
                    <Tooltip placement="topLeft" title={mail}>
                        <span>{subString(mail, 30, '...')}</span>
                    </Tooltip>
                );
            }
        },
        {
            title: t('action'),
            dataIndex: '_id',
            key: '_id',
            align: 'center' as AlignType,
            fixed: 'true' as FixedType,
            width: 100,
            render: (_id: string, record: any) => (
                <Space size="middle">
                    <Link
                        to="#"
                        onClick={() => {
                            setUserDelete(record);
                            setShowModalRemove(true);
                        }}
                    >
                        <DeleteOutlined
                            style={{ color: constStyle.COLOR_MAIN_2 }}
                        />
                    </Link>
                </Space>
            )
        }
    ];

    return (
        <Card>
            <SectionHeader
                contentLeft={
                    <Breadcrumb>
                        <Breadcrumb.Item
                            onClick={() => history.push(GROUP_USER_LIST)}
                        >
                            {t('group_user')}
                        </Breadcrumb.Item>
                        <Breadcrumb.Item
                            onClick={() =>
                                history.push(
                                    getUrl(GROUP_USER_UPDATE, {
                                        groupId: params.groupId
                                    })
                                )
                            }
                        >
                            {resDetailGroupUser?.fullName}
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>{t('list_user')}</Breadcrumb.Item>
                    </Breadcrumb>
                }
                contentRight={
                    <Button
                        onClick={() => setShowModalAdd(true)}
                        type="primary"
                    >
                        {t('add_user')}
                    </Button>
                }
            />
            <Table
                columns={columns}
                dataSource={resDetailGroupUser?.users}
                rowKey="_id"
                scroll={{ x: 'auto' }}
            />
            <Modal
                title={t('add_user')}
                visible={showModalAdd}
                onOk={() => setShowModalAdd(false)}
                onCancel={() => setShowModalAdd(false)}
                footer={null}
            >
                <Select
                    style={{ width: '100%' }}
                    mode="multiple"
                    value={idsSelected}
                    showSearch={true}
                    placeholder={t('enter_text_to_search')}
                    showArrow={false}
                    filterOption={false}
                    notFoundContent={null}
                    onSearch={onSearchModal}
                    onChange={(values: string[]) => {
                        setIdsSelected(values);
                    }}
                    autoClearSearchValue={false}
                >
                    {resListUserFilter &&
                        resListUserFilter.map((user) => (
                            <Select.Option
                                key={user._id}
                                value={user._id}
                                title={`${user.fullName} - ${user.mail}`}
                            >
                                {user.mail
                                    ? `${user.fullName} - ${user.mail}`
                                    : `${user.fullName}`}
                            </Select.Option>
                        ))}
                </Select>
                <Divider />
                <div style={{ textAlign: 'right' }}>
                    <Button
                        onClick={() => {
                            onAddUserGroup({
                                groupId: params.groupId,
                                userIds: idsSelected
                            });
                            setShowModalAdd(false);
                        }}
                        type="primary"
                    >
                        {t('save')}
                    </Button>
                </div>
            </Modal>

            <Modal
                centered
                title={t('remove_user')}
                visible={showModalRemove}
                onOk={async () => {
                    await (userDelete &&
                        onRemoveUserGroup({
                            groupId: params.groupId,
                            userIds: [userDelete._id]
                        }));
                    onDetailGroupUser({ groupId: params.groupId });
                    setShowModalRemove(false);
                    setUserDelete(undefined);
                }}
                okText={t('accept')}
                onCancel={() => {
                    setShowModalRemove(false);
                    setUserDelete(undefined);
                }}
                cancelText={t('cancel')}
            >
                <p>
                    {t('question_remove_user_group', {
                        name: userDelete?.fullName
                    })}
                </p>
            </Modal>
        </Card>
    );
}

export default List;
