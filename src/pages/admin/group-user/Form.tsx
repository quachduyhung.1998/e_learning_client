import { Col, Form, Input, Row } from 'antd';
import { FormInstance } from 'antd/lib/form';
import LabelRequired from 'components/shares/LabelRequired';
import React from 'react';
import { useTranslation } from 'react-i18next';

interface Props {
    form: FormInstance;
}

function FormData(props: Props) {
    const { t } = useTranslation();
    const { form } = props;
    return (
        <Form
            form={form}
            hideRequiredMark={true}
            labelAlign="left"
            labelCol={{ md: { span: 4 } }}
            wrapperCol={{ md: { span: 20 } }}
        >
            <Row gutter={[8, 8]}>
                <Col md={{ span: 12 }} sm={{ span: 24 }}>
                    <Form.Item
                        name="fullName"
                        label={<LabelRequired label={t('name')} />}
                        rules={[
                            {
                                transform: (value: string) => value?.trim(),
                                required: true,
                                message: t('name_is_required')
                            },
                            {
                                transform: (value: string) => value?.trim(),
                                max: 200,
                                message: t('fullName_max', { max: 200 })
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="mail"
                        label={t('email')}
                        rules={[
                            {
                                pattern: /\S+@\S+\.\S+/,
                                message: t('no_is_email')
                            },
                            {
                                transform: (value: string) => value?.trim(),
                                max: 200,
                                message: t('email_max', { max: 200 })
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col md={{ span: 12 }} sm={{ span: 24 }}>
                    <Form.Item
                        name="description"
                        label={t('description')}
                        rules={[
                            {
                                max: 1000,
                                message: `${t('max_length')} 1000`
                            }
                        ]}
                    >
                        <Input.TextArea rows={4} />
                    </Form.Item>
                </Col>
            </Row>
        </Form>
    );
}

export default FormData;
