import { Breadcrumb, Button, Card, Form, Space } from 'antd';
import useDetailGroupUser from 'hooks/group-user/useDetailGroupUser';
import useUpdateGroupUser from 'hooks/group-user/useUpdateGroupUser';
import SectionHeader from 'layout/admin/SectionHeader';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useParams } from 'react-router-dom';
import { GROUP_USER_LIST } from 'routes/admin/paths';
import FormData from './Form';

function Create() {
    const { t } = useTranslation();
    const history = useHistory();
    const params = useParams<{
        groupId: string;
    }>();
    const [form] = Form.useForm();
    const { resDetailGroupUser, onDetailGroupUser } = useDetailGroupUser();
    const { resUpdateGroupUser, onUpdateGroupUser } = useUpdateGroupUser();

    useEffect(() => {
        if (params.groupId) {
            onDetailGroupUser({ groupId: params.groupId });
        }
    }, [params.groupId]);

    useEffect(() => {
        if (resDetailGroupUser) {
            form.setFieldsValue({
                fullName: resDetailGroupUser.fullName,
                mail: resDetailGroupUser.mail,
                description: resDetailGroupUser.description
            });
        }
    }, [resDetailGroupUser]);

    useEffect(() => {
        if (resUpdateGroupUser) {
            history.push(GROUP_USER_LIST);
        }
    }, [resUpdateGroupUser]);

    const onSubmit = () => {
        form.validateFields()
            .then((values) => {
                onUpdateGroupUser({
                    groupId: params?.groupId || '',
                    fullName: values.fullName,
                    mail: values.mail,
                    description: values.description
                });
            })
            .catch((err) => err);
    };

    return (
        <Card>
            <SectionHeader
                contentLeft={
                    <Breadcrumb>
                        <Breadcrumb.Item
                            onClick={() => history.push(GROUP_USER_LIST)}
                        >
                            {t('group_user')}
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>{t('edit')}</Breadcrumb.Item>
                    </Breadcrumb>
                }
                contentRight={
                    <Space>
                        <Button onClick={() => history.goBack()}>
                            {t('cancel')}
                        </Button>
                        <Button onClick={() => onSubmit()} type="primary">
                            {t('update')}
                        </Button>
                    </Space>
                }
            />
            <FormData form={form} />
        </Card>
    );
}

export default Create;
