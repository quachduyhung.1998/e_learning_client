import {
    Breadcrumb,
    Card,
    Row,
    Form,
    Col,
    Input,
    Select,
    Button,
    Space,
    message
} from 'antd';
import SectionHeader from 'layout/admin/SectionHeader';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import * as qs from 'query-string';
import useDetailUser from 'hooks/user/useDetailUser';
import useAddUser from 'hooks/user/useAddUser';
import { USER_LIST } from 'routes/admin/paths';
import { useHistory } from 'react-router';
import useUpdateUser from 'hooks/user/useUpdateUser';

function actionUser() {
    const { t } = useTranslation();
    const query = qs.parse(window.location.search);
    const history = useHistory();
    const [onFetchInfoUser, resUserInfor] = useDetailUser();
    const [form] = Form.useForm();
    const { onAddUser, loading } = useAddUser();
    const { onUpdateUser, loading: loadingUpdate } = useUpdateUser();
    useEffect(() => {
        if (query?.id) {
            onFetchInfoUser({ _id: query?.id.toString() });
        }
    }, [query?.id]);
    useEffect(() => {
        if (resUserInfor)
            form.setFieldsValue({
                name: resUserInfor?.name,
                email: resUserInfor?.email,
                role: resUserInfor?.role
            });
    }, [resUserInfor]);
    const onSubmit = () => {
        form.validateFields().then(async (values) => {
            let res;
            if (query?.id) {
                res = await onUpdateUser({
                    // name: values?.name,
                    // email: values?.email,
                    role: values?.role.toString(),
                    user_id: query.id
                    // password: values?.password,
                    // password_confirmation: values?.confirm_password
                });
            } else {
                res = await onAddUser({
                    name: values?.name,
                    email: values?.email,
                    role: values?.role.toString(),
                    password: values?.password,
                    password_confirmation: values?.confirm_password
                });
            }
            if (res) {
                message.success(t('success'));
                history.push(USER_LIST);
            }
        });
    };
    return (
        <Card>
            <SectionHeader
                contentLeft={
                    <Breadcrumb>
                        <Breadcrumb.Item>{t('users')}</Breadcrumb.Item>
                        <Breadcrumb.Item>{t('create')}</Breadcrumb.Item>
                    </Breadcrumb>
                }
            />
            <Row gutter={[8, 8]} align="middle" justify="end">
                <Col span={22}>
                    <Form form={form}>
                        <Row justify="end">
                            <Col span={24}>
                                <Form.Item
                                    name="name"
                                    label={t('name')}
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 18 }}
                                    labelAlign="left"
                                    rules={[
                                        {
                                            required: true,
                                            message: t('required', {
                                                name: t('name')
                                            })
                                        }
                                    ]}
                                >
                                    <Input
                                        readOnly={query?.id ? true : false}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    name="role"
                                    label={t('role')}
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 18 }}
                                    labelAlign="left"
                                    rules={[
                                        {
                                            required: true,
                                            message: t('required', {
                                                name: t('role')
                                            })
                                        }
                                    ]}
                                    initialValue={0}
                                >
                                    <Select>
                                        <Select.Option value={0}>
                                            {t('student')}
                                        </Select.Option>
                                        <Select.Option value={1}>
                                            {t('teacher')}
                                        </Select.Option>
                                        <Select.Option value={2}>
                                            {t('admin')}
                                        </Select.Option>
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    name="email"
                                    label={t('email')}
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 18 }}
                                    labelAlign="left"
                                    rules={[
                                        {
                                            pattern: /\S+@\S+\.\S+/,
                                            message: t('no_is_email')
                                        },
                                        {
                                            required: true,
                                            message: t('required', {
                                                name: t('email')
                                            })
                                        }
                                    ]}
                                >
                                    <Input
                                        readOnly={query?.id ? true : false}
                                    />
                                </Form.Item>
                            </Col>
                            {!query?.id && (
                                <Col
                                    span={24}
                                    hidden={query?.id ? true : false}
                                >
                                    <Form.Item
                                        name="password"
                                        label={t('password')}
                                        labelCol={{ span: 6 }}
                                        wrapperCol={{ span: 18 }}
                                        labelAlign="left"
                                        rules={[
                                            {
                                                required: true,
                                                message: t('required', {
                                                    name: t('password')
                                                })
                                            },
                                            {
                                                min: 8,
                                                message:
                                                    'Mật khẩu phải tối thiểu 8 ký tự'
                                            }
                                        ]}
                                    >
                                        <Input.Password />
                                    </Form.Item>
                                </Col>
                            )}
                            {!query?.id && (
                                <Col
                                    span={24}
                                    hidden={query?.id ? true : false}
                                >
                                    <Form.Item
                                        name="confirm_password"
                                        label={t('confirm_password')}
                                        labelCol={{ span: 6 }}
                                        wrapperCol={{ span: 18 }}
                                        labelAlign="left"
                                        rules={[
                                            {
                                                required: true,
                                                message: t('required', {
                                                    name: t('password')
                                                })
                                            },
                                            {
                                                min: 8,
                                                message:
                                                    'Mật khẩu phải tối thiểu 8 ký tự'
                                            },
                                            ({ getFieldValue }) => ({
                                                validator(_, value) {
                                                    if (
                                                        !value ||
                                                        getFieldValue(
                                                            'password'
                                                        ) === value
                                                    ) {
                                                        return Promise.resolve();
                                                    }
                                                    return Promise.reject(
                                                        new Error(
                                                            'Mật khẩu không trùng khớp!'
                                                        )
                                                    );
                                                }
                                            })
                                        ]}
                                    >
                                        <Input.Password />
                                    </Form.Item>
                                </Col>
                            )}

                            <Col span={6}>
                                <Space align="end">
                                    <Button onClick={() => history.goBack()}>
                                        {t('cancel')}
                                    </Button>
                                    <Button type="primary" onClick={onSubmit}>
                                        {query?.id ? t('update') : t('create')}
                                    </Button>
                                </Space>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </Row>
        </Card>
    );
}

export default actionUser;
