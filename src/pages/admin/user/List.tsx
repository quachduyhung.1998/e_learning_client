/* eslint-disable react/jsx-key */
import {
    EyeOutlined,
    LoadingOutlined,
    SearchOutlined
} from '@ant-design/icons';
import {
    Breadcrumb,
    Button,
    Card,
    Col,
    Divider,
    Drawer,
    Dropdown,
    Form,
    Input,
    Layout,
    Menu,
    Pagination,
    Row,
    Select,
    Space,
    Table,
    Tag
} from 'antd';
import { ToastError, ToastSuccess } from 'components/admin/Toast';
import useQuery from 'hooks/route/useQuery';
import SectionHeader from 'layout/admin/SectionHeader';
import truncate from 'lodash/truncate';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { USER_LIST, USER_ACTION } from 'routes/admin/paths';
import { BaseResponse } from 'types/responses/BaseResponse';
import { RoleHandle, RoleType, User } from 'types/User';
import useListUser from 'hooks/user/useListUser';

const { Content } = Layout;
const { Search } = Input;
const { Option } = Select;

const Users = () => {
    const history = useHistory();
    const query = useQuery();
    const { t } = useTranslation();
    const [isLoading, setLoading] = useState(true);
    const [listUser, setListUser] = useState<User[]>([]);
    const [dataLoaded, setDataLoaded] = useState([]);
    const [onFetchUsers, users, loading] = useListUser();
    const [total, setTotal] = useState<number>(0);
    const [text, setText] = useState('');
    const [visible, setVisible] = useState(false);
    const [display, setDisplay] = useState('none');
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);
    const [selectedRoles, setSelectedRoles] = useState([]);
    const [user, setUser] = useState<User>();
    const [_userId, setUserId] = useState('');
    const [page, setPage] = useState<number>(Number(query.get('page')) || 1);
    const [params, setParams] = useState({
        text: '',
        page,
        limit: 10,
        role: ''
    });

    const [roleSelected, setRoleSelected] = useState(RoleType.ADMIN);
    const [roleHandleSelected, setRoleHandleSelected] = useState(
        RoleHandle.DEFAULT
    );

    const rolesForAssign = [RoleType.ADMIN, RoleType.TEACHER];
    const rolesHandles = [
        RoleHandle.DEFAULT,
        RoleHandle.ASSIGN,
        RoleHandle.UN_ASSIGN
    ];
    useEffect(() => {
        onFetchUsers({
            text,
            limit: params.limit,
            page: params.page,
            role: params.role
        });
        history.push(`${USER_LIST}?page=${params.page}`);
    }, [params]);

    const onSearching = (q: string) => {
        setText(q);
        setParams({
            ...params,
            text: q
        });
    };

    useEffect(() => {
        setParams({
            ...params,
            text
        });
    }, [text]);
    useEffect(() => {
        if (users) {
            setListUser(users);
            setTotal(users.total);
        }
    }, [users]);
    const html = ({ id }: { id: string }) => (
        <Menu>
            <Menu.Item>
                <span
                    onClick={() => {
                        history.push({
                            pathname: USER_ACTION,
                            hash: 'detail',
                            search: `id=${id}`
                        });
                    }}
                >
                    {t('see_detail')}
                </span>
            </Menu.Item>
            <Menu.Item>
                <span>{t('delete')}</span>
            </Menu.Item>
        </Menu>
    );
    const columns = [
        {
            title: t('full_name'),
            dataIndex: 'name',
            render: (fullName: string) => {
                return fullName
                    ? truncate(fullName, {
                          length: 30,
                          separator: '...'
                      })
                    : '';
            }
        },
        {
            title: 'Email',
            dataIndex: 'email',
            render: (mail: string) => {
                return mail
                    ? truncate(mail, {
                          length: 50,
                          separator: '...'
                      })
                    : '';
            }
        },
        {
            title: t('role'),
            width: 300,
            dataIndex: 'role',
            // eslint-disable-next-line react/display-name
            render: (role: number) => {
                return role === 0
                    ? t('student')
                    : role === 1
                    ? t('teacher')
                    : t('admin');
            }
        },
        {
            title: t('option'),
            dataIndex: 'id',
            // eslint-disable-next-line react/display-name
            render: (id: any) => {
                return (
                    <Space>
                        <Dropdown
                            overlay={html({
                                id: id
                            })}
                            placement="bottomRight"
                        >
                            <Button style={{ float: 'right' }}>
                                {t('action')}
                            </Button>
                        </Dropdown>
                    </Space>
                );
            }
        }
    ];
    const onChangeUser = (_page: number) => {
        setParams({ ...params, page: _page });
        setPage(_page);
    };
    const onShowSizeChange = (current: number, pageSize: number) => {
        const cOffset = total / pageSize;
        const cOffsetUpward = Math.ceil(cOffset);
        let nOffset = params.page;
        if (cOffsetUpward < params.page) {
            nOffset = cOffsetUpward;
        }
        setParams({
            ...params,
            limit: pageSize,
            page: nOffset
        });
    };

    useEffect(() => {
        if (loading) {
            setLoading(true);
        }

        if (!loading) {
            setLoading(false);
            // @ts-ignore
            if (users && users.results) {
                // @ts-ignore
                dataLoaded.push(users.results);
                setDataLoaded(dataLoaded);
            }
        }
    }, [loading]);

    const onSelectChange = (selectedRowKeys: any) => {
        setSelectedRowKeys(selectedRowKeys);
        if (selectedRowKeys && selectedRowKeys.length) {
            setDisplay('flex');
        } else {
            setDisplay('none');
        }
    };

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    };

    const showDrawer = (row: User) => {
        setVisible(true);
        setUser(row);
        setUserId(row._id);
    };

    const onClose = () => {
        setVisible(false);
    };

    const onAssignRoleChange = (e: any) => {
        setRoleSelected(e);
    };

    const onHandleRoleChange = (e: any) => {
        setRoleHandleSelected(e);
    };
    const handleChangeRoles = (value: any) => {
        if (value && value.indexOf('student') === -1) {
            value = [...value, 'student'];
            setSelectedRoles(value);
        } else {
            setSelectedRoles(value);
        }
    };
    const onAssignRoleForMultipleUsers = () => {
        let selectedUser: string[] = [];
        if (selectedRowKeys && selectedRowKeys.length) {
            selectedUser = selectedRowKeys;
        }

        ApiHelper.post<{ userIds: string[]; role: string }, BaseResponse>(
            roleHandleSelected === RoleHandle.ASSIGN
                ? Endpoint.ASSIGN_ROLE
                : Endpoint.UN_ASSIGN_ROLE,
            {
                userIds: selectedUser,
                role: roleSelected
            }
        )
            .then(() => {
                onFetchUsers({
                    text,
                    limit: params.limit,
                    page: params.page,
                    role: params.role
                });
                setDisplay('none');
                setSelectedRowKeys([]);
                ToastSuccess({
                    message: t('success'),
                    description: t('success')
                });
            })
            .catch((error) => {
                return ToastError({
                    message: t('fail'),
                    description: error
                });
            });
    };

    const options: {
        label: string;
        value: string;
        disabled?: boolean;
        color?: string;
    }[] = [
        {
            label: t(RoleType.ADMIN),
            value: RoleType.ADMIN
        },
        {
            label: t(RoleType.TEACHER),
            value: RoleType.TEACHER
        },
        {
            label: t(RoleType.STUDENT),
            value: RoleType.STUDENT,
            disabled: true,
            color: 'red !important'
        }
    ];

    const onFinish = async () => {
        await ApiHelper.post<{ userId: string; roles: string[] }, BaseResponse>(
            Endpoint.ASSIGN_ROLES,
            {
                userId: _userId,
                roles: selectedRoles
            }
        )
            .then(() => {
                onFetchUsers({
                    text,
                    limit: params.limit,
                    page: params.page,
                    role: params.role
                });
                setVisible(false);
                ToastSuccess({
                    message: t('success'),
                    description: t('add_success')
                });
            })
            .catch((error) => {
                return ToastError({
                    message: t('fail'),
                    description: error
                });
            });
    };

    const currentRoles: string[] = [];
    if (user && user.userRoles) {
        for (const role of user.userRoles) {
            currentRoles.push(role);
        }
    }

    const [dynamicColumns] = useState(columns);

    const tagRender = (props: any) => {
        // eslint-disable-next-line prefer-const
        let { label, value, closable, onClose } = props;
        closable = value !== RoleType.STUDENT ? closable : false;
        return (
            <Tag
                closable={closable}
                onClose={onClose}
                style={{
                    marginRight: 3,
                    display: 'flex',
                    alignItems: 'center'
                }}
            >
                {label}
            </Tag>
        );
    };

    return (
        <Card>
            <SectionHeader
                contentLeft={
                    <Breadcrumb>
                        <Breadcrumb.Item>{t('users')}</Breadcrumb.Item>
                        <Breadcrumb.Item>{t('list')}</Breadcrumb.Item>
                    </Breadcrumb>
                }
            />
            <Row gutter={[8, 8]} align="middle" justify="space-between">
                <Col md={{ span: 6 }} sm={{ span: 8 }} xs={{ span: 24 }}>
                    <Search
                        placeholder={t('enter_text_to_search')}
                        enterButton={<SearchOutlined />}
                        size="middle"
                        onSearch={(value) => onSearching(value)}
                    />
                </Col>
                <Col
                    md={{ span: 6 }}
                    sm={{ span: 8 }}
                    xs={{ span: 24 }}
                    style={{ textAlign: 'right' }}
                >
                    <Button
                        type="primary"
                        onClick={() => {
                            history.push({
                                pathname: USER_ACTION,
                                hash: 'create'
                            });
                        }}
                        className="style-btn"
                    >
                        {t('add_new')}
                    </Button>
                </Col>
            </Row>

            <Table
                rowSelection={rowSelection}
                rowKey="_id"
                columns={dynamicColumns}
                dataSource={listUser}
                pagination={false}
                loading={{
                    spinning: isLoading,
                    indicator: <LoadingOutlined spin />
                }}
                scroll={{ x: 'auto' }}
            />

            <div style={{ marginTop: '2em', textAlign: 'right' }}>
                <Pagination
                    showSizeChanger
                    onShowSizeChange={onShowSizeChange}
                    defaultCurrent={Number(query.get('page'))}
                    total={users?.total}
                    defaultPageSize={params.limit}
                    onChange={onChangeUser}
                    showQuickJumper
                />
            </div>
            {visible && user && (
                <Drawer
                    width={640}
                    placement="right"
                    closable={false}
                    onClose={onClose}
                    visible={visible}
                >
                    <span
                        style={{
                            textAlign: 'center',
                            fontWeight: 'bold',
                            fontSize: 20
                        }}
                    >
                        {t('update')} {user.fullName}
                    </span>
                    <Divider />
                    <Form
                        name="nest-messages"
                        onFinish={onFinish}
                        initialValues={{
                            roles: currentRoles
                        }}
                    >
                        <Form.Item label={t('role')} name="roles">
                            <Select
                                id="abc"
                                mode="multiple"
                                size="middle"
                                tagRender={tagRender}
                                placeholder={t('please_select')}
                                options={options}
                                style={{ width: '100%' }}
                                onChange={handleChangeRoles}
                            ></Select>
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                {t('update')}
                            </Button>
                        </Form.Item>
                    </Form>
                </Drawer>
            )}
        </Card>
    );
};

export default Users;
