import ListCourseAction from 'components/lecturers_course/ListCourseAction';
import TableApprovalCourse from 'components/lecturers_course/TableApprovalCourse';
import Pagination from 'components/pagination/PaginationComponent';
import constPaginate from 'constants/Paginate';
import { category_course } from 'helper/Utils';
import useFetchListApprove from 'hooks/course/useFetchListApprove';
import useListCourse from 'hooks/course/useListCourse';
import useFetchCategories from 'hooks/useFetchCategories';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import React, { useEffect, useState } from 'react';
import { Category } from 'types/Category';
import { Course } from 'types/Course';
import { BaseResponse } from 'types/responses/BaseResponse';
import { RoleType, User } from 'types/User';
import { Filter } from './CourseListPage';

/**
 * Màn hình danh sách khóa học được Admin assign để duyệt
 */
interface userParams {
    page: number;
    limit: number;
    role: RoleType;
}
interface Props {
    filter: Filter;
    changeFilter: (newFilter: Filter) => void;
    change: boolean;
}

function CourseListApproval(props: Props): JSX.Element {
    const [categories, setCategories] = useState<any[]>(category_course);
    const [listCourse, setListCourse] = useState<Course[]>([]);
    const [listTeacher, setListTeacher] = useState<User[]>([]);
    const [total, setTotal] = useState<number>(0);
    const [totalPage, setTotalPage] = useState<number>(0);

    const [onFetchTopic, categoryListRes] = useFetchCategories();
    const [
        onFetchApproveCourse,
        resCoursesApprove,
        loading
    ] = useFetchListApprove();

    const [teacherParams, setTeacherParams] = useState<userParams>({
        page: 1,
        limit: 1000,
        role: RoleType.TEACHER
    });
    const listDataTeacher = async () => {
        try {
            const res = await ApiHelper.fetch<
                {
                    page: number;
                    limit: number;
                    role: RoleType;
                },
                BaseResponse
            >(Endpoint.USERS_LIST, {
                ...teacherParams
            });
            if (res && res.status === 200) {
                setListTeacher(res.data.items);
            }
        } catch (error) {}
    };

    useEffect(() => {
        listDataTeacher();
    }, [teacherParams]);

    useEffect(() => {
        props.change && onFetchApproveCourse({ ...props.filter, status: 1 });
    }, [props.filter]);

    useEffect(() => {
        if (resCoursesApprove) {
            setListCourse(resCoursesApprove);
            // setTotal(resCoursesApprove.total);
            // setTotalPage(resCourses.totalPage);
        }
    }, [resCoursesApprove]);

    useEffect(() => {
        onFetchTopic();
    }, []);

    // useEffect(() => {
    //     const categories: Category[] = [];
    //     categoryListRes.forEach((element) => {
    //         categories.push(element);
    //         if (element.children.length) {
    //             element.children.forEach((item) => {
    //                 categories.push(item);
    //             });
    //         }
    //     });
    //     setCategories(categories);
    // }, [categoryListRes]);

    const handleSearch = (search: string) => {
        props.changeFilter({ ...props.filter, search: search });
    };

    const handleChangeCategory = (categoryId: number) => {
        props.changeFilter({
            ...props.filter,
            categoryId: categoryId !== 0 ? categoryId : 0
        });
    };

    const handleNextPage = (selectedItem: { selected: number }): void => {
        props.changeFilter({
            ...props.filter,
            page: selectedItem.selected + 1
        });
    };

    const handleChangeStatus = (status: number | '') => {
        props.changeFilter({
            ...props.filter,
            status: status
        });
    };

    const handleChangeAuthor = (_id: string) => {
        props.changeFilter({
            ...props.filter,
            createdBy: _id
        });
    };

    const refreshData = () => {
        props.changeFilter({
            ...props.filter
        });
    };
    return (
        <div>
            <ListCourseAction
                filter={props.filter}
                onSearch={handleSearch}
                onChangeCategory={handleChangeCategory}
                onChangeStatus={handleChangeStatus}
                onChangeAuthor={handleChangeAuthor}
                categories={categories}
                authors={listTeacher}
                searchType={props.filter.searchType}
            />
            <TableApprovalCourse
                listCourse={listCourse}
                page={props.filter.page}
                refreshData={refreshData}
            />
            {totalPage > 1 && (
                <Pagination
                    pageCount={totalPage}
                    pageRangeDisplayed={constPaginate.NUMBER_PAGE_SHOW}
                    initialPage={props.filter.page - 1}
                    marginPagesDisplayed={constPaginate.NUMBER_PAGE_SHOW}
                    onPageChange={handleNextPage}
                />
            )}
        </div>
    );
}

export default React.memo(CourseListApproval);
