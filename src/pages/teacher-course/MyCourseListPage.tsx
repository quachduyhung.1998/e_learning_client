import ListCourseAction from 'components/lecturers_course/ListCourseAction';
import TableMyCourse from 'components/lecturers_course/TableMyCourse';
import Pagination from 'components/pagination/PaginationComponent';
import constPaginate from 'constants/Paginate';
import useListCourse from 'hooks/course/useListCourse';
import userDeleteCourse from 'hooks/course/userDeleteCourse';
import useFetchCategories from 'hooks/useFetchCategories';
import debounce from 'lodash/debounce';
import React, { useCallback, useEffect, useState } from 'react';
import { Category } from 'types/Category';
import { Filter } from './CourseListPage';
import ModalError from 'components/shares/modal/ModalError';

/**
 * Màn hình danh sách khóa học được tạo bởi giảng viên
 */

interface Props {
    filter: Filter;
    changeFilter: (newFilter: Filter) => void;
    change: boolean;
}

function MyCourseListPage(props: Props): JSX.Element {
    const [categories, setCategories] = useState<Category[]>([]);

    const [onFetchTopic, categoryListRes] = useFetchCategories();
    const [onFetchCourses, resCourses] = useListCourse();
    const [
        onDeleteCourse,
        resDeleteCourse,
        deleteCourseError
    ] = userDeleteCourse();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (deleteCourseError) {
            setShowMessageError(true);
        }
    }, [deleteCourseError]);
    useEffect(() => {
        props.change &&
            onFetchCourses({
                search: props.filter.search,
                limit: props.filter.limit,
                categoryId: props.filter.categoryId
                    ? props.filter.categoryId
                    : 0,
                status: props.filter.status,
                page: props.filter.page
            });
    }, [props.filter, resDeleteCourse]);

    useEffect(() => {
        onFetchTopic();
    }, []);

    const handleSearch = (search: string) => {
        props.changeFilter({ ...props.filter, search: search });
    };

    const debouncedSearch = useCallback(debounce(handleSearch, 350), []);

    const handleChangeCategory = (categoryId: number) => {
        props.changeFilter({
            ...props.filter,
            categoryId: categoryId !== 0 ? categoryId : 0
        });
    };

    const handleNextPage = (selectedItem: { selected: number }): void => {
        props.changeFilter({
            ...props.filter,
            page: selectedItem.selected + 1
        });
    };

    const handleChangeStatus = (status: number | '') => {
        props.changeFilter({
            ...props.filter,
            status: status
        });
    };

    return (
        <div>
            <ListCourseAction
                filter={props.filter}
                onSearch={debouncedSearch}
                onChangeCategory={handleChangeCategory}
                onChangeStatus={handleChangeStatus}
                categories={categories}
                authors={[]}
                onChangeAuthor={() => []}
            />
            <TableMyCourse
                onDeleteCourse={(id) => onDeleteCourse({ courseId: id })}
                page={props.filter.page}
                listCourse={resCourses || []}
            />
            {(resCourses?.totalPage || 0) > 1 && (
                <Pagination
                    pageCount={resCourses?.totalPage || 0}
                    pageRangeDisplayed={constPaginate.NUMBER_PAGE_SHOW}
                    initialPage={props.filter.page - 1}
                    marginPagesDisplayed={constPaginate.NUMBER_PAGE_SHOW}
                    onPageChange={handleNextPage}
                />
            )}
            {showMessageError && (
                <ModalError
                    messageError={deleteCourseError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
}

export default React.memo(MyCourseListPage);
