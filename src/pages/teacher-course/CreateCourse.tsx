import { Form, Space } from 'antd';
import CourseForm from 'components/lecturers_course/CourseForm';
import { StorageKey } from 'helper/LocalStorageHelper';
import ButtonBack from 'components/shares/button/ButtonBack';
import ButtonCreate from 'components/shares/button/ButtonCreate';
import useAddCourse from 'hooks/course/useAddCourse';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { Col, Container, Row } from 'reactstrap';
import { LECTURERS_COURSE } from 'routes/web/paths';
import ModalError from 'components/shares/modal/ModalError';

function CreateCourse(): JSX.Element {
    const { t } = useTranslation();
    const history = useHistory();
    const [onAddCourse, resAddCourse, messageError] = useAddCourse();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (messageError) {
            setShowMessageError(true);
        }
    }, [messageError]);
    const [form] = Form.useForm();

    const onSubmit = () => {
        form.validateFields()
            .then((values) => {
                onAddCourse({
                    code: values?.code,
                    name: values?.fullName.trim(),
                    course_category_id: values?.topicId,
                    description: values?.description,
                    image: values?.image,
                    is_sequence: values?.is_sequence
                });
            })
            .catch((e) => e);
    };

    //load cache

    if (window.performance) {
        if (
            performance.navigation.type == 0 &&
            performance.navigation.redirectCount == 0
        ) {
            localStorage.removeItem(StorageKey.CACHE_FORM_CREATE_COURSE);
        }
    }
    const cacheFormCreateCourse = JSON.parse(
        localStorage.getItem(StorageKey.CACHE_FORM_CREATE_COURSE) || '{}'
    );

    useEffect(() => {
        if (resAddCourse) {
            history.push({
                pathname: `${LECTURERS_COURSE}/${resAddCourse.id}`,
                hash: '#content'
            });
        }
    }, [resAddCourse]);

    const onCancelClicked = () => {
        history.goBack();
    };

    //save cache
    const updateCacheForm = () => {
        localStorage.setItem(
            StorageKey.CACHE_FORM_CREATE_COURSE,
            JSON.stringify(form.getFieldsValue())
        );
    };

    return (
        <Container>
            <div style={{ padding: '0.5em', backgroundColor: 'white' }}>
                <Form
                    form={form}
                    labelAlign="left"
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 18 }}
                    hideRequiredMark={true}
                    onFieldsChange={updateCacheForm}
                    initialValues={cacheFormCreateCourse}
                >
                    <Row>
                        <Col sm={12}>
                            <div className="d-flex mt-3 mb-2">
                                <h3
                                    style={{ color: 'rgb(39, 54, 121)' }}
                                    className="mr-auto"
                                >
                                    {t('add_course')}
                                </h3>
                                <div className="list-action">
                                    <Space>
                                        <ButtonBack onClick={onCancelClicked} />
                                        <ButtonCreate
                                            onClick={() => onSubmit()}
                                        />
                                    </Space>
                                </div>
                            </div>
                        </Col>
                    </Row>

                    <CourseForm
                        form={form}
                        cache={cacheFormCreateCourse}
                        updateCacheForm={updateCacheForm}
                    />
                </Form>
            </div>
            {showMessageError && (
                <ModalError
                    messageError={messageError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </Container>
    );
}

export default CreateCourse;
