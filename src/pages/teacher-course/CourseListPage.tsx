import { BookOutlined, FileDoneOutlined } from '@ant-design/icons';
import { Tabs } from 'antd';
import ListClass from 'components/lecturers-question/ListClass';
import ListQuestion from 'components/lecturers-question/ListQuestion';
import constPaginate from 'constants/Paginate';
import { SEARCH_COURSE_TYPE } from 'constants/Search';
import queryString from 'query-string';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { Container } from 'reactstrap';
import CourseListApproval from './CourseListApproval';
import MyCourseListPage from './MyCourseListPage';

const { TabPane } = Tabs;

export interface Filter {
    search: string;
    categoryId: number;
    page: number;
    limit: number;
    status: number | '';
    createdBy: string;
    searchType?: string;
}

const filterDefault: Filter = {
    search: '',
    categoryId: 0,
    page: constPaginate.PAGE_DEFAULT,
    limit: constPaginate.LIMIT_DEFAULT,
    status: '',
    createdBy: '',
    searchType: SEARCH_COURSE_TYPE.CAN_APPROVE
};

const MY_COURSE_TAB = '#my-course';
const LIST_QUESTION_TAB = '#list-question';
const LIST_CLASS_TAB = '#list-class';
const LIST_POST_TAB = '#list-post';
const APPROVE_COURSE_TAB = '#approve-course';
function CourseListPage(): JSX.Element {
    const { t } = useTranslation();
    const history = useHistory();
    const didMount = useRef(false);
    const [tabKey, setTabKey] = useState(
        history.location.hash || MY_COURSE_TAB
    );
    const search = queryString.parse(history.location.search);

    const [filter, setFilter] = useState<Filter>({
        search: String(search?.search || ''),
        categoryId: Number(search?.categoryId || 0),
        page: Number(search?.page || constPaginate.PAGE_DEFAULT),
        limit: Number(search?.limit || constPaginate.LIMIT_DEFAULT),
        status: search?.status ? Number(search?.status) : '',
        createdBy: String(search.createdBy || ''),
        searchType: SEARCH_COURSE_TYPE.CAN_APPROVE
    });

    useEffect(() => {
        if (tabKey === MY_COURSE_TAB) {
            const { createdBy, searchType, ...newFilter } = filter;
            history.push({
                ...history.location,
                hash: tabKey,
                search: queryString.stringify(newFilter)
            });
        } else if (tabKey === LIST_QUESTION_TAB) {
            const { createdBy, searchType, ...newFilter } = filter;
            history.push({
                ...history.location,
                hash: tabKey,
                search: queryString.stringify(newFilter)
            });
        } else if (tabKey === LIST_CLASS_TAB) {
            const { createdBy, searchType, ...newFilter } = filter;
            history.push({
                ...history.location,
                hash: tabKey,
                search: queryString.stringify(newFilter)
            });
        } else {
            history.push({
                ...history.location,
                hash: tabKey,
                search: queryString.stringify(filter)
            });
        }
    }, [filter]);

    useEffect(() => {
        if (didMount.current) {
            setFilter(filterDefault);
        } else {
            didMount.current = true;
        }
    }, [tabKey]);

    const changeFilter = (filter: Filter) => {
        setFilter(filter);
    };

    return (
        <Container>
            <div style={{ padding: '0.5em', background: 'white' }}>
                <Tabs
                    type="card"
                    onChange={(key: string) => {
                        setTabKey(key);
                    }}
                    activeKey={tabKey}
                >
                    <TabPane
                        tab={
                            <div style={{ fontSize: 16 }}>
                                <BookOutlined />
                                {t('my_course')}
                            </div>
                        }
                        key={MY_COURSE_TAB}
                    >
                        <MyCourseListPage
                            filter={filter}
                            changeFilter={changeFilter}
                            change={tabKey === MY_COURSE_TAB}
                        />
                    </TabPane>
                    <TabPane
                        tab={
                            <div style={{ fontSize: 16 }}>
                                <FileDoneOutlined />
                                {t('approve_course')}
                            </div>
                        }
                        key={APPROVE_COURSE_TAB}
                    >
                        <CourseListApproval
                            filter={filter}
                            changeFilter={changeFilter}
                            change={tabKey === APPROVE_COURSE_TAB}
                        />
                    </TabPane>
                    <TabPane
                        tab={
                            <div style={{ fontSize: 16 }}>
                                <BookOutlined />
                                {t('list_question')}
                            </div>
                        }
                        key={LIST_QUESTION_TAB}
                    >
                        <ListQuestion />
                    </TabPane>
                    <TabPane
                        tab={
                            <div style={{ fontSize: 16 }}>
                                <BookOutlined />
                                {t('class')}
                            </div>
                        }
                        key={LIST_CLASS_TAB}
                    >
                        <ListClass />
                    </TabPane>
                </Tabs>
            </div>
        </Container>
    );
}

export default CourseListPage;
