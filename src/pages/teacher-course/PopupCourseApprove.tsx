import { Button, Modal } from 'antd';
import ButtonApprove from 'components/shares/button/ButtonApprove';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
interface Props {
    onConfirm: () => void;
    btnLabel: string;
    modalLabel: string;
    confirmLabel: string;
}
function PopupCourseApprove(props: Props): JSX.Element {
    const { t } = useTranslation();
    const [visible, setVisible] = useState<boolean>(false);
    const handleOk = () => {
        props.onConfirm();
        setVisible(false);
    };
    const handleCancel = () => setVisible(false);
    return (
        <>
            <ButtonApprove onClick={() => setVisible(true)} />
            <Modal
                title={props.modalLabel}
                visible={visible}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={
                    <div style={{ textAlign: 'center' }}>
                        <Button onClick={handleCancel}>{t('cancel')}</Button>
                        <Button onClick={handleOk} type="primary">
                            {t('accept')}
                        </Button>
                    </div>
                }
            >
                <p className="text-center">{props.confirmLabel}</p>
            </Modal>
        </>
    );
}

export default PopupCourseApprove;
