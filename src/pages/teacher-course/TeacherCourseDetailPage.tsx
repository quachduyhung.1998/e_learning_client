import { PlusOutlined } from '@ant-design/icons';
import { Button, Form, message, Space, Tabs } from 'antd';
import CourseForm from 'components/lecturers_course/CourseForm';
import PopupApprovalCourse from 'components/lecturers_course/PopupApprovalCourse';
import PopupNewLesson from 'components/lecturers_course/PopupNewLesson';
import TopicList from 'components/lecturers_course/TopicList';
import ContentCourse from 'components/review-course/ContentCourse';
import ButtonApprove from 'components/shares/button/ButtonApprove';
import ButtonBack from 'components/shares/button/ButtonBack';
import ButtonRequestApprove from 'components/shares/button/ButtonRequestApprove';
import ButtonUpdate from 'components/shares/button/ButtonUpdate';
import HeaderPage from 'components/shares/HeaderPage';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import useApproveCourse from 'hooks/course/useApproveCourse';
import useCourseFinish from 'hooks/course/useCourseFinish';
import useDetailCourse from 'hooks/course/useDetailCourse';
import useUpdateCourse from 'hooks/course/useUpdateCourse';
import useFetchTopic from 'hooks/useFetchTopic';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { COURSE_ALREADY_APPROVED, SUCCESS } from 'network/ResponseCode';
import React, { MouseEvent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, useHistory, useLocation, useParams } from 'react-router-dom';
import { Container } from 'reactstrap';
import { Course, CourseStatus } from 'types/Course';
import { AddTopicReq } from 'types/requests';
import { AddTopicRes } from 'types/responses';
import { Topic } from 'types/Topic';
import TitleComponent from 'components/text/TitleComponent';
import color from 'constants/Style';
const { TabPane } = Tabs;
import { LECTURERS_COURSE } from 'routes/web/paths';
import ModalError from 'components/shares/modal/ModalError';

interface Params {
    id: string;
}

export const OVERVIEW_TAB = '#overview';
export const CONTENT_TAB = '#content';

function TeacherCourseDetailPage(): JSX.Element {
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    const params: Params = useParams();
    const [loadingPage, setLoadingPage] = useState(true);
    const [messageError, setMessageError] = useState<string>('');
    const [detailCourse, setDetailCourse] = useState<Course>();
    const [isShowCreateTopicPopup, setShowCreateTopicPopup] = useState<boolean>(
        false
    );

    const history = useHistory();
    const { t } = useTranslation();
    const location = useLocation();
    const [onUpdateCourse, updateCourseError] = useUpdateCourse();
    const [
        onFetchInfoCourse,
        resCourseInfo,
        loading,
        detailCourseError
    ] = useDetailCourse();
    const [
        resCourseFinish,
        onCourseFinish,
        courseFinishError
    ] = useCourseFinish();
    const [
        resApproveCourse,
        onApproveCourse,
        courseApproveError
    ] = useApproveCourse();
    const courseId = params.id;
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (
            courseApproveError ||
            courseFinishError ||
            detailCourseError ||
            updateCourseError ||
            messageError
        ) {
            setShowMessageError(true);
        }
    }, [
        courseApproveError,
        courseFinishError,
        updateCourseError,
        detailCourseError,
        messageError
    ]);
    const [topics, setTopics, onFetchTopics] = useFetchTopic(courseId);

    const [form] = Form.useForm();

    useEffect(() => {
        setShowTabContent();
    }, []);

    const setShowTabContent = () => {
        const pathName = `${LECTURERS_COURSE}/${params.id}${CONTENT_TAB}`;
        if (location.pathname + hash == pathName) {
            setTabContent(true);
        }
    };

    useEffect(() => {
        if (courseId) onFetchInfoCourse({ id: courseId });
    }, [resCourseFinish, resApproveCourse]);

    useEffect(() => {
        const set = async () => {
            await setDetailCourse(resCourseInfo);
            setLoadingPage(false);
        };
        set();
    }, [resCourseInfo]);

    let hash = location.hash;
    const [showApprovalCourse, setShowApprovalCourse] = useState(false);
    const [tabContent, setTabContent] = useState(false);
    const [showSendCourseForApproval, setShowSendCourseForApproval] = useState(
        false
    );

    const [isViewContent, setIsViewContent] = useState(false);
    const [viewContentModule, setViewContentModule] = useState<{
        instanceId: number;
        type: string;
    }>();

    const renderButtonApprove = () => {
        if (
            detailCourse?.status === CourseStatus.STATUS_REVIEWING &&
            user &&
            detailCourse?.assignees?.includes(user._id)
        ) {
            return (
                <>
                    <ButtonApprove
                        onClick={() => setShowApprovalCourse(true)}
                    />

                    <PopupApprovalCourse
                        visible={showApprovalCourse}
                        courseId={detailCourse.id}
                        onConfirmApproveCourse={() => {
                            onApproveCourse({
                                courseId: detailCourse.id
                            });
                            setShowApprovalCourse(false);
                        }}
                        onFalseModal={() => setShowApprovalCourse(false)}
                    />
                </>
            );
        }
        return <div></div>;
    };

    if (!hash) {
        hash = OVERVIEW_TAB;
        return <Redirect to={location.pathname + OVERVIEW_TAB} />;
    }

    const onSubmit = () => {
        form.validateFields()
            .then((values) => {
                onUpdateCourse({
                    id: params.id,
                    code: values?.code,
                    name: values?.fullName.trim(),
                    course_category_id: values?.topicId,
                    description: values?.description ?? '',
                    image: values?.image,
                    is_sequence: values?.is_sequence
                });
            })
            .catch((e) => e);
    };

    const handleAddTopic = async (name: string) => {
        const topic = await addTopic(name);
        if (topic) {
            const newTopics = [...topics, topic];
            setTopics(newTopics);
            setIsViewContent(false);
        }
    };

    const addTopic = async (name: string): Promise<Topic | undefined> => {
        try {
            const res = await ApiHelper.post<AddTopicReq, AddTopicRes>(
                Endpoint.ADD_TOPIC,
                {
                    courseid: courseId,
                    name
                }
            );

            if (res.status === SUCCESS) {
                return res.data;
            } else if (res.code === COURSE_ALREADY_APPROVED) {
                setMessageError(`${t('messages.COURSE_ALREADY_APPROVED')}`);
                return;
            }
            return res.data;
        } catch (error) {}
        return undefined;
    };

    const onCancelClicked = (): void => {
        if (isViewContent) {
            setIsViewContent(false);
        } else {
            history.goBack();
        }
    };

    const onShowCreateTopicPopup = (e: MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        setShowCreateTopicPopup(true);
    };

    const handleConfirmCourse = () => {
        detailCourse && onCourseFinish({ id: detailCourse.id });
    };

    const handleCourseStatus = () => {
        if (
            detailCourse?.status === CourseStatus.STATUS_DRAFT
            // detailCourse?.creator?._id === user?._id
        ) {
            return (
                <>
                    <ButtonRequestApprove
                        onClick={() => setShowSendCourseForApproval(true)}
                    />

                    <PopupApprovalCourse
                        visible={showSendCourseForApproval}
                        courseId={detailCourse.id}
                        onConfirmApproveCourse={() => {
                            handleConfirmCourse();
                            setShowSendCourseForApproval(false);
                        }}
                        onFalseModal={() => setShowSendCourseForApproval(false)}
                        modalLabel={t('send_request_review')}
                        confirmLabel={t('are_you_sure_to_send_request')}
                    />
                </>
            );
        }
    };
    const renderUpdateButton = () => {
        if (
            hash === OVERVIEW_TAB &&
            // detailCourse?.creator &&
            // user &&
            // detailCourse?.creator?._id === user?._id &&
            detailCourse?.status !== CourseStatus.STATUS_APPROVED &&
            user?.id === detailCourse?.created_by?.id
        ) {
            return <ButtonUpdate onClick={onSubmit} />;
        }
    };

    const onViewModule = (instanceId: number, type: string) => {
        setIsViewContent(true);
        setViewContentModule({
            instanceId: instanceId,
            type
        });
    };

    const handleReloadTopic = () => {
        onFetchTopics(Number(courseId));
    };

    const operations = () => {
        if (
            detailCourse?.creator &&
            user &&
            detailCourse?.creator?._id === user?._id &&
            detailCourse?.status !== CourseStatus.STATUS_APPROVED
        )
            return (
                <Button
                    icon={<PlusOutlined />}
                    shape="round"
                    onClick={onShowCreateTopicPopup}
                    type="primary"
                >
                    {t('add_lesson')}
                </Button>
            );
    };
    return (
        <Container className="teacher-course-detail-page p-2">
            <HeaderPage
                title={
                    <TitleComponent
                        title={resCourseInfo?.name}
                        color={color.COLOR_MAIN}
                    />
                }
                action={
                    <Space>
                        <ButtonBack onClick={onCancelClicked} />
                        {renderUpdateButton()}

                        {handleCourseStatus()}

                        {renderButtonApprove()}
                    </Space>
                }
            />

            <Tabs
                tabBarGutter={0}
                style={{
                    backgroundColor: 'white',
                    marginTop: '1em',
                    minHeight: '70vh'
                }}
                tabBarExtraContent={tabContent ? operations() : null}
                defaultActiveKey={hash}
                type="card"
                onChange={(key: string) => {
                    if (key === CONTENT_TAB) {
                        setTabContent(true);
                    } else {
                        setTabContent(false);
                    }
                    history.replace(`${location.pathname}${key}`);
                }}
            >
                <TabPane tab={t('general_information')} key={OVERVIEW_TAB}>
                    <Container>
                        <Form
                            form={form}
                            labelAlign="left"
                            labelCol={{ md: { span: 6 }, sm: { span: 8 } }}
                            wrapperCol={{ md: { span: 18 }, sm: { span: 16 } }}
                            hideRequiredMark={true}
                        >
                            <CourseForm
                                form={form}
                                detailCourse={detailCourse}
                            />
                        </Form>
                    </Container>
                </TabPane>
                <TabPane
                    tab={t('course_content')}
                    key={CONTENT_TAB}
                    style={{ padding: '0.5em' }}
                >
                    <div style={{ overflow: 'hidden' }}>
                        {!isViewContent && (
                            <TopicList
                                topics={topics}
                                detailCourse={detailCourse}
                                courseId={courseId}
                                onViewModule={onViewModule}
                                onReLoadTopic={handleReloadTopic}
                            />
                        )}
                        {isViewContent && (
                            <ContentCourse
                                courseId={detailCourse?.id || 0}
                                activeModule={viewContentModule}
                            />
                        )}
                    </div>
                </TabPane>
            </Tabs>

            <PopupNewLesson
                hidePopup={() => setShowCreateTopicPopup(false)}
                showPopup={isShowCreateTopicPopup}
                onAddlesson={handleAddTopic}
            />
            {showMessageError && (
                <ModalError
                    messageError={
                        courseApproveError ||
                        courseFinishError ||
                        detailCourseError ||
                        updateCourseError ||
                        messageError
                    }
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </Container>
    );
}

export default TeacherCourseDetailPage;
