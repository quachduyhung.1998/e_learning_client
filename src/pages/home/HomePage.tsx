/* eslint-disable react/prop-types */
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import MyCoursePage from 'pages/my-course/MyCoursePage';
import React from 'react';
import { Redirect, RouteComponentProps } from 'react-router-dom';
import { LOGIN } from 'routes/web/paths';
import { RoleType } from 'types/User';
import { getRoleByRoute } from 'helper/Utils';

interface Props extends RouteComponentProps {}

function HomePage(props: Props): JSX.Element {
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    // const role = getRoleByRoute();

    if (!user) {
        return <Redirect exact to={LOGIN} />;
    }

    const renderContent = () => {
        // switch (role) {
        //     case RoleType.STUDENT:
        return <MyCoursePage />;
        // case RoleType.TEACHER:
        //     return <Redirect exact to="/teacher" />;
        // default:
        //     return <Redirect exact to="/admin" />;
    };
    // };

    return <>{renderContent()}</>;
}

export default HomePage;
