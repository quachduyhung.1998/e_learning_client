import React from 'react';
import CourseListPage from 'pages/teacher-course/CourseListPage';
function HomeTeacherPage(): JSX.Element {
    return <CourseListPage />;
}

export default HomeTeacherPage;
