import React from 'react';
import { Link } from 'react-router-dom';
export interface IPageNotFound {}

const NotFoundPage: React.FC<IPageNotFound> = () => {
    return (
        <div>
            404 Not Found <Link to="/">Homepage</Link>
        </div>
    );
};

export default NotFoundPage;
