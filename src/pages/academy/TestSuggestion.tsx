import { Button, Col, Progress, Row, Space } from 'antd';
import {
    TYPE_CONTENT_HTML,
    TYPE_CONTENT_IMAGE,
    TYPE_CONTENT_VIDEO,
    TYPE_MULTI,
    TYPE_SINGLE
} from 'constants/SettingQuestion';
import LocalStorageHelper from 'helper/LocalStorageHelper';
import { ToastSuccess } from 'helper/Toast';
import { loginNavigation } from 'helper/Utils';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { RatedCapacityStyle } from 'pages/knowledge-test/KnowledgeTestPage.jss';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { Modal, ModalBody } from 'reactstrap';
import { PlacementExam } from 'types/responses/PlacementExam';
import { Answers, Question } from 'types/responses/RatedCapacityResponse';
import { Role, User } from 'types/User';
import constStyle from 'constants/Style';
import './TestSuggestion.scss';
import images from 'images';
interface Props {
    list: Question[];
    timeDown: number;
    user: User;
    role: string;
    onFinish: () => void;
}
function TestSuggestion(props: Props): JSX.Element {
    const history = useHistory();
    const data = props.list;
    const [step, setStep] = useState<number>(0);
    const { t } = useTranslation();
    const [timeDown, setTimeDown] = useState({
        minutes: props.timeDown / 60,
        seconds: 0
    });

    const [realSecondTime, setrealSecondTime] = useState<number>(0);
    const [disableButton, setDisableButton] = useState<boolean>(false);
    const [showResult, setShowResult] = useState<boolean>(false);
    const dataQuestion: { id: number; answers: number[] }[] = [];
    const [numberOfAnswered, setNumberOfAnswered] = useState(0);

    data.map((index, key) => {
        dataQuestion[key] = {
            id: index.id,
            answers: []
        };
    });
    const [questionSelected, setquestionSelected] = useState<
        { id: number; answers: number[] }[]
    >(dataQuestion);

    // Click sang câu hỏi tiếp theo
    const handleNextTask = (e: any) => {
        e.preventDefault();
        setStep(step + 1);
        if (!questionSelected[step + 1]) {
            const answerTemp = {
                id: data[step + 1].id,
                answers: []
            };
            questionSelected[step + 1] = answerTemp;
            setquestionSelected(questionSelected);
        }
    };

    // Click trở về câu hỏi trước
    const handlePrevTask = (e: any) => {
        e.preventDefault();
        if (step <= 0) {
            return;
        }
        setStep(step - 1);
    };

    // Kiểu nội dung câu hỏi
    const showTypeContentQuestion = () => {
        if (data[step]?.type_content === TYPE_CONTENT_HTML) {
            return <h3>{data[step]?.content}</h3>;
        } else if (data[step]?.type_content === TYPE_CONTENT_IMAGE) {
            return <img src={data[step]?.content} style={{ width: '300px' }} />;
        } else if (data[step]?.type_content === TYPE_CONTENT_VIDEO) {
            return (
                <div
                    className="embed-responsive embed-responsive-21by9"
                    style={{ margin: 'auto', maxWidth: '500px' }}
                >
                    <iframe
                        className="embed-responsive-item"
                        src={data[step]?.content}
                    ></iframe>
                </div>
            );
        }
    };

    // Kiểu nội dung câu trả lời
    const TypeAnswers = (type_content: string, content: string) => {
        if (type_content === TYPE_CONTENT_HTML) {
            return content;
        } else if (type_content === TYPE_CONTENT_IMAGE) {
            return <img src={content} alt="" style={{ width: '100px' }} />;
        } else if (type_content === TYPE_CONTENT_VIDEO) {
            return (
                <div
                    className="embed-responsive embed-responsive-21by9"
                    style={{ width: '100px' }}
                >
                    <iframe
                        className="embed-responsive-item"
                        src={content}
                    ></iframe>
                </div>
            );
        }
    };

    // Xử lý phần selected các câu trả lời của từng câu hỏi
    const handleChangeselectedAnswers = (e: any) => {
        const id = parseInt(e.target.value);
        const questionSelectedTemp = [...questionSelected];
        let answersTemp = questionSelectedTemp[step].answers;
        if (data[step].type === TYPE_MULTI) {
            if (answersTemp.indexOf(id) === -1) {
                answersTemp.push(id);
            } else {
                answersTemp = answersTemp.filter((i: number) => i !== id);
            }
        } else {
            answersTemp = [id];
        }

        questionSelectedTemp[step].answers = answersTemp;
        setquestionSelected(questionSelectedTemp);
        const arrayAnswered = questionSelected.filter((question) => {
            return question.answers.length > 0;
        });
        arrayAnswered && setNumberOfAnswered(arrayAnswered?.length);
    };

    const classes = RatedCapacityStyle();

    // Các câu trả lời trong từng câu hỏi
    const listTypeAnswers = () => {
        return (
            <Row gutter={[8, 8]}>
                {' '}
                {data[step]?.answers.map((index: Answers) => {
                    return (
                        <Col key={index.id} sm={12} className=" mt-4 mb-4">
                            <div
                                className={
                                    'custom-control ' +
                                    (data[step]?.type === TYPE_SINGLE
                                        ? 'custom-radio'
                                        : 'custom-checkbox')
                                }
                            >
                                <input
                                    type={
                                        data[step]?.type === TYPE_SINGLE
                                            ? 'radio'
                                            : 'checkbox'
                                    }
                                    className="custom-control-input"
                                    id={'answers' + index.id}
                                    onChange={handleChangeselectedAnswers}
                                    value={index.id}
                                    checked={
                                        questionSelected[step]?.answers.indexOf(
                                            index.id
                                        ) !== -1
                                    }
                                />
                                <label
                                    className="custom-control-label"
                                    htmlFor={`answers${index.id}`}
                                >
                                    {TypeAnswers(
                                        index.type_content,
                                        index.content
                                    )}
                                </label>
                            </div>
                        </Col>
                    );
                })}
            </Row>
        );
    };

    // Thao tác nộp bài
    const handlePostPlacementExam = async () => {
        props.onFinish && props.onFinish();
        const res: PlacementExam = await ApiHelper.post(
            Endpoint.PLACEMENT_EXAM,
            {
                data: questionSelected
            }
        );
        if (res.status === 200) {
            if (res.data.needPlacementExam === true) {
                setShowResult(true);
            } else {
                ToastSuccess({ message: t('success') });
                LocalStorageHelper.updateUserStorage({
                    ...props.user,
                    needPlacementExam: false
                });
                loginNavigation(props.user, props.role, history, true, true);
            }
        }
    };

    useEffect(() => {
        // Tính tổng thời gian đếm ngược làm bài tập
        const timeOut = setTimeout(() => {
            const { seconds, minutes } = timeDown;
            setrealSecondTime(realSecondTime + 1);
            if (seconds === 0) {
                setTimeDown({
                    minutes: minutes - 1,
                    seconds: 59
                });
            } else {
                setTimeDown({
                    ...timeDown,
                    seconds: seconds - 1
                });
            }
        }, 1000);
        return () => {
            clearTimeout(timeOut);
        };
    }, [timeDown]);

    useEffect(() => {
        if (realSecondTime === props.timeDown) {
            handlePostPlacementExam();
        }
    }, [realSecondTime]);

    const handleReTest = () => {
        window.location.reload();
    };

    const handleIgnore = () => {
        loginNavigation(props.user, props.role, history, true);
    };

    const renderResultTestModal = (): JSX.Element | null => {
        if (showResult) {
            return (
                <Modal centered isOpen={true} size="md">
                    <ModalBody>
                        <h3 className="p-4">{t('the_system_recommends')}</h3>
                        <div className="float-right">
                            <Button
                                color="danger"
                                className="mr-2"
                                onClick={handleReTest}
                            >
                                {t('begin')}
                            </Button>
                            <Button onClick={handleIgnore}>
                                {t('ignore')}
                            </Button>
                        </div>
                    </ModalBody>
                </Modal>
            );
        }
        return null;
    };

    return (
        <div
            className="test-suggestion-content"
            style={{ background: 'white', padding: '10px' }}
        >
            {renderResultTestModal()}
            <div className="d-flex align-items-center progress-content">
                <div className="mr-3 number-question">
                    <span
                        style={{
                            whiteSpace: 'nowrap'
                        }}
                    >
                        {step + 1}/{data.length}
                    </span>
                </div>
                <div style={{ width: '100%' }}>
                    {/* {props.list.length && ( */}
                    <Progress
                        showInfo={false}
                        percent={(numberOfAnswered / props.list?.length) * 100}
                        status="active"
                        strokeColor={constStyle.COLOR_MAIN}
                    />
                    {/* )} */}
                </div>
                <div className="ml-3 time-todo">
                    {realSecondTime >= props.timeDown ? (
                        <div>{t('time_out')}</div>
                    ) : (
                        <div className="d-flex ">
                            <img src={images.ic_hourglass} alt="" />
                            {timeDown.minutes}:
                            {timeDown.seconds < 10
                                ? `0${timeDown.seconds}`
                                : timeDown.seconds}
                        </div>
                    )}
                </div>
            </div>

            <form onSubmit={handleNextTask}>
                <Row style={{ minHeight: '70vh' }}>
                    <input type="hidden" value={data[step]?.id} name="id" />

                    <Col sm={24}>
                        <h2 className="text-center">
                            {'Câu hỏi ' + (step + 1) + ': '}
                        </h2>
                        <div className="text-center">
                            {showTypeContentQuestion()}
                        </div>
                    </Col>
                    <Col sm={24} className="text-center">
                        {listTypeAnswers()}
                    </Col>
                </Row>
                <Row justify="end">
                    <Col style={{ textAlign: 'center' }}>
                        <Space>
                            {step > 0 ? (
                                <Button shape="round" onClick={handlePrevTask}>
                                    {t('previous_question')}
                                </Button>
                            ) : null}
                            {step < data.length - 1 ? (
                                <Button shape="round" danger htmlType="submit">
                                    {t('next_question')}
                                </Button>
                            ) : null}
                            {step >= 0 && step === data.length - 1 ? (
                                <Button
                                    type="primary"
                                    shape="round"
                                    onClick={handlePostPlacementExam}
                                    disabled={disableButton}
                                    danger
                                >
                                    {t('submit_test')}
                                </Button>
                            ) : null}
                        </Space>
                    </Col>
                </Row>
            </form>
        </div>
    );
}

export default TestSuggestion;
