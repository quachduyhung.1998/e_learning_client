import useFetchTestResult from 'hooks/test/useFetchTestResult';
import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Row, Col, Table, Menu, Button, Dropdown } from 'antd';
import { Link } from 'react-router-dom';
import qs from 'query-string';
import ButtonBack from 'components/shares/button/ButtonBack';
import { useTranslation } from 'react-i18next';
import { Container } from 'reactstrap';
interface Options {
    data: any;
}
function OverView(props: any) {
    const params: any = useParams();
    const query = qs.parse(window.location.search);
    const history = useHistory();
    const { t } = useTranslation();
    const {
        listResultTest,
        loading,
        onFetchListResultTest
    } = useFetchTestResult();

    useEffect(() => {
        if (params?.courseId)
            onFetchListResultTest({
                courseId: params?.courseId,
                user_id: query?.userId
            });
    }, [params?.courseId, query?.userId]);
    const onCancelClicked = () => {
        history.goBack();
    };
    const [menu] = useState(() => (option: Options) => {
        return (
            <Menu>
                <Menu.Item>
                    <Link
                        to={{
                            pathname: `/teacher/course/overview/test-detail`,
                            search: `test_id=${
                                option?.data?.id
                            }&number_time_worked=${
                                option?.data?.result?.number_time_worked
                            }${
                                query?.userId ? `&user_id=${query?.userId}` : ''
                            }`
                        }}
                    >
                        {t('see_detail')}
                    </Link>
                </Menu.Item>
            </Menu>
        );
    });
    const renderColums = [
        {
            title: t('num_order'),
            align: 'center' as any,
            dataIndex: 'id',
            render: (text: any, record: any, index: any) => {
                return index + 1;
            }
        },
        {
            title: 'Bài kiểm tra',
            align: 'center' as any,
            render: (record: any, index: any) => {
                return <span>{record?.name}</span>;
            }
        },
        {
            title: 'Lần làm bài',
            align: 'center' as any,
            render: (record: any, index: any) => {
                return <span>{record?.result?.number_time_worked}</span>;
            }
        },
        {
            title: 'Câu trả lời đúng',
            align: 'center' as any,
            render: (record: any, index: any) => {
                return (
                    <span>
                        {record?.result?.total_answer_right}/
                        {record?.total_question}
                    </span>
                );
            }
        },

        {
            title: 'Thời gian làm bài',
            align: 'center' as any,
            render: (record: any, index: any) => {
                const timework = `${Math.floor(
                    record?.result?.total_time_work / 60
                )} phút ${record?.result?.total_time_work % 60} giây`;
                return <span>{timework}</span>;
            }
        },
        {
            title: 'Điểm',
            align: 'center' as any,
            render: (record: any, index: any) => {
                return (
                    <span>
                        {record?.result?.mark_achieved}/{record?.total_mark}
                    </span>
                );
            }
        },
        {
            title: t('action'),
            dataIndex: 'id',
            align: 'center' as any,
            // eslint-disable-next-line react/display-name
            render: (id: string, record: any) => {
                return (
                    <>
                        <Dropdown
                            overlay={() =>
                                menu({
                                    data: record
                                })
                            }
                            placement="bottomCenter"
                        >
                            <Button>{t('action')}</Button>
                        </Dropdown>
                    </>
                );
            }
        }
    ];
    return (
        <Container className="bg-white ">
            <Row justify="center" className="my-3 bg-white">
                <Col sm={21} className="mt-3">
                    <h2 className="float-left title">
                        {t('Lịch sử kiểm tra')}
                    </h2>
                    <div className="float-right">
                        <ButtonBack onClick={onCancelClicked} />
                    </div>
                </Col>
            </Row>
            <Row justify="center">
                <Col md={21} className="mb-3">
                    <Table
                        bordered
                        dataSource={listResultTest || []}
                        columns={renderColums}
                        rowKey="id"
                    />
                </Col>
            </Row>
        </Container>
    );
}

export default OverView;
