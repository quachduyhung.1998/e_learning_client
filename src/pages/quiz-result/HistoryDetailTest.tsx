import { Checkbox, Col, Form, Row, Table } from 'antd';
import ButtonBack from 'components/shares/button/ButtonBack';
import useFetchTestResultDetail from 'hooks/test/useFetchTestResultDetail';
import qs from 'query-string';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { Container } from 'reactstrap';
function HistoryDetailTest(props: any) {
    const query = qs.parse(window.location.search);
    const { t } = useTranslation();
    const [form] = Form.useForm();
    const history = useHistory();
    const {
        listResultTest,
        loading,
        onFetchListResultTestDetail
    } = useFetchTestResultDetail();
    useEffect(() => {
        if (query?.number_time_worked && query?.test_id) {
            onFetchListResultTestDetail({
                test_id: query?.test_id,
                number_time_worked: query?.number_time_worked,
                user_id: query?.user_id
            });
        }
    }, [query?.test_id, query?.number_time_worked, query?.user_id]);
    const renderContentQuestion = (question: any): JSX.Element => {
        if (question) {
            const arr_student_ans: any = [];
            question?.student_answer?.key?.forEach((item: any) => {
                arr_student_ans.push(JSON.parse(item));
            });
            return (
                <>
                    <div
                        className="font-weight-bold"
                        dangerouslySetInnerHTML={{
                            __html: `${t('question')} ${question?.id}: ${
                                question?.name
                            }`
                        }}
                    ></div>
                    <Row style={{ padding: '1em' }}>
                        {question?.answers?.map(
                            (answer: any, index: number) => {
                                return (
                                    <Col key={index} span={12}>
                                        {
                                            <div style={{ display: 'flex' }}>
                                                <Checkbox
                                                    className={
                                                        answer?.right
                                                            ? 'check-box-answer-right'
                                                            : 'check-box-answer-not-correct'
                                                    }
                                                    value={index}
                                                    checked={arr_student_ans?.includes(
                                                        answer?.key
                                                    )}
                                                ></Checkbox>
                                                {'\u00A0\u00A0'}
                                                <div
                                                    dangerouslySetInnerHTML={{
                                                        __html: `${answer?.key}`
                                                    }}
                                                ></div>
                                            </div>
                                        }
                                    </Col>
                                );
                            }
                        )}
                    </Row>
                </>
            );
        }
        return <></>;
    };
    const renderColums = [
        {
            title: t('num_order'),
            align: 'center' as any,
            dataIndex: 'id',
            render: (text: any, record: any, index: any) => {
                return index + 1;
            }
        },
        {
            title: 'Bài kiểm tra',
            align: 'center' as any,
            render: (record: any, index: any) => {
                return <span>{record?.name}</span>;
            }
        },
        {
            title: 'Lần làm bài',
            align: 'center' as any,
            render: (record: any, index: any) => {
                return <span>{record?.result?.number_time_worked}</span>;
            }
        },
        {
            title: 'Câu trả lời đúng',
            align: 'center' as any,
            render: (record: any, index: any) => {
                return (
                    <span>
                        {record?.result?.total_answer_right}/
                        {record?.total_question}
                    </span>
                );
            }
        },

        {
            title: 'Thời gian làm bài',
            align: 'center' as any,
            render: (record: any, index: any) => {
                const timework = `${Math.floor(
                    record?.result?.total_time_work / 60
                )}phút${record?.result?.total_time_work % 60}giây`;
                return <span>{timework}</span>;
            }
        }
    ];
    const onCancelClicked = () => {
        history.goBack();
    };
    return (
        <Container>
            <Row justify="center" className="bg-white py-3">
                <Col sm={21} className="mt-3">
                    <h3
                        style={{ color: 'rgb(39 54 121 / 11)' }}
                        className="float-left title"
                    >
                        Tổng quan bài làm
                    </h3>
                    <div className="float-right ">
                        <ButtonBack onClick={onCancelClicked} />
                    </div>
                </Col>
                <Col md={21} className="mb-3">
                    <Table
                        bordered
                        dataSource={listResultTest ? [listResultTest] : []}
                        columns={renderColums}
                        pagination={false}
                        rowKey="id"
                    />
                </Col>
            </Row>
            <Row justify="center" className="bg-white py-3">
                <Col md={21} style={{ color: 'rgb(39 54 121 / 11)' }}>
                    <h3 style={{ color: 'rgb(39 54 121 / 11)' }}>
                        Chi tiết bài làm
                    </h3>
                </Col>
                <Col md={21} className="mt-3">
                    {listResultTest?.list_question?.map((item: any) => {
                        return renderContentQuestion(item);
                    })}
                </Col>
            </Row>
        </Container>
    );
}

export default HistoryDetailTest;
