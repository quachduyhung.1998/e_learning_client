import { Col, Form, Row, Space } from 'antd';
import PageCourseForm from 'components/lecturers-page-course/PageCourseForm';
import ButtonBack from 'components/shares/button/ButtonBack';
import ButtonCreate from 'components/shares/button/ButtonCreate';
import TitleComponent from 'components/text/TitleComponent';
import constPageCourse from 'constants/PageCourse';
import color from 'constants/Style';
import useCreatePageCourse from 'hooks/page-course/useCreatePageCourse';
import { CONTENT_TAB } from 'pages/teacher-course/TeacherCourseDetailPage';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useParams } from 'react-router-dom';
import { Container } from 'reactstrap';
import { LECTURERS_COURSE } from 'routes/web/paths';
import CreatePageCourseReq from 'types/requests/CreatePageCourseReq';
import ModalError from 'components/shares/modal/ModalError';

interface Params {
    courseId: string;
    sectionId: string;
}

function CreatePageCoursePage(): JSX.Element {
    const { t } = useTranslation();
    const [
        resCreatePageCourse,
        onCreatePageCourse,
        createPageCourseError
    ] = useCreatePageCourse();
    const history = useHistory();
    const params = useParams<Params>();
    const [form] = Form.useForm();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (createPageCourseError) {
            setShowMessageError(true);
        }
    }, [createPageCourseError]);
    useEffect(() => {
        if (resCreatePageCourse) {
            history.push(
                `${LECTURERS_COURSE}/${params?.courseId}${CONTENT_TAB}`
            );
        }
    }, [resCreatePageCourse]);

    const onSubmit = () => {
        form.validateFields()
            .then((values) => {
                const course = parseInt(params?.courseId || '');
                const section = parseInt(params?.sectionId || '');
                let newContent =
                    values?.type === 0 ? values.content : values?.video;

                let NewParams: CreatePageCourseReq = {
                    type: values.type,
                    name: values.name,
                    content: newContent,
                    course_id: course,
                    section: section
                };
                onCreatePageCourse(NewParams);
            })
            .then((err) => console.log(err));
    };

    const handleCancel = (
        e?: React.MouseEvent<HTMLElement, globalThis.MouseEvent>
    ) => {
        e && e.preventDefault();
        history.goBack();
    };

    if (!(params?.courseId && params?.sectionId)) {
        //Lỗi vì ko biết tạo page cho course, sessionId
        return <div>Error</div>;
    } else {
        return (
            <Container>
                <div style={{ padding: '0.5em', backgroundColor: 'white' }}>
                    <Form
                        form={form}
                        hideRequiredMark={true}
                        labelCol={{ sm: { span: 24 }, md: { span: 6 } }}
                        wrapperCol={{ sm: { span: 24 }, md: { span: 18 } }}
                        labelAlign="left"
                    >
                        <Row>
                            <Col sm={24}>
                                <div className="d-flex update-profile-wrapper">
                                    <TitleComponent
                                        color={color.COLOR_MAIN}
                                        title={t('create_lesson')}
                                    />
                                    <h3 className="mr-auto pt-2"></h3>
                                    <div className="p-2 list-action">
                                        <Space>
                                            <ButtonBack
                                                onClick={handleCancel}
                                            />
                                            <ButtonCreate onClick={onSubmit} />
                                        </Space>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <PageCourseForm
                            courseId={parseInt(params?.courseId)}
                            form={form}
                        />
                    </Form>
                </div>
                {showMessageError && (
                    <ModalError
                        messageError={createPageCourseError}
                        handleCancel={handleCancelMessage}
                    ></ModalError>
                )}
            </Container>
        );
    }
}

export default CreatePageCoursePage;
