import { Col, Form, Row, Space } from 'antd';
import PageCourseForm from 'components/lecturers-page-course/PageCourseForm';
import ButtonBack from 'components/shares/button/ButtonBack';
import ButtonUpdate from 'components/shares/button/ButtonUpdate';
import ModalError from 'components/shares/modal/ModalError';
import constPageCourse from 'constants/PageCourse';
import useGetPageCourse from 'hooks/page-course/useGetPageCourse';
import useUpdatePageCourse from 'hooks/page-course/useUpdatePageCourse';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useParams } from 'react-router-dom';
import { Container } from 'reactstrap';
import LocalStorageHelper, {
    StorageKey
} from '../../helper/LocalStorageHelper';

interface Params {
    courseId: string;
}

function UpdatePageCoursePage(): JSX.Element {
    const { t } = useTranslation();
    const history = useHistory();
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    const [pageCourse, onGetPageCourse] = useGetPageCourse();
    const [
        resUpdatePageCourse,
        onUpdatePageCourse,
        updatePageCourseError
    ] = useUpdatePageCourse();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (updatePageCourseError) {
            setShowMessageError(true);
        }
    }, [updatePageCourseError]);
    const params = useParams<Params>();
    const checkShowUpdateButton = () => {
        return;
    };
    const [form] = Form.useForm();
    useEffect(() => {
        onGetPageCourse({
            courseId: parseInt(params.courseId)
        });

        checkShowUpdateButton();
    }, [params?.courseId]);

    const onSubmit = () => {
        form.validateFields()
            .then((values) => {
                let newContent =
                    values?.type === 0 ? values.content : values?.video;

                onUpdatePageCourse({
                    name: values.name,
                    type: values.type,
                    content: newContent,
                    course_id: params?.courseId
                });
            })
            .then((err) => console.log(err));
    };

    useEffect(() => {
        if (resUpdatePageCourse) {
            onGetPageCourse({
                courseId: parseInt(params.courseId)
            });
        }
    }, [resUpdatePageCourse]);

    if (!pageCourse) {
        return <div></div>;
    }

    const onCancelClicked = (
        e?: React.MouseEvent<HTMLElement, globalThis.MouseEvent>
    ) => {
        e && e.preventDefault();
        history.goBack();
    };

    return (
        <Container>
            <div style={{ padding: '0.5em', backgroundColor: 'white' }}>
                <Form
                    form={form}
                    hideRequiredMark={true}
                    labelCol={{ sm: { span: 24 }, md: { span: 6 } }}
                    wrapperCol={{ sm: { span: 24 }, md: { span: 18 } }}
                    labelAlign="left"
                    initialValues={pageCourse}
                >
                    <Row>
                        <Col sm={24}>
                            <div className="d-flex">
                                <h3 className="mr-auto pt-3 pb-3">
                                    {t('info_page_course')}
                                </h3>
                                <div className="p-2 list-action">
                                    <Space>
                                        <ButtonBack onClick={onCancelClicked} />
                                        {pageCourse?.created_by?.id ? (
                                            pageCourse?.created_by?.id ===
                                                user?.id && (
                                                <ButtonUpdate
                                                    onClick={onSubmit}
                                                />
                                            )
                                        ) : (
                                            <ButtonUpdate onClick={onSubmit} />
                                        )}
                                    </Space>
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <PageCourseForm
                        pageCourse={pageCourse}
                        courseId={parseInt(params?.courseId)}
                        form={form}
                    />
                </Form>
            </div>
            {showMessageError && (
                <ModalError
                    messageError={updatePageCourseError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </Container>
    );
}

export default UpdatePageCoursePage;
