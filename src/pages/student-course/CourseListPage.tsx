import ListCourse from 'components/list-course/ListCourse';
import React from 'react';
import { Container } from 'reactstrap';

function CourseListPage(): JSX.Element {
    return (
        <Container>
            <div style={{ padding: '0.5em', backgroundColor: 'white' }}>
                <ListCourse typeList="my_course" getPageFromParams={true} />
            </div>
        </Container>
    );
}

export default CourseListPage;
