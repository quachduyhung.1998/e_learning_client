import {
    Form,
    Input,
    InputNumber,
    Space,
    Tabs,
    message,
    Select,
    DatePicker
} from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { Rule } from 'antd/lib/form';
import SetupExam from 'components/add-exam-topic/SetupExam';
import CKEditor from 'components/ckeditor/ckEditor';
import LoadingManager from 'components/loading/LoadingManager';
import ButtonBack from 'components/shares/button/ButtonBack';
import ButtonCreate from 'components/shares/button/ButtonCreate';
import ButtonUpdate from 'components/shares/button/ButtonUpdate';
import HeaderPage from 'components/shares/HeaderPage';
import LabelRequired from 'components/shares/LabelRequired';
import TitleComponent from 'components/text/TitleComponent';
import { VALIDATION } from 'constants/Constants';
import color from 'constants/Style';
import { ISO_DATE_TIME } from 'helper/DateTimeHelper';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import { format, getUrl } from 'helper/String';
import { ToastSuccess } from 'helper/Toast';
import { validateDateTime } from 'helper/Utils';
import useFetchTopic from 'hooks/useFetchTopic';
import { round } from 'lodash';
import moment from 'moment';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import { CONTENT_TAB } from 'pages/teacher-course/TeacherCourseDetailPage';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useParams } from 'react-router-dom';
import { Container } from 'reactstrap';
import { EXAM_TOPIC_DETAIL, LECTURERS_COURSE } from 'routes/web/paths';
import Module from 'types/Module';
import { AddQuizResponse, ShowCourseQuizResponse } from 'types/responses';
import { BaseResponse } from 'types/responses/BaseResponse';
import { Topic } from 'types/Topic';
import ExamQuestionPage from './../exam-question/ExamQuestionPage';
import './AddExamTopicPage.scss';
import useDetailCourse from 'hooks/course/useDetailCourse';
import { CourseStatus } from 'types/Course';
import ModalError from 'components/shares/modal/ModalError';
import useGetQuestionsOfQuiz from 'hooks/question/useGetQuestionsOfQuiz';
import useListCourse from 'hooks/course/useListCourse';
import * as qs from 'query-string';
import useDetailTest from 'hooks/test/useDetailTest';
const { TabPane } = Tabs;
/**
 * Type Date Time
 */
export interface DateTime {
    date: string;
    time: string;
}
interface Request {
    id?: string;
    course?: string | number;
    name: string;
    description: string;
    lesson_id?: string;
    list_question_id?: string;
    time_work: number;
    attempts?: number;
    gradePass?: number;
    timeOpen?: string;
    timeClose?: string;
    section?: number;
    courseModuleId?: number;
    conditions?: number[];
    time_start?: string;
    time_end?: string;
}
/**
 * Type Form Data
 */

const OVERVIEW_TAB = '#overview';
const SETTINGS_TAB = '#settings';
export const QUESTIONS_TAB = '#questions';

// Khai báo tab key mặc định ban đầu active
let tabKey = window.location.hash;

function AddExamTopicPage(): JSX.Element {
    const query = qs.parse(window.location.search);
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    const {
        resGetQuestionsNewPage,
        resGetQuestionsOfQuiz,
        setResGetQuestionsOfQuiz,
        resTotalQuestion,
        onGetQuestionsOfQuiz
    } = useGetQuestionsOfQuiz();

    const history = useHistory();
    const params = useParams<{
        courseId: string;
        sectionId: string;
        quizId: string;
    }>();
    const { t } = useTranslation();
    const [messageError, setMessageError] = useState<string>('');
    const [showQuizData, setQuizData] = useState<ShowCourseQuizResponse>();
    const [modules, setModules] = useState<Module[]>([]);

    const [checkTimeStart, setCheckTimeStart] = useState<boolean>(false);
    const [checkTimeEnd, setCheckTimeEnd] = useState<boolean>(false);
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    const [
        onFetchInfoCourse,
        resCourseInfo,
        loading,
        detailCourseError
    ] = useDetailCourse();
    useEffect(() => {
        if (params?.courseId) {
            onFetchInfoCourse({ id: params.courseId });
        }
    }, [params?.courseId]);
    useEffect(() => {
        if (messageError || detailCourseError) {
            setShowMessageError(true);
        }
    }, [messageError, detailCourseError]);

    const [form] = Form.useForm();
    const [onDetailTest, res, loadingDetail] = useDetailTest();
    useEffect(() => {
        if (query?.exam_id) onDetailTest({ id: query.exam_id.toString() });
    }, [query?.exam_id]);
    useEffect(() => {
        if (res) {
            form.setFieldsValue({
                description: res.description,
                lesson_id: res.lesson_id,
                list_question_id: JSON.parse(res.list_question_id),
                name: res.name,
                time_work: res.time_work,
                time_end: res.time_end ? moment(res.time_end) : undefined,
                time_start: res.time_start ? moment(res.time_start) : undefined
            });
        }
    }, [res]);
    useEffect(() => {
        onGetQuestionsOfQuiz({
            category: Number(resCourseInfo?.course_category_id) || 1
        });
    }, [resCourseInfo]);

    // Get các bài học/ bài kiểm tra của khóa họ
    const [topics, setTopics] = useFetchTopic(
        params?.courseId.toString() || '0'
    );

    const rules: Rule[] = [
        {
            required: true,
            message: t('this_is_required')
        },
        {
            pattern: /^\d+$/,
            message: t('Wrong_format_number')
        },
        ({ getFieldValue }) => ({
            validator(rule, value) {
                if (!value) {
                    return Promise.resolve();
                }

                const duration = getFieldValue('duration');
                if (Number(duration) < 1) {
                    return Promise.reject(t('time_do_quiz_invalid'));
                }

                return Promise.resolve();
            }
        })
    ];

    const onFinish = (values: any) => {
        if (!values) return;
        if (query?.exam_id) {
            updateQuiz({
                id: query?.exam_id.toString(),
                description: values.description,
                lesson_id: values.lesson_id,
                list_question_id: JSON.stringify(values.list_question_id),
                name: values.name,
                time_work: values.time_work,
                time_end: values?.time_end,
                time_start: values?.time_start
            });
        } else
            createQuiz({
                description: values.description,
                lesson_id: values.lesson_id,
                list_question_id: JSON.stringify(values.list_question_id),
                name: values.name,
                time_work: values.time_work,
                time_end: values?.time_end,
                time_start: values?.time_start
            });
    };

    /**
     * Gửi yêu cầu cập nhật lại thông tin bài kiểm tra cho server
     * @param data Request params
     *
     */

    /**
     * Gửi yêu cầu tạo mới thông tin bài kiểm tra cho server
     * @param data Request params
     *
     */
    const createQuiz = async (data: Request) => {
        const res = await ApiHelper.post<Request, AddQuizResponse>(
            Endpoint.QUIZ_CREATE,
            data
        );
        LoadingManager.hide();
        try {
            if (res.status === SUCCESS) {
                ToastSuccess({ message: t('test_successfully') });
                const path = getUrl(EXAM_TOPIC_DETAIL, {
                    courseId: data.course,
                    quizId: res.data.id
                });
                LocalStorageHelper.remove(StorageKey.CACHE_FORM_CREATE_EXAM);
                history.goBack();
            } else {
                setMessageError(`${t('test_failed')}`);
            }
        } catch (error) {
            setMessageError(`${t('test_failed')}`);
        }
    };
    const updateQuiz = async (data: Request) => {
        const res = await ApiHelper.put<Request, AddQuizResponse>(
            Endpoint.QUIZ_CREATE + `/${data?.id}`,
            data
        );
        LoadingManager.hide();
        try {
            if (res.status === SUCCESS) {
                ToastSuccess({ message: 'Cập nhật thành công' });

                LocalStorageHelper.remove(StorageKey.CACHE_FORM_CREATE_EXAM);
                history.goBack();
            } else {
                setMessageError(`${t('test_failed')}`);
            }
        } catch (error) {
            setMessageError(`${t('test_failed')}`);
        }
    };

    const cacheForm = LocalStorageHelper.getObject(
        StorageKey.CACHE_FORM_CREATE_EXAM
    );

    //cache form
    const updateCacheFormCreate = () => {
        if (!params?.quizId) {
            LocalStorageHelper.saveObject(
                StorageKey.CACHE_FORM_CREATE_EXAM,
                form.getFieldsValue()
            );
        }
    };

    const onCancelClicked = (
        e?: React.MouseEvent<HTMLElement, globalThis.MouseEvent>
    ) => {
        e && e?.preventDefault();
        history.push(`${LECTURERS_COURSE}/${params?.courseId}${CONTENT_TAB}`);
    };

    const onChangeEnableDate = (name: string) => (e: CheckboxChangeEvent) => {
        if (!e.target.checked) {
            form.setFieldsValue({
                [name]: undefined
            });
        } else {
            form.setFieldsValue({
                [name]: moment()
            });
        }

        if (name === 'timeOpen') {
            setCheckTimeStart(e.target.checked);
        } else {
            setCheckTimeEnd(e.target.checked);
        }
    };

    const submitForm = () => {
        form.validateFields()
            .then((values) => {
                onFinish(values);
            })
            .catch((err) => err);
    };

    const renderOverviewTab = () => {
        return (
            <>
                <Form.Item
                    label={<LabelRequired label={t('name_test')} />}
                    name="name"
                    rules={[
                        {
                            required: true,
                            message: t('this_is_required')
                        },
                        ({ getFieldValue }) => ({
                            validator(rule, value) {
                                if (!value) {
                                    return Promise.resolve();
                                }

                                const name = getFieldValue('name');

                                if (
                                    name.trim().length <
                                        VALIDATION.MIN_QUIZ_NAME_LENGTH ||
                                    name.trim().length >
                                        VALIDATION.MAX_QUIZ_NAME_LENGTH
                                ) {
                                    return Promise.reject(t('max_quiz_length'));
                                }

                                return Promise.resolve();
                            }
                        })
                    ]}
                >
                    <Input style={{ width: '100%', fontWeight: 'bold' }} />
                </Form.Item>

                <Form.Item
                    label={<LabelRequired label={t('duration_exam')} />}
                    name="time_work"
                    rules={[
                        {
                            required: true,
                            message: t('duration_exam')
                        }
                    ]}
                >
                    <InputNumber
                        className="antd-input-number-bold"
                        style={{ width: '100%' }}
                        min={1}
                    />
                </Form.Item>
                <Form.Item
                    label={<LabelRequired label={t('lesson')} />}
                    name="lesson_id"
                    rules={[
                        {
                            required: true,
                            message: t('lesson')
                        }
                    ]}
                >
                    <Select>
                        {resCourseInfo?.lessons?.map((item) => {
                            return (
                                <Select.Option value={item.id} key={item.id}>
                                    {item.name}
                                </Select.Option>
                            );
                        })}
                    </Select>
                </Form.Item>
                <Form.Item
                    label={<LabelRequired label={t('question')} />}
                    name="list_question_id"
                    rules={[
                        {
                            required: true,
                            message: t('question')
                        }
                    ]}
                >
                    <Select mode="multiple">
                        {resGetQuestionsNewPage?.map((item) => {
                            return (
                                <Select.Option value={item.id} key={item.id}>
                                    {item.name}
                                </Select.Option>
                            );
                        })}
                    </Select>
                </Form.Item>
                <Form.Item label="Thời gian bắt đầu" name="time_start">
                    <DatePicker format={format.time.HH_mm_yyyy_mm_dd} />
                </Form.Item>
                <Form.Item label="Thời gian kết thúc" name="time_end">
                    <DatePicker format={format.time.HH_mm_yyyy_mm_dd} />
                </Form.Item>
                <Form.Item
                    label={t('description')}
                    name="description"
                    className="ckh-200"
                    rules={[
                        {
                            transform: (value: string) => {
                                const el = window.document.createElement('div');
                                el.innerHTML = value;
                                return el.textContent;
                            },
                            max: 1000,
                            message: t('desc_exam_length_max_invalidate', {
                                max: 1000
                            })
                        }
                    ]}
                >
                    <Input hidden style={{ width: '100%' }} />
                    <CKEditor
                        onChange={(event: any, editor: any) => {
                            form.setFieldsValue({
                                description: editor.getData()
                            });
                            updateCacheFormCreate();
                        }}
                        data={
                            // showQuizData?.data?.description ||
                            cacheForm.description
                        }
                    />
                </Form.Item>
            </>
        );
    };

    return (
        <Container>
            <HeaderPage
                background={'white'}
                borderLeft={false}
                title={
                    params?.quizId ? (
                        <TitleComponent
                            title={showQuizData?.data?.name}
                            color={color.COLOR_MAIN}
                        />
                    ) : (
                        <TitleComponent
                            title={
                                !query?.exam_id
                                    ? t('create_exam')
                                    : t('update_exam')
                            }
                            color={color.COLOR_MAIN}
                        />
                    )
                }
                action={
                    <Space>
                        <ButtonBack onClick={onCancelClicked} />
                        {user?.id === resCourseInfo?.created_by?.id ? (
                            <Space>
                                {!query?.exam_id ? (
                                    <ButtonCreate
                                        onClick={() => submitForm()}
                                    />
                                ) : (
                                    <ButtonUpdate
                                        onClick={() => submitForm()}
                                    />
                                )}
                            </Space>
                        ) : (
                            <></>
                        )}
                    </Space>
                }
            />
            <Form
                style={{ marginTop: '1em' }}
                form={form}
                hideRequiredMark={true}
                labelCol={{ sm: { span: 24 }, md: { span: 6 } }}
                wrapperCol={{ sm: { span: 24 }, md: { span: 18 } }}
                labelAlign="left"
                onFieldsChange={updateCacheFormCreate}
                initialValues={showQuizData}
            >
                <Tabs
                    style={{
                        background: 'white',
                        padding: '0.5em',
                        minHeight: '70vh'
                    }}
                    defaultActiveKey={tabKey}
                    onChange={(key: string) => {
                        history.replace(`${location.pathname}${key}`);
                        tabKey = key;
                    }}
                    type="card"
                >
                    <TabPane
                        forceRender
                        tab={t('general_information')}
                        key={OVERVIEW_TAB}
                    >
                        {renderOverviewTab()}
                    </TabPane>
                </Tabs>
            </Form>
            {showMessageError && (
                <ModalError
                    messageError={messageError || detailCourseError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </Container>
    );
}

export default React.memo(AddExamTopicPage);
