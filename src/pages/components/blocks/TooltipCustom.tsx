import { Tooltip } from 'antd';
import { sliceString } from 'helper/String';
import React from 'react';
interface Props {
    length: number;
    text: string;
    color?: string;
    className?: string;
}
function TooltipCustom(props: Props) {
    const { text, length, color, className } = props;
    return text ? (
        <Tooltip title={text} className={className}>
            <span style={{ color: color }}>{sliceString(text, length)}</span>
        </Tooltip>
    ) : (
        <span></span>
    );
}

export default TooltipCustom;
