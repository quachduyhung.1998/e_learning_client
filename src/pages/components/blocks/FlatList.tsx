import { useScroll } from 'ahooks';
import React, { CSSProperties, useEffect, useRef } from 'react';
// import Loading from './Loading';

interface Props {
    style?: CSSProperties;
    children?: JSX.Element | React.ReactNode | React.ReactNodeArray;
    onNext?: () => void;
    bottomNext?: number;
    className?: string;
    loading?: boolean;
    reverseScroll?: true;
}

function FlatList(props: Props) {
    const {
        style,
        children,
        onNext,
        bottomNext,
        className,
        reverseScroll
    } = props;
    const ref = useRef<any>();
    const scroll = useScroll(ref);
    useEffect(() => {
        const max = ref?.current?.scrollHeight - ref?.current?.offsetHeight;
        if (reverseScroll && max) {
            if (scroll?.top === 0) {
                onNext && onNext();
            }
        } else {
            if (scroll.top && (scroll.top || 0) + (bottomNext || 0) >= max) {
                onNext && onNext();
            }
        }
    }, [scroll]);
    return (
        <div
            ref={ref}
            style={{
                maxHeight: '82vh',
                padding: '.5em 0',
                overflow: 'hidden scroll',
                ...style
            }}
            className={className}
        >
            {children}
            {/* {props?.loading && (
                <div>
                    <LoadingManager />
                </div>
            )} */}
        </div>
    );
}

export default FlatList;
