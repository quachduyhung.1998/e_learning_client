import { Col, Row, Select } from 'antd';
import CourseSearchListComponent from 'components/course-search/CourseSearchListComponent';
import { SEARCH_COURSE_TYPE } from 'constants/Search';
import useListCourse from 'hooks/course/useListCourse';
import useQuery from 'hooks/route/useQuery';
import useFetchCategories from 'hooks/useFetchCategories';
import React, { useEffect, useState, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, useHistory } from 'react-router-dom';
import { Container } from 'reactstrap';
import { HOME, TEACHER_SEARCH_COURSE, SEARCH_COURSE } from 'routes/web/paths';
import { Category } from 'types/Category';
import { Course, CourseStatus } from 'types/Course';
import constPaginate from 'constants/Paginate';
import { getRoleByRoute } from 'helper/Utils';
import { RoleType } from 'types/User';

function SearchCoursePage(): JSX.Element {
    const { t } = useTranslation();
    const query = useQuery();
    const history = useHistory();
    const keyword = query.get('query') || '';
    const page = Number(query.get('page'));
    const role = getRoleByRoute();
    const [onFetchCourses, resCourses] = useListCourse();
    const [categories, setCategories] = useState<Category[]>([]);
    const [onFetchCategories, resFetchCategories] = useFetchCategories();
    const [prefixPath, setPrefixPath] = useState('');
    const [filter, setFilter] = useState({
        limit: constPaginate.LIMIT_DEFAULT,
        searchType: SEARCH_COURSE_TYPE.GLOBAL,
        search: keyword,
        page,
        categoryId: 0,
        status: CourseStatus.STATUS_APPROVED.toString()
    });

    const [listCourse, setListCourse] = useState<Course[]>([]);
    const [total, setTotal] = useState<number>(0);
    const [totalPage, setTotalPage] = useState<number>(0);

    const [indexView, setIndexView] = useState<{
        start: number;
        end: number;
    }>({ start: 0, end: 0 });

    useEffect(() => {
        onFetchCategories();
        role && role === RoleType.TEACHER
            ? setPrefixPath(TEACHER_SEARCH_COURSE)
            : setPrefixPath(SEARCH_COURSE);
    }, []);

    useEffect(() => {
        const newCategories: Category[] = [];
        resFetchCategories.forEach((element) => {
            newCategories.push(element);
            if (element.children.length) {
                element.children.forEach((item) => {
                    newCategories.push(item);
                });
            }
        });
        setCategories(newCategories);
    }, [resFetchCategories]);

    useEffect(() => {
        setFilter({
            ...filter,
            page,
            search: keyword
        });
    }, [page, keyword]);

    useEffect(() => {
        onFetchCourses(filter);
    }, [filter]);

    useEffect(() => {
        if (resCourses) {
            setListCourse(resCourses.results);
            setTotal(resCourses.total);
            setTotalPage(resCourses.totalPage);
            setIndexView({
                start: (filter.page - 1) * filter.limit + 1,
                end: filter.page * filter.limit
            });
        }
    }, [resCourses]);

    if (page < 1 && prefixPath) {
        return <Redirect exact to={`${prefixPath}/?query=${keyword}&page=1`} />;
    }

    if (!keyword || keyword.length === 0) {
        return <Redirect to={HOME} />;
    }

    const onPageChange = ({ selected }: { selected: number }) => {
        history.push(`${prefixPath}/?query=${keyword}&page=${selected + 1}`);
    };

    const onChangeCategory = (value: number) => {
        setFilter({ ...filter, categoryId: value });
    };

    const renderInfo = () => {
        if (!total) {
            return (
                <p style={{ textAlign: 'right' }}>
                    {`${total} ${t('results_search_for')} "`}
                    <span style={{ fontWeight: 'bold' }}>
                        {query.get('query')}
                    </span>
                    &quot;
                </p>
            );
        }
        return (
            <p style={{ textAlign: 'right' }}>
                {`${t('show')} ${indexView.start}- ${indexView.end} ${t(
                    'of'
                )} ${total} ${t('results_search_for')} "`}
                <span style={{ fontWeight: 'bold' }}>{query.get('query')}</span>
                &quot;
            </p>
        );
    };

    return (
        <Container>
            <div style={{ background: 'white' }}>
                <Row justify="space-between" style={{ padding: '0.5em' }}>
                    <Col sm={{ span: 6 }}>
                        <Select
                            style={{ width: '100%' }}
                            onChange={onChangeCategory}
                            defaultValue={0}
                        >
                            <Select.Option value={0}>
                                {t('theme')}
                            </Select.Option>
                            {categories &&
                                categories.map((item: Category) => {
                                    return (
                                        <Select.Option
                                            key={item.id}
                                            value={item.id}
                                        >
                                            {item.depth === 1
                                                ? item.name
                                                : `\u00A0\u00A0${item.name}`}
                                        </Select.Option>
                                    );
                                })}
                        </Select>
                    </Col>
                    <Col>{renderInfo()}</Col>
                </Row>

                <CourseSearchListComponent
                    onPageChange={onPageChange}
                    page={Number(page)}
                    query={keyword}
                    totalPage={totalPage}
                    listCourse={listCourse}
                    total={total}
                />
            </div>
        </Container>
    );
}

export default SearchCoursePage;
