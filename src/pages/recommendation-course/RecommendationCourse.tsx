import React from 'react';
import { Container } from 'reactstrap';
import ListCourse from 'components/list-course/ListCourse';

function RecommendationCourse(): JSX.Element {
    return (
        <Container>
            <div style={{ padding: '0.5em', backgroundColor: 'white' }}>
                <ListCourse
                    typeList="recommendation_course"
                    getPageFromParams={true}
                />
            </div>
        </Container>
    );
}

export default RecommendationCourse;
