import { ConfigProvider } from 'antd';
import vnVN from 'antd/es/locale/vi_VN';
import Loading from 'layout/common/Loading';
import RootLayout from 'layout/RootLayout';
import { default as React, Suspense } from 'react';
import { BrowserRouter, Switch, useRouteMatch } from 'react-router-dom';
import routes from 'routes';
import { IRoute } from 'types/Route';
import './App.css';
import './index.scss';
(window as any).Pusher = require('pusher-js');
const App: React.FC<any> = () => {
    return (
        <ConfigProvider locale={vnVN}>
            <BrowserRouter>
                <RootLayout>{renderRoutes(routes)}</RootLayout>
            </BrowserRouter>
        </ConfigProvider>
    );
};

export default App;

export const renderRoutes = (configRoutes: IRoute[]) => {
    return (
        <Suspense fallback={<Loading style={{ minHeight: '100vh' }} />}>
            <Switch>
                {configRoutes &&
                    configRoutes.length &&
                    configRoutes.map((route: IRoute, index: number) => {
                        return (
                            <route.route
                                key={route.path}
                                path={route.path}
                                exact={
                                    route?.exact === undefined
                                        ? false
                                        : route?.exact
                                }
                                component={route.component}
                            />
                        );
                    })}
            </Switch>
        </Suspense>
    );
};
