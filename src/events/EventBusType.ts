export interface BaseEventType {
    type: EventBusName;
}

export enum EventBusName {
    LOGOUT_EVENT = 'LOGOUT_EVENT',
    SHOW_LOADING_EVENT = 'SHOW_LOADING_EVENT',
    HIDE_LOADING_EVENT = 'HIDE_LOADING_EVENT'
}

export interface LogoutPayloadType extends BaseEventType {}
