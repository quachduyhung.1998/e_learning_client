import { configureStore } from '@reduxjs/toolkit';
import authReducer from 'reducer/authReducer';

const rootReducer = {
    auth: authReducer
};

const store = configureStore({
    reducer: rootReducer
});

export type RootState = ReturnType<typeof store.getState>;

export default store;
