export interface UpdateAnswerQuestionRevisionReq {
    answerCorrect: number[];
    moodleQuestionId: number;
    revisionId: string;
    thinkingDuration: number;
    type?: string;
}
