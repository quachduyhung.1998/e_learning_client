import { Gender } from 'types/User';

export interface UpdateUserReq {
    age?: string;
    gender?: Gender;
    interesting?: string;
    longTermGoal?: string;
    shortTermGoal?: string;
    weakPoint?: string;
    strongPoint?: string;
    academicLevel?: string;
    avatar?: string;
}

export interface ListUserReq {
    courseId?: string;
    search?: string;
    text?: string;
    limit?: number;
    page?: number;
    role?: string;
    groupId?: string;
    excludedUserIds?: string[];
}

export interface ListUserSearchUnerolledReq {
    text?: string;
    courseId?: string;
    limit: number;
    page: number;
}

export interface RemoveStudentsCourseReq {
    courseId: number;
    userId: string;
    collectionType: string;
}

export interface AssignStudentReq {
    courseId: number;
    userIds: string[];
    groupIds: string[];
}

export interface InforUserReq {
    _id: string;
}
