export default interface GetCourseInJoinReq {
    page: number;
    limit: number;
}
