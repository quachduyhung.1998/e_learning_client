export interface GetQuestionsStartedQuizReq {
    attemptId: number;
    page: number;
}
