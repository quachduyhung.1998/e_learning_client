export interface ListQuestionReq {
    quizId: string;
}

export interface AddQuestionReq {
    courseId: number;
    quizId: number;
    name: string;
    answerCorrect: number[];
    questionText: string;
    single: number;
    answer: string[];
}

export interface UpdateQuestionReq {
    id: number;
    courseId: number;
    quizId: number;
    name: string;
    answerCorrect: number[];
    questionText: string;
    single: number;
    answer: string[];
}

export interface DetailQuestionReq {
    id?: string;
}
