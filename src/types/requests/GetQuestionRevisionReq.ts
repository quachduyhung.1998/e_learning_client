export interface GetQuestionRevisionReq {
    revisionId: string;
    page?: number;
}
