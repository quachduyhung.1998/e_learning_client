export default interface GetHistoryStartedQuizReq {
    quizId: number;
    userId: number;
}
