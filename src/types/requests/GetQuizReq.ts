export default interface GetQuizReq {
    courseId: number;
    instanceId: number;
}
