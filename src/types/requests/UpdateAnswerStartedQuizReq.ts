export interface UpdateAnswerStartedQuizReq {
    attemptId: number;
    uniqueId: number;
    number: number;
    answerCorrect: number[];
    single: number;
    sequencecheck: number;
    finishAttempt: number;
    timeUp: number;
}
