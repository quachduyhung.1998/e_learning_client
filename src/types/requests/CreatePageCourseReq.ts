export default interface CreatePageCourseReq {
    type: string;
    name: string;
    intro?: string;
    content: string;
    course_id: number;
    section: number;
    conditions?: number[];
}
