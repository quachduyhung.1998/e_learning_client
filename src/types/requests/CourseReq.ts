import { CourseStatus } from '../Course';
import { SearchType } from '../Base';
/**
 * Params thêm mới khóa học
 */
export interface AddCourseReq {
    image: string;
    name: string;
    code: string;
    course_category_id: number;
    description: string;
    is_sequence?: number;
}

/**
 * Params lấy thông tin chi tiết của khóa học
 */
export interface InfoCourseReq {
    id: string;
}

/**
 * Params cập nhật khóa học
 */
export interface UpdateCourseReq {
    id: string;
    image?: string;
    course_category_id: string;
    name?: string;
    code?: string;
    description: string;
    is_sequence?: number;
}

export interface ListCourseReq {
    search?: string | null;
    categoryId?: number;
    page?: string | number;
    status?: string | number;
    limit?: string | number;
    createdBy?: string;
    searchType?: string;
}

/**
 * Params xóa khóa học
 */
export interface DeleteCourseReq {
    courseId: number;
}
/**
 * Params cập nhật tương tác khóa học
 */
export interface UpdateInteractiveReq {
    courseId: number;
    userId: string[];
}
