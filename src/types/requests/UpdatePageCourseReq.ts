export default interface UpdatePageCourseReq {
    type: string;
    name: string;
    course: number;
    section: number;
    intro: string;
    content: string;
    courseModuleId: number;
    newVideo: boolean;
    conditions: number[];
}
