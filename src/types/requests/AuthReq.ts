export interface LoginReq {
    email: string;
    password: string;
}

export interface RefreshTokenReq {
    refreshToken: string;
}
