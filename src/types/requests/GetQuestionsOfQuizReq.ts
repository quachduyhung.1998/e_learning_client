export default interface GetQuestionsOfQuizReq {
    quizId?: number;
    page?: number;
    limit?: number;
    category?: number;
}
