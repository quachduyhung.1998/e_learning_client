export interface CreateRevisionReq {
    categoryIds: number[];
    conceptName: string;
    totalQuestion: number;
    maxDuration: number;
}
