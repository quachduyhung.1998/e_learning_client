export interface Question {
    id: number;
    name: string;
    questiontext: string;
}

export interface DetailQuestion {
    id: number;
    quizId: number;
    courseId: number;
    questionType: string;
    name: string;
    answer: string[];
    answerRandom: string[];
    defaultMark: number;
    single: number;
    questionText: string;
    answerCorrect: number[];
    rightAnswers?: number[];
    number: number;
    type: string;
    type_question?: string;
    sequencecheck: number;
    mark?: number;
    course_category_id?: number;
    answers?: any;
    teacher?: any;
    total_student?: number;
}

export enum QuestionTypeAnswer {
    MULTI_ANSWERS = 2, // nhiều đáp án đúng
    ONE_ANSWER = 1, // chỉ 1 đáp án đúng
    TRUE_FALSE = 3 // Chọn đúng sai
}

export enum AnswerCorrect {
    CORRECT = 1, // đáp án đúng
    INCORRECT = 0 // đáp án sai
}
