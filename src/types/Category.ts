export interface Category {
    coursecount: string;
    depth: number;
    description: string;
    descriptionformat: any;
    id: number;
    idnumber: any;
    name: string;
    parent: number;
    path: string;
    sortorder: any;
    theme: any;
    timemodified: any;
    visible: number;
    visibleold: number;
    children: Category[];
    title: string;
    key: number;
    value: number;
}
