import { Course } from './Course';
export interface User {
    id?: string;
    _id: string;
    cn: string;
    role?: number;
    email?: string;
    givenName: string;
    fullName: string;
    name: string;
    username: string;
    mail: string;
    roles: Role[];
    userRoles: string[];
    needUpdateProfile: boolean;
    needPlacementExam: boolean;
    gender?: Gender;
    age?: string;
    interesting?: string;
    longTermGoal?: string;
    shortTermGoal?: string;
    strongPoint?: string;
    weakPoint?: string;
    level?: number;
    academicLevel?: string;
    completion?: number;
    moodleUserId: number;
    collectionType: string;
    courses: Course[];
    avatar: string;
    sn?: string;
}

export interface Role {
    id: string;
    archetype: RoleType;
    name: string;
    shortname: string;
    description: string;
    sortorder: string;
}

export enum RoleType {
    STUDENT = 'student',
    TEACHER = 'editingteacher',
    ADMIN = 'manager'
}

export enum RoleHandle {
    DEFAULT = 'DEFAULT',
    ASSIGN = 'ASSIGN',
    UN_ASSIGN = 'UN_ASSIGN'
}

export enum Gender {
    MALE = 'MALE',
    FEMALE = 'FEMALE',
    OTHER = 'OTHER'
}
export enum RouteType {
    STUDENT = 'student',
    TEACHER = 'teacher',
    ADMIN = 'admin'
}
