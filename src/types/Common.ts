//antd table
export type AlignType = 'left' | 'center' | 'right';
export type FixedType = 'left' | boolean | 'right';
