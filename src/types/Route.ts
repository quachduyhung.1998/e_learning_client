import React, { LazyExoticComponent, ComponentType } from 'react';

export interface IRoute {
    route: any;
    path: string;
    component: LazyExoticComponent<ComponentType<any>>;
    fallback?: React.FC;
    exact?: boolean;
}
