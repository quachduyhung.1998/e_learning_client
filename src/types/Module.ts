export default interface Module {
    id: number;
    name: string;
    modame: string;
    instanceId: number;
    moduleName: string;
    courseModuleId: number;
    completed: boolean;
    conditions?: [];
    timeLimit?: number;
}
