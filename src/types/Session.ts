import { User } from './User';

export interface Session {
    idToken: string;
    refreshToken: string;
    expiresAt: string;
    idTokenPayload: User;
}
