export default interface PageCourse {
    id: number;
    created_by?: any;
    name: string;
    intro: string;
    content: string;
    type: string | number;
    section: number;
    courseModuleId: number;
    conditions?: number[];
}
