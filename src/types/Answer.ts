export interface Answer {
    id: number;
    value?: string | undefined;
    correct?: boolean;
}
