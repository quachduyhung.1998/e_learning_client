export default interface Quiz {
    id: number;
    name: string;
    intro: string;
    courseModuleId: number;
    timeLimit: number;
    grade: number;
    gradePass: number;
    totalQuestions: number;
    timeOpen?: Date;
    time_start?: Date;
    time_end?: Date;
    timeClose?: Date;
    attempts?: number;
    time_work?: number;
    total_question?: number;
}

export interface ResultStartQuiz {
    attemptId: number;
    uniqueId: number;
    attemptNumber: number;
    currentPage: number;
    preview: boolean;
    quizId: number;
    state: string;
    sumGrades: number | null;
    timeModified: number;
    timeModifiedOffline: number;
    timeStart: number;
    userId: number;
}
