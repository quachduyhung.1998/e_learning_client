export enum SearchType {
    GLOBAL_CASE = 'GLOBAL_CASE',
    STUDENT_CASE = 'STUDENT_CASE'
}

export enum CourseHandle {
    REVIEW = 'REVIEW',
    APPROVE = 'APPROVE'
}

export enum CollectionType {
    USER = 'USER',
    GROUP = 'GROUP'
}
