import Module from 'types/Module';

export interface Topic {
    id: string;
    name: string;
    visible?: number;
    section?: number;
    modules?: Module[];
    completed: boolean;
}
