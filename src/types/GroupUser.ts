import { User } from './User';

export interface GroupUser {
    _id: string;
    fullName: string;
    userIds: string[];
    createdBy: string;
    domain: string;
    mail: string;
    description: string;
    creator: User;
    users: User[];
}
