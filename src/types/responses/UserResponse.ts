import { BaseResponse } from 'types/responses/BaseResponse';
import { User } from 'types/User';

export interface ListUserData {
    total: number;
    totalPage: number;
    results: User[];
}

export interface ListUserResponse extends BaseResponse {
    data: ListUserData;
}

export interface AssignStudentResponse extends BaseResponse {
    data: boolean;
}

export interface RemoveStudentResponse extends BaseResponse {
    data: boolean;
}
