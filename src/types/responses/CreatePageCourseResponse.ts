import PageCourse from 'types/PageCourse';
import { BaseResponse } from 'types/responses/BaseResponse';

export default interface CreatePageCourseResponse extends BaseResponse {
    data: PageCourse | null;
}
