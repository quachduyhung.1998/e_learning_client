import { BaseResponse } from 'types/responses/BaseResponse';

export default interface CourseFinishRes extends BaseResponse {
    data: any;
}
