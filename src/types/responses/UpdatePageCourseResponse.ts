import { BaseResponse } from 'types/responses/BaseResponse';
import PageCourse from 'types/PageCourse';

export default interface UpdatePageCourseResponse extends BaseResponse {
    data: PageCourse | null;
}
