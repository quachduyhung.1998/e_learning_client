import { BaseResponse } from 'types/responses/BaseResponse';

export interface UpdateAnswerStartedQuizRes extends BaseResponse {
    data: any;
}
