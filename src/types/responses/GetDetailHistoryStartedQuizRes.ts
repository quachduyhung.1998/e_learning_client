import { BaseResponse } from 'types/responses/BaseResponse';
import { DetailQuestion } from 'types/Question';

export interface DetailHistoryStaredQuiz {}

export default interface GetDetailHistoryStartedQuizRes extends BaseResponse {
    data: DetailQuestion[];
}
