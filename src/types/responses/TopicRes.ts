import { Topic } from 'types/Topic';
import { BaseResponse } from 'types/responses/BaseResponse';

export interface AddTopicRes extends BaseResponse {
    data: Topic;
}

export interface TopicListResponse extends BaseResponse {
    data: Topic[];
}
