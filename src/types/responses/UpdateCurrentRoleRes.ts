import { BaseResponse } from 'types/responses/BaseResponse';

export default interface UpdateCurrentRoleRes extends BaseResponse {
    data: boolean;
}
