import { User } from '../User';
import { BaseResponse } from 'types/responses/BaseResponse';

export interface ListUserData {
    total: number;
    results: User[];
}
export interface ListUserFilterRes extends BaseResponse {
    total: number;
    items: User[];
}

export interface ListUserResponse extends BaseResponse {
    data: ListUserData;
}

export interface InforUserResponse extends BaseResponse {
    data: User;
}
