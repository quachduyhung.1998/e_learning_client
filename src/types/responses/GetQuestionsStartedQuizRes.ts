import { BaseResponse } from 'types/responses/BaseResponse';
import { DetailQuestion } from 'types/Question';

export interface GetQuestionsStartedQuizRes extends BaseResponse {
    data: DetailQuestion;
}
