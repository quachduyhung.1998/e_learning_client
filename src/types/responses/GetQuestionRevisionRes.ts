import { BaseResponse } from 'types/responses/BaseResponse';
import { DetailQuestion } from 'types/Question';

export interface GetQuestionRevisionRes extends BaseResponse {
    data: DetailQuestion;
}
