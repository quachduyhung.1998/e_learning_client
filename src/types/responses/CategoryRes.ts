import { BaseResponse } from 'types/responses/BaseResponse';
import { Category } from 'types/Category';

export interface CategoryListResponse extends BaseResponse {
    data: Category[];
}
