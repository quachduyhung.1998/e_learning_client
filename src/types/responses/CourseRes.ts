import { Course } from './../Course';
import { BaseResponse } from 'types/responses/BaseResponse';

export interface AddCourseData {
    id: number;
    code: string;
}

export interface ListCourseData {
    total: number;
    totalPage: number;
    results: Course[];
}

export interface UpdateCourseData {
    id: number;
    code: string;
}

export interface InfoCourseResponse extends BaseResponse {
    data: Course;
}

export interface AddCourseResponse extends BaseResponse {
    data: AddCourseData;
}

export interface DeleteCourseResponse extends BaseResponse {
    data: Course[];
}

export interface UpdateCourseResponse extends BaseResponse {
    data: UpdateCourseData;
}

export interface ListCourseResponse extends BaseResponse {
    data: ListCourseData | any;
}

export interface ShowCourseQuizResponse extends BaseResponse {
    data: QuizResponse;
}

export interface AddQuizResponse extends BaseResponse {
    data: QuizResponse;
}

export interface QuizResponse {
    id: number;
    name: string;
    intro: string;
    course: number;
    section: number;
    timeLimit: number;
    attempts: number;
    grade: number;
    gradePass: number;
    timeOpen: string;
    timeClose: string;
    courseModuleId: number;
    conditions: number[];
}
