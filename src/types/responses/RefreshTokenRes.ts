import { BaseResponse } from 'types/responses/BaseResponse';

export interface RefreshTokenRes extends BaseResponse {
    data: {
        idToken: string;
    };
}
