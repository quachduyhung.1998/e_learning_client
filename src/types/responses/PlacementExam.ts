import { BaseResponse } from 'types/responses/BaseResponse';

export interface PlacementExam extends BaseResponse {
    data: {
        needPlacementExam: boolean;
    };
}
