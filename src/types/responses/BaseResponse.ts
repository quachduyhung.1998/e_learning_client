export interface BaseResponse {
    data: any;
    statusCode: number;
    code?: string;
    user?: any;
    status?: number;
    message: string;
}
