import { ResultStartQuiz } from 'types/Quiz';
import { BaseResponse } from 'types/responses/BaseResponse';

export interface StartQuizRes extends BaseResponse {
    data: ResultStartQuiz;
}
