import { Course } from 'types/Course';
import { BaseResponse } from 'types/responses/BaseResponse';

export default interface ApproveCourseRes extends BaseResponse {
    data: Course;
}
