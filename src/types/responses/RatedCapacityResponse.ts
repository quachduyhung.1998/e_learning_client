import { BaseResponse } from 'types/responses/BaseResponse';

export interface RatedCapacityResponse extends BaseResponse {
    data: RatedCapacity;
}

export interface RatedCapacity {
    timeLimit: number;
    questions: Question[];
}

export interface Question {
    id: number;
    type: string;
    type_content: string;
    content: string;
    answers: Answers[];
}

export interface Answers {
    id: number;
    type_content: string;
    content: string;
}
