import { BaseResponse } from 'types/responses/BaseResponse';

export interface UploadResponse extends BaseResponse {
    data: Upload;
}

export interface Upload {
    name: string;
    url: string;
    path: string;
    size: number;
    mimetype: string;
}
