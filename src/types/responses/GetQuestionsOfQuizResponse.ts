import { DetailQuestion } from 'types/Question';
import { BaseResponse } from 'types/responses/BaseResponse';

export default interface GetQuestionsOfQuizResponse extends BaseResponse {
    data: {
        total: number;
        results: DetailQuestion[] | null;
    };
}
