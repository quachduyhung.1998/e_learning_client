import { BaseResponse } from 'types/responses/BaseResponse';
import { Question, DetailQuestion } from './../Question';

export interface ListQuestionResponse extends BaseResponse {
    data: Question[];
}

export interface QuestionResponse extends BaseResponse {
    data: DetailQuestion;
}
