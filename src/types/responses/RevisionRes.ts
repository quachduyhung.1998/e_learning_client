import { BaseResponse } from 'types/responses/BaseResponse';
import { Revision } from 'types/Revision';

export interface CreateRevisionRes extends BaseResponse {
    data: Revision;
}
