import { BaseResponse } from 'types/responses/BaseResponse';
import PageCourse from 'types/PageCourse';

export default interface GetPageCourseResponse extends BaseResponse {
    data: PageCourse | null;
}
