import { BaseResponse } from 'types/responses/BaseResponse';
import Quiz from 'types/Quiz';

export default interface GetQuizResponse extends BaseResponse {
    data: Quiz | null;
}
