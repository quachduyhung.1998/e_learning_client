import { BaseResponse } from 'types/responses/BaseResponse';

export interface ResultRevision {
    totalQuestion: number;
    totalQuestionCorrect: number;
}

export interface UpdateAnswerQuestionRevisionRes extends BaseResponse {
    // eslint-disable-next-line @typescript-eslint/ban-types
    data: ResultRevision | {};
}
