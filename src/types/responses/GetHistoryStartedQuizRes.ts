import { BaseResponse } from 'types/responses/BaseResponse';

export interface HistoryStaredQuiz {
    attemptId: number;
    completion: number;
    correct: number;
    status: string;
    timeFinish: number;
    timeStart: number;
    totalQuestion: number;
}

export default interface GetHistoryStartedQuizRes extends BaseResponse {
    data: HistoryStaredQuiz[];
}
