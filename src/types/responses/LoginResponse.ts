import { User } from './../User';
import { BaseResponse } from 'types/responses/BaseResponse';
import { Session } from 'types/Session';

interface LoginResponse extends BaseResponse {
    data: Session;
    access_token?: any;
}

export default LoginResponse;
