import { GroupUser } from 'types/GroupUser';
import { BaseResponse } from 'types/responses/BaseResponse';

export default interface DetailGroupUserRes extends BaseResponse {
    data: {
        result: GroupUser;
    };
}
