import { User } from 'types/User';
import { BaseResponse } from 'types/responses/BaseResponse';

export interface UpdateUserRes extends BaseResponse {
    data: User;
}
