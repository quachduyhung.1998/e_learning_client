import { User } from './User';

export interface Course {
    id: number;
    course_id?: number;
    code?: string;
    fullName: string;
    course_name?: string;
    course_category_id?: string;
    percent_progress?: number;
    name: string;
    categoryName: string;
    image: string;
    summary?: string;
    description?: string;
    status: CourseStatus;
    categoryId?: number;
    createdBy?: string;
    creator?: creator;
    createdAt?: string;
    approver?: User;
    assignees?: string[];
    lessons?: any[];
    completion?: number | null;
    created_by: any;
    course_category?: string;
    is_sequence?: number;
}

interface creator {
    cn: string;
    _id: string;
    username: string;
    givenName: string;
    sn: string;
    mail: string;
}

export enum CourseStatus {
    STATUS_DRAFT = 0,
    STATUS_REVIEWING = 1,
    STATUS_APPROVED = 2
}
