export interface QuestionRevision {
    categoryId: number;
    conceptName: string;
    totalQuestion: number;
    maxDuration: number;
}
