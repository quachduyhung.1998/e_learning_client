import { User } from './User';

export interface Revision {
    _id: string;
    conceptIds: string[];
    concepts: [Concept];
    maxDuration: number;
    categoryId: number;
    totalQuestion: number;
    currentTotalQuestion: number;
    difficulty: number;
    createdBy: string;
    creator?: User;
    questions?: number[];
    correct?: string[];
    startingTime?: number;
    /** Thời điểm nộp bài ôn tập */
    finishingTime?: number;
    /** Số câu trả lời đúng */
    correctQuiz?: number;
    createdAt: Date;
    timeStart?: Date;
    questionIndex: number;
}

export type Concept = {
    _id: string;
    /** Id của concept từ NamRank */
    conceptId: number;
    /** Tên */
    name: string;
    /** Ngày tạo */
    createdAt: Date;
};
