import FullPageLoadingComponent from 'components/loading/FullPageLoadingComponent';
import EventBus from 'events/EventBus';
import { EventBusName, LogoutPayloadType } from 'events/EventBusType';
import { logout } from 'helper/Utils';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Subscription } from 'rxjs';
import { ToastContainer } from 'react-toastify';

interface Props {
    children: JSX.Element;
}

function RootLayout(props: Props): JSX.Element {
    let subscriptions: Subscription;
    const history = useHistory();
    const [loading, setLoading] = useState<boolean>(false);

    function toggleLoading(loading: boolean) {
        setLoading(loading);
    }

    useEffect(() => {
        subscriptions = new Subscription();

        const registerEventBus = () => {
            subscriptions.add(
                EventBus.getInstance().events.subscribe(
                    (data: LogoutPayloadType) => {
                        switch (data.type) {
                            case EventBusName.LOGOUT_EVENT:
                                logout();
                                break;

                            case EventBusName.SHOW_LOADING_EVENT:
                                toggleLoading(true);
                                break;

                            case EventBusName.HIDE_LOADING_EVENT:
                                toggleLoading(false);
                                break;
                        }
                    }
                )
            );
        };

        registerEventBus();

        return () => {
            subscriptions.unsubscribe();
        };
    }, []);

    return (
        <div style={{ minHeight: '100vh' }}>
            <ToastContainer />
            <FullPageLoadingComponent loading={loading} />
            {props.children}
        </div>
    );
}

export default RootLayout;
