import ElementCenter from 'layout/common/ElementCenter';
import React from 'react';
import { Loading3QuartersOutlined } from '@ant-design/icons';
import { CSSProperties } from 'styled-components';

interface Props {
    style?: CSSProperties;
}

const Loading = (props: Props): JSX.Element => {
    return (
        <ElementCenter style={props.style}>
            <Loading3QuartersOutlined spin style={{ fontSize: '3em' }} />
        </ElementCenter>
    );
};

export default Loading;
