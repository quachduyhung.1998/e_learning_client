import { Col, Row } from 'antd';
import React from 'react';
import { CSSProperties } from 'styled-components';

interface Props {
    children: JSX.Element;
    style?: CSSProperties;
}

const ElementCenter = (props: Props): JSX.Element => {
    return (
        <Row
            justify="center"
            align="middle"
            style={{ height: '100%', ...(props.style || {}) }}
        >
            <Col style={{ display: 'flex' }}>{props.children}</Col>
        </Row>
    );
};

export default ElementCenter;
