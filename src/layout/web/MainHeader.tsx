import {
    BellTwoTone,
    MessageTwoTone,
    PlusCircleOutlined
} from '@ant-design/icons';
import {
    Avatar,
    Badge,
    Button,
    Col,
    Dropdown,
    Grid,
    Input,
    Menu,
    Modal,
    Row,
    Space,
    Card
} from 'antd';
import Pusher from 'pusher-js';
import FormReq from 'components/revision/FormReq';
import { TIME_OUT_SEARCH } from 'constants/Search';
import constStyle from 'constants/Style';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import { logout, getRoleByRoute } from 'helper/Utils';
import useQuery from 'hooks/route/useQuery';
import useUpdateCurrentRole from 'hooks/user/useUpdateCurrentRole';
import images from 'images';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { Container } from 'reactstrap';
import {
    HOME_TEACHER,
    HOME,
    TEACHER_SEARCH_COURSE,
    SEARCH_COURSE
} from 'routes/web/paths';
import { Revision } from 'types/Revision';
import { RoleType, RouteType } from 'types/User';
import './MainHeader.scss';
import getImageServer from 'helper/ImageHelper';
import useFetchListChat from 'hooks/chat/useFetchListChat';
import ModalCreateNewChat from 'pages/chats/ModalCreateNewChat';
import useFetchListNotification from 'hooks/chat/useFetchListNotification';
import TooltipCustom from 'pages/components/blocks/TooltipCustom';
// import { usePath } from 'hookrouter';
const { useBreakpoint } = Grid;

const { Search } = Input;

function MainHeader(): JSX.Element {
    const history = useHistory();
    const { Meta } = Card;
    const query = useQuery();
    // const path = usePath();
    const currentRole = getRoleByRoute();
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    const userRoles = LocalStorageHelper.getUserStorage()?.userRoles || [];
    const [keyword, setKeyword] = useState<string>(query.get('query') || '');
    const { t } = useTranslation();
    const [showModalRevision, setShowModalRevision] = useState(
        location.hash === '#revision-request' ? true : false
    );
    const [visibleModalCreateChat, setVisibleModalCreateChat] = useState(false);
    const [showBtnRevision, setShowBtnRevision] = useState(true);
    const screens = useBreakpoint();
    const revisionInfo: Revision = LocalStorageHelper.getObject(
        StorageKey.REVISION_INFO
    );
    const locations = useLocation();
    const role = getRoleByRoute();
    const [prefixPathName, setPrefixPathName] = useState(TEACHER_SEARCH_COURSE);
    const { listChat, loading, onFetchListChat } = useFetchListChat();
    const {
        listNotification,
        loading: loadingNotifi,
        onFetchListNotification
    } = useFetchListNotification();
    useEffect(() => {
        onFetchListChat();
        onFetchListNotification();
    }, []);
    const {
        resUpdateCurrentRole,
        onUpdateCurrentRole
    } = useUpdateCurrentRole();
    useEffect(() => {
        if (locations?.pathname == '/revision/start' && revisionInfo) {
            setShowBtnRevision(false);
        }
        role && role === RoleType.TEACHER
            ? setPrefixPathName(TEACHER_SEARCH_COURSE)
            : setPrefixPathName(SEARCH_COURSE);
    }, []);

    useEffect(() => {
        if (resUpdateCurrentRole) {
            if (resUpdateCurrentRole === RoleType.ADMIN) {
                window.location.href = '/admin';
            } else if (resUpdateCurrentRole === RoleType.TEACHER) {
                window.location.href = '/teacher';
            } else {
                window.location.href = '/';
            }
        }
    }, [resUpdateCurrentRole]);

    const timeOutRef = useRef<number>(0);
    const onKeywordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        timeOutRef.current && clearTimeout(timeOutRef.current);
        timeOutRef.current = setTimeout(
            async () => {
                await setKeyword(value);
                await history.push(`${prefixPathName}/?query=${value}&page=0`);
            },
            window.location.href === `${SEARCH_COURSE}/`
                ? TIME_OUT_SEARCH
                : 1000
        );
    };

    const onSearchSubmit = (q: string) => {
        history.push(`${prefixPathName}/?query=${q}&page=0`);
    };

    const menuChangeRole = () => {
        if (userRoles?.length > 1) {
            return (
                <Menu.SubMenu
                    icon={
                        <img
                            src={images.ic_change_role}
                            className="style-icon"
                        />
                    }
                    title={t('change_role_account')}
                >
                    {userRoles.map((role: string) => {
                        return (
                            <Menu.Item
                                key={role}
                                onClick={() => {
                                    if (role === currentRole) {
                                        return;
                                    } else {
                                        onUpdateCurrentRole(role);
                                    }
                                }}
                            >
                                {' '}
                                {t(role)}
                            </Menu.Item>
                        );
                    })}
                </Menu.SubMenu>
            );
        }
        return <></>;
    };

    const updateUrl: string = `/update-profile/?query=${user?.fullName}`;
    const dropdownAvatar = () => (
        <Menu>
            <Menu.Item>
                <Link to={`/student-detail/${user?.moodleUserId}`}>
                    <img
                        src={images.ic_user_account}
                        alt=""
                        className="style-icon"
                    />
                    {t('my_account')}
                </Link>
            </Menu.Item>
            {menuChangeRole()}
            <Menu.Item onClick={() => logout()}>
                {' '}
                <img
                    src={images.ic_logout}
                    alt=""
                    className="style-icon"
                />{' '}
                {t('logout')}
            </Menu.Item>
        </Menu>
    );
    const toggleModalCreateNewChat = () => {
        setVisibleModalCreateChat(!visibleModalCreateChat);
    };
    const dropdownNotice = () => (
        <Menu style={{ width: 300 }}>
            <div
                style={{
                    textAlign: 'center',
                    marginBottom: 0,
                    fontSize: 16,
                    color: constStyle.COLOR_MAIN
                }}
            >
                <div>
                    <span>
                        <b>{t('message')}</b>
                    </span>
                    <span
                        style={{ float: 'right', margin: '-5px 1em 0 0' }}
                        onClick={toggleModalCreateNewChat}
                    >
                        <PlusCircleOutlined />
                    </span>
                </div>
            </div>
            <Menu.Divider />

            {listChat?.map((item: any) => {
                return (
                    <div
                        className=" pointerCursor"
                        onClick={() => {
                            history.push({
                                pathname: '/chats/list',
                                search: `room_id=${item?.room_id}`
                            });
                        }}
                    >
                        <Menu.Divider />

                        <Menu.Item>
                            <Card>
                                <Meta
                                    avatar={
                                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                    }
                                    title={
                                        item?.room_name ||
                                        item?.send_to_user?.name
                                    }
                                    description={item?.last_message?.message}
                                />
                            </Card>
                        </Menu.Item>
                    </div>
                );
            })}

            <Menu.Divider />
            <div
                style={{
                    textAlign: 'center',
                    marginBottom: 0,
                    color: constStyle.COLOR_MAIN
                }}
            >
                <Link to={`/chats/list`}>{t('see_all')}</Link>
            </div>
        </Menu>
    );
    const dropdownNotification = () => (
        <Menu style={{ width: 500 }}>
            <div
                style={{
                    textAlign: 'center',
                    marginBottom: 0,
                    fontSize: 16,
                    color: constStyle.COLOR_MAIN
                }}
            >
                <div>
                    <span>
                        <b>Thông báo</b>
                    </span>
                </div>
            </div>
            <Menu.Divider />
            <div style={{ maxHeight: '500px', overflow: 'scroll' }}>
                {listNotification?.map((item: any) => {
                    return (
                        <div
                            className=" pointerCursor"
                            onClick={() => {
                                window.location.href = item?.url;
                            }}
                        >
                            <Menu.Divider />

                            <Menu.Item>
                                <Card>
                                    <Meta
                                        title={
                                            <span style={{ display: '' }}>
                                                {
                                                    <TooltipCustom
                                                        text={item?.message}
                                                        length={50}
                                                    />
                                                }
                                            </span>
                                        }
                                    />
                                </Card>
                            </Menu.Item>
                        </div>
                    );
                })}
            </div>

            <Menu.Divider />
            {/* <div
                style={{
                    textAlign: 'center',
                    marginBottom: 0,
                    color: constStyle.COLOR_MAIN
                }}
            >
                <Link to={`/chats/list`}>{t('see_all')}</Link>
            </div> */}
        </Menu>
    );

    useEffect(() => {
        return () => clearTimeout(timeOutRef.current);
    }, []);

    return (
        <>
            <Row
                align="middle"
                style={{
                    background: constStyle.COLOR_MAIN,
                    marginBottom: '2em'
                }}
            >
                <Container>
                    <Row
                        gutter={screens.xs ? [8, 8] : [8, 0]}
                        justify="space-between"
                        align="middle"
                        style={{ background: constStyle.COLOR_MAIN }}
                    >
                        <Col
                            md={{ span: 4, order: 0 }}
                            xs={{ span: 12, order: 0 }}
                            onClick={() => {
                                user?.role === 2
                                    ? history.push('/admin')
                                    : user?.role === 1
                                    ? history.push(HOME_TEACHER)
                                    : history.push(HOME);
                            }}
                            className="cursor-pointer"
                        >
                            <img
                                style={{
                                    height: 64,
                                    padding: 10
                                }}
                                src={images.ic_logo}
                                alt=""
                            />
                        </Col>
                        <Col
                            md={{ span: 14, order: 1 }}
                            xs={{ span: 24, order: 2 }}
                        >
                            <Row
                                gutter={[8, 8]}
                                justify={screens.xs ? 'space-between' : 'start'}
                            ></Row>
                        </Col>
                        <Col
                            md={{ span: 6, order: 1 }}
                            xs={{ span: 12, order: 1 }}
                        >
                            <Row justify="end">
                                <Space>
                                    <Dropdown
                                        overlay={dropdownNotification}
                                        placement="bottomRight"
                                        trigger={['click', 'hover']}
                                    >
                                        {/* <Badge count={3}> */}
                                        <BellTwoTone
                                            style={{
                                                fontSize: 24,
                                                color: 'white'
                                            }}
                                        />
                                        {/* </Badge> */}
                                    </Dropdown>
                                    <Dropdown
                                        overlay={dropdownNotice}
                                        placement="bottomRight"
                                        trigger={['click', 'hover']}
                                    >
                                        {/* <Badge count={3}> */}
                                        <MessageTwoTone
                                            style={{
                                                fontSize: 24,
                                                color: 'white'
                                            }}
                                        />
                                        {/* </Badge> */}
                                    </Dropdown>

                                    <Dropdown
                                        overlay={dropdownAvatar}
                                        placement="bottomRight"
                                    >
                                        <Avatar
                                            size={32}
                                            src={
                                                user?.avatar
                                                    ? getImageServer(
                                                          user?.avatar
                                                      )
                                                    : 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'
                                            }
                                        />
                                    </Dropdown>
                                </Space>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </Row>
            <ModalCreateNewChat
                visible={visibleModalCreateChat}
                toggleModal={toggleModalCreateNewChat}
            />
            {showModalRevision && currentRole === RoleType.STUDENT && (
                <Modal
                    title={t('revision')}
                    visible={showModalRevision}
                    onCancel={() => {
                        history.goBack();
                        setShowModalRevision(false);
                        LocalStorageHelper.remove(
                            StorageKey.CACHE_FORM_REVISION_REQ
                        );
                    }}
                    footer=""
                >
                    <FormReq onCancel={() => setShowModalRevision(false)} />
                </Modal>
            )}
        </>
    );
}

export default React.memo(MainHeader);
