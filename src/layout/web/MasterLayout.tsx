import { renderRoutes } from 'App';
import React from 'react';
import routes from 'routes/web';
import MainHeader from './MainHeader';
import useSocketIO from 'hooks/socketIO/useSocketIO';
function MasterLayout() {
    // useSocketIO();
    return (
        <div>
            <MainHeader />
            <main style={{ minHeight: '100%' }}>{renderRoutes(routes)}</main>
        </div>
    );
}

export default MasterLayout;
