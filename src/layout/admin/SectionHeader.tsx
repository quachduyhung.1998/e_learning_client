import { Col, Divider, Row } from 'antd';
import React from 'react';

interface Props {
    contentLeft?: React.ReactNode;
    contentRight?: React.ReactNode;
}

function SectionHeader(props: Props) {
    return (
        <>
            <Row gutter={[8, 8]} align="middle">
                <Col md={{ span: 12 }}>{props.contentLeft}</Col>
                <Col md={{ span: 12 }} style={{ textAlign: 'right' }}>
                    {props.contentRight}
                </Col>
            </Row>
            <Divider />
        </>
    );
}

export default SectionHeader;
