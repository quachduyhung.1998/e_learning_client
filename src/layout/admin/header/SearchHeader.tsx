import { Loading3QuartersOutlined, SearchOutlined } from '@ant-design/icons';
import { Button, Card, Drawer, Dropdown, Empty, Grid, Input } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

const { useBreakpoint } = Grid;

const styleDefault = {
    width: 300,
    maxHeight: 300
};

interface Props {
    width?: number;
    maxHeight?: number;
}

function SearchHeader(props: Props) {
    const { width, maxHeight } = props;
    const { t } = useTranslation();
    const screens = useBreakpoint();
    const [showDrawerSearch, setShowDrawerSearch] = useState(false);
    const [inputSearch, setInputSearch] = useState<Input | null>(null);
    const [value, setValue] = useState('');
    const [loading, setLoading] = useState(false);
    const [dataSearch, setDataSearch] = useState<boolean>();
    const timeOutRef = useRef<number>(0);

    useEffect(() => {
        const inputSearchFocus = () => {
            if (inputSearch) {
                inputSearch.focus();
            }
        };
        inputSearchFocus();
    }, [showDrawerSearch]);

    const onChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setValue(value);
        timeOutRef.current && clearTimeout(timeOutRef.current);
        if (!value) {
            return;
        }
        timeOutRef.current = window.setTimeout(async () => {
            setLoading(true);
            await new Promise((resolve) => {
                setDataSearch(undefined);
                setTimeout(() => {
                    setDataSearch(false);
                    resolve(true);
                }, 3000);
            });
            setLoading(false);
        }, 300);
    };

    useEffect(() => {
        return () => clearTimeout(timeOutRef.current);
    }, []);

    const renderResult = (style: {
        width: string | number;
        maxHeight: string | number;
    }) => {
        if (!loading && value !== '' && dataSearch !== undefined) {
            return (
                <Card
                    style={{
                        width: style.width,
                        maxHeight: style.maxHeight,
                        overflowY: 'auto'
                    }}
                >
                    <>
                        {dataSearch ? (
                            <div>Data</div>
                        ) : (
                            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                        )}
                    </>
                </Card>
            );
        } else {
            return <></>;
        }
    };

    const renderBoxSearch = (style: {
        width: string | number;
        maxHeight: string | number;
    }) => {
        return (
            <Dropdown trigger={['click']} overlay={renderResult(style)}>
                <Input
                    ref={(input) => {
                        setInputSearch(input);
                    }}
                    style={{ width: style.width }}
                    placeholder={t('input_search')}
                    suffix={
                        loading ? (
                            <Loading3QuartersOutlined spin />
                        ) : (
                            <SearchOutlined />
                        )
                    }
                    onChange={onChangeSearch}
                />
            </Dropdown>
        );
    };

    if (screens.xs) {
        return (
            <>
                <Button
                    onClick={() => setShowDrawerSearch(true)}
                    icon={<SearchOutlined />}
                    type="text"
                    shape="circle"
                />
                <Drawer
                    title={undefined}
                    placement="top"
                    closable={false}
                    onClose={() => setShowDrawerSearch(false)}
                    visible={showDrawerSearch}
                    height="auto"
                >
                    {renderBoxSearch({
                        width: '100%',
                        maxHeight: maxHeight || styleDefault.maxHeight
                    })}
                </Drawer>
            </>
        );
    } else {
        return (
            <>
                {renderBoxSearch({
                    width: width || styleDefault.width,
                    maxHeight: maxHeight || styleDefault.maxHeight
                })}
            </>
        );
    }
}

export default SearchHeader;
