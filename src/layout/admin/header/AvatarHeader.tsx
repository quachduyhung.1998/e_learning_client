import { Avatar, Dropdown, Menu } from 'antd';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import useUpdateCurrentRole from 'hooks/user/useUpdateCurrentRole';
import React, { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'store';
import { RoleType } from 'types/User';
import images from 'images';
import '../../web/MainHeader.scss';
import getImageServer from 'helper/ImageHelper';
import { getRoleByRoute } from 'helper/Utils';

function MenuAccount() {
    const { t } = useTranslation();
    const user = LocalStorageHelper.getUserStorage();
    const userRoles = LocalStorageHelper.getUserStorage()?.userRoles || [];
    const currentRole = getRoleByRoute();
    const {
        resUpdateCurrentRole,
        onUpdateCurrentRole
    } = useUpdateCurrentRole();
    const history = useHistory();
    const flagNavigationInfoPage = useRef(false);

    useEffect(() => {
        if (resUpdateCurrentRole) {
            if (resUpdateCurrentRole === RoleType.ADMIN) {
                window.location.href = '/admin';
            } else if (flagNavigationInfoPage.current === true) {
                window.location.href = `/student-detail/${user?.moodleUserId}`;
            } else if (resUpdateCurrentRole === RoleType.TEACHER) {
                window.location.href = '/teacher';
            } else {
                window.location.href = '/';
            }
        }
    }, [resUpdateCurrentRole]);

    const logout = () => {
        LocalStorageHelper.remove(StorageKey.USER_SESSION);
        LocalStorageHelper.remove(StorageKey.CURRENT_ROLE);
        window.location.href = '/login';
    };
    const handleNavigationInfoPage = async () => {
        const role = userRoles
            ? userRoles.filter((role) => {
                  return role === 'student';
              })
            : '';
        if (role) {
            flagNavigationInfoPage.current = true;
            onUpdateCurrentRole(role[0]);
        }
    };

    const menuChangeRole = () => {
        if (userRoles?.length > 1) {
            return (
                <Menu.SubMenu
                    icon={
                        <img
                            src={images.ic_change_role}
                            className="style-icon"
                        />
                    }
                    title={t('change_role_account')}
                >
                    {userRoles.map((role: string) => {
                        return (
                            <Menu.Item
                                key={role}
                                onClick={() => {
                                    if (role === currentRole) {
                                        return;
                                    } else {
                                        onUpdateCurrentRole(role);
                                    }
                                }}
                            >
                                {t(role)}
                            </Menu.Item>
                        );
                    })}
                </Menu.SubMenu>
            );
        }
        return <></>;
    };
    return (
        <Menu selectable={false}>
            <Menu.Item onClick={handleNavigationInfoPage}>
                <img
                    src={images.ic_user_account}
                    alt=""
                    className="style-icon"
                />
                {t('my_account')}
            </Menu.Item>
            {menuChangeRole()}
            <Menu.Item onClick={() => logout()}>
                {' '}
                <img
                    src={images.ic_logout}
                    alt=""
                    className="style-icon"
                />{' '}
                {t('logout')}
            </Menu.Item>
        </Menu>
    );
}

function AvatarHeader() {
    const user = LocalStorageHelper.getUserStorage();
    return (
        <Dropdown overlay={<MenuAccount />} placement="bottomRight">
            <Avatar
                size={32}
                src={
                    user?.avatar
                        ? getImageServer(user?.avatar)
                        : 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'
                }
            />
        </Dropdown>
    );
}

export default AvatarHeader;
