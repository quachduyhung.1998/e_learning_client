import { BellOutlined } from '@ant-design/icons';
import { Badge, Button, Card, Dropdown, Tabs } from 'antd';
import React from 'react';
const { TabPane } = Tabs;

function MenuNotification() {
    return (
        <Card
            bodyStyle={{ padding: '.5em' }}
            style={{ width: 300, padding: 0 }}
        >
            <Tabs defaultActiveKey="account">
                <TabPane tab="Account" key="account">
                    Content of Account
                </TabPane>
                <TabPane tab="System" key="system">
                    Content of Tab System
                </TabPane>
            </Tabs>
        </Card>
    );
}

interface Props {}

function NotificationHeader(props: Props) {
    return (
        <Dropdown overlay={<MenuNotification />} placement="bottomRight">
            <Button type="text" shape="circle">
                <Badge dot>
                    <BellOutlined />
                </Badge>
            </Button>
        </Dropdown>
    );
}

export default NotificationHeader;
