import {
    BellTwoTone,
    MessageTwoTone,
    PlusCircleOutlined
} from '@ant-design/icons';
import { Card, Col, Dropdown, Layout, Menu, Row, Space } from 'antd';
import useFetchListNotification from 'hooks/chat/useFetchListNotification';
import React, { useEffect, useState } from 'react';
import AvatarHeader from './AvatarHeader';
import LogoHeader from './LogoHeader';
import NotificationHeader from './NotificationHeader';
import SearchHeader from './SearchHeader';
import constStyle from 'constants/Style';
import useFetchListChat from 'hooks/chat/useFetchListChat';
import TooltipCustom from 'pages/components/blocks/TooltipCustom';
import { Link, useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import Avatar from 'antd/lib/avatar/avatar';
const { Header } = Layout;

interface Props {
    showHeader?: boolean;
    height?: string;
    toggleSider?: (status?: boolean) => void;
    toggleHeader?: (status?: boolean) => void;
    widthLogo?: number | string;
}
const { Meta } = Card;

function HeaderLayout(props: Props) {
    const { showHeader, height } = props;
    const [visibleModalCreateChat, setVisibleModalCreateChat] = useState(false);
    const { t } = useTranslation();
    const history = useHistory();
    const { listChat, loading, onFetchListChat } = useFetchListChat();
    const {
        listNotification,
        loading: loadingNotifi,
        onFetchListNotification
    } = useFetchListNotification();
    useEffect(() => {
        onFetchListChat();
        onFetchListNotification();
    }, []);
    const dropdownNotification = () => (
        <Menu style={{ width: 500 }}>
            <div
                style={{
                    textAlign: 'center',
                    marginBottom: 0,
                    fontSize: 16,
                    color: constStyle.COLOR_MAIN
                }}
            >
                <div>
                    <span>
                        <b>Thông báo</b>
                    </span>
                </div>
            </div>
            <Menu.Divider />
            <div style={{ maxHeight: '500px', overflow: 'scroll' }}>
                {listNotification?.map((item: any) => {
                    return (
                        <div
                            className=" pointerCursor"
                            onClick={() => {
                                window.location.href = item?.url;
                            }}
                        >
                            <Menu.Divider />

                            <Menu.Item>
                                <Card>
                                    <Meta
                                        title={
                                            <span style={{ display: '' }}>
                                                {
                                                    <TooltipCustom
                                                        text={item?.message}
                                                        length={50}
                                                    />
                                                }
                                            </span>
                                        }
                                    />
                                </Card>
                            </Menu.Item>
                        </div>
                    );
                })}
            </div>

            <Menu.Divider />
            {/* <div
                style={{
                    textAlign: 'center',
                    marginBottom: 0,
                    color: constStyle.COLOR_MAIN
                }}
            >
                <Link to={`/chats/list`}>{t('see_all')}</Link>
            </div> */}
        </Menu>
    );

    const toggleModalCreateNewChat = () => {
        setVisibleModalCreateChat(!visibleModalCreateChat);
    };
    const dropdownNotice = () => (
        <Menu style={{ width: 300 }}>
            <div
                style={{
                    textAlign: 'center',
                    marginBottom: 0,
                    fontSize: 16,
                    color: constStyle.COLOR_MAIN
                }}
            >
                <div>
                    <span>
                        <b>{t('message')}</b>
                    </span>
                    <span
                        style={{ float: 'right', margin: '-5px 1em 0 0' }}
                        onClick={toggleModalCreateNewChat}
                    >
                        <PlusCircleOutlined />
                    </span>
                </div>
            </div>
            <Menu.Divider />

            {listChat?.map((item: any) => {
                return (
                    <div
                        className=" pointerCursor"
                        onClick={() => {
                            history.push({
                                pathname: '/chats/list',
                                search: `room_id=${item?.room_id}`
                            });
                        }}
                    >
                        <Menu.Divider />

                        <Menu.Item>
                            <Card>
                                <Meta
                                    avatar={
                                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                    }
                                    title={
                                        item?.room_name ||
                                        item?.send_to_user?.name
                                    }
                                    description={item?.last_message?.message}
                                />
                            </Card>
                        </Menu.Item>
                    </div>
                );
            })}

            <Menu.Divider />
            <div
                style={{
                    textAlign: 'center',
                    marginBottom: 0,
                    color: constStyle.COLOR_MAIN
                }}
            >
                <Link to={`/chats/list`}>{t('see_all')}</Link>
            </div>
        </Menu>
    );
    return (
        <Header
            hidden={!showHeader !== undefined ? !showHeader : false}
            id="main-header"
            style={{
                padding: '0em',
                width: '100%',
                height: height || '4em',
                lineHeight: height || '4em',
                background: 'white'
            }}
        >
            <Row gutter={[8, 0]} justify="space-between" align="middle">
                <Col md={{ span: 12 }} sm={{ span: 6 }}>
                    <Row gutter={[8, 0]}>
                        <Col md={{ span: 6 }}>
                            {/* <LogoHeader widthLogo={props.widthLogo} /> */}
                        </Col>
                    </Row>
                </Col>
                <Col md={{ span: 12 }} sm={{ span: 18 }}>
                    <div style={{ textAlign: 'right', paddingRight: '1em' }}>
                        <Space>
                            <Dropdown
                                overlay={dropdownNotification}
                                placement="bottomRight"
                                trigger={['click', 'hover']}
                            >
                                {/* <Badge count={3}> */}
                                <BellTwoTone
                                    style={{
                                        fontSize: 24,
                                        color: 'white'
                                    }}
                                />
                                {/* </Badge> */}
                            </Dropdown>
                            <Dropdown
                                overlay={dropdownNotice}
                                placement="bottomRight"
                                trigger={['click', 'hover']}
                            >
                                {/* <Badge count={3}> */}
                                <MessageTwoTone
                                    style={{
                                        fontSize: 24,
                                        color: 'white'
                                    }}
                                />
                                {/* </Badge> */}
                            </Dropdown>

                            <AvatarHeader />
                        </Space>
                    </div>
                </Col>
            </Row>
        </Header>
    );
}

export default HeaderLayout;
