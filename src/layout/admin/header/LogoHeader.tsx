import images from 'images';
import React from 'react';
import { CSSProperties } from 'styled-components';

interface Props {
    style?: CSSProperties;
}
// const;
function LogoHeader(props: Props) {
    return (
        <div
            style={{
                width: '250px',
                padding: '0 1em',
                alignItems: 'center',
                display: 'flex',
                justifyContent: 'center',
                ...props.style
            }}
        >
            <a href="/admin">
                <img src={images.ic_logo} alt="" style={{ height: '3em' }} />
            </a>
        </div>
    );
}

export default LogoHeader;
