import React from 'react';

interface Props {
    children?: JSX.Element;
}

function FooterSider(props: Props) {
    const { children } = props;
    return <div>{children}</div>;
}

export default FooterSider;
