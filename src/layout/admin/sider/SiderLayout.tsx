import { Layout } from 'antd';
import React from 'react';
import FooterSider from './FooterSider';
import HeaderSider from './HeaderSider';
import MenuSider from './MenuSider';
import { SiderProps } from 'antd/lib/layout';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';

const { Sider } = Layout;

interface Props extends SiderProps {
    showSider?: boolean;
    toggleSider?: (status?: boolean) => void;
    collapsedWidth?: number;
    header?: JSX.Element;
    footer?: JSX.Element;
}

function SiderLayout(props: Props) {
    const {
        showSider,
        toggleSider,
        collapsedWidth,
        header,
        footer,
        ...reset
    } = props;
    const screens = useBreakpoint();
    return (
        <Sider
            id="main-sider"
            width={reset.style?.width}
            breakpoint="lg"
            collapsedWidth={
                collapsedWidth !== undefined ? collapsedWidth.toString() : '0'
            }
            trigger={null}
            collapsible
            collapsed={!showSider !== undefined ? !showSider : false}
            onCollapse={(collapsed, type) => {
                toggleSider && toggleSider();
            }}
            style={!screens.lg ? { zIndex: 10, height: '100vh' } : {}}
        >
            <div
                style={{
                    height: '100%',
                    display: 'flex',
                    flexDirection: 'column'
                }}
            >
                <HeaderSider children={header} />
                <MenuSider />
                <FooterSider children={footer} />
            </div>
        </Sider>
    );
}

export default SiderLayout;
