import {
    BookOutlined,
    DashboardOutlined,
    TeamOutlined,
    UserOutlined
} from '@ant-design/icons';
import { Menu } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import {
    CLASS_LIST,
    DASHBOARD,
    GROUP_USER_LIST,
    REVIEWING_COURSES,
    USER_LIST
} from 'routes/admin/paths';

const { SubMenu, Item } = Menu;
export interface ConfigMenu {
    icon?: React.ReactNode;
    title: string;
    path: string;
    children?: ConfigMenu[];
}

const findOpenKeys = (
    configMenus: ConfigMenu[],
    withOpenKeys: string[] = [],
    menuSelected: ConfigMenu[] = []
): { openKeys: string[]; menuSelected: ConfigMenu[] } => {
    if (!configMenus.length) {
        return { openKeys: withOpenKeys, menuSelected };
    }
    const newWithOpenKeys: string[] = [...withOpenKeys];
    const newConfigMenus: ConfigMenu[] = [];
    const newMenuSelected: ConfigMenu[] = [...menuSelected];
    configMenus.map((configMenu) => {
        if (configMenu.path === window.location.pathname) {
            newMenuSelected.push(configMenu);
        }
        if (configMenu.children?.length) {
            configMenu.children.map((config: ConfigMenu) => {
                newConfigMenus.push(config);
                if (config.path === window.location.pathname) {
                    newWithOpenKeys.push(configMenu.path);
                }
                return config;
            });
        }
        return configMenu;
    });
    return findOpenKeys(newConfigMenus, newWithOpenKeys, newMenuSelected);
};

interface Props {}

function MenuSider(props: Props) {
    const { t } = useTranslation();
    const history = useHistory();
    const [configMenus] = useState<ConfigMenu[]>([
        {
            icon: <DashboardOutlined />,
            title: t('home'),
            path: DASHBOARD
        },
        {
            icon: <BookOutlined />,
            title: t('learning_management'),
            path: REVIEWING_COURSES,
            children: [
                {
                    title: 'Khóa học',
                    path: REVIEWING_COURSES
                },
                {
                    title: t('class'),
                    path: CLASS_LIST
                }
            ]
        },
        {
            icon: <UserOutlined />,
            title: t('user_management'),
            path: USER_LIST,
            children: [
                {
                    title: t('users'),
                    path: USER_LIST
                }
            ]
        }
    ]);
    const [findOpenConfigMenus] = useState(findOpenKeys(configMenus));
    const [openKeys] = useState<string[]>(findOpenConfigMenus.openKeys);
    const [selectKeys] = useState<string[]>([history.location.pathname]);
    useEffect(() => {
        const firstMenuOpen = findOpenConfigMenus.menuSelected;
    }, []);

    const renderItem = (configMenu: ConfigMenu) => {
        if (configMenu?.children?.length) {
            return (
                <SubMenu
                    icon={configMenu.icon}
                    key={configMenu.path}
                    title={configMenu.title}
                >
                    {configMenu.children.map((config) => {
                        return renderItem(config);
                    })}
                </SubMenu>
            );
        } else {
            return (
                <Item icon={configMenu.icon} key={configMenu.path}>
                    <Link to={configMenu.path}>{configMenu.title}</Link>
                </Item>
            );
        }
    };

    return (
        <Menu
            style={{
                height: '100%',
                width: '100%',
                overflowY: 'auto',
                overflowX: 'hidden',
                borderRight: 'none'
            }}
            mode="inline"
            defaultSelectedKeys={selectKeys}
            defaultOpenKeys={openKeys}
            theme="dark"
        >
            {configMenus.map((configMenu) => renderItem(configMenu))}
        </Menu>
    );
}

export default MenuSider;
