import { VerticalAlignTopOutlined } from '@ant-design/icons';
import { BackTop, Button, Layout } from 'antd';
import React from 'react';
import { CSSProperties } from 'styled-components';

const { Content } = Layout;

interface Props {
    children?: JSX.Element;
    toggleSider?: (status?: boolean) => void;
    style?: CSSProperties;
}

function ContentLayout(props: Props) {
    const { children, style } = props;
    return (
        <div
            id="main-content"
            style={{
                overflowY: 'auto',
                height: '100%',
                position: 'relative',
                padding: '.5em',
                ...style
            }}
        >
            {children}

            <BackTop
                style={{
                    right: '0.5em',
                    bottom: '0.5em'
                }}
                target={() =>
                    window.document.getElementById('main-content') || window
                }
            >
                <Button
                    type="primary"
                    icon={<VerticalAlignTopOutlined />}
                    shape="circle"
                />
            </BackTop>
        </div>
    );
}

export default ContentLayout;
