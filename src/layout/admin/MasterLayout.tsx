import { DoubleLeftOutlined, DoubleRightOutlined } from '@ant-design/icons';
import { Button, Layout } from 'antd';
import { renderRoutes } from 'App';
import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import routes from 'routes/admin';
import ContentLayout from './content/ContentLayout';
import HeaderLayout from './header/HeaderLayout';
import LogoHeader from './header/LogoHeader';
import SiderLayout from './sider/SiderLayout';
import useSocketIO from 'hooks/socketIO/useSocketIO';

const heightHeader = '5em';
const widthSider = '250px';

function MasterLayout() {
    // useSocketIO();
    const [showHeader, setShowHeader] = useState<boolean>(true);
    const [showSider, setShowSider] = useState<boolean>(true);
    const toggleSider = (status?: boolean) => {
        setShowSider(status !== undefined ? status : !showSider);
    };
    const toggleHeader = (status?: boolean) => {
        setShowHeader(status !== undefined ? status : !setShowHeader);
    };
    return (
        <>
            <Helmet
                titleTemplate={''}
                defaultTitle={'Hưng Pro Learning'}
                meta={[]}
            />
            <Layout style={{ height: '100vh' }}>
                <SiderLayout
                    style={{ width: widthSider }}
                    showSider={showSider}
                    toggleSider={toggleSider}
                    header={
                        <LogoHeader
                            style={{ width: widthSider, height: heightHeader }}
                        />
                    }
                    footer={
                        <div
                            style={{
                                height: '3em',
                                lineHeight: '3em',
                                textAlign: 'center'
                            }}
                            onClick={() => setShowSider(!showSider)}
                        >
                            <DoubleLeftOutlined style={{ color: 'white' }} />
                        </div>
                    }
                />
                <Layout
                    style={{
                        height: '100vh',
                        width: `calc(100vw - ${widthSider})`
                    }}
                >
                    <HeaderLayout
                        showHeader={showHeader}
                        height={heightHeader}
                        toggleSider={toggleSider}
                        toggleHeader={toggleHeader}
                        widthLogo={widthSider}
                    />

                    <div
                        hidden={showSider}
                        style={{
                            position: 'fixed',
                            bottom: '.5em',
                            left: '.5em',
                            zIndex: 100
                        }}
                    >
                        <Button
                            type="link"
                            icon={<DoubleRightOutlined />}
                            shape="circle"
                            onClick={() => setShowSider(!showSider)}
                        />
                    </div>
                    <ContentLayout
                        style={{ height: `calc(100vh - ${heightHeader})` }}
                    >
                        {renderRoutes(routes)}
                    </ContentLayout>
                </Layout>
            </Layout>
        </>
    );
}

export default MasterLayout;
