export default {
    COLOR_MAIN: '#273679',
    COLOR_MAIN_2: '#ed2228',
    COLOR_MAIN_3: '#4962cf',
    COLOR_MAIN_4: '#52c41a'
};
