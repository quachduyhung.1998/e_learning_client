export const TYPE_SINGLE = 'single';
export const TYPE_MULTI = 'multi';
export const TYPE_CONTENT_HTML = 'html';
export const TYPE_CONTENT_VIDEO = 'video';
export const TYPE_CONTENT_IMAGE = 'image';
