export default {
    TYPE_TEXT: 'text',
    TYPE_VIDEO: 'video',
    MAX_SIZE_UPLOAD: 500,
    MB_CONVERT: 1048576
};

export enum AcademicLevel {
    STUDENT = 'STUDENT', // Học sinh, sinh viên
    COLLEGE_DEGREE = 'COLLEGE_DEGREE', // Cao đẳng
    INTERMEDIATE = 'INTERMEDIATE', // Trung cấp
    UNIVERSITY_DEGREE = 'UNIVERSITY_DEGREE', // Đại học
    BACHELOR_DEGREE = 'BACHELOR_DEGREE' // Cử nhân
}
