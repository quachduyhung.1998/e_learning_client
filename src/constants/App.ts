export const API_DOMAIN = process.env.API_DOMAIN
    ? process.env.API_DOMAIN
    : 'https://moodle.yez.vn/api';

export const SEP_API_DOMAIN = process.env.SEP_API_DOMAIN
    ? process.env.SEP_API_DOMAIN
    : 'https://moodle.yez.vn';
