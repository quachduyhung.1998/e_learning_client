import React from 'react';
import { LoadingOutlined } from '@ant-design/icons';
import { Row, Spin } from 'antd';

interface Props {
    loading: boolean;
}

function FullPageLoadingComponent(props: Props): JSX.Element | null {
    if (!props.loading) return null;
    return (
        <Row
            justify="center"
            align="middle"
            style={{
                height: '100vh',
                width: '100vw',
                position: 'fixed'
            }}
        >
            <Spin size="large" indicator={<LoadingOutlined spin />} />
        </Row>
    );
}

export default React.memo(FullPageLoadingComponent);
