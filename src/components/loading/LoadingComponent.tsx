import { LoadingOutlined } from '@ant-design/icons';
import { Row, Spin } from 'antd';
import React from 'react';

interface Props {
    size?: 'small' | 'large' | 'default';
}
function LoadingComponent(props: Props): JSX.Element | null {
    return (
        <Row
            justify="center"
            align="middle"
            style={{
                height: '100%',
                width: '100%',
                position: 'fixed'
            }}
        >
            <Spin
                size={props?.size || 'large'}
                indicator={<LoadingOutlined spin />}
            />
        </Row>
    );
}

export default React.memo(LoadingComponent);
