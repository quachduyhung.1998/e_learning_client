import EventBus from 'events/EventBus';
import { EventBusName } from 'events/EventBusType';

function show(): void {
    EventBus.getInstance().post({
        type: EventBusName.SHOW_LOADING_EVENT
    });
}

function hide(): void {
    EventBus.getInstance().post({
        type: EventBusName.HIDE_LOADING_EVENT
    });
}

export default {
    show,
    hide
};
