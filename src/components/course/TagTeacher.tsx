import { PlusOutlined } from '@ant-design/icons';
import { Input, Tag, Tooltip, Typography, Row } from 'antd';
import React, { useState, useEffect } from 'react';
import './TagTeacher.scss';
import { useTranslation } from 'react-i18next';
import ModalError from 'components/shares/modal/ModalError';

function TagTeacher(props: any) {
    const { t } = useTranslation();
    const [tags, setTags] = useState<any>([]);
    const [input, setInput] = useState();
    const [editInput, setEditInput] = useState();
    const [inputVisible, setInputVisible] = useState(false);
    const [inputValue, setInputValue] = useState('');
    const [editInputIndex, setEditInputIndex] = useState(-1);
    const [editInputValue, setEditInputValue] = useState('');
    const [messageError, setMessageError] = useState<string>('');
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (messageError) {
            setShowMessageError(true);
        }
    }, [messageError]);
    const handleClose = (tag: string) => {
        const newTags = tags.filter((t: string) => t !== tag);
        setTags([...newTags]);
    };

    const showInput = () => {
        setInputVisible(true);
        // setState({ inputVisible: true }, () => input.focus());
    };

    const [showMessage, setShowMessage] = useState(true);
    let timeOut = 0;
    const validateInput = (e: any) => {
        const value = e.target.value;
        if (value.trim().length > 20) {
            if (showMessage) {
                setMessageError(`${t('max_tag_teacher_input')}`);
                setShowMessage(false);
                timeOut = setTimeout(() => {
                    setShowMessage(true);
                }, 3000);
            }

            return;
        } else if (value.trim().length < 0) return false;
        return true;
    };

    useEffect(() => {
        return () => clearTimeout(timeOut);
    }, []);

    const handleInputChange = (e: any) => {
        if (validateInput(e)) {
            setInputValue(e.target.value);
        }
        return;
    };

    const handleInputConfirm = (input: any) => {
        let newTags = [...tags];
        if (newTags.length >= 10) {
            setMessageError(`${t('max_tag_teacher')}`);
            setInputVisible(false);
            return;
        }
        if (inputValue.trim().length === 0) {
            setInputVisible(false);
            setInputValue('');
            return;
        }
        if (inputValue && newTags.indexOf(inputValue) === -1) {
            newTags = [...newTags, inputValue.trim()];
        }

        setTags(newTags);
        setInputVisible(false);
        setInputValue('');
    };

    const handleEditInputChange = (e: any) => {
        if (validateInput(e)) {
            setEditInputValue(e.target.value);
        }
        return;
    };

    const handleEditInputConfirm = () => {
        const newTags = [...tags];
        if (editInputValue.trim().length === 0) {
            handleClose(newTags[editInputIndex]);
            return;
        }
        newTags[editInputIndex] = editInputValue.trim();

        setTags(newTags);
        setEditInputIndex(-1);
        setEditInputValue('');
    };

    const saveInputRef = (input: any) => {
        setInput(input);
        input?.focus();
    };

    useEffect(() => {
        if (props.data) {
            const data = props.data.split(',');
            setTags(data);
        }
    }, []);

    return (
        <div className="">
            <Input name={props?.name} value={tags} type="hidden" />
            {tags.map((tag: any, index: number) => {
                if (editInputIndex === index) {
                    return (
                        <Input
                            key={tag}
                            size="large"
                            className="tag-input w-auto"
                            value={editInputValue}
                            onChange={handleEditInputChange}
                            onBlur={handleEditInputConfirm}
                            onPressEnter={handleEditInputConfirm}
                        />
                    );
                }

                const isLongTag = tag.length > 20;

                const tagElem = (
                    <Tag
                        color="#EBEDF2"
                        className="edit-tag span-edit-tag"
                        key={tag}
                        closable={index !== -1}
                        onClose={() => handleClose(tag)}
                    >
                        <span
                            style={{ color: '#27367A' }}
                            onDoubleClick={(e) => {
                                setEditInputIndex(index);
                                setEditInputValue(tag);
                                e.preventDefault();
                            }}
                        >
                            {isLongTag ? `${tag.slice(0, 20)}...` : tag}
                        </span>
                    </Tag>
                );
                return isLongTag ? (
                    <Tooltip title={tag} key={tag}>
                        {tagElem}
                    </Tooltip>
                ) : (
                    tagElem
                );
            })}
            {inputVisible && (
                <Input
                    ref={saveInputRef}
                    type="text"
                    size="large"
                    className="tag-input w-auto"
                    value={inputValue}
                    onChange={handleInputChange}
                    onBlur={handleInputConfirm}
                    onPressEnter={handleInputConfirm}
                />
            )}
            {!inputVisible && tags.length < 10 && (
                <Tag
                    color="#EBEDF2"
                    className="site-tag-plus"
                    onClick={showInput}
                >
                    <PlusOutlined />
                    <Typography.Text className="tag-label-color">
                        {t('add')}
                    </Typography.Text>
                </Tag>
            )}
            {showMessageError && (
                <ModalError
                    messageError={messageError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
}

export default TagTeacher;
