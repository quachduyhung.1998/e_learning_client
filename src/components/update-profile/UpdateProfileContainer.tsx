import { message, Radio } from 'antd';
import TagTeacher from 'components/course/TagTeacher';
import TitleComponent from 'components/text/TitleComponent';
import { default as constStyle, default as styleColor } from 'constants/Style';
import LocalStorageHelper from 'helper/LocalStorageHelper';
import { ToastSuccess } from 'helper/Toast';
import { loginNavigation } from 'helper/Utils';
import useQuery from 'hooks/route/useQuery';
import useUpload from 'hooks/useUpload';
import images from 'images';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useLocation } from 'react-router-dom';
import {
    Button,
    Col,
    Container,
    Form,
    FormGroup,
    Input,
    Label,
    Row
} from 'reactstrap';
import { UpdateUserReq } from 'types/requests';
import { UpdateUserRes } from 'types/responses/UpdateUserRes';
import { Gender, User, RoleType } from 'types/User';
import './UpdateProfileContainer.scss';
import { HOME } from 'routes/web/paths';
import ModalError from 'components/shares/modal/ModalError';

interface Props {}

const UpdateProfileContainer = (props: Props): JSX.Element => {
    const { state } = useLocation<{ user: User; selectedRole: string }>();

    const { t } = useTranslation();
    const history = useHistory();
    const query = useQuery();
    const [gender, setGender] = useState<Gender | undefined>();
    const [age, setAge] = useState<string | undefined>('');
    const [selectedFile, setSelectedFile] = useState<File>();
    const [receivedFile, onUpload, messageError] = useUpload();
    const [preview, setPreview] = useState();
    const user = state?.user || LocalStorageHelper.getUserStorage();
    const roleSelected =
        state?.selectedRole || LocalStorageHelper.getCurrentRole();
    const [messageErrorIModal, setMessageErrorIModal] = useState<string>('');
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };

    useEffect(() => {
        if (messageErrorIModal || messageError) {
            setShowMessageError(true);
        }
    }, [messageErrorIModal, messageError]);
    useEffect(() => {
        if (!user) return;
        setGender(user.gender);
        if (!user.age || user.age === 'null' || user.age === 'undefined') {
            setAge('');
        } else {
            setAge(user.age + '');
        }
    }, [state]);

    const onUpdateInfoClicked = async (e: any) => {
        e.preventDefault();
        if (!user || !roleSelected) return;
        updateUserInfo(e.target);
    };

    const onSkipUpdateClicked = () => {
        if (!user || !roleSelected) return;
        loginNavigation(user, roleSelected, history, true);
    };

    const onNavigation = (user: User) => {
        if (!user || !roleSelected) return;
        if (roleSelected === RoleType.STUDENT) {
            history.push(HOME);
            return;
        }
        loginNavigation(user, roleSelected, history);
    };

    const onUpdateUserSuccess = (user: User) => {
        ToastSuccess({ message: t('update_info_success') });
        LocalStorageHelper.updateUserStorage(user);
        onNavigation(user);
    };

    const onUpdateUserFail = () => {
        setMessageErrorIModal(`${t('update_info_failed')}`);
    };

    const updateUserInfo = async (form: any) => {
        let ageValid = age;
        if (!ageValid || ageValid === 'null' || ageValid == 'undefined') {
            ageValid = undefined;
        }
        if (Number(ageValid) === 0) {
            setMessageErrorIModal(`${t('age_must_be_greater_than_0')}`);
            return;
        }
        const interests = form.interests.value;
        const strong = form.strong.value;
        const weakness = form.weakness.value;
        const shortTerm = form.shortTerm.value;
        const longTerm = form.longTerm.value;
        try {
            const res = await ApiHelper.post<UpdateUserReq, UpdateUserRes>(
                Endpoint.UPDATE_USER_PROFILE,
                {
                    gender,
                    age: ageValid,
                    interesting: interests,
                    strongPoint: strong,
                    weakPoint: weakness,
                    shortTermGoal: shortTerm,
                    longTermGoal: longTerm
                    // avatar: receivedFile ? receivedFile[0].path : ''
                }
            );
            if (res.status === SUCCESS) {
                // Dựa theo role để điều hướng
                onUpdateUserSuccess(res.data);
            } else {
                onUpdateUserFail();
                // Thông báo lỗi
            }
        } catch (error) {
            onUpdateUserFail();
            // Thông báo lỗi
        }
    };

    const onCheckChange = (gender: Gender) => () => {
        setGender(gender);
    };

    // Sự kiện click nút quay lại
    const handClickBack = () => {
        history.goBack();
    };

    //Check hiển thị nút quay lại
    const renderTitle = () => {
        if (query.get('query')) {
            return (
                <Row>
                    <Col span={12}>
                        <TitleComponent
                            title={t('update_information')}
                            color={styleColor.COLOR_MAIN}
                        />
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <Button
                            className="btn-style-user"
                            onClick={handClickBack}
                        >
                            <img src={images.ic_down} alt="" />
                            {t('back')}
                        </Button>
                    </Col>
                </Row>
            );
        } else {
            return (
                <Row>
                    <Col span={12}>
                        <TitleComponent
                            title={t('update_information')}
                            color={styleColor.COLOR_MAIN}
                        />
                    </Col>
                </Row>
            );
        }
    };

    useEffect(() => {
        if (!selectedFile) {
            setPreview(undefined);
            return;
        }

        const objectUrl: any = URL.createObjectURL(selectedFile);
        setPreview(objectUrl);
        const bodyFormData = new FormData();
        bodyFormData.append('image', selectedFile);
        onUpload(bodyFormData);
        return () => URL.revokeObjectURL(objectUrl);
    }, [selectedFile]);

    const onSelectFile = (e: ChangeEvent<HTMLInputElement>) => {
        if (!e.target.files || e.target.files.length === 0) {
            setSelectedFile(undefined);
            return;
        }
        const file = e.target.files[0];
        const isJpgOrPng =
            file.type === 'image/jpeg' || file.type === 'image/png';
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            setMessageErrorIModal(`${t('image_must_smaller_than_2MB')}`);
            setSelectedFile(undefined);
            return;
        }
        if (!isJpgOrPng) {
            setMessageErrorIModal(`${t('you_can_only_upload_JPG_PNG_file')}`);
            setSelectedFile(undefined);
            return;
        }
        setSelectedFile(file);
    };
    return (
        <div>
            <Container className="update-profile-wrapper bg-white mt-2 p-4">
                {renderTitle()}
                {query.get('query') && (
                    <>
                        <Col span={12} style={{ marginTop: '30px' }}>
                            <Label className="update-profile-label">
                                {t('avatar')}
                            </Label>
                        </Col>
                        <div className="avatar-upload">
                            <div className="avatar-edit">
                                <input
                                    type="file"
                                    id="imageUpload"
                                    onChange={onSelectFile}
                                />
                                <label
                                    htmlFor="imageUpload"
                                    style={{
                                        backgroundPositionY: '0',
                                        backgroundImage: `url(${images.ic_upload_avatar})`
                                    }}
                                />
                            </div>
                            <div className="avatar-preview">
                                {selectedFile ? (
                                    <img
                                        src={preview}
                                        style={{
                                            width: '120px',
                                            height: '120px',
                                            borderRadius: '50%'
                                        }}
                                    />
                                ) : (
                                    <div
                                        id="imagePreview"
                                        style={{
                                            backgroundPositionY: '0',
                                            backgroundImage: `url(${images.ic_avatar})`
                                        }}
                                    ></div>
                                )}
                            </div>
                        </div>
                    </>
                )}
                <Form className="m-4" onSubmit={onUpdateInfoClicked}>
                    <Row form>
                        <Col span={12}>
                            <Label className="update-profile-label">
                                {t('gender')}
                            </Label>

                            <Row className="mr-2 mt-1">
                                <Col>
                                    <Radio
                                        id="male"
                                        onChange={onCheckChange(Gender.MALE)}
                                        checked={gender === Gender.MALE}
                                        name="gender"
                                    >
                                        {t('gender_male')}
                                    </Radio>
                                </Col>

                                <Col>
                                    <Radio
                                        onChange={onCheckChange(Gender.FEMALE)}
                                        checked={gender === Gender.FEMALE}
                                        id="female"
                                        name="gender"
                                    >
                                        {t('gender_female')}
                                    </Radio>
                                </Col>

                                <Col>
                                    <Radio
                                        checked={gender === Gender.OTHER}
                                        id="other"
                                        name="gender"
                                        onChange={onCheckChange(Gender.OTHER)}
                                    >
                                        {t('gender_other')}
                                    </Radio>
                                </Col>
                            </Row>
                        </Col>

                        <Col span={12}>
                            <FormGroup>
                                <Label className="update-profile-label">
                                    {t('age')}
                                </Label>
                                <Input
                                    onChange={(evt: any) => {
                                        const age = evt.target.value.replace(
                                            /[^\d]/g,
                                            ''
                                        );
                                        if (
                                            Number(age) === 0 &&
                                            age &&
                                            age.length > 2
                                        ) {
                                            setMessageErrorIModal(
                                                `${t(
                                                    'age_must_be_greater_than_0'
                                                )}`
                                            );
                                            return;
                                        }
                                        if (Number(age) > 100) {
                                            setMessageErrorIModal(
                                                `${t('age_not_correct')}`
                                            );
                                            return;
                                        }
                                        setAge(age);
                                    }}
                                    name="age"
                                    value={age}
                                    placeholder={t('age')}
                                />
                            </FormGroup>
                        </Col>
                    </Row>

                    <Row form className="mt-4">
                        <Col>
                            <Button
                                type="submit"
                                className="mr-2"
                                style={{ width: '15%' }}
                                color="primary"
                            >
                                {t('cta_update_profile')}
                            </Button>
                            {!query.get('query') && (
                                <Button
                                    onClick={onSkipUpdateClicked}
                                    color="red"
                                    style={{
                                        width: '15%',
                                        border: '1px solid gray',
                                        color: constStyle.COLOR_MAIN,
                                        fontWeight: 'bold'
                                    }}
                                >
                                    {t('ignore')}
                                </Button>
                            )}
                        </Col>
                    </Row>
                </Form>
            </Container>
            {showMessageError && (
                <ModalError
                    messageError={
                        messageErrorIModal ? messageErrorIModal : messageError
                    }
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
};

export default React.memo(UpdateProfileContainer);
