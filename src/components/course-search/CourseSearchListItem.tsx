import { Col, Row, Button, Tooltip } from 'antd';
import getImageServer from 'helper/ImageHelper';
import LocalStorageHelper from 'helper/LocalStorageHelper';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { LECTURERS_COURSE, STUDENT_COURSE_DETAIL } from 'routes/web/paths';
import { Course } from 'types/Course';
import { RoleType } from 'types/User';
import './CourseSearchListItem.scss';
import { getRoleByRoute } from 'helper/Utils';

interface Props {
    course: Course;
}

function CourseSearchListItem(props: Props): JSX.Element {
    const { t } = useTranslation();
    const history = useHistory();
    const { course } = props;

    /**
     * Click vào button xem chi tiết
     * Nếu là admin thì return;
     * Nếu là học sinh/giảng viên (Không phải chủ khóa học) thì đi đến màn hình chi tiết khóa học
     * Nếu là giáo viên (Chủ khóa học) thì đi vào màn chỉnh sửa khóa học
     */
    const onSeeDetailClicked = () => {
        const user = LocalStorageHelper.getUserStorage();
        const role = getRoleByRoute();
        if (!user || !role) return;

        if (role === RoleType.ADMIN) {
            alert('To be defined');
            return;
        }

        if (role === RoleType.STUDENT) {
            history.push(`${STUDENT_COURSE_DETAIL}/${course.id}`);
            return;
        }

        if (role === RoleType.TEACHER) {
            history.push(`${LECTURERS_COURSE}/${course.id}`);
            return;
        }
    };

    return (
        <Row gutter={[8, 8]}>
            <Col sm={{ span: 10 }}>
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <div style={{ marginRight: '.5em' }}>
                        <img
                            style={{ width: '150px' }}
                            className="course-item"
                            src={getImageServer(course.image)}
                        />
                    </div>
                    <div style={{ padding: '0.5em' }}>
                        <Tooltip placement="topLeft" title={course.fullName}>
                            <p
                                onClick={onSeeDetailClicked}
                                className="hover-active-no-bold"
                                style={{
                                    overflow: 'hidden',
                                    whiteSpace: 'nowrap',
                                    padding: 0,
                                    textOverflow: 'ellipsis',
                                    maxWidth: 250
                                }}
                            >
                                <b
                                    className="text-active"
                                    style={{ fontSize: 18 }}
                                >
                                    {course.fullName}
                                </b>
                            </p>
                        </Tooltip>
                        <p>
                            {t('theme')}: <b>{course.categoryName}</b>
                        </p>
                        <Button onClick={onSeeDetailClicked}>
                            {t('see_detail')}
                        </Button>
                    </div>
                </div>
            </Col>
            <Col sm={{ span: 14 }} style={{ padding: '0.5em' }}>
                <div
                    style={{
                        backgroundColor: '#f5f6f9',
                        height: '100%',
                        padding: '0.5em'
                    }}
                >
                    <b>{t('summary')}</b>
                    <div
                        className="content"
                        dangerouslySetInnerHTML={{
                            __html: course.description ? course.description : ''
                        }}
                    />
                </div>
            </Col>
        </Row>
    );
}

export default CourseSearchListItem;
