import PaginationComponent from 'components/pagination/PaginationComponent';
import React from 'react';
import { Course } from 'types/Course';
import './CourseSearchListComponent.scss';
import CourseSearchListItem from './CourseSearchListItem';
import { Space } from 'antd';
import MainEmpty from 'components/shares/MainEmpty';
interface Props {
    query: string;
    page: number;
    onPageChange: (selectedItem: { selected: number }) => void;
    totalPage: number;
    listCourse: Course[];
    total: number;
}

function CourseSearchListComponent(props: Props): JSX.Element {
    const page = props.page - 1;
    const { totalPage, listCourse } = props;

    return (
        <div style={{ padding: '0.5em' }}>
            {listCourse.map((course: Course) => {
                return <CourseSearchListItem key={course.id} course={course} />;
            })}
            {!listCourse.length && <MainEmpty />}

            <PaginationComponent
                pageCount={totalPage}
                initialPage={page}
                pageRangeDisplayed={3}
                marginPagesDisplayed={5}
                onPageChange={props.onPageChange}
            />
        </div>
    );
}

export default CourseSearchListComponent;
