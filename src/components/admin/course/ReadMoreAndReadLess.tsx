import React, { useState, useEffect } from 'react';
import { User } from 'types/User';
import slice from 'lodash/slice';
import truncate from 'lodash/truncate';
import { useTranslation } from 'react-i18next';
import { Row, Tag, Popconfirm, Tooltip } from 'antd';
import {
    EyeOutlined,
    EyeInvisibleOutlined,
    DoubleLeftOutlined,
    DoubleRightOutlined
} from '@ant-design/icons';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { BaseResponse } from 'types/responses/BaseResponse';
import { SUCCESS } from 'network/ResponseCode';
import { ToastError, ToastSuccess } from 'components/admin/Toast';
interface BaseProps {
    users: User[];
    onFetchCourses: () => void;
    courseId: number;
    courseHandle: string;
}
const ReadMoreAndReadLess = (props: BaseProps): JSX.Element => {
    const { t } = useTranslation();
    const [data, setData] = useState<User[]>(slice(props.users, 0, 5));
    const [length] = useState<number>(props.users.length);
    const [expand, setExpand] = useState<boolean>(false);
    const onHover = (e: any) => {
        e.target.style.background = '#ad4e00';
    };

    useEffect(() => {
        setData(slice(props.users, 0, 5));
    }, [props.users]);

    const onMouseOut = (e: any) => {
        e.target.style.background = '#6d2492';
    };

    const icon = (length: number, expand: boolean) => {
        if (length > 5) {
            return (
                <a
                    href="javascript:void(0)"
                    onClick={() => onHandleData(expand)}
                >
                    <Tag
                        icon={
                            expand === true ? (
                                <DoubleLeftOutlined />
                            ) : (
                                <DoubleRightOutlined />
                            )
                        }
                        color="rgb(49, 71, 90)"
                    >
                        {expand === true ? (
                            <EyeInvisibleOutlined />
                        ) : (
                            <EyeOutlined />
                        )}
                    </Tag>
                </a>
            );
        } else {
            return '';
        }
    };

    const handleDelete = async (
        courseId: number,
        userId: string,
        courseHandle: string,
        collectionType: string
    ) => {
        const res = await ApiHelper.post<
            {
                courseId: number;
                userId: string;
                courseHandle: string;
                collectionType: string;
            },
            BaseResponse
        >(Endpoint.REMOVE_USER_FROM_ASSIGNED_LIST, {
            courseId,
            userId,
            courseHandle,
            collectionType
        });
        if (res.status === SUCCESS) {
            props.onFetchCourses();
            ToastSuccess({
                message: t('success'),
                description: t('success')
            });
        } else {
            ToastError({
                message: t('fail'),
                description: res.data.results
                    ? res.data.results + ' ' + t(`status_code.${res.status}`)
                    : t(`status_code.${res.status}`)
            });
        }
    };

    const onHandleData = (expand: boolean) => {
        if (expand === false) {
            setData(props.users);
            setExpand(true);
        } else {
            setData(slice(props.users, 0, 5));
            setExpand(false);
        }
    };

    const renderListUser = (users: User[]) => {
        const rs = [];
        for (const user of users) {
            rs.push(
                <Popconfirm
                    title={t('are_you_sure_to_remove')}
                    onConfirm={(e) =>
                        handleDelete(
                            props.courseId,
                            user._id,
                            props.courseHandle,
                            user.collectionType
                        )
                    }
                    okText={t('yes')}
                    cancelText={t('no')}
                >
                    <Tooltip placement="topLeft" title={t('click_to_remove')}>
                        <Tag
                            onMouseOver={onHover}
                            onMouseOut={onMouseOut}
                            color="#6d2492"
                            style={{ margin: '1px' }}
                        >
                            {user && user.fullName
                                ? truncate(user.fullName, {
                                      length: 20,
                                      separator: '...'
                                  })
                                : truncate(user.username, {
                                      length: 10,
                                      separator: '...'
                                  })}
                        </Tag>
                    </Tooltip>
                </Popconfirm>
            );
        }
        return rs;
    };

    return (
        <Row>
            {renderListUser(data)}
            {icon(length, expand)}
        </Row>
    );
};

export default ReadMoreAndReadLess;
