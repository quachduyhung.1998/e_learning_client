import { Form, message, Modal, Radio, Select } from 'antd';
import { keySplit } from 'helper/String';
import useListUser from 'hooks/user/useListUserWithPermisson';
import filter from 'lodash/filter';
import map from 'lodash/map';
import takeWhile from 'lodash/takeWhile';
import truncate from 'lodash/truncate';
import ApiHelper from 'network/ApiClient';
import Endpoint from 'network/Endpoint';
import { SUCCESS } from 'network/ResponseCode';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { CollectionType, CourseHandle } from 'types/Base';
import { BaseResponse } from 'types/responses/BaseResponse';
import { RoleType } from 'types/User';

interface userParams {
    page: number;
    limit: number;
    role: RoleType;
    courseId: number;
    courseHandle: string;
}

const ModalAddTeacher = (props: {
    hiddenModal: () => void;
    visible: boolean;
    id: number;
    form: any;
    onFetchCourses: () => void;
}): JSX.Element => {
    const { t } = useTranslation();
    const [modalState, setModalState] = useState(props);
    const [listTeacher, setListTeacher] = useState([]);
    const [selectedUserIds, setSelectedUserIds] = useState<string[]>([]);
    const [selectedGroupIds, setSelectedGroupIds] = useState<string[]>([]);
    const [onFetchUsers, users, loading] = useListUser();
    const [courseHandle, setCourseHandle] = useState(CourseHandle.REVIEW);
    const [values, setValues] = useState<string>();

    useEffect(() => {
        setModalState(props);
    }, [props]);

    const [params, setParams] = useState<userParams>({
        page: 1,
        limit: 1000,
        role: RoleType.TEACHER,
        courseId: modalState.id,
        courseHandle: CourseHandle.APPROVE
    });
    const timeAlert = 1500;
    useEffect(() => {
        onFetchUsers({ role: 1 });
    }, []);
    useEffect(() => {
        if (users) {
            setListTeacher(users);
        }
    }, [users]);
    // const listDataTeacher = async () => {
    //     try {
    //         const res = await ApiHelper.fetch<
    //             {
    //                 page: number;
    //                 limit: number;
    //                 role: RoleType;
    //                 courseId: number;
    //                 courseHandle: string;
    //             },
    //             BaseResponse
    //         >(Endpoint.USERS_SEARCH_UNEROLLED, {
    //             ...params
    //         });
    //         if (res && res.status === 200) {
    //             setListTeacher(res.data.results);
    //         }
    //     } catch (error) {}
    // };

    const handleCancel = () => {
        props.hiddenModal();
        setValues('');
        setSelectedUserIds([]);
        setSelectedGroupIds([]);
    };

    useEffect(() => {
        setParams({ ...params, courseId: modalState.id });
    }, [modalState.id]);

    // useEffect(() => {
    //     listDataTeacher();
    // }, [params]);

    const options: { label: string; value: string }[] = [];
    listTeacher?.map((item: any) => {
        options.push({
            label: truncate(item.name, {
                length: 60,
                separator: '...'
            }),
            value: item.id
        });
    });

    const [modal, contextHolder] = Modal.useModal();
    /**
     * Thông báo học viên gán vào khóa học
     * @param param0
     */
    const alertModal: ({
        title,
        content,
        type
    }: {
        title: any;
        content: any;
        type: any;
    }) => void = ({ title, content, type }) => {
        const option: {
            title: string;
            content: string;
            okButtonProps: {
                disabled: boolean;
                className: string;
            };
        } = {
            title: title,
            content: content,
            okButtonProps: {
                disabled: true,
                className: 'modal-hiden-button'
            }
        };
        let newModal: any = null;

        if (type === 'success') {
            newModal = modal.success(option);
        } else {
            newModal = modal.error(option);
        }
        if (newModal) {
            setTimeout(() => {
                newModal.destroy();
            }, timeAlert);
        }
    };

    /**
     * Gán học viên của khóa học
     * @param e
     */
    const handleSubmit = async (e: { preventDefault: () => void }) => {
        e.preventDefault();
        if (values?.length === 0) {
            alertModal({
                title: t('error'),
                content: t('please_select_teachers'),
                type: 'error'
            });
            return;
        }
        const approved = await ApiHelper.post<
            {
                course_id: number;
                user_id: string | undefined;
            },
            BaseResponse
        >(Endpoint.ASSIGN_TEACHER_TO_APPROVE, {
            course_id: props.id,
            user_id: values
        });
        if (approved.status === SUCCESS || approved.status === 201) {
            props.hiddenModal();
            props.onFetchCourses();
            alertModal({
                title: t('success'),
                content: t('add_success'),
                type: 'success'
            });
            setValues(undefined);
            setSelectedUserIds([]);
            setSelectedGroupIds([]);
        } else {
            message.success('fail');
        }
    };
    const handleChange = (selectedItems: string) => {
        // const items: any[] = [];
        // if (selectedItems && selectedItems.length) {
        //     selectedItems.map((item) => {
        //         items.push(keySplit(item));
        //     });
        // }
        // if (items.length) {
        //     const users = filter(items, {
        //         collectionType: CollectionType.USER
        //     });
        //     const groups = filter(items, {
        //         collectionType: CollectionType.GROUP
        //     });
        //     const userIds = users.length ? map(takeWhile(users), 'userId') : [];
        //     const groupIds = groups.length
        //         ? map(takeWhile(groups), 'userId')
        //         : [];
        //     setSelectedUserIds(userIds);
        //     setSelectedGroupIds(groupIds);
        // }
        setValues(selectedItems);
    };
    const onChangeCourseHandle = (e: any) => {
        e.preventDefault();
        setParams({
            ...params,
            courseHandle: e.target.value
        });
        setCourseHandle(e.target.value);
        setValues(undefined);
    };

    return (
        <>
            <Modal
                visible={props.visible}
                title={t('select_teacher_to_improve_review_course')}
                onOk={handleSubmit}
                onCancel={handleCancel}
            >
                <Form name="nest-messages">
                    <Radio.Button value={CourseHandle.APPROVE}>
                        {t(CourseHandle.APPROVE)}
                    </Radio.Button>

                    <Form.Item>
                        <Select
                            // mode="multiple"
                            size="middle"
                            placeholder={t('please_select')}
                            options={options}
                            style={{ width: '100%' }}
                            onChange={handleChange}
                            allowClear={true}
                            value={values}
                            optionFilterProp="label"
                        ></Select>
                    </Form.Item>
                </Form>
            </Modal>
            {contextHolder}
        </>
    );
};

export default ModalAddTeacher;
