import { Select } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Module from 'types/Module';

const { Option } = Select;

interface Props {
    onUpdate: (module: Module | undefined) => void;
    modules: Module[];
    conditions: number[];
}

function SelectCondition(props: Props): JSX.Element {
    const { t } = useTranslation();

    const handleChangeSelect = (value: string): void => {
        const moduleTemp = props.modules.find(
            (x: Module) => x.id === parseInt(value)
        );
        props.onUpdate(moduleTemp);
    };
    return (
        <Select
            defaultActiveFirstOption={true}
            defaultValue={props.conditions.join(',')}
            onChange={handleChangeSelect}
            style={{ width: '100%' }}
        >
            <Option value="">{t('no')}</Option>
            {props.modules &&
                props.modules.length &&
                props.modules.map((item: Module) => {
                    return (
                        <Option key={item.id} value={item.id.toString()}>
                            {item.name}
                        </Option>
                    );
                })}
        </Select>
    );
}

export default SelectCondition;
