import React from 'react';
import { SEP_API_DOMAIN } from 'constants/App';

interface Props {
    width?: string;
    controls?: boolean;
    src?: string;
}

function PreView(props: Props): JSX.Element {
    const { width, controls, src } = props;
    if (src) {
        return (
            <video
                width={width || '100%'}
                controls={controls || true}
                src={`${SEP_API_DOMAIN}${src}`}
            />
        );
    } else {
        return <div></div>;
    }
}

export default PreView;
