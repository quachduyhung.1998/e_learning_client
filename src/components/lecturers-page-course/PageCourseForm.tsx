import { Form, Input, Radio, message } from 'antd';
import { FormInstance } from 'antd/lib/form';
import CKEditor from 'components/ckeditor/ckEditor';
import SelectCondition from 'components/lecturers-page-course/SelectCondition';
import LabelRequired from 'components/shares/LabelRequired';
import constPageCourse from 'constants/PageCourse';
import useFetchTopic from 'hooks/useFetchTopic';
import useUpload from 'hooks/useUpload';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Module from 'types/Module';
import PageCourse from 'types/PageCourse';
import { Topic } from 'types/Topic';
import PreView from './Preview';
import ModalError from 'components/shares/modal/ModalError';

interface Props {
    pageCourse?: PageCourse | null;
    courseId: number;
    form: FormInstance;
}

function PageCourseForm(props: Props): JSX.Element {
    const { pageCourse, courseId } = props;
    const { t } = useTranslation();

    const validate = {
        name: [
            {
                required: true,
                message: t('this_is_required')
            },
            {
                max: 200,
                message: t('name_length_200_invalidate')
            },
            {
                min: 2,
                message: t('name_length_200_invalidate')
            }
        ],
        type: [
            {
                required: true,
                message: t('this_is_required')
            }
        ],
        content: [
            {
                required: true,
                message: t('this_is_required')
            }
        ],
        video: [
            {
                required: true,
                message: t('this_is_required')
            }
        ]
    };

    const [type, setType] = useState<string | number>(pageCourse?.type || 0);
    const [file, setFile] = useState<any>();
    const [urlPreview, setUrlPreview] = useState<string>(
        pageCourse?.content || ''
    );

    //get các bài học/ bài kiểm tra của khóa họ
    const [topics, setTopics] = useFetchTopic(courseId.toString());
    const [messageErrorIModal, setMessageErrorIModal] = useState<string>('');
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    const [resImages, onUpload, messageError] = useUpload();

    useEffect(() => {
        if (messageErrorIModal || messageError) {
            setShowMessageError(true);
        }
    }, [messageErrorIModal, messageError]);

    useEffect(() => {
        props?.form?.setFieldsValue({
            type: props?.pageCourse?.type,
            newVideo: 0,
            content: props?.pageCourse?.type === 0 ? pageCourse?.content : '',
            video: props?.pageCourse?.type === 1 ? pageCourse?.content : ''
        });
    }, [props.pageCourse]);

    const handleChangeContent = (
        e: React.ChangeEvent<HTMLInputElement>,
        editor: any
    ): void => {
        props?.form?.setFieldsValue({ content: editor.getData() });
    };

    const handleFocusBack = () => {
        if (pageCourse?.type === constPageCourse.TYPE_VIDEO) {
            setUrlPreview(pageCourse?.content || '');
            props?.form?.setFieldsValue({
                newVideo: 0,
                video: pageCourse?.content || ''
            });
        }
        window.removeEventListener('focus', handleFocusBack);
    };

    useEffect(() => {
        return () => window.removeEventListener('focus', handleFocusBack);
    }, []);

    const renderContent = (): any => {
        switch (type) {
            case 0:
                return (
                    <Form.Item
                        label={<LabelRequired label={t('content')} />}
                        name="content"
                        className="ckh-200"
                        // rules={validate.content}
                    >
                        <Input hidden style={{ width: '100%' }} />
                        <CKEditor
                            onChange={handleChangeContent}
                            data={
                                props.pageCourse?.type === 0
                                    ? pageCourse?.content
                                    : ''
                            }
                        />
                    </Form.Item>
                );

            case 1:
                return (
                    <Form.Item
                        label={<LabelRequired label={'URL Video'} />}
                        name="video"
                        className="ckh-100"
                        rules={validate.video}
                    >
                        <Input />
                    </Form.Item>
                );
        }
    };

    return (
        <>
            <Form.Item
                name="name"
                label={<LabelRequired label={t('title')} />}
                rules={validate.name}
            >
                <Input style={{ fontWeight: 'bold' }} />
            </Form.Item>
            <Form.Item
                name="type"
                label={<LabelRequired label={t('type')} />}
                rules={validate.type}
            >
                <Radio.Group
                    onChange={(e) => setType(e.target.value)}
                    value={type}
                >
                    <Radio value={0}>{t('normal_lesson')}</Radio>
                    <Radio value={1}>{t('video_lesson')}</Radio>
                </Radio.Group>
            </Form.Item>
            {renderContent()}
            {showMessageError && (
                <ModalError
                    messageError={messageErrorIModal}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </>
    );
}

export default PageCourseForm;
