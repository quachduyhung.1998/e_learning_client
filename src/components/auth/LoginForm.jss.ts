import { makeStyles } from '@material-ui/styles';

export const useLoginFormStyles = makeStyles({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        height: '100vh',
        display: 'flex',
        flexDirection: 'column'
    },
    input: {
        width: '20%',
        marginTop: 20
    }
});
