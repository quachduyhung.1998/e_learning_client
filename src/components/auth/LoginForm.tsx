import {
    Button,
    Checkbox,
    Col,
    Form,
    Input,
    Modal,
    Row,
    message,
    Avatar
} from 'antd';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import { loginNavigation } from 'helper/Utils';
import useLoginApi from 'hooks/useLoginApi';
import useUpdateCurrentRole from 'hooks/user/useUpdateCurrentRole';
import images from 'images';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { getStatusRemember } from './../../helper/AuthHelper';
import './LoginForm.scss';
import LoginRole from './LoginRole';
import ModalError from 'components/shares/modal/ModalError';
interface Remember {
    email: string;
    password: string;
    remember: boolean;
}

interface Props {}

const LoginForm: React.FC<Props> = (props: Props) => {
    const { t } = useTranslation();
    const [isLoading, onLogin, messageError, user] = useLoginApi();
    const history = useHistory();
    const [form] = Form.useForm();
    const [showModalRole, setShowModalRole] = useState<boolean>(false);
    const checkRememberMe: Remember = getStatusRemember();
    const [remember, setRemember] = useState<boolean>(
        checkRememberMe ? true : false
    );
    const handleChecked = () => {
        setRemember(!remember);
    };
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (messageError) {
            setShowMessageError(true);
        }
    }, [messageError]);
    const {
        resUpdateCurrentRole,
        onUpdateCurrentRole
    } = useUpdateCurrentRole();

    // useEffect(() => {
    //     if (resUpdateCurrentRole) {
    //         const role = LocalStorageHelper.getCurrentRole();
    //         user && role && loginNavigation(user, role, history);
    //     }
    // }, [resUpdateCurrentRole]);

    // const handleUpdateRole = (index: number) => {
    //     const role = user?.userRoles
    //         ? user.userRoles[index]
    //         : user?.roles[index].archetype;
    //     if (role) {
    //         onUpdateCurrentRole(role);
    //     }
    // };

    /**
     * Kiểm tra user đã login chưa, nếu login rồi thì thực hiện điều hướng vào màn hình phù hợp
     */
    // useEffect(() => {
    //     const userLocal = LocalStorageHelper.getUserStorage();
    //     const role = LocalStorageHelper.getCurrentRole();
    //     if (userLocal && role) {
    //         // loginNavigation(userLocal, role, history);
    //         history.push('/');
    //     }
    // }, []);

    /**
     * Function render khi user thay đổi
     */
    const onNavigation = () => {
        if (!user) return;
        if (user && user.userRoles) {
            if (user.userRoles.length !== 1) {
                return;
            }
        }
        if (user && user.roles) {
            if (user && user.roles.length !== 1) return;
        }
        LocalStorageHelper.save(
            StorageKey.CURRENT_ROLE,
            user.userRoles ? user.userRoles[0] : user.roles[0].archetype
        );
        // handleUpdateRole(0);
        loginNavigation(
            user,
            user.userRoles ? user.userRoles[0] : user.roles[0].archetype,
            history
        );
    };
    // useEffect(() => {
    //     onNavigation();
    //     setShowModalRole(true);
    // }, [user]);

    /**
     * Lắng nghe sự kiện khi click đăng nhập˘
     */
    const onSubmit = () => {
        form.validateFields()
            .then((values) => {
                onLogin(
                    {
                        email: values.userName,
                        password: values.password
                    },
                    remember
                );
            })
            .catch((e) => e);
    };

    /**
     * Render popup lựa chọn quyền để truy cập vào hệ thống
     * Popup chỉ được hiển thị nếu người dùng có 2 chức năng trở lên
     */
    // const renderRoleModal = (): JSX.Element | null => {
    //     if (!user) return null;
    //     if (user && user.userRoles) {
    //         if (user.userRoles.length <= 1) {
    //             return null;
    //         }
    //     }
    //     if (user && user.roles) {
    //         if (user && user.roles.length <= 1) return null;
    //     }
    //     return (
    //         <Modal
    //             title={
    //                 <div style={{ textAlign: 'center' }}>
    //                     {t('login_with_role')}
    //                 </div>
    //             }
    //             centered
    //             visible={showModalRole}
    //             footer={null}
    //             onCancel={() => setShowModalRole(false)}
    //         >
    //             <LoginRole updateRole={handleUpdateRole} user={user} />
    //         </Modal>
    //     );
    // };

    return (
        <div
            className="background-login"
            style={{
                height: '100% !important',
                backgroundImage: `url(${images.bg_login})`,
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat'
            }}
        >
            <div className="container">
                <div className="d-flex row justify-content-center align-items-center container-style">
                    <div className="col-md-6 login-form-item">
                        <div className="card">
                            <div className="cart-content">
                                <div className=" text-center cart-content-item">
                                    {/* <img
                                        src={images.ic_new_logo}
                                        alt=""
                                        className="cart-content-image"
                                    /> */}
                                    <Avatar className="cart-content-image" />
                                </div>
                            </div>

                            <div className="card-body">
                                <Row>
                                    <Col
                                        sm={{ span: 18, offset: 3 }}
                                        xs={{ span: 24 }}
                                    >
                                        <Form
                                            form={form}
                                            labelAlign="left"
                                            layout="vertical"
                                            // hideRequiredMark={true}
                                        >
                                            <Form.Item
                                                label={t('account')}
                                                name="userName"
                                                initialValue={
                                                    checkRememberMe
                                                        ? checkRememberMe?.email
                                                        : ''
                                                }
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: t(
                                                            'account_is_required'
                                                        )
                                                    }
                                                ]}
                                            >
                                                <Input />
                                            </Form.Item>
                                            <Form.Item
                                                label={t('password')}
                                                name="password"
                                                initialValue={
                                                    checkRememberMe
                                                        ? checkRememberMe?.password
                                                        : ''
                                                }
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: t(
                                                            'password_is_required'
                                                        )
                                                    }
                                                ]}
                                            >
                                                <Input.Password />
                                            </Form.Item>
                                            <Form.Item name="remember_me">
                                                <Checkbox
                                                    onChange={handleChecked}
                                                    checked={remember}
                                                >
                                                    {t('remember_me')}
                                                </Checkbox>
                                            </Form.Item>
                                            <Button
                                                type="primary"
                                                loading={isLoading}
                                                onClick={onSubmit}
                                                htmlType="submit"
                                            >
                                                {t('login')}
                                            </Button>
                                        </Form>
                                    </Col>
                                </Row>
                            </div>
                        </div>

                        {/* {renderRoleModal()} */}
                    </div>
                </div>
            </div>
            {showMessageError && (
                <ModalError
                    messageError={messageError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
};

export default React.memo(LoginForm);
