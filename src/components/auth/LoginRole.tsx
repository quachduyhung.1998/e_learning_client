import { Space } from 'antd';
import 'antd/dist/antd.css';
import { getRoleName } from 'helper/Utils';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { ListGroup } from 'reactstrap';
import { Role, User } from 'types/User';
import './LoginForm.scss';

interface Props {
    user: User;
    updateRole: (index: number) => void;
}

function LoginRole(props: Props): JSX.Element {
    const { t } = useTranslation();
    const role = ['manager', 'student', 'editingteacher'];
    const renderItem = (): JSX.Element[] => {
        return role.map((role: string, index: number) => {
            return (
                <ListGroup
                    key={role}
                    onClick={() => {
                        props.updateRole(index);
                    }}
                    size={8}
                    direction="vertical"
                >
                    <Space
                        size={8}
                        direction="vertical"
                        className="list-group-item list-group-item-action role-content-item text-label-style"
                    >
                        {t(getRoleName(role))}
                    </Space>
                </ListGroup>
            );
        });
    };

    return <div className="role-content">{renderItem()}</div>;
}

export default LoginRole;
