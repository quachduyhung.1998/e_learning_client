import { PlusOutlined } from '@ant-design/icons';
import { Input, Tag, Tooltip, message } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import ModalError from './modal/ModalError';

interface Props {
    tags?: string[];
    maxLengthItem?: number;
    maxItem?: number;
    textNew?: string;
    onChange?: (tags: string[]) => void;
}

const EditableTagGroup = (props: Props) => {
    const { t } = useTranslation();

    const [tags, setTags] = useState<string[]>(props.tags || []);
    const [inputVisible, setInputVisible] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState<string>('');
    const [editInputIndex, setEditInputIndex] = useState<number>(-1);
    const [editInputValue, setEditInputValue] = useState<string>('');

    const [input, setInput] = useState<Input>();
    const [editInput, setEditInput] = useState<Input>();
    const [messageError, setMessageError] = useState<string>('');
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (messageError) {
            setShowMessageError(true);
        }
    }, [messageError]);
    useEffect(() => {
        setTags(props?.tags?.filter((tag) => tag !== '') || []);
    }, [props.tags]);

    useEffect(() => {
        props?.onChange && props.onChange(tags);
    }, [tags]);

    const handleClose = (removedTag: string) => {
        const newTags = tags.filter((tag) => tag !== removedTag || tag === '');
        setTags(newTags);
    };

    const showInput = () => {
        const number = 10;
        if (tags.length <= number) {
            setInputVisible(true);
            input?.focus();
        } else {
            setMessageError(
                `${t('tags_length', { name: t('content'), max: number })}`
            );
        }
    };

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (
            props?.maxLengthItem &&
            e.target.value.trim().length > props?.maxLengthItem
        ) {
            return false;
        }
        setInputValue(e.target.value);
    };

    const handleInputOnBlur = () => {
        let newTags = [...tags];
        const value = inputValue.trim();
        if (value !== '' && newTags.indexOf(value) === -1) {
            newTags = [...newTags, value];
        }
        setTags(newTags);
        setInputVisible(false);
        setInputValue('');
    };

    const handleInputConfirm = (e: React.KeyboardEvent<HTMLInputElement>) => {
        handleInputOnBlur();
        showInput();
        e.preventDefault();
    };

    const handleEditInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (
            props?.maxLengthItem &&
            e.target.value.trim().length > props?.maxLengthItem
        ) {
            return false;
        }
        setEditInputValue(e.target.value);
    };

    const handleEditInputConfirm = () => {
        const newTags = [...tags];
        const value = editInputValue.trim();
        if (value !== '' && newTags.indexOf(value) === -1) {
            newTags[editInputIndex] = editInputValue;
        }
        setTags(newTags);
        setEditInputValue('');
        setEditInputIndex(-1);
    };

    const saveInputRef = (input: Input) => {
        setInput(input);
        input?.focus();
    };

    const saveEditInputRef = (input: Input) => {
        setEditInput(input);
        input?.focus();
    };

    return (
        <div
            style={{
                border: '1px solid #d9d9d9',
                minHeight: '30px',
                lineHeight: '30px'
            }}
        >
            {tags.map((tag: string, index: number) => {
                if (editInputIndex === index) {
                    return (
                        <Input
                            ref={saveEditInputRef}
                            key={tag}
                            size="small"
                            style={{
                                width: '78px',
                                marginRight: '8px',
                                verticalAlign: 'middle'
                            }}
                            value={editInputValue}
                            onChange={handleEditInputChange}
                            onBlur={handleEditInputConfirm}
                            onPressEnter={handleEditInputConfirm}
                        />
                    );
                }

                const isLongTag = tag.length > 20;

                const tagElem = (
                    <Tag
                        style={{ userSelect: 'none', margin: '0.125em' }}
                        key={tag}
                        closable={true}
                        onClose={() => handleClose(tag)}
                    >
                        <span
                            onDoubleClick={(e) => {
                                setEditInputIndex(index);
                                setEditInputValue(tag);
                                editInput?.focus();
                                e.preventDefault();
                            }}
                        >
                            {isLongTag ? `${tag.slice(0, 20)}...` : tag}
                        </span>
                    </Tag>
                );
                return isLongTag ? (
                    <Tooltip title={tag} key={tag}>
                        {tagElem}
                    </Tooltip>
                ) : (
                    tagElem
                );
            })}
            {inputVisible &&
                !(props?.maxItem && tags.length >= props?.maxItem) && (
                    <Input
                        ref={saveInputRef}
                        type="text"
                        size="small"
                        style={{
                            width: '78px',
                            marginRight: '8px',
                            verticalAlign: 'middle'
                        }}
                        value={inputValue}
                        onChange={handleInputChange}
                        onBlur={handleInputOnBlur}
                        onPressEnter={handleInputConfirm}
                    />
                )}
            {!inputVisible &&
                !(props?.maxItem && tags.length >= props?.maxItem) && (
                    <Tag
                        style={{
                            background: '#fff',
                            borderStyle: 'dashed',
                            padding: 0
                        }}
                        onClick={showInput}
                    >
                        <PlusOutlined style={{ verticalAlign: '0.125em' }} />{' '}
                        {props.textNew || t('add')}
                    </Tag>
                )}
            {showMessageError && (
                <ModalError
                    messageError={messageError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
};

export default EditableTagGroup;
