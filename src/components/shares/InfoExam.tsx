import { Button, Col, Space } from 'antd';
import React, { useRef, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Row } from 'reactstrap';
import { DoubleRightOutlined } from '@ant-design/icons';
import moment from 'antd/node_modules/moment';

interface Props {
    name: string;
    typeQuestion: string;
    totalQuestion: number;
    timeLimit: number;
    onStart?: () => void;
    onCancel?: () => void;
    grade?: number;
    gradePass?: number;
    height?: string;
    timeOpen?: Date;
    time_start?: any;
    time_end?: any;
    timeClose?: Date;
}

const InfoExam = (props: Props) => {
    const { t } = useTranslation();

    // - new Date() >=0  && (new Date(props.timeOpen)-new Date()) <=0) ){
    // disableBtnStart = true

    return (
        <div
            style={{
                padding: '1em',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between'
            }}
        >
            <Row gutter={[8, 8]}>
                <Col span={8}>{t('name_exam')}</Col>
                <Col style={{ fontWeight: 'bold' }} span={16}>
                    {props.name}
                </Col>

                <Col span={8}>{t('question_form')}</Col>
                <Col style={{ fontWeight: 'bold' }} span={16}>
                    {props.typeQuestion}
                </Col>

                <Col span={8}>{t('number_of_questions')}</Col>
                <Col style={{ fontWeight: 'bold' }} span={16}>
                    {props.totalQuestion}
                </Col>

                <Col span={8}>
                    {t('time')} ({t('minute').toLowerCase()})
                </Col>
                <Col style={{ fontWeight: 'bold' }} span={16}>
                    {props.timeLimit}
                </Col>
                {props?.gradePass !== undefined && props?.grade !== undefined && (
                    <>
                        <Col span={8}>
                            {t('target_exam')} {'(%)'}
                        </Col>
                        <Col style={{ fontWeight: 'bold' }} span={16}>
                            {(props.gradePass / props.grade) * 100}
                        </Col>
                    </>
                )}
                {props.time_start && (
                    <>
                        <Col span={8}>{t('time_open')}</Col>
                        <Col span={16}>
                            {props.time_start &&
                                new Date(props.time_start)?.toLocaleString()}
                        </Col>
                    </>
                )}
                {props.time_end && (
                    <>
                        <Col span={8}>{t('time_close')}</Col>
                        <Col span={16}>
                            {props.time_end &&
                                new Date(props.time_end)?.toLocaleString()}
                        </Col>
                    </>
                )}
            </Row>
            <Row justify="end">
                <Col span={24} style={{ textAlign: 'right' }}>
                    <Space>
                        {props?.onCancel && (
                            <Button
                                shape="round"
                                onClick={props.onCancel}
                                type="primary"
                                ghost
                            >
                                {t('ignore')}
                            </Button>
                        )}
                        {props?.onStart && (
                            <Button
                                shape="round"
                                onClick={props.onStart}
                                danger
                            >
                                {t('begin')} <DoubleRightOutlined />
                            </Button>
                        )}
                    </Space>
                </Col>
            </Row>
        </div>
    );
};

export default InfoExam;
