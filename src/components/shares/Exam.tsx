import {
    Button,
    Checkbox,
    Col,
    Popconfirm,
    Progress,
    Radio,
    Row,
    Space
} from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { RadioChangeEvent } from 'antd/lib/radio';
import ModalConfirm from 'components/shares/modal/Confirm';
import useTimeDown from 'hooks/time/useTimeDown';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DetailQuestion, QuestionTypeAnswer } from 'types/Question';
import { useHistory } from 'react-router-dom';
import moment from 'antd/node_modules/moment';
import { format } from 'helper/String';

interface Props {
    initIndex?: number;
    getQuestion?: (index: number) => void;
    updateAnswer: (id: number, index: number, answers: number[]) => void;
    setQuestion?: any;
    finishExam: (index: any, timeStart: string, timeFinish: string) => void;
    timeLimit: number;
    totalQuestion: number;
    question?: any;
    answers?: number[];
    timeStart?: number;
}

const Exam = (props: Props): JSX.Element => {
    const history = useHistory();
    const { t } = useTranslation();
    const { running, setRunning, percent, minuteString } = useTimeDown({
        seconds: props.timeLimit,
        secondsStart: props?.timeStart
    });
    const [questionIndex, setQuestionIndex] = useState<number>(
        props.initIndex || 0
    );
    const [questionIndexLast, setQuestionIndexLast] = useState<number>(
        props.totalQuestion || 1
    );
    const [timeStart, setTimeStart] = useState(
        moment().format(format.time.yyyy_mm_dd_hh_mm_ss)
    );
    const [timeFinish, setTimeFinish] = useState(
        moment().format(format.time.yyyy_mm_dd_hh_mm_ss)
    );
    const [answers, setAnswers] = useState<number[]>([]);
    const [indexAnser, setIndexAnswers] = useState<any>();
    const [showModalFinishExam, setShowModalFinishExam] = useState(false);
    const [currentQuestion, setCurrentQuestion] = useState<any>();
    const [answerChecked, setAnswerChecked] = useState<any>([]);
    // useEffect(() => {
    //     if (props.question) {
    //         if (props?.answers?.length) {
    //             const { question } = props;
    //             if (question && question?.single == 0) {
    //                 setAnswers(props?.answers);
    //             } else {
    //                 const newAnswers = [...props.question.answerCorrect];
    //                 const AnserNew = newAnswers.map((e, i) => {
    //                     if (e > 0) {
    //                         checkBoxAnswerChange(null, i);
    //                     }
    //                     return e;
    //                 });
    //                 setAnswers(AnserNew);
    //             }
    //         } else {
    //             setAnswers(props.question?.answerRandom.map(() => 0));
    //         }
    //     }
    // }, [props.question]);
    useEffect(() => {
        if (props.question) {
            setCurrentQuestion(props.question[questionIndex]);
            setQuestionIndexLast(props.question?.length - 1);
        }
    }, [questionIndex, props.question]);
    useEffect(() => {
        if (props.question) {
            let answerTemp: any[] = [];
            const answerTemp2: any = [];
            props.question?.map((item: any) => {
                answerTemp = [];
                item?.answers?.forEach((_e: any) => {
                    answerTemp.push({ key: _e.key, checked: false });
                });
                answerTemp2.push({ question: item.id, answer: answerTemp });
            });
            setAnswerChecked(answerTemp2);
            setTimeStart(moment().format(format.time.yyyy_mm_dd_hh_mm_ss));
        }
    }, [props.question]);

    // hết giời auto nộp bài
    useEffect(() => {
        if (!running) {
            props.finishExam(
                answerChecked,
                timeStart,
                moment().format(format.time.yyyy_mm_dd_hh_mm_ss)
            );
        }
    }, [running]);

    const checkBoxAnswerChange = (
        e: CheckboxChangeEvent | any,
        index: number
    ) => {
        let arrTemp = answerChecked;
        arrTemp[questionIndex].answer[index].checked = e.target.checked;
        setAnswerChecked(arrTemp);
    };

    const nextQuestion = () => {
        updateAnswer();
        if (running && questionIndex !== questionIndexLast) {
            setQuestionIndex(questionIndex + 1);
            setIndexAnswers(null);
        }
    };

    const preQuestion = () => {
        updateAnswer();
        if (running && questionIndex >= 1) {
            setQuestionIndex(questionIndex - 1);
            setIndexAnswers(null);
        }
    };

    const updateAnswer = () => {
        // props.updateAnswer &&
        //     props.updateAnswer(
        //         props?.question?.id || 0,
        //         questionIndex,
        //         answers
        //     );
    };

    const renderContentQuestion = (): JSX.Element => {
        if (currentQuestion) {
            return (
                <>
                    <div
                        dangerouslySetInnerHTML={{
                            __html: `${t('question')} ${questionIndex}: ${
                                currentQuestion?.name
                            }`
                        }}
                    ></div>
                    <Row style={{ padding: '1em' }}>
                        {currentQuestion?.answers?.map(
                            (answer: any, index: number) => {
                                return (
                                    <Col key={index} span={12}>
                                        {
                                            <div style={{ display: 'flex' }}>
                                                <Checkbox
                                                    value={index}
                                                    checked={
                                                        answerChecked?.length >
                                                            0 &&
                                                        answerChecked[
                                                            questionIndex
                                                        ]?.answer[index]
                                                            ?.checked
                                                    }
                                                    onChange={(e) =>
                                                        checkBoxAnswerChange(
                                                            e,
                                                            index
                                                        )
                                                    }
                                                ></Checkbox>
                                                {'\u00A0\u00A0'}
                                                <div
                                                    dangerouslySetInnerHTML={{
                                                        __html: `${answer?.key}`
                                                    }}
                                                ></div>
                                            </div>
                                        }
                                    </Col>
                                );
                            }
                        )}
                    </Row>
                </>
            );
        }
        return <></>;
    };
    return (
        <>
            <Row>
                <Col span={24}>
                    <div style={{ display: 'flex', fontWeight: 'bold' }}>
                        {`${questionIndex}/${questionIndexLast}`}
                        <Progress
                            showInfo={false}
                            percent={(questionIndex / questionIndexLast) * 100}
                            status={running ? 'active' : 'normal'}
                            className="mr-2 ml-2"
                        />
                        {minuteString}
                    </div>
                </Col>
            </Row>
            <div style={{ minHeight: '70vh' }}>{renderContentQuestion()}</div>

            {running && (
                <Row justify="end">
                    <Space>
                        <Button
                            disabled={!running || questionIndex <= 0}
                            onClick={preQuestion}
                            shape="round"
                        >
                            {t('previous_question')}
                        </Button>
                        <Button
                            disabled={
                                !running || questionIndex === questionIndexLast
                            }
                            onClick={nextQuestion}
                            shape="round"
                        >
                            {t('next_question')}
                        </Button>
                        {questionIndex === questionIndexLast && (
                            <Button
                                shape="round"
                                type="primary"
                                danger
                                onClick={() => setShowModalFinishExam(true)}
                            >
                                {t('submit_test')}
                            </Button>
                        )}
                    </Space>
                </Row>
            )}

            <ModalConfirm
                title={t('submit_exam')}
                content={t('question_submit_exam')}
                onVisible={showModalFinishExam}
                onOk={() => {
                    setShowModalFinishExam(false);
                    setRunning(false);
                }}
                onCancel={() => setShowModalFinishExam(false)}
                cancelText={t('no')}
                okText={t('yes')}
            />
        </>
    );
};

export default Exam;
