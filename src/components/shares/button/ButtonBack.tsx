import { Button } from 'antd';
import { ButtonType } from 'antd/lib/button';
import images from 'images';
import React from 'react';
import { useTranslation } from 'react-i18next';

interface Props {
    onClick: (e?: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    type?: ButtonType;
}

function ButtonBack(props: Props) {
    const { t } = useTranslation();
    return (
        <Button
            shape="round"
            onClick={() => props?.onClick && props?.onClick()}
        >
            <img
                style={{ paddingBottom: '0.3em' }}
                src={images.ic_down}
                alt=""
            />
            {`\u00A0\u00A0${t('back')}`}
        </Button>
    );
}

export default ButtonBack;
