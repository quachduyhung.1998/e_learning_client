import { Button } from 'antd';
import { ButtonType } from 'antd/lib/button';
import images from 'images';
import React from 'react';
import { useTranslation } from 'react-i18next';

interface Props {
    onClick: (e?: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    type?: ButtonType;
    title?: React.ReactNode;
    icon?: string;
}

function ButtonCreate(props: Props) {
    const { t } = useTranslation();
    return (
        <Button
            type={props?.type || 'primary'}
            shape="round"
            onClick={() => props?.onClick && props?.onClick()}
        >
            <img
                style={{ paddingBottom: '0.3em' }}
                src={props.icon || images.ic_upload}
                alt=""
            />
            {`\u00A0\u00A0${props?.title || t('create')}`}
        </Button>
    );
}

export default ButtonCreate;
