import React from 'react';
import { Button } from 'antd';
import images from 'images';
import { useTranslation } from 'react-i18next';
import { ButtonType } from 'antd/lib/button';

interface Props {
    onClick: (e?: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    type?: ButtonType;
}

function ButtonRequestApprove(props: Props) {
    const { t } = useTranslation();
    return (
        <Button
            type={props?.type || 'primary'}
            shape="round"
            onClick={() => props?.onClick && props?.onClick()}
        >
            <img
                style={{ paddingBottom: '0.3em' }}
                src={images.ic_send}
                alt=""
            />
            {`\u00A0\u00A0${t('send_request_review')}`}
        </Button>
    );
}

export default ButtonRequestApprove;
