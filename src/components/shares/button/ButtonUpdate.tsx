import { SyncOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { ButtonType } from 'antd/lib/button';
import React from 'react';
import { useTranslation } from 'react-i18next';

interface Props {
    onClick: (e?: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    type?: ButtonType;
}

function ButtonUpdate(props: Props) {
    const { t } = useTranslation();
    return (
        <Button
            icon={<SyncOutlined />}
            type={props?.type || 'primary'}
            shape="round"
            onClick={() => props?.onClick && props?.onClick()}
        >
            {t('update')}
        </Button>
    );
}

export default ButtonUpdate;
