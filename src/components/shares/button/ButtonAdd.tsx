import { Button } from 'antd';
import { ButtonType, ButtonProps } from 'antd/lib/button';
import images from 'images';
import React from 'react';
import { useTranslation } from 'react-i18next';

interface Props extends ButtonProps, React.RefAttributes<HTMLElement> {
    onClick: (e?: React.MouseEvent<HTMLElement, MouseEvent>) => void;
}

function ButtonAdd(props: Props) {
    const { t } = useTranslation();
    return (
        <Button
            {...props}
            type={props?.type || 'primary'}
            shape={props?.shape || 'round'}
            onClick={() => props?.onClick && props?.onClick()}
        >
            <img
                style={{ paddingBottom: '0.3em' }}
                src={props.icon || images.ic_upload}
                alt=""
            />
            {`\u00A0\u00A0${props?.title || t('add')}`}
        </Button>
    );
}

export default ButtonAdd;
