import React from 'react';

interface Props {
    label: string;
}

function LabelRequired(props: Props) {
    return (
        <div>
            {props.label} <span style={{ color: 'red' }}>*</span>
        </div>
    );
}

export default LabelRequired;
