import React, { MouseEvent } from 'react';
import { Row, Col } from 'reactstrap';

interface Props {
    total: number;
    totalPage: number;
    onNextPage: (page: string) => void;
    page: string;
}

function Pagination(props: Props): JSX.Element {
    const { totalPage, onNextPage, page } = props;

    const handleNextPage = (e: MouseEvent<HTMLButtonElement>, page: number) => {
        e.preventDefault();
        onNextPage(page.toString());
    };

    const pages = [];
    if (totalPage > 1) {
        for (let i = 1; i <= totalPage; i++) {
            pages.push(
                <li key={i} className="page-item">
                    <button
                        onClick={(e) => handleNextPage(e, i)}
                        className="page-link"
                        style={
                            parseInt(page) === i
                                ? { background: '#ccc' }
                                : { background: 'none' }
                        }
                    >
                        {i}
                    </button>
                </li>
            );
        }
    }

    return (
        <Row>
            <Col>
                <nav
                    aria-label="Page navigation example"
                    className="float-right"
                >
                    <ul className="pagination">{pages}</ul>
                </nav>
            </Col>
        </Row>
    );
}

export default React.memo(Pagination);
