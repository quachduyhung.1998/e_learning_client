import { Empty } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

const MainEmpty = () => {
    const { t } = useTranslation();
    return (
        <div style={{ textAlign: 'center' }}>
            <Empty
                image={Empty.PRESENTED_IMAGE_SIMPLE}
                description={t('no_data')}
            />
        </div>
    );
};

export default MainEmpty;
