import { Button, Modal } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
interface Props {
    title: string;
    content: string;
    onVisible: boolean;
    onOk: () => void;
    onCancel: () => void;
    okText?: string;
    cancelText?: string;
}
function Confirm(props: Props): JSX.Element {
    const {
        title,
        content,
        onVisible,
        onOk,
        onCancel,
        okText,
        cancelText
    } = props;
    const { t } = useTranslation();
    const handleOk = () => {
        onOk();
    };
    const handleCancel = () => {
        onCancel();
    };
    return (
        <>
            <Modal
                title={title}
                visible={onVisible}
                onOk={handleOk}
                // confirmLoading={confirmLoading}
                onCancel={handleCancel}
                footer={
                    <div style={{ textAlign: 'center' }}>
                        <Button onClick={handleCancel}>
                            {cancelText || t('ignore')}
                        </Button>
                        <Button onClick={handleOk} type="primary">
                            {okText || t('delete')}
                        </Button>
                    </div>
                }
            >
                <h6 style={{ textAlign: 'center' }}>{content}</h6>
            </Modal>
        </>
    );
}

export default Confirm;
