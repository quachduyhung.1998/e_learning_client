import React from 'react';
import Modal from 'antd/lib/modal/Modal';
import { Alert, Button } from 'antd';
import images from 'images';
import { useTranslation } from 'react-i18next';
interface Props {
    messageAlert?: string;
    handleCancel: () => void;
    messageError: string;
}
function ModalError(props: Props): JSX.Element {
    const { messageAlert, handleCancel, messageError } = props;
    const { t } = useTranslation();
    return (
        <Modal
            centered
            visible
            title={
                <div style={{ textAlign: 'center' }}>
                    <img style={{ width: '40px' }} src={images.ic_error} />
                </div>
            }
            footer={null}
            closable={false}
        >
            <div style={{ textAlign: 'center' }}>
                {messageAlert && (
                    <Alert
                        message={t(`${messageAlert}`)}
                        type="warning"
                    ></Alert>
                )}
                <h4></h4>
                <h6>{`${messageError}`}</h6>
                <Button
                    style={{ alignItems: 'center', marginTop: '20px' }}
                    shape="round"
                    danger
                    onClick={handleCancel}
                >
                    {t('close')}
                </Button>
            </div>
        </Modal>
    );
}

export default ModalError;
