import React from 'react';
import { Row, Col, Typography } from 'antd';
import constStyle from 'constants/Style';

interface Props {
    title: JSX.Element;
    action: JSX.Element;
    alignAction?: 'left' | 'right' | 'center';
    background?: string;
    borderLeft?: boolean;
}

function HeaderPage(props: Props) {
    const { title, action } = props;
    return (
        <Row
            gutter={[8, 8]}
            align="middle"
            style={{
                minHeight: '4em',
                borderLeft:
                    props?.borderLeft === undefined
                        ? `.25em solid ${constStyle.COLOR_MAIN}`
                        : 'none',
                background: props?.background || '#ebedf2',
                marginLeft: props?.borderLeft === undefined ? '.25em' : 0
            }}
        >
            <Col md={{ span: 12 }} sm={{ span: 24 }}>
                <Typography.Title
                    level={3}
                    style={{ color: constStyle.COLOR_MAIN, margin: 0 }}
                >
                    {title}
                </Typography.Title>
            </Col>
            <Col md={{ span: 12 }} sm={{ span: 24 }}>
                <div style={{ textAlign: props?.alignAction || 'right' }}>
                    {action}
                </div>
            </Col>
        </Row>
    );
}

export default HeaderPage;
