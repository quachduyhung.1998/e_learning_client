import { TreeSelect } from 'antd';
import { valueType } from 'antd/lib/statistic/utils';
import { DataNode } from 'antd/lib/tree';
import { isArray } from 'lodash';
import React, { useState, useEffect } from 'react';
import Icon, { CaretDownOutlined } from '@ant-design/icons';

const { SHOW_PARENT } = TreeSelect;
interface KeyData {
    value: string;
    title: string;
    children: string;
}

interface Props {
    value?: string;
    data: any;
    keyData: KeyData;
    placeholder?: string;
    onChange?: (value: valueType) => void;
    multiple?: boolean;
    showSearch?: boolean;
    treeDefaultExpandAll?: boolean;
    treeDefaultExpandedKeys?: string[];
    disabled?: boolean;
    treeCheckStrictly?: boolean;
    allowClear?: boolean;
    treeCheckable?: boolean;
}

const convertItemTree = (data: any, keyData: KeyData) => {
    const newItem: any = {
        value: data[keyData.value],
        title: data[keyData.title]
    };

    if (isArray(data[keyData.children])) {
        newItem.children = data[keyData.children].map((item: any) =>
            convertItemTree(item, keyData)
        );
    }
    return newItem;
};

export const convertArrTree = (data: any[], keyData: KeyData) => {
    return data.map((item) => convertItemTree(item, keyData));
};

const SelectTree = (props: Props) => {
    const [treeData, setTreeData] = useState<DataNode[]>();
    useEffect(() => {
        const { data, keyData } = props;
        setTreeData(convertArrTree(data, keyData));
    }, [props.data, props.keyData]);
    return (
        <TreeSelect

            suffixIcon={<CaretDownOutlined />}
            disabled={props?.disabled || false}
            multiple={props.multiple || false}
            showSearch={
                props?.showSearch || (props?.multiple || false ? false : true)
            }
            showCheckedStrategy={SHOW_PARENT}
            treeNodeFilterProp="title"
            style={{ width: '100%', fontWeight: 'bold' }}
            value={props?.value}
            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
            treeData={treeData}
            placeholder={props?.placeholder || ''}
            onChange={props?.onChange}
            treeDefaultExpandAll={props?.treeDefaultExpandAll || false}
            treeDefaultExpandedKeys={props?.treeDefaultExpandedKeys || []}
            treeCheckable={props?.treeCheckable || false}
            treeCheckStrictly={
                props?.treeCheckStrictly !== undefined
                    ? props?.treeCheckStrictly
                    : false
            }
            allowClear={
                props?.allowClear !== undefined ? props?.allowClear : false
            }
        />
    );
};

export default SelectTree;
