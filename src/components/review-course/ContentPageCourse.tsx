import React, { useEffect } from 'react';
import { Col, Row } from 'reactstrap';
import PageCourse from 'types/PageCourse';
import constPageCourse from 'constants/PageCourse';
import { SEP_API_DOMAIN } from 'constants/App';
import constStyle from 'constants/Style';
interface Props {
    pageCourse?: PageCourse | null;
    onCheckView?: () => void;
}

function ContentPageCourse(props: Props): JSX.Element {
    const { pageCourse } = props;

    const renderContentPageCourse = () => {
        if (pageCourse && pageCourse?.type === 0) {
            return (
                <div
                    dangerouslySetInnerHTML={{
                        __html: pageCourse?.content || ''
                    }}
                ></div>
            );
        }
        if (pageCourse && pageCourse?.type === 1) {
            return (
                <iframe
                    width="560"
                    height="315"
                    src={pageCourse.content}
                    title="YouTube video player"
                    frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen
                ></iframe>
            );
        }
        return <div></div>;
    };

    if (pageCourse) {
        return (
            <div>
                <Row>
                    <Col sm={12}>
                        <h4 style={{ color: constStyle.COLOR_MAIN }}>
                            <b>{pageCourse.name}</b>
                        </h4>
                    </Col>
                    <Col sm={12}>{renderContentPageCourse()}</Col>
                </Row>
            </div>
        );
    }
    return <div></div>;
}

export default React.memo(ContentPageCourse);
