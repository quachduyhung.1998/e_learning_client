import getImageServer from 'helper/ImageHelper';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Col, Row } from 'antd';
import { Course } from 'types/Course';

interface Props {
    course?: Course;
}

function InfoCourse(props: Props): JSX.Element {
    const { course } = props;
    const { t } = useTranslation();
    if (course) {
        return (
            <Row>
                <Col sm={{ span: 6 }}>
                    <img
                        width="100%"
                        src={getImageServer(course?.image || '')}
                        alt={course?.fullName}
                    />
                </Col>
                <Col sm={{ span: 16 }}>
                    <Row>
                        <Col sm={{ span: 6 }}>
                            <p>{t('name_course')}</p>
                        </Col>
                        <Col sm={{ span: 16 }}>
                            <b>{course?.fullName}</b>
                        </Col>
                        <Col sm={{ span: 6 }}>
                            <p>{t('category')}</p>
                        </Col>
                        <Col sm={{ span: 16 }}>
                            <b>{course?.categoryName}</b>
                        </Col>
                        <Col sm={{ span: 6 }}>
                            <p>{t('author')}</p>
                        </Col>
                        <Col sm={{ span: 16 }}>
                            <b>{course?.createdBy}</b>
                        </Col>
                    </Row>
                </Col>
                <Col sm={12}>
                    <div
                        dangerouslySetInnerHTML={{
                            __html: course?.summary || ''
                        }}
                    ></div>
                </Col>
            </Row>
        );
    }
    return <div></div>;
}

export default InfoCourse;
