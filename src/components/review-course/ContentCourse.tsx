import { Col, Row } from 'antd';
import ContentPageCourse from 'components/review-course/ContentPageCourse';
import ContentPageQuiz from 'components/review-course/ContentPageQuiz';
import Sidebar from 'components/review-course/Sidebar';
import constTypeContentCourse from 'constants/TypeContentCourse';
import useGetPageCourse from 'hooks/page-course/useGetPageCourse';
import useGetQuiz from 'hooks/quiz/useGetQuiz';
import useFetchTopic from 'hooks/useFetchTopic';
import React, { useState } from 'react';
interface Props {
    courseId: number;
    activeModule?: {
        instanceId: number;
        type: string;
    };
}

function ContentCourse(props: Props): JSX.Element {
    const { courseId } = props;
    const [topics] = useFetchTopic(courseId.toString());

    const [typeContent, setTypeContent] = useState<string>('');
    const [contentPageCourse, onGetPageCourse] = useGetPageCourse();
    const [contentQuiz, onGetQuiz] = useGetQuiz();

    const onChangeContentModule = async (
        instanceId: number,
        type: string
    ): Promise<void> => {
        await setTypeContent(type);
        if (type === constTypeContentCourse.TYPE_PAGE) {
            onGetPageCourse({
                courseId: courseId
            });
        } else {
            if (type === constTypeContentCourse.TYPE_QUIZ) {
                onGetQuiz({
                    courseId: courseId,
                    instanceId: instanceId
                });
            }
        }
    };

    const renderContent = () => {
        if (typeContent === constTypeContentCourse.TYPE_PAGE) {
            return <ContentPageCourse pageCourse={contentPageCourse} />;
        }
        if (typeContent === constTypeContentCourse.TYPE_QUIZ) {
            return <ContentPageQuiz quiz={contentQuiz} />;
        }
        return <div></div>;
    };

    return (
        <Row>
            <Col sm={{ span: 18 }}>
                <div style={{ paddingRight: '15px' }}>{renderContent()}</div>
            </Col>
            <Col sm={{ span: 6 }}>
                <Sidebar
                    topics={topics}
                    onChangeContentModule={onChangeContentModule}
                    activeModule={props.activeModule}
                />
            </Col>
        </Row>
    );
}

export default ContentCourse;
