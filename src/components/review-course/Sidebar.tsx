import { Collapse, Space, Checkbox, Tooltip } from 'antd';
import React, { useEffect, useState } from 'react';
import { Topic } from 'types/Topic';
import { MAX_LENGTH_NUMBER } from '../../constants/Constants';
import { isArray } from 'lodash';

const { Panel } = Collapse;

interface Props {
    topics?: Topic[];
    onChangeContentModule: (instance: number, moduleName: string) => void;
    activeModule?: {
        instanceId: number;
        type: string;
    };
    checkBox?: boolean;
}

interface IndexActive {
    index: number;
    parent: number;
}

function Sidebar(props: Props): JSX.Element {
    const { checkBox, topics, onChangeContentModule } = props;
    const [indexActive, setIndexActive] = useState<IndexActive>({
        index: -1,
        parent: -1
    });
    // Độ tài tối đa của tab nội dung
    const MAX_LENGTH: number = MAX_LENGTH_NUMBER.MAX_LENGTH_CONTENT_SIDEBAR;
    const handleChangeIndexActive = (
        index: number,
        parent: number,
        instanceId: number,
        moduleName: string
    ): void => {
        setIndexActive({ index, parent });
        onChangeContentModule(instanceId, moduleName);
    };
    useEffect(() => {
        const activeModule = props.activeModule;
        if (activeModule) {
            if (topics?.length) {
                let isBreak = false;
                for (let i = 0; i < topics.length; i++) {
                    if (isBreak) {
                        break;
                    }
                    const modulesTemp = topics[i]?.modules;
                    if (modulesTemp) {
                        for (let j = 0; j < modulesTemp.length; j++) {
                            if (
                                modulesTemp[j].instanceId ===
                                    activeModule.instanceId &&
                                modulesTemp[j].moduleName === activeModule.type
                            ) {
                                handleChangeIndexActive(
                                    modulesTemp[j].id,
                                    parseInt(topics[i].id),
                                    activeModule.instanceId,
                                    activeModule.type
                                );
                                isBreak = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }, [props.activeModule, topics]);

    if (topics) {
        return (
            <Collapse
                activeKey={[indexActive.parent]}
                expandIconPosition="right"
                style={{ width: '100%' }}
                onChange={(keys) => {
                    const newKeys = keys as string[];
                    setIndexActive({
                        ...indexActive,
                        parent: Number(newKeys.pop())
                    });
                }}
            >
                {topics.map((topic: Topic) => {
                    return (
                        <Panel
                            header={
                                <>
                                    {checkBox && (
                                        <Checkbox checked={topic.completed} />
                                    )}{' '}
                                    <Tooltip
                                        placement="topLeft"
                                        title={topic?.name}
                                    >
                                        {topic.name.length > MAX_LENGTH ? (
                                            <span>
                                                {`${topic.name.substring(
                                                    0,
                                                    MAX_LENGTH
                                                )} ...`}
                                            </span>
                                        ) : (
                                            <span>{topic.name}</span>
                                        )}
                                    </Tooltip>
                                </>
                            }
                            key={topic.id}
                        >
                            <Space
                                direction="vertical"
                                style={{ width: '100%' }}
                            >
                                {topic?.modules &&
                                    topic?.modules?.map((module) => {
                                        return (
                                            <div
                                                key={module.id}
                                                className={`cursor-pointer animation-hover ${
                                                    indexActive.index ===
                                                    module.id
                                                        ? 'text-active'
                                                        : ''
                                                }`}
                                                onClick={() => {
                                                    handleChangeIndexActive(
                                                        module.id,
                                                        parseInt(topic.id),
                                                        module.instanceId,
                                                        module.moduleName
                                                    );
                                                }}
                                            >
                                                {checkBox && (
                                                    <Checkbox
                                                        checked={
                                                            module.completed
                                                        }
                                                    />
                                                )}{' '}
                                                <Tooltip
                                                    placement="topLeft"
                                                    title={module?.name}
                                                >
                                                    {module.name.length >
                                                    MAX_LENGTH ? (
                                                        <span>
                                                            {`${module.name.substring(
                                                                0,
                                                                MAX_LENGTH
                                                            )} ...`}
                                                        </span>
                                                    ) : (
                                                        <span>
                                                            {module.name}
                                                        </span>
                                                    )}
                                                </Tooltip>
                                            </div>
                                        );
                                    })}
                            </Space>
                        </Panel>
                    );
                })}
            </Collapse>
        );
    }
    return <div></div>;
}

export default React.memo(Sidebar);
