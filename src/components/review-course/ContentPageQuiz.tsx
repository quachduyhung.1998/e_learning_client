import constPaginate from 'constants/Paginate';
import useGetQuestionsOfQuiz from 'hooks/question/useGetQuestionsOfQuiz';
import React, { useEffect, useState } from 'react';
import { Check2 } from 'react-bootstrap-icons';
import { useTranslation } from 'react-i18next';
import { Button, Col, Label, Row } from 'reactstrap';
import { DetailQuestion } from 'types/Question';
import Quiz from 'types/Quiz';

interface Props {
    quiz?: Quiz | null;
}

function ContentPageQuiz(props: Props): JSX.Element {
    // const { quiz } = props;
    const { t } = useTranslation();

    const [quiz, setQuiz] = useState<Quiz | null>(props?.quiz || null);
    const [page, setPage] = useState<number>(constPaginate.PAGE_DEFAULT);
    const [limit, setLimit] = useState<number>(constPaginate.LIMIT_DEFAULT);
    const {
        resGetQuestionsOfQuiz,
        resTotalQuestion,
        setResGetQuestionsOfQuiz,
        onGetQuestionsOfQuiz
    } = useGetQuestionsOfQuiz();

    useEffect(() => {
        setResGetQuestionsOfQuiz([]);
        if (props.quiz?.id !== quiz?.id) {
            setQuiz(props?.quiz || null);
        }
    }, [props.quiz]);

    useEffect(() => {
        setPage(constPaginate.PAGE_DEFAULT);
        if (quiz) {
            onGetQuestionsOfQuiz({ quizId: quiz?.id || 0, page, limit });
        }
    }, [quiz]);

    const loadMore = () => {
        setPage(page + 1);
        if (quiz) {
            onGetQuestionsOfQuiz({ quizId: quiz?.id || 0, page, limit });
        }
    };

    const renderDetailQuestion = (question: DetailQuestion, index: number) => {
        const answerCorrect: number[] = [];
        question.answerCorrect.map((v: number, i: number) => {
            if (v) {
                answerCorrect.push(i);
            }
        });
        return (
            <div key={index}>
                <div
                    className="d-flex"
                    dangerouslySetInnerHTML={{
                        __html: `<p><strong>${index}${'.'}<span style="margin-left:0.5em">${
                            question?.questionText || ''
                        }</span></strong></p>`
                    }}
                ></div>
                {question &&
                    question.answer &&
                    question.answer.map((item: string, i: number) => {
                        return (
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    marginLeft: '1em'
                                }}
                                key={i}
                            >
                                {answerCorrect.includes(i) && (
                                    <div>
                                        <Check2 size={24} />
                                    </div>
                                )}
                                <div
                                    style={{
                                        marginLeft: !answerCorrect.includes(i)
                                            ? '1.5em'
                                            : 0
                                    }}
                                    dangerouslySetInnerHTML={{
                                        __html: `${item || ''}`
                                    }}
                                ></div>
                            </div>
                        );
                    })}
            </div>
        );
    };

    if (quiz) {
        return (
            <div>
                <Row>
                    <Col sm={3}>
                        <Label>{t('name_quiz')}</Label>
                    </Col>
                    <Col sm={9}>
                        <b>{quiz.name}</b>
                    </Col>
                    <Col sm={3} hidden={quiz.timeLimit === 0 ? true : false}>
                        <Label>{t('time_limit_quiz')}</Label>
                    </Col>
                    <Col sm={9} hidden={quiz.timeLimit === 0 ? true : false}>
                        <b>{quiz.timeLimit / 60}</b>
                    </Col>
                    <Col sm={3}>
                        <Label>{t('intro_quiz')}</Label>
                    </Col>
                    <Col sm={9}>
                        <div
                            dangerouslySetInnerHTML={{
                                __html: quiz?.intro || ''
                            }}
                        ></div>
                    </Col>
                    <Col sm={12} className="mt-3">
                        <p>{t('question')}</p>
                        {resGetQuestionsOfQuiz &&
                            resGetQuestionsOfQuiz.map(
                                (question: DetailQuestion, index: number) => {
                                    return renderDetailQuestion(
                                        question,
                                        index + 1
                                    );
                                }
                            )}
                        {page * limit < resTotalQuestion && (
                            <div className="text-right">
                                <Button onClick={loadMore}>
                                    {t('see_more')}
                                </Button>
                            </div>
                        )}
                    </Col>
                </Row>
            </div>
        );
    }
    return <div></div>;
}

export default ContentPageQuiz;
