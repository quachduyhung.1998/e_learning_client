import ItemHistory from 'components/quiz-result/ItemHistory';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { HistoryStaredQuiz } from 'types/responses/GetHistoryStartedQuizRes';

interface Props {
    onViewDetail?: (h: HistoryStaredQuiz) => void;
    history?: HistoryStaredQuiz[];
    labelNoData?: React.ReactNode;
}

const ListHistory = (props: Props) => {
    const { t } = useTranslation();
    const [indexOpen, setIndexOpen] = useState(0);
    if (props.history && props.history.length) {
        return (
            <>
                {props.history.map((h, index: number) => {
                    return (
                        <ItemHistory
                            history={h}
                            onClick={() => setIndexOpen(index)}
                            key={index}
                            onViewDetail={
                                props?.onViewDetail
                                    ? () =>
                                        props?.onViewDetail &&
                                        props?.onViewDetail(h)
                                    : undefined
                            }
                            open={indexOpen === index}
                        />
                    );
                })}
            </>
        );
    }
    return (
        <div style={{ textAlign: 'center' }}>
            {props.labelNoData !== undefined
                ? props.labelNoData
                : t('no_history')}
        </div>
    );
};

export default ListHistory;
