import React from 'react';
import { DetailQuestion } from 'types/Question';
import ItemQuestion from 'components/quiz-result/ItemQuestion';

interface Props {
    questions: DetailQuestion[];
}

const DetailExam = (props: Props) => {
    const { questions } = props;

    if (props?.questions) {
        return (
            <>
                {questions.map((question, index: number) => (
                    <ItemQuestion
                        key={index}
                        index={index + 1}
                        question={question}
                    />
                ))}
            </>
        );
    }
    return <></>;
};

export default DetailExam;
