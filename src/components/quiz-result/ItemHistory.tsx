import { Button, Card, Col, Progress, Row } from 'antd';
import { DATE_TIME_VIEW } from 'helper/DateTimeHelper';
import moment from 'moment';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { HistoryStaredQuiz } from 'types/responses/GetHistoryStartedQuizRes';
import images from 'images';

interface Props {
    history: HistoryStaredQuiz;
    open?: boolean;
    onClick?: () => void;
    onViewDetail?: () => void;
}

const ItemHistory = (props: Props) => {
    const { t } = useTranslation();
    const percent = (props.history.correct / props.history.totalQuestion) * 100;
    const timeFinish =
        `0${Math.floor(
            (props.history.timeFinish - props.history.timeStart) / 60
        )}`.slice(-2) +
        ':' +
        `0${(props.history.timeFinish - props.history.timeStart) % 60}`.slice(
            -2
        );
    const [open, setOpen] = useState(props?.open || false);
    return (
        <Card
            style={{ marginBottom: '1em' }}
            onClick={() => props?.onClick && props?.onClick()}
        >
            {!open && (
                <div>
                    <Row
                        className="cursor-pointer"
                        justify="space-between"
                        align="middle"
                        gutter={[8, 8]}
                        onClick={() => setOpen(true)}
                    >
                        <Col>
                            <Progress
                                type="circle"
                                percent={percent}
                                width={32}
                                format={() => ''}
                            />
                        </Col>
                        <Col>{t(`status_exam.${props.history.status}`)}</Col>
                        <Col>{Math.round(percent * 100) / 100}%</Col>
                        <Col>{timeFinish}</Col>
                        <Col>
                            {moment(props.history.timeStart * 1000).format(
                                DATE_TIME_VIEW
                            )}
                        </Col>
                        <Col>
                            <Row justify="end">
                                <img
                                    style={{ cursor: 'pointer' }}
                                    src={images.ic_down_arrow}
                                    alt=""
                                />
                            </Row>
                        </Col>
                    </Row>
                </div>
            )}

            {open && (
                <div onClick={() => setOpen(false)}>
                    <Row justify="end">
                        <img
                            src={images.ic_up_arrow}
                            style={{ cursor: 'pointer' }}
                            alt=""
                        />
                    </Row>

                    <Row
                        className="cursor-pointer"
                        justify="center"
                        align="middle"
                        gutter={[8, 8]}
                    >
                        <Col
                            md={{ span: 12 }}
                            sm={{ span: 24 }}
                            style={{ textAlign: 'right' }}
                        >
                            <Progress
                                type="circle"
                                percent={percent}
                                format={() => ''}
                            />
                        </Col>
                        <Col md={{ span: 12 }} sm={{ span: 24 }}>
                            <div style={{ display: 'flex' }}>
                                <p style={{ marginBottom: 0 }}>
                                    <b style={{ fontSize: '2.5em' }}>
                                        {Math.round(percent * 100) / 100}%
                                    </b>{' '}
                                    {t('correct')} ({props.history.correct}/
                                    {props.history.totalQuestion})
                                </p>
                            </div>
                            <p style={{ marginBottom: 0 }}>
                                {t('time_finish')}: {timeFinish}
                            </p>
                            <p style={{ marginBottom: 0 }}>
                                {t('time_exam')}:{' '}
                                {moment(props.history.timeStart * 1000).format(
                                    DATE_TIME_VIEW
                                )}
                            </p>
                            {props?.onViewDetail && (
                                <Button
                                    onClick={() =>
                                        props?.onViewDetail &&
                                        props?.onViewDetail()
                                    }
                                    type="primary"
                                    shape="round"
                                    ghost
                                >
                                    {t('see_detail')}
                                </Button>
                            )}
                        </Col>
                    </Row>
                </div>
            )}
        </Card>
    );
};

export default ItemHistory;
