import { Card, Checkbox, Col, Radio, Row } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { DetailQuestion, QuestionTypeAnswer } from 'types/Question';
import { isBuffer } from 'lodash';

interface Props {
    index: number;
    question: DetailQuestion;
}

const ItemQuestion = (props: Props) => {
    const { t } = useTranslation();

    if (props?.question) {
        const question = props?.question;

        return (
            <Card>
                <div
                    className={
                        JSON.stringify(question?.rightAnswers) !==
                        JSON.stringify(question?.answerCorrect)
                            ? 'item-question'
                            : ''
                    }
                    dangerouslySetInnerHTML={{
                        __html: `${t('question')} ${props.index}: ${
                            question.questionText
                        }`
                    }}
                ></div>
                <Row>
                    {question.answerRandom.map(
                        (answer: string, index: number) => {
                            return (
                                <Col key={index} span={12}>
                                    {
                                        <Row>
                                            <Col span={23}>
                                                <div
                                                    style={{
                                                        display: 'flex'
                                                    }}
                                                    className={
                                                        question.rightAnswers &&
                                                        question.rightAnswers[
                                                            index
                                                        ]
                                                            ? 'item-question'
                                                            : ''
                                                    }
                                                >
                                                    <Checkbox
                                                        value={index}
                                                        checked={
                                                            question?.answerCorrect &&
                                                            question
                                                                ?.answerCorrect[
                                                                index
                                                            ]
                                                                ? true
                                                                : false
                                                        }
                                                    ></Checkbox>
                                                    {'\u00A0\u00A0'}
                                                    <div
                                                        dangerouslySetInnerHTML={{
                                                            __html: `${answer}`
                                                        }}
                                                    ></div>
                                                </div>
                                            </Col>
                                        </Row>
                                    }
                                </Col>
                            );
                        }
                    )}
                </Row>
            </Card>
        );
    }
    return <></>;
};

export default ItemQuestion;
