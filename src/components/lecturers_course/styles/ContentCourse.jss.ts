import { makeStyles } from '@material-ui/styles';

export const ContentCourseStyle = makeStyles({
    boderGray: {
        border: '1px solid gray'
    },
    boderGrayBottom: {
        borderBottom: '1px solid gray'
    },
    bgWhite: {
        backgroundColor: 'white'
    },
    bgGray: {
        backgroundColor: '#ccc'
    }
});
