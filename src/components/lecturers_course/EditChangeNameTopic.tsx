import { CloseOutlined, EditOutlined } from '@ant-design/icons';
import { Input, message, Space } from 'antd';
import useEditTopic from 'hooks/topic/useEditName';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Course, CourseStatus } from 'types/Course';
import { Topic } from 'types/Topic';
import ModalError from 'components/shares/modal/ModalError';

interface Props {
    onIndexOpen: () => void;
    item: Topic;
    userId?: string;
    detailCourse?: Course;
    onReloadTopic?: () => void;
}

function EditChangeNameTopic(props: Props) {
    const { onIndexOpen, item, userId, detailCourse } = props;
    const [getInput, setInput] = useState(false);
    const [text, SetText] = useState('');
    const [messageError, setMessageError] = useState<string>('');
    const { t } = useTranslation();
    const [
        resEditTopic,
        onEditTopic,
        loading,
        useEditNameError
    ] = useEditTopic();
    const [name, setName] = useState<string>('');
    const onPressKey = (e: any) => {
        if (e.keyCode === 13) {
            if (text.length === 0) {
                setMessageError(
                    `${t('lesson_content', { name: t('lesson_content') })}`
                );
            } else if (2 > text.length || text.length > 200) {
                setMessageError(`${t('name_length_200_invalidate')}`);
            } else {
                onEditTopic({
                    name: text,
                    sectionId: Number(item.id),
                    courseId: Number(detailCourse?.id)
                });
                setName(text);
            }
        }
    };
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (messageError || useEditNameError) {
            setShowMessageError(true);
        }
    }, [messageError, useEditNameError]);
    useEffect(() => {
        if (item) {
            SetText(item.name);
            setName(item.name);
        }
    }, []);

    useEffect(() => {
        if (!resEditTopic) {
            props?.onReloadTopic && props.onReloadTopic();
        }
    }, [resEditTopic]);

    const handleChangeText = (e: {
        target: { value: React.SetStateAction<string> };
    }) => {
        SetText(e.target.value);
    };
    useEffect(() => {
        setInput(loading);
    }, [loading]);

    const handClickHide = () => {
        setInput(false);
        SetText(name);
    };

    return (
        <>
            {getInput ? (
                <>
                    <Space align="baseline">
                        <Input
                            type="text"
                            value={text}
                            onKeyDown={onPressKey}
                            onChange={handleChangeText}
                        />
                        <CloseOutlined
                            onClick={() => handClickHide()}
                            style={{ verticalAlign: 'text-bottom' }}
                        />
                    </Space>
                </>
            ) : (
                <>
                    <Space align="baseline">
                        <b
                            className="cursor-pointer"
                            onClick={() => onIndexOpen()}
                        >
                            {text}
                        </b>
                        {userId === detailCourse?.creator?._id &&
                            detailCourse?.status !==
                                CourseStatus.STATUS_APPROVED && (
                                <EditOutlined
                                    onClick={() => setInput(true)}
                                    style={{ verticalAlign: 'text-bottom' }}
                                />
                            )}
                    </Space>
                </>
            )}
            {showMessageError && (
                <ModalError
                    messageError={messageError || useEditNameError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </>
    );
}

export default EditChangeNameTopic;
