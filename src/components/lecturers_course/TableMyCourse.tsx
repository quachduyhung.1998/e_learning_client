import {
    Avatar,
    Button,
    Dropdown,
    Form,
    Menu,
    Modal,
    Select,
    Table,
    Tooltip,
    Typography
} from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { DATE_TIME_VIEW } from 'helper/DateTimeHelper';
import getImageServer from 'helper/ImageHelper';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import { getStatusFormatCourse } from 'helper/StatusFormat';
import useAssignClassWithCourse from 'hooks/class/useAssignClassWithCourse';
import useFetchListClass from 'hooks/class/useFetchListClass';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { LECTURERS_COURSE } from 'routes/web/paths';
import { Course, CourseStatus } from 'types/Course';
import { User } from 'types/User';
import { MAX_LENGTH_NUMBER } from '../../constants/Constants';
import './TableMyCourse.scss';
interface Props {
    listCourse: Course[];
    page: number;
    onDeleteCourse: (id: number) => void;
}
interface Options {
    detail: string;
    listStudent: string;
    delete: number;
    isApproved?: boolean;
}

type AlignType = 'left' | 'center' | 'right';

function TableMyCourse(props: Props): JSX.Element {
    const { t } = useTranslation();
    const { page, listCourse } = props;
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    const { listClass, loading, onFetchListClass } = useFetchListClass();
    const [form] = Form.useForm();
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [visibleAddClassModal, setVisibleAddClassModal] = useState(false);
    const [deleteCourserId, setDeleteCourserId] = useState<number>(0);
    const [assignCourserId, setAssignCourserId] = useState<number>(0);
    const { onAssignClass, res } = useAssignClassWithCourse();
    useEffect(() => {
        onFetchListClass({});
    }, []);
    // const [page, setPage] = useState(props.page);
    const toggleModalAddClass = () => {
        setVisibleAddClassModal(!visibleAddClassModal);
    };
    const [menu] = useState(() => (data: Options) => {
        return (
            <Menu>
                <Menu.Item>
                    <Link to={data.detail}>{t('see_detail')}</Link>
                </Menu.Item>
                {data.isApproved && (
                    <Menu.Item>
                        <Link to={data.listStudent}>{t('list_student')}</Link>
                    </Menu.Item>
                )}
                {!data.isApproved && (
                    <Menu.Item>
                        <span
                            // to={'/'}
                            onClick={() => {
                                setShowModalDelete(true),
                                    setDeleteCourserId(data.delete);
                            }}
                        >
                            {t('delete')}
                        </span>
                    </Menu.Item>
                )}
                {user?.role !== 2 && (
                    <Menu.Item>
                        <span
                            // to={'/'}
                            onClick={() => {
                                setAssignCourserId(data?.delete);
                                toggleModalAddClass();
                            }}
                        >
                            {t('assign_student')}
                        </span>
                    </Menu.Item>
                )}
            </Menu>
        );
    });

    const [columns, setColumns] = useState<ColumnsType<Course>>([]);

    // Độ dài tối đa
    const MAX_LENGTH: number = MAX_LENGTH_NUMBER.MAX_LENGTH_TABLE_MY_COURSE;
    useEffect(() => {
        setColumns([
            {
                title: t('stt'),
                dataIndex: 'id',
                align: 'center' as AlignType,
                render: (id: number, item: Course, index: number) => {
                    return index + 1 + (listCourse?.length || 0) * (page - 1);
                }
            },
            {
                title: t('image'),
                dataIndex: 'image',
                align: 'center' as AlignType,
                // eslint-disable-next-line react/display-name
                render: (image: string) => {
                    return <Avatar shape="square" size={100} src={image} />;
                }
            },
            // {
            //     title: t('course_code'),
            //     dataIndex: 'code',
            //     align: 'center' as AlignType
            // },
            {
                title: t('course_name'),
                dataIndex: 'name',
                align: 'center' as AlignType,
                // eslint-disable-next-line react/display-name
                render: (fullName: string, course: Course) => {
                    return (
                        <Tooltip placement="topLeft" title={fullName}>
                            <Link
                                to={LECTURERS_COURSE + '/' + course.id}
                                // component={Typography.Link}
                            >
                                {fullName.length > MAX_LENGTH ? (
                                    <div>
                                        {`${fullName.substring(
                                            0,
                                            MAX_LENGTH
                                        )} ...`}
                                    </div>
                                ) : (
                                    <p>{fullName}</p>
                                )}
                            </Link>
                        </Tooltip>
                    );
                }
            },
            {
                title: t('time_create'),
                dataIndex: 'createdAt',
                align: 'center' as AlignType,
                // eslint-disable-next-line react/display-name
                render: (createdAt: string) => (
                    <>{moment(createdAt).format(DATE_TIME_VIEW)}</>
                )
            },
            {
                title: t('status'),
                dataIndex: 'status',
                align: 'center' as AlignType,
                // eslint-disable-next-line react/display-name
                render: (status: CourseStatus) => (
                    <>{t(getStatusFormatCourse(status))}</>
                )
            },
            {
                title: t('approved_by'),
                dataIndex: 'approver',
                align: 'center' as AlignType,
                render: (approver: User) => approver?.name
            },
            {
                title: t('action'),
                dataIndex: 'id',
                align: 'center' as AlignType,
                // eslint-disable-next-line react/display-name
                render: (id: number, course: Course) => (
                    <Dropdown
                        overlay={() =>
                            menu({
                                detail: LECTURERS_COURSE + '/' + id,
                                listStudent: `/teacher/course/${course.id}/students`,
                                delete: id,
                                isApproved:
                                    course.status ===
                                    CourseStatus.STATUS_APPROVED
                            })
                        }
                        placement="bottomLeft"
                    >
                        <Button>{t('action')}</Button>
                    </Dropdown>
                )
            }
        ]);
    }, [props.page]);
    const submitAssignClass = () => {
        form.validateFields().then((values) => {
            onAssignClass({
                class_id: values.class_id,
                course_id: assignCourserId
            });
            toggleModalAddClass();
        });
    };
    return (
        <div>
            <Table
                style={{ overflowX: 'scroll' }}
                columns={columns}
                dataSource={listCourse || []}
                bordered
                rowKey={(course: Course) => course.id}
                pagination={false}
                locale={{
                    emptyText: t('no_data')
                }}
                scroll={{ x: 'auto' }}
            />

            <Modal
                centered
                title={t('delete_cource')}
                visible={showModalDelete}
                onOk={() => {
                    props.onDeleteCourse(deleteCourserId);
                    setShowModalDelete(false);
                }}
                okText={t('delete')}
                onCancel={() => {
                    setShowModalDelete(false);
                }}
                cancelText={t('cta_skip')}
            >
                <h6 style={{ textAlign: 'center' }}>
                    {t('delete_confirmation_content', { name: 'khóa học' })}
                </h6>
            </Modal>
            <Modal
                centered
                title={t('Gán lớp học vào khóa học')}
                visible={visibleAddClassModal}
                onOk={submitAssignClass}
                okText={t('add')}
                onCancel={() => {
                    toggleModalAddClass();
                }}
                cancelText={t('cancel')}
            >
                <Form form={form}>
                    <Form.Item name="class_id" label="Lớp học">
                        <Select>
                            {listClass &&
                                listClass?.length > 0 &&
                                listClass?.map((item) => {
                                    return (
                                        <Select.Option value={item?.id}>
                                            {item?.name}
                                        </Select.Option>
                                    );
                                })}
                        </Select>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}

export default React.memo(TableMyCourse);
