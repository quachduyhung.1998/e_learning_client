import React from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Topic } from 'types/Topic';
import { useTranslation } from 'react-i18next';

interface Props {
    topics: Topic[];
    isOpen: boolean;
    toggle: ((event: React.MouseEvent<any, MouseEvent>) => void) | undefined;
    onSendReviewRequestClicked:
        | ((event: React.MouseEvent<any, MouseEvent>) => void)
        | undefined;
}

function CompleteCourseConfirmationPopup(props: Props): JSX.Element {
    const { t } = useTranslation();

    let message = '';

    if (props.topics.length === 0) {
        message = t('send_review_confirmation_empty_topic');
    } else {
        message = t('send_review_confirmation');
    }

    return (
        <Modal className="p-4" isOpen={props.isOpen} toggle={props.toggle}>
            <ModalBody className="p-3 text-center">
                <h5>{message}</h5>
            </ModalBody>
            <div className="pb-3 pr-3 align-self-end">
                <Button
                    color="primary"
                    onClick={props.onSendReviewRequestClicked}
                >
                    {t('send_request_review')}
                </Button>{' '}
                <Button color="secondary" onClick={props.toggle}>
                    {t('back')}
                </Button>
            </div>
        </Modal>
    );
}

export default React.memo(CompleteCourseConfirmationPopup);
