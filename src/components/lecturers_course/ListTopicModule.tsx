import { BarsOutlined } from '@ant-design/icons';
import { Button, Card, Col, Dropdown, Menu, Row, Space } from 'antd';
import Confirm from 'components/shares/modal/Confirm';
import { getUrl, subString } from 'helper/String';
import useDeleteQuiz from 'hooks/quiz/useDeleteQuiz';
import useSorterQuiz from 'hooks/quiz/useSorterQuiz';
import React, { useEffect, useState } from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { EXAM_TOPIC_DETAIL, UPDATE_PAGE_COURSE } from 'routes/web/paths';
import Module from 'types/Module';
import { Topic } from 'types/Topic';
import ModalError from 'components/shares/modal/ModalError';
interface TopicProps {
    courseId: string;
    onViewModule?: (instanceId: number, type: string) => void;
    showIcon: boolean;
    topic?: Topic;
    visibleTop?: boolean;
    onReloadDataQuiz: () => void;
}

interface ModuleProps {
    module: Module;
    courseId: string;
    onViewModule?: (instanceId: number, type: string) => void;
    showIcon: boolean;
    visibleTopic?: boolean;
    showIconMove?: boolean;
    topic?: Topic;
    onReloadDataQuiz: () => void;
    dragHandleProps?: any;
}

// a little function to help us with reordering the result
const reorder = (list: Module[], startIndex: number, endIndex: number) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
};

function ListTopicModule(props: TopicProps): JSX.Element {
    const {
        courseId,
        onViewModule,
        showIcon,
        onReloadDataQuiz,
        visibleTop
    } = props;
    const [topic, setTopic] = useState(props.topic);
    const [quotes, setQuotes] = useState([]);
    const { resQuiz, onSorterQuiz, loading } = useSorterQuiz();

    useEffect(() => {
        setTopic(props.topic);
    }, [props.topic]);

    const onDragEnd = async (result: any) => {
        // dropped outside the list
        if (!result.destination) {
            return;
        }
        const start = result.source.index;
        const end = result.destination.index;
        const modules = topic?.modules || [];

        let newTopic = { ...topic } as Topic;
        newTopic = {
            ...newTopic,
            modules: reorder(newTopic?.modules || [], start, end)
        };
        setTopic(newTopic);

        if (start > end) {
            await onSorterQuiz({
                courseModuleId: modules[result.source.index].id,
                beforeCourseModuleId: modules[result.destination.index].id,
                courseId: Number(courseId),
                section: topic?.section || -1
            });
        } else {
            if (modules.length === result.destination.index + 1) {
                await onSorterQuiz({
                    courseModuleId: modules[result.source.index].id,
                    beforeCourseModuleId: 0,
                    courseId: Number(courseId),
                    section: topic?.section || -1
                });
            } else {
                await onSorterQuiz({
                    courseModuleId: modules[result.source.index].id,
                    beforeCourseModuleId:
                        modules[result.destination.index + 1].id,
                    courseId: Number(courseId),
                    section: topic?.section || -1
                });
            }
        }
        if (!result.destination) {
            return;
        }
        onReloadDataQuiz && onReloadDataQuiz();
    };
    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable_child">
                {(provided, snapshot) => (
                    <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={{
                            padding: 'gird',
                            height: visibleTop
                                ? `${
                                      topic?.modules &&
                                      topic?.modules?.length * 64
                                  }px` || '64px'
                                : 'auto'
                        }}
                    >
                        {topic?.modules?.map((module: any, index: number) => {
                            return (
                                <Draggable
                                    key={`item-${module.id}`}
                                    draggableId={`item-${module.id}`}
                                    index={index}
                                >
                                    {(provided, snapshot) => (
                                        <div
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                        >
                                            <ComponentModule
                                                dragHandleProps={
                                                    provided.dragHandleProps
                                                }
                                                courseId={courseId}
                                                module={module}
                                                key={module.id}
                                                onViewModule={onViewModule}
                                                showIcon={showIcon}
                                                showIconMove={
                                                    topic?.modules &&
                                                    topic?.modules.length > 1
                                                }
                                                visibleTopic={visibleTop}
                                                onReloadDataQuiz={() =>
                                                    onReloadDataQuiz()
                                                }
                                            />
                                        </div>
                                    )}
                                </Draggable>
                            );
                        })}
                    </div>
                )}
            </Droppable>
        </DragDropContext>
    );
}

const ComponentModule = (props: ModuleProps): JSX.Element => {
    const {
        module,
        courseId,
        onViewModule,
        showIcon,
        showIconMove,
        onReloadDataQuiz,
        visibleTopic
    } = props;
    const { t } = useTranslation();
    const [resDeleteQuiz, SetDeleteQuiz, deleteQuizError] = useDeleteQuiz();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (deleteQuizError) {
            setShowMessageError(true);
        }
    }, [deleteQuizError]);
    useEffect(() => {
        if (resDeleteQuiz) {
            onReloadDataQuiz();
        }
    }, [resDeleteQuiz]);
    const renderAction = (module: Module) => {
        let html = undefined;
        let menuLink = undefined;

        if (module?.moduleName === 'page') {
            menuLink = (
                <Menu.Item>
                    {showIcon && (
                        <Link
                            to={getUrl(UPDATE_PAGE_COURSE, {
                                courseId: courseId
                            })}
                        >
                            {t('edit')}
                        </Link>
                    )}
                </Menu.Item>
            );
        } else {
            menuLink = (
                <Menu.Item>
                    {showIcon && (
                        <Link
                            to={getUrl(EXAM_TOPIC_DETAIL, {
                                courseId: courseId,
                                quizId: module?.instanceId
                            })}
                            style={{ textAlign: 'center' }}
                        >
                            {t('edit')}
                        </Link>
                    )}
                </Menu.Item>
            );
        }
        html = (
            <Menu>
                {menuLink}
                <Menu.Item>
                    <Link
                        to="#"
                        onClick={(e) => {
                            e.preventDefault();
                            setVisible(true);
                            setObject({
                                courseId: Number(courseId),
                                courseModuleId: module?.id,
                                moduleType: module?.moduleName
                            });
                        }}
                    >
                        {t('delete_' + module?.moduleName)}
                    </Link>
                </Menu.Item>
            </Menu>
        );
        if (showIcon) {
            return (
                <Space>
                    {showIconMove && (
                        <div {...props.dragHandleProps}>
                            <Button icon={<BarsOutlined />} type="text" />
                        </div>
                    )}
                    <Dropdown overlay={html} placement="bottomRight">
                        <Button style={{ float: 'right' }}>
                            {t('action')}
                        </Button>
                    </Dropdown>
                </Space>
            );
        }
    };

    const [visible, setVisible] = useState(false);
    const [object, setObject] = useState<{
        courseId: number;
        courseModuleId: number;
        moduleType: string;
    }>();
    /**
     * Xóa bài kiểm tra hoặc bài học
     */
    const onOkDeleteConfirm = () => {
        if (object) SetDeleteQuiz(object);
        setVisible(false);
    };
    return (
        <div
            style={{ padding: '0.5em', height: visibleTopic ? '64px' : '64px' }}
        >
            <Card
                bodyStyle={{
                    padding: '0.5em'
                }}
            >
                <Row>
                    <Col sm={20} style={{ overflow: 'hidden' }}>
                        <Button
                            type="link"
                            onClick={() =>
                                onViewModule &&
                                onViewModule(
                                    module.instanceId,
                                    module?.moduleName || ''
                                )
                            }
                        >
                            <span className="text-ellipsis">
                                * {subString(module.name, 120, '...')}
                            </span>
                        </Button>
                    </Col>
                    <Col sm={4} style={{ textAlign: 'right' }}>
                        {renderAction(module)}
                    </Col>
                </Row>
            </Card>
            <Confirm
                title={`${t(`delete_${module?.moduleName}`)}?`}
                content={t('delete_confirmation_content', {
                    name:
                        object?.moduleType == 'quiz'
                            ? 'bài kiểm tra'
                            : 'bài học'
                })}
                onVisible={visible}
                onOk={onOkDeleteConfirm}
                onCancel={() => setVisible(false)}
            />
            {showMessageError && (
                <ModalError
                    messageError={deleteQuizError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
};

export default ListTopicModule;
