// @ts-ignore
import { UploadOutlined } from '@ant-design/icons';
import { Col, Form, Input, Row, Select } from 'antd';
import CKEditor from 'components/ckeditor/ckEditor';
import SelectTree from 'components/shares/SelectTree';
import constStyle from 'constants/Style';
import getImageServer from 'helper/ImageHelper';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import useFetchCategories from 'hooks/useFetchCategories';
import useUpload from 'hooks/useUpload';
import React, { ChangeEvent, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import { Category } from 'types/Category';
import { Course, CourseStatus } from 'types/Course';
import { Upload } from 'types/responses/UploadResponse';
import './CourseForm.scss';

const { Option } = Select;
interface Props {
    detailCourse?: Course;
    form?: any;
    cache?: any;
    updateCacheForm?: () => void;
}
interface Params {
    id: string;
}

function CourseForm(props: Props): JSX.Element {
    const params: Params = useParams();

    const { t } = useTranslation();
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    const { detailCourse, form, cache, updateCacheForm } = props;
    // Tạo 1 user Ref để custom file upload
    const inputFile: any = useRef(null);
    const validateForm = {
        topicId: [
            {
                required: true,
                message: t('categoryId_course_is_required')
            }
        ],
        is_sequence: [
            {
                required: true,
                message: 'Loại khóa học là bắt buộc'
            }
        ],
        fullName: [
            {
                required: true,
                message: t('fullName_course_is_required')
            },
            {
                max: 200,
                message: t('course_length_invalidate')
            },
            {
                min: 2,
                message: t('course_length_invalidate')
            }
        ],
        code: [
            {
                max: 10,
                message: t('code_course_length_max_invalidate', { max: 10 })
            },
            {
                pattern: /^[a-zA-Z0-9-_]+$/,
                message: t('regex_code_course')
            }
        ],
        description: [
            // {
            //     transform: (value: string) => {
            //         const el = window.document.createElement('div');
            //         el.innerHTML = value;
            //         return el.textContent;
            //     },
            //     max: 1000,
            //     message: t('desc_course_length_max_invalidate', { max: 1000 })
            // }
        ]
    };
    const [selectedFile, setSelectedFile] = useState<File>();
    const [preview, setPreview] = useState<Upload | null>(null);
    const [categories, setCategories] = useState<Category[]>([]);
    const [resImages, onUpload, messageError] = useUpload();
    const [onFetchCategories, categoryListRes] = useFetchCategories();
    const [infoCourse, setInfoCourse] = useState<Course>();
    const [disabledForm, setDisabledForm] = useState<boolean>(false);
    const userId = user?._id;

    useEffect(() => {
        if (detailCourse) {
            setInfoCourse(detailCourse);
            props.form &&
                props.form.setFieldsValue({
                    topicId: detailCourse.course_category_id,
                    fullName: detailCourse.name,
                    code: detailCourse.code,
                    createdBy: detailCourse?.created_by?.name,
                    approver: detailCourse.approver?.fullName,
                    is_sequence: detailCourse?.is_sequence
                });
            const creatorCourseId = detailCourse?.creator?._id;
            if (
                (params?.id && userId !== creatorCourseId) ||
                detailCourse?.status === CourseStatus.STATUS_APPROVED
            ) {
                setDisabledForm(true);
            }
        }
    }, [detailCourse]);

    useEffect(() => {
        onFetchCategories();
    }, []);

    useEffect(() => {
        if (!selectedFile) {
            setPreview(null);
            return;
        }
        const bodyFormData = new FormData();
        bodyFormData.append('image', selectedFile);

        onUpload(bodyFormData);
    }, [selectedFile]);

    useEffect(() => {
        if (resImages) {
            setPreview(resImages);
        }
    }, [resImages]);

    const handleChangeFile = (e: ChangeEvent<HTMLInputElement>) => {
        if (e.target.files) {
            if (e.target.files[0]) {
                setSelectedFile(e.target.files[0]);
            }
        }
    };

    const handleChangeDesc = (event: any, editor: any) => {
        if (props?.form) {
            props.form.setFieldsValue({
                description: editor.getData()
            });
            updateCacheForm && updateCacheForm();
        }
    };

    let imageUrl: string | undefined = undefined;
    if (preview) {
        imageUrl = preview?.url;
    } else if (detailCourse) {
        imageUrl = detailCourse.image;
    }

    const [loadImage, setLoadImage] = useState<string | undefined>(undefined);
    useEffect(() => {
        if (imageUrl) {
            setLoadImage(imageUrl);
        }
    }, [imageUrl]);
    const handleDelete = () => {
        setLoadImage(undefined);
        setSelectedFile(undefined);
    };
    const renderImage = (image?: string) => {
        props?.form &&
            props?.form.setFieldsValue({
                image: preview?.url
            });
        if (loadImage) {
            return (
                <div className="mt-2 position-relation">
                    {detailCourse?.status !== CourseStatus.STATUS_APPROVED && (
                        <span
                            onClick={handleDelete}
                            className="position-absolute"
                        >
                            x
                        </span>
                    )}
                    <img
                        width="100"
                        src={loadImage}
                        alt=""
                        className="image-upload-course"
                    />
                </div>
            );
        }
    };

    // Sự kiện click chọn ảnh
    const onButtonClick = () => {
        document.getElementById('clickUpload')?.click();
    };
    const cateGoryCourse = [
        { id: 1, name: 'Toán' },
        { id: 2, name: 'Tiếng anh' }
    ];
    const CourseType = [
        { id: 1, name: 'Học lần lượt' },
        { id: 0, name: 'Học song hành' }
    ];
    // Trường hợp chưa phê duyệt thì show form
    const formCourse = () => {
        return (
            <Row gutter={[8, 8]}>
                <Col md={{ span: 12 }} sm={{ span: 24 }}>
                    <Form.Item
                        label={
                            <>
                                Môn học
                                <span
                                    style={{ color: constStyle.COLOR_MAIN_2 }}
                                >
                                    *
                                </span>
                            </>
                        }
                        name="topicId"
                        rules={validateForm.topicId}
                    >
                        <Select>
                            {cateGoryCourse.map((item) => {
                                return (
                                    <Select.Option value={item.id}>
                                        {item.name}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                    </Form.Item>

                    <Form.Item
                        label={
                            <>
                                {t('course_name')}
                                <span
                                    style={{ color: constStyle.COLOR_MAIN_2 }}
                                >
                                    *
                                </span>
                            </>
                        }
                        name="fullName"
                        rules={validateForm.fullName}
                    >
                        <Input
                            style={{ fontWeight: 'bold' }}
                            disabled={disabledForm}
                        />
                    </Form.Item>
                    <Form.Item
                        label={
                            <>
                                Loại khóa học
                                <span
                                    style={{ color: constStyle.COLOR_MAIN_2 }}
                                >
                                    *
                                </span>
                            </>
                        }
                        name="is_sequence"
                        rules={validateForm.is_sequence}
                    >
                        <Select>
                            {CourseType.map((item) => {
                                return (
                                    <Select.Option value={item.id}>
                                        {item.name}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                    {params?.id && (
                        <Form.Item label={t('author')} name="createdBy">
                            <Input disabled />
                        </Form.Item>
                    )}
                    {params?.id && (
                        <Form.Item label={t('approved')} name="approver">
                            <Input disabled />
                        </Form.Item>
                    )}
                </Col>
                <Col md={{ span: 12 }} sm={{ span: 24 }}>
                    <Form.Item label={t('image')} name="image">
                        <Input hidden />
                        <Input
                            disabled={disabledForm}
                            accept="image/*"
                            type="file"
                            onChange={handleChangeFile}
                            ref={inputFile}
                            style={{ display: 'none' }}
                            id="clickUpload"
                        />
                        {
                            <div
                                onClick={onButtonClick}
                                className={
                                    'ant-btn ant-btn-primary ant-btn-round'
                                }
                            >
                                <UploadOutlined
                                    style={{
                                        fontSize: '20px',
                                        position: 'relative',
                                        top: '-3px',
                                        right: '3px'
                                    }}
                                />{' '}
                                {t('upload')}
                            </div>
                        }
                        {renderImage(imageUrl)}
                    </Form.Item>
                </Col>
                <Col span={24}>
                    <Form.Item
                        className="ckh-200"
                        label={t('description')}
                        name="description"
                        labelCol={{ md: { span: 3 }, sm: { span: 8 } }}
                        wrapperCol={{ md: { span: 21 }, sm: { span: 16 } }}
                        rules={validateForm.description}
                    >
                        <Input hidden />
                        <CKEditor
                            onChange={handleChangeDesc}
                            data={
                                infoCourse?.description ||
                                cache?.description ||
                                ''
                            }
                            disabled={disabledForm}
                        />
                    </Form.Item>
                </Col>
            </Row>
        );
    };
    /**
     * Trường hợp đã phê duyệt thì show thôn tin
     */
    const showCourse = () => {
        return (
            <Row gutter={[8, 8]}>
                <Col md={{ span: 4 }} sm={{ span: 4 }}>
                    <img
                        src={imageUrl}
                        alt={detailCourse?.fullName}
                        style={{ width: '100%' }}
                    />
                </Col>
                <Col md={{ span: 20 }} sm={{ span: 20 }}>
                    <h1
                        style={{
                            textAlign: 'left',
                            fontFamily: 'Times New Roman',
                            fontSize: '32px',
                            letterSpacing: '0px',
                            color: '#333333'
                        }}
                    >
                        {detailCourse?.fullName}
                    </h1>
                    <ul style={{ listStyle: 'none', padding: 0 }}>
                        <li style={{ fontSize: '16px' }}>
                            Môn học:{' '}
                            <span style={{ color: 'red' }}>
                                {detailCourse?.course_category}
                            </span>
                        </li>
                        <li style={{ fontSize: '16px' }}>
                            {t('author')}:{' '}
                            <span style={{ color: 'red' }}>
                                {detailCourse?.created_by?.name}
                            </span>
                        </li>
                        {/* <li style={{ fontSize: '16px' }}>
                            {t('approved_by')}:{' '}
                            <span style={{ color: 'red' }}>
                                {detailCourse?.approver?.fullName}
                            </span>
                        </li> */}
                    </ul>
                </Col>
                <Col sm={{ span: 24 }} md={{ span: 24 }}>
                    <label
                        style={{
                            color: '#333333',
                            borderBottom: '1px solid #333333',
                            fontSize: '20px'
                        }}
                    >
                        {t('description')}
                    </label>
                    <p
                        style={{
                            fontSize: '16px',
                            color: '#333333'
                        }}
                        dangerouslySetInnerHTML={{
                            __html: `${
                                infoCourse?.description ||
                                cache?.description ||
                                ''
                            }`
                        }}
                    ></p>
                </Col>
            </Row>
        );
    };

    if (infoCourse && params.id) {
        return disabledForm ? showCourse() : formCourse();
    }
    if (!params.id) {
        return formCourse();
    }
    return <></>;
}

export default CourseForm;
