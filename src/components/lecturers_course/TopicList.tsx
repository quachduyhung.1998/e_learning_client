import { BarsOutlined } from '@ant-design/icons';
import { Button, Card, Dropdown, Menu, Modal, Space, Collapse } from 'antd';
import Confirm from 'components/shares/modal/Confirm';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import { getUrl } from 'helper/String';
import useDeleteTopic from 'hooks/topic/useDeleteTopic';
import useSorterTopic from 'hooks/topic/useSorterTopic';
import { default as React, MouseEvent, useEffect, useState } from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { Col, Row } from 'reactstrap';
import {
    ADD_EXAM_TOPIC,
    ADD_PAGE_COURSE,
    CREATE_QUESTION,
    UPDATE_PAGE_COURSE
} from 'routes/web/paths';
import { Course, CourseStatus } from 'types/Course';
import { Topic } from 'types/Topic';
import EditChangeNameTopic from './EditChangeNameTopic';
import ListTopicModule from './ListTopicModule';
import PopupAddContent from './PopupAddContent';
import './styles/CourseTopic.scss';
import ModalError from 'components/shares/modal/ModalError';

const getItems = (count: number) =>
    Array.from({ length: count }, (v, k) => k).map((k) => ({
        id: `item-${k}`,
        content: `item ${k}`
    }));

interface Props {
    topics: Topic[];
    courseId: string;
    detailCourse?: Course;
    onViewModule?: (instanceId: number, type: string) => void;
    onReLoadTopic: () => void;
}
interface Topics {}

// a little function to help us with reordering the result
const reorder = (list: any[], startIndex: number, endIndex: number) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
};

function TopicList(props: Props): JSX.Element {
    const { confirm } = Modal;
    function destroyAll() {
        Modal.destroyAll();
    }
    const { t } = useTranslation();
    const [topicSelected, setTopicSelected] = useState<Topic>();
    const [showAddPagePopup, setShowAddPagePopup] = useState<boolean>(false);
    const [visibleTopic, setDraggedGroup] = useState(false);
    const [indexOpen, setIndexOpen] = useState<number>(-1);

    const onShowAddContentPopup = (topic: Topic) => (
        e: MouseEvent<HTMLButtonElement>
    ) => {
        e.preventDefault();
        setTopicSelected(topic);
        setShowAddPagePopup(true);
    };

    const onHideAddContentPopup = () => {
        setTopicSelected(undefined);
        setShowAddPagePopup(false);
    };
    const { detailCourse, onReLoadTopic } = props;
    const [topics, setTopics] = useState(props.topics);

    const [showIcon, setShowIcon] = useState<boolean>(true);
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    const userId = user?._id;
    const creatorCourseId = detailCourse?.creator?._id;
    const { resTopic, onSorterTopic } = useSorterTopic();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (userId !== creatorCourseId) {
            setShowIcon(false);
        } else {
            setShowIcon(true);
        }
    }, [detailCourse]);

    useEffect(() => {
        setTopics(props.topics);
    }, [props.topics]);

    const onDragEnd = async (result: any) => {
        // dropped outside the list
        if (!result.destination) {
            return;
        }
        let newTopics = [...topics];
        newTopics = reorder(
            topics,
            result.source.index - 1,
            result.destination.index - 1
        );
        setTopics(newTopics);
        await onSorterTopic({
            courseId: Number(props.courseId),
            section: result.source.index,
            targetSection: result.destination.index
        });
        onReLoadTopic();
    };
    const [
        onDeleteTopic,
        setDeleteTopic,
        loadingDeleteTopic,
        messageError
    ] = useDeleteTopic();
    useEffect(() => {
        if (messageError) {
            setShowMessageError(true);
        }
    }, [messageError]);
    useEffect(() => {
        if (!loadingDeleteTopic) {
            if (onDeleteTopic) {
                setVisible(false);
                onReLoadTopic();
                setObject(undefined);
            }
        }
    }, [loadingDeleteTopic]);

    const [object, setObject] = useState<{
        courseId: number;
        sectionId: number;
    }>();
    const [visible, setVisible] = useState(false);
    /**
     * Xóa Bài giảng
     */
    const onOkDeleteConfirm = () => {
        if (object) setDeleteTopic(object);
        setVisible(false);
    };

    const handleMouseDown = () => {
        setDraggedGroup(true);
    };

    return (
        <>
            <Row>
                {user?.id === detailCourse?.created_by?.id && (
                    <Col align="right">
                        <Space>
                            <Dropdown
                                overlay={
                                    <Menu>
                                        {/* <Menu.Item>
                                                    <Link to={'/'}>
                                                        {t(
                                                            'see_detail'
                                                        )}
                                                    </Link>
                                                </Menu.Item> */}
                                        <Menu.Item>
                                            <Link
                                                to={`${getUrl(ADD_EXAM_TOPIC, {
                                                    courseId: props.courseId
                                                    // sectionId:
                                                    //     topic.section
                                                })}`}
                                            >
                                                {t('add_exam')}
                                            </Link>
                                        </Menu.Item>

                                        <Menu.Item>
                                            <Link
                                                to={`${getUrl(ADD_PAGE_COURSE, {
                                                    courseId: props.courseId
                                                    // sectionId:
                                                    //     topic.id
                                                })}`}
                                            >
                                                {t('addlesson')}
                                            </Link>
                                        </Menu.Item>
                                        {/* <Menu.Item>
                                                <Link
                                                    to={'#'}
                                                    onClick={(e) => {
                                                        e.preventDefault();
                                                        setVisible(true);
                                                        // setObject(
                                                        //     {
                                                        //         courseId: Number(
                                                        //             props.courseId
                                                        //         ),
                                                        //         sectionId: Number(
                                                        //             topic?.id
                                                        //         )
                                                        //     }
                                                        // );
                                                    }}
                                                >
                                                    {t('delete_lesson')}
                                                </Link>
                                            </Menu.Item> */}
                                    </Menu>
                                }
                                placement="bottomLeft"
                            >
                                <Button type="primary">{t('action')}</Button>
                            </Dropdown>
                        </Space>
                    </Col>
                )}
            </Row>
            <div style={{ padding: '0.5em' }}>
                {detailCourse?.lessons?.map((topic) => {
                    return (
                        <Card key={topic.id}>
                            <Row>
                                <Col>
                                    <span className="font-weight-bold">
                                        {topic?.name}
                                    </span>
                                    <Row hidden={!topic?.test}>
                                        <Col>
                                            <span>
                                                Bài Kiểm tra :{' '}
                                                {topic?.test?.name}
                                            </span>
                                            {/* <EditChangeNameTopic
                                    item={topic}
                                    onIndexOpen={() =>
                                        index !== indexOpen
                                            ? setIndexOpen(index)
                                            : setIndexOpen(-1)
                                    }
                                    userId={userId}
                                    detailCourse={detailCourse}
                                    onReloadTopic={onReLoadTopic}
                                /> */}
                                        </Col>

                                        <Col align="right">
                                            <Space>
                                                <Dropdown
                                                    overlay={
                                                        <Menu>
                                                            <Menu.Item>
                                                                <Link
                                                                    to={{
                                                                        pathname: `/teacher/course/${props.courseId}/detail`,
                                                                        search: `exam_id=${topic?.test?.id}`
                                                                    }}
                                                                >
                                                                    {t(
                                                                        'see_detail'
                                                                    )}
                                                                </Link>
                                                            </Menu.Item>
                                                            <Menu.Item
                                                                hidden={
                                                                    user?.id !==
                                                                    detailCourse
                                                                        .created_by
                                                                        .id
                                                                }
                                                            >
                                                                <Link
                                                                    to={'#'}
                                                                    onClick={(
                                                                        e
                                                                    ) => {
                                                                        e.preventDefault();
                                                                        setVisible(
                                                                            true
                                                                        );
                                                                        setObject(
                                                                            {
                                                                                courseId: Number(
                                                                                    props.courseId
                                                                                ),
                                                                                sectionId: Number(
                                                                                    topic?.id
                                                                                )
                                                                            }
                                                                        );
                                                                    }}
                                                                >
                                                                    {t(
                                                                        'delete'
                                                                    )}
                                                                </Link>
                                                            </Menu.Item>
                                                        </Menu>
                                                    }
                                                    placement="bottomLeft"
                                                >
                                                    <Button>
                                                        {t('action')}
                                                    </Button>
                                                </Dropdown>
                                            </Space>
                                        </Col>
                                    </Row>
                                </Col>

                                <Col align="right">
                                    <Space>
                                        <Dropdown
                                            overlay={
                                                <Menu>
                                                    <Menu.Item>
                                                        <Link
                                                            to={`${getUrl(
                                                                UPDATE_PAGE_COURSE,
                                                                {
                                                                    courseId:
                                                                        props.courseId
                                                                }
                                                            )}`}
                                                        >
                                                            {t('see_detail')}
                                                        </Link>
                                                    </Menu.Item>

                                                    <Menu.Item
                                                        hidden={
                                                            user?.id !==
                                                            detailCourse
                                                                .created_by.id
                                                        }
                                                    >
                                                        <Link
                                                            to={'#'}
                                                            onClick={(e) => {
                                                                e.preventDefault();
                                                                setVisible(
                                                                    true
                                                                );
                                                                setObject({
                                                                    courseId: Number(
                                                                        props.courseId
                                                                    ),
                                                                    sectionId: Number(
                                                                        topic?.id
                                                                    )
                                                                });
                                                            }}
                                                        >
                                                            {t('delete')}
                                                        </Link>
                                                    </Menu.Item>
                                                </Menu>
                                            }
                                            placement="bottomLeft"
                                        >
                                            <Button>{t('action')}</Button>
                                        </Dropdown>
                                    </Space>
                                </Col>
                            </Row>
                        </Card>
                    );
                })}
            </div>

            <PopupAddContent
                courseId={props.courseId}
                topic={topicSelected}
                showPopup={showAddPagePopup}
                hidePopup={onHideAddContentPopup}
            />
            <Confirm
                title={`${t('delete_lesson')}?`}
                content={t('delete_confirmation_content', {
                    name: 'bài giảng'
                })}
                onVisible={visible}
                onOk={onOkDeleteConfirm}
                onCancel={() => setVisible(false)}
            />
            {showMessageError && (
                <ModalError
                    messageError={messageError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </>
    );
}

export default TopicList;
