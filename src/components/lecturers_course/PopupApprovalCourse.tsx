import React, { useEffect } from 'react';
import { Modal, Alert } from 'antd';
import { useTranslation } from 'react-i18next';
import { get } from 'lodash';
import useCourseCanApprove from 'hooks/course/useCourseCanApprove';
import './styles/PopupApprovalCourse.scss';
interface Props {
    visible: boolean;
    courseId: number;
    onConfirmApproveCourse: () => void;
    onFalseModal: () => void;
    modalLabel?: string;
    confirmLabel?: string;
}
type Reason = {
    code: string;
    invalid: Invalid[];
};
type Invalid = {
    id: number;
    name: string;
    sectionId?: number;
    sectionName?: string;
};
function PopupApprovalCourse(props: Props): JSX.Element {
    const {
        visible,
        courseId,
        onConfirmApproveCourse,
        onFalseModal,
        modalLabel,
        confirmLabel
    } = props;
    const { t } = useTranslation();
    const [onApproveCourses, res] = useCourseCanApprove();
    useEffect(() => {
        onApproveCourses({
            courseId: courseId
        });
    }, [visible]);
    const handleOk = () => {
        onConfirmApproveCourse();
    };
    const handleCancel = () => {
        onFalseModal();
    };
    return (
        <>
            <Modal
                title={modalLabel || t('approve_course')}
                visible={visible}
                okText={t('accept')}
                cancelText={t('cancel')}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                {res?.code
                    ? get(res, 'data.reason', []).map((reason: Reason) =>
                          (reason.invalid || [{}]).map((invalid) => (
                              <Alert
                                  message={t(`messages.${reason.code}`, {
                                      name: invalid.name,
                                      section: invalid.sectionName
                                  })}
                                  type="warning"
                              />
                          ))
                      )
                    : ''}
                <h6 style={{ textAlign: 'center', fontWeight: 'bold' }}>
                    {confirmLabel
                        ? confirmLabel
                        : t('do_you_really_approve_this_course')}
                </h6>
            </Modal>
        </>
    );
}

export default PopupApprovalCourse;
