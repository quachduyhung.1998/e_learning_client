import { PlusOutlined, CaretDownOutlined } from '@ant-design/icons';
import {
    Button,
    Col,
    Dropdown,
    Form,
    Input,
    Menu,
    Row,
    Select,
    Space
} from 'antd';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import { getStatusFormatCourse } from 'helper/StatusFormat';
import { Filter } from 'pages/teacher-course/CourseListPage';
import React, { ChangeEvent, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import { ADD_COURSE, CREATE_QUESTION } from 'routes/web/paths';
import { Category } from 'types/Category';
import { CourseStatus } from 'types/Course';
import { User } from 'types/User';
const { Option } = Select;
const { Search } = Input;
interface Props {
    categories: Category[];
    authors: User[];
    onSearch: (search: string) => void;
    onChangeCategory: (categoryId: number) => void;
    onChangeAuthor: (createdBy: string) => void;
    onChangeStatus: (status: number | '') => void;
    searchType?: string;
    filter: Filter;
}

function ListCourseAction(props: Props): JSX.Element {
    const { t } = useTranslation();
    const {
        categories,
        authors,
        onSearch,
        onChangeCategory,
        onChangeStatus,
        onChangeAuthor,
        searchType
    } = props;
    const history = useHistory();
    const user = LocalStorageHelper.getObject(StorageKey.USER_SESSION);
    const [form] = Form.useForm();
    useEffect(() => {
        form.setFieldsValue(props.filter);
    }, [props.filter]);

    const handleRedirect = (): void => {
        history.push(ADD_COURSE);
    };

    const handleSearch = (e: ChangeEvent<HTMLInputElement>) => {
        onSearch(e.target.value);
    };
    const onSearchSubmit = (q: string) => {
        onSearch(q);
    };

    const handleChangeCategory = (value: number) => {
        onChangeCategory(value);
    };

    const handleChangeStatus = (value: number | string) => {
        onChangeStatus(value !== '' ? Number(value) : '');
    };

    const handleChangeAuthor = (_id: string) => {
        onChangeAuthor(_id);
    };

    return (
        <Form form={form}>
            <Row gutter={[8, 8]} justify="end">
                {/* <Col sm={{ span: 6 }} className="mb-1">
                    <Search
                        placeholder={t('search') + '...'}
                        style={{
                            maxWidth: '350px'
                        }}
                        size="middle"
                        onSearch={(value) => onSearchSubmit(value)}
                        onChange={handleSearch}
                        defaultValue={props.filter.search}
                    />
                </Col>
                <Col sm={{ span: 6 }} className="mb-1">
                    <Form.Item name="categoryId">
                        <Select
                            suffixIcon={<CaretDownOutlined />}
                            style={{ width: '100%' }}
                            defaultValue={props.filter.categoryId}
                            onChange={handleChangeCategory}
                        >
                            <Option value={0}>{t('all_category')}</Option>
                            {categories.map((item: Category) => {
                                return (
                                    <Option key={item.id} value={item.id}>
                                        {item.depth === 1
                                            ? item.name
                                            : `\u00A0\u00A0${item.name}`}
                                    </Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={{ span: 6 }} className="mb-1">
                    <Form.Item name="status">
                        <Select
                            suffixIcon={<CaretDownOutlined />}
                            style={{ width: '100%' }}
                            defaultValue={props.filter.status}
                            onChange={handleChangeStatus}
                        >
                            <Option value="">{t('all_status')}</Option>
                            {!searchType ? (
                                <Option value={CourseStatus.STATUS_DRAFT}>
                                    {t(
                                        getStatusFormatCourse(
                                            CourseStatus.STATUS_DRAFT
                                        )
                                    )}
                                </Option>
                            ) : (
                                ''
                            )}
                            <Option value={CourseStatus.STATUS_REVIEWING}>
                                {t(
                                    getStatusFormatCourse(
                                        CourseStatus.STATUS_REVIEWING
                                    )
                                )}
                            </Option>
                            <Option value={CourseStatus.STATUS_APPROVED}>
                                {t(
                                    getStatusFormatCourse(
                                        CourseStatus.STATUS_APPROVED
                                    )
                                )}
                            </Option>
                        </Select>
                    </Form.Item>
                </Col>
                {!authors || !authors.length ? (
                    ''
                ) : (
                    <Col sm={{ span: 6 }} className="mb-1">
                        <Form.Item name="createBy">
                            <Select
                                suffixIcon={<CaretDownOutlined />}
                                style={{ width: '100%' }}
                                defaultValue={props.filter.createdBy}
                                onChange={handleChangeAuthor}
                            >
                                <Option value="">{t('all_authors')}</Option>
                                {authors.map((item: User) => {
                                    return (
                                        <Option key={item._id} value={item._id}>
                                            {item.fullName || item.username}
                                        </Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>
                    </Col>
                )} */}

                {!searchType && user?.role !== 2 ? (
                    <Col
                        sm={{ span: 6 }}
                        className="mb-1 d-flex justify-content-end"
                    >
                        <Space>
                            <Button
                                shape="round"
                                icon={<PlusOutlined />}
                                type="primary"
                                onClick={handleRedirect}
                            >
                                {t('create_course')}
                            </Button>
                        </Space>
                    </Col>
                ) : (
                    ''
                )}
            </Row>
        </Form>
    );
}

export default ListCourseAction;
