import {
    Button,
    Dropdown,
    Menu,
    Popconfirm,
    Table,
    Typography,
    Avatar,
    Tooltip
} from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { DATE_TIME_VIEW } from 'helper/DateTimeHelper';
import getImageServer from 'helper/ImageHelper';
import { getStatusFormatCourse } from 'helper/StatusFormat';
import useApproveCourse from 'hooks/course/useApproveCourse';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { LECTURERS_COURSE } from 'routes/web/paths';
import { Course, CourseStatus } from 'types/Course';
import { User } from 'types/User';
import PopupApprovalCourse from './PopupApprovalCourse';
import { MAX_LENGTH_NUMBER } from 'constants/Constants';
import ModalError from 'components/shares/modal/ModalError';

interface Props {
    listCourse: Course[];
    page: number;
    refreshData: () => void;
}
interface Options {
    id: number;
    detail: string;
    browseCourse: string;
    isApproved?: boolean;
}

type AlignType = 'left' | 'center' | 'right';

interface VisibleIds {
    [key: number]: boolean;
}

function TableApprovalCourse(props: Props): JSX.Element {
    const { t } = useTranslation();

    const { listCourse, page } = props;
    const [columns, setColumns] = useState<ColumnsType<Course>>([]);
    const [
        resApproveCourse,
        onApproveCourse,
        courseApproveError
    ] = useApproveCourse();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (courseApproveError) {
            setShowMessageError(true);
        }
    }, [courseApproveError]);

    const [visibleIds, setVisibleIds] = useState<VisibleIds>({});

    const handleVisibleChange = (flag: boolean, id: number) => {
        setVisibleIds({ ...visibleIds, [id]: flag });
    };

    const handleMenuClick = (key: string, id: number) => {
        if (key === '2') {
            setVisibleIds({ ...visibleIds, [id]: true });
        }
    };
    const [showApprovalCourse, setShowApprovalCourse] = useState(false);
    const [courseId, setCourseId] = useState<number>(0);

    // Độ dài tối đa
    const MAX_LENGTH: number = MAX_LENGTH_NUMBER.MAX_LENGTH_TABLE_MY_COURSE;

    const [menu] = useState(() => (data: Options) => {
        return (
            <Menu onClick={(e) => handleMenuClick(e.key, data.id)}>
                <Menu.Item key="1">
                    <Link to={data.detail}>{t('see_detail')}</Link>
                </Menu.Item>
                {!data.isApproved && (
                    <Menu.Item key="2">
                        <Link
                            to="#"
                            onClick={(e) => {
                                e.preventDefault();
                                setShowApprovalCourse(true);
                                setCourseId(data.id);
                            }}
                        >
                            {t('approve')}
                        </Link>
                        {/* <Popconfirm
                            placement="topRight"
                            title={t('question_approve_course')}
                            onConfirm={() => {
                                data.id &&
                                    onApproveCourse({
                                        courseId: data.id
                                    });
                            }}
                            okText={t('accept')}
                            cancelText={t('no')}
                        >
                            {t('approve')}
                        </Popconfirm> */}
                    </Menu.Item>
                )}
            </Menu>
        );
    });

    useEffect(() => {
        props?.refreshData && props?.refreshData();
    }, [resApproveCourse]);

    useEffect(() => {
        setColumns([
            {
                title: t('stt'),
                dataIndex: 'course_id',
                align: 'center' as AlignType,
                render: (id: number, item: Course, index: number) => {
                    return index + 1 + (listCourse?.length || 0) * (page - 1);
                }
            },
            {
                title: t('image'),
                dataIndex: 'course_image',
                align: 'center' as AlignType,
                // eslint-disable-next-line react/display-name
                render: (image: string) => {
                    return <Avatar shape="square" size={100} src={image} />;
                }
            },

            {
                title: t('course_name'),
                // eslint-disable-next-line react/display-name
                render: (row: any) => {
                    return (
                        <Tooltip placement="topLeft" title={row?.course_name}>
                            <Link
                                to={LECTURERS_COURSE + '/' + row.course_id}
                                // component={Typography.Link}
                            >
                                {row?.course_name?.length > MAX_LENGTH ? (
                                    <div>
                                        {`${row?.course_name.substring(
                                            0,
                                            MAX_LENGTH
                                        )} ...`}
                                    </div>
                                ) : (
                                    <div>{row?.course_name}</div>
                                )}
                            </Link>
                        </Tooltip>
                    );
                }
            },
            {
                title: t('time_create'),
                dataIndex: 'createdAt',
                align: 'center' as AlignType,
                // eslint-disable-next-line react/display-name
                render: (createdAt: string) => (
                    <>{moment(createdAt).format(DATE_TIME_VIEW)}</>
                )
            },
            {
                title: t('status'),
                dataIndex: 'status',
                align: 'center' as AlignType,
                // eslint-disable-next-line react/display-name
                render: (status: CourseStatus) => <>Chờ duyệt</>
            },
            {
                title: t('author'),
                dataIndex: 'created_by',
                render: (creator: User) => creator?.name
            },
            {
                title: t('action'),
                dataIndex: 'course_id',
                align: 'center' as AlignType,
                // eslint-disable-next-line react/display-name
                render: (id: number, course: Course) => (
                    <Dropdown
                        // trigger={['click']}
                        visible={visibleIds[id]}
                        onVisibleChange={(flag: boolean) =>
                            handleVisibleChange(flag, id)
                        }
                        overlay={() =>
                            menu({
                                id: id,
                                detail: LECTURERS_COURSE + '/' + id,
                                browseCourse: '/',
                                isApproved:
                                    course.status ===
                                    CourseStatus.STATUS_APPROVED
                            })
                        }
                        placement="bottomLeft"
                    >
                        <Button
                            onClick={() =>
                                setVisibleIds({ ...visibleIds, [id]: true })
                            }
                        >
                            {t('action')}
                        </Button>
                    </Dropdown>
                )
            }
        ]);
    }, [props.page, visibleIds]);
    return (
        <>
            <Table
                style={{ overflowX: 'scroll' }}
                columns={columns}
                dataSource={listCourse}
                bordered
                pagination={false}
                locale={{
                    emptyText: t('no_data')
                }}
                rowKey={(course: any) => course.course_id}
                scroll={{ x: 'auto' }}
            />
            <PopupApprovalCourse
                visible={showApprovalCourse}
                courseId={courseId}
                onConfirmApproveCourse={() => {
                    onApproveCourse({
                        courseId: courseId
                    });
                    setShowApprovalCourse(false);
                }}
                onFalseModal={() => setShowApprovalCourse(false)}
            />
            {showMessageError && (
                <ModalError
                    messageError={courseApproveError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </>
    );
}

export default React.memo(TableApprovalCourse);
