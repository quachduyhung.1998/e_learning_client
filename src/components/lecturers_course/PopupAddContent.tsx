import { getUrl } from 'helper/String';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import {
    ListGroup,
    ListGroupItem,
    Modal,
    ModalBody,
    ModalHeader
} from 'reactstrap';
import { ADD_EXAM_TOPIC, ADD_PAGE_COURSE } from 'routes/web/paths';
import { Topic } from 'types/Topic';

interface Props {
    hidePopup: () => void;
    showPopup: boolean;
    topic?: Topic;
    courseId: string;
}

function PopupAddContent(props: Props): JSX.Element {
    const { t } = useTranslation();
    const toggle = () => props.hidePopup();
    const history = useHistory();

    const handleRedirectAddLesson = () => {
        if (!props.topic) return;
        history.push(
            `${getUrl(ADD_PAGE_COURSE, {
                courseId: props.courseId,
                sectionId: props.topic.section
            })}`
        );
    };

    const onCreateExamClicked = () => {
        if (!props.topic) return;

        history.push(
            `${getUrl(ADD_EXAM_TOPIC, {
                courseId: props.courseId,
                sectionId: props.topic.section
            })}`
        );
    };

    return (
        <Modal onClosed={toggle} isOpen={props.showPopup} toggle={toggle}>
            <ModalHeader toggle={toggle}>{t('select_the_content')}</ModalHeader>
            <ModalBody>
                <ListGroup>
                    <ListGroupItem
                        onClick={onCreateExamClicked}
                        tag="button"
                        action
                    >
                        {t('test')}
                    </ListGroupItem>
                    <ListGroupItem
                        tag="button"
                        action
                        onClick={handleRedirectAddLesson}
                    >
                        {t('lesson_content')}
                    </ListGroupItem>
                </ListGroup>
            </ModalBody>
        </Modal>
    );
}

export default PopupAddContent;
