import React, { ChangeEvent, useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Input, Modal, message } from 'antd';
import './CourseForm.scss';
import ModalError from 'components/shares/modal/ModalError';
interface Props {
    hidePopup: () => void;
    onAddlesson: (name: string) => void;
    showPopup: boolean;
}

function PopupNewLesson(props: Props): JSX.Element {
    const { t } = useTranslation();

    const [name, setName] = useState<string>('');
    const toggle = () => props.hidePopup();
    const [messageError, setMessageError] = useState<string>('');
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (messageError) {
            setShowMessageError(true);
        }
    }, [messageError]);
    const handleAddLesson = () => {
        if (name.trim().length < 2 || name.trim().length > 200) {
            setMessageError(`${t('topic_length_invalidate')}`);
            return;
        }

        props.onAddlesson(name);
        setName('');
        props.hidePopup();
    };

    const handleChangeName = (e: ChangeEvent<HTMLInputElement>) => {
        setName(e.target.value);
    };

    return (
        <div>
            <Modal
                className="popup-new-lesson"
                title={t('add_lesson')}
                visible={props.showPopup}
                onOk={handleAddLesson}
                onCancel={toggle}
                okText={t('create')}
                cancelText={t('ignore')}
            >
                <Input
                    autoFocus
                    type="text"
                    name="lesson"
                    id="lesson"
                    placeholder={t('name')}
                    onChange={handleChangeName}
                />
            </Modal>
            {showMessageError && (
                <ModalError
                    messageError={messageError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
}

export default React.memo(PopupNewLesson);
