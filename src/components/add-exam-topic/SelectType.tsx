import React from 'react';
import { Nav, NavItem, NavLink } from 'reactstrap';
import { useTranslation } from 'react-i18next';
interface Props {
    selectActiveTab: string | undefined;
    onHandleSelect: (value: string) => void;
}
function SelectType(props: Props): JSX.Element {
    const { t } = useTranslation();
    return (
        <Nav className="box-tab-pill">
            <NavItem>
                <NavLink
                    className={
                        props.selectActiveTab === '1' ? 'active' : undefined
                    }
                    onClick={() => {
                        props.onHandleSelect('1');
                    }}
                >
                    {t('general_information')}
                </NavLink>
            </NavItem>
            <NavItem>
                <NavLink
                    className={
                        props.selectActiveTab === '2' ? 'active' : undefined
                    }
                    onClick={() => {
                        props.onHandleSelect('2');
                    }}
                >
                    {t('setup')}
                </NavLink>
            </NavItem>
        </Nav>
    );
}

export default SelectType;
