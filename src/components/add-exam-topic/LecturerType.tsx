import CKEditor from 'components/ckeditor/ckEditor';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Col, Row } from 'reactstrap';
import { Form, Input } from 'antd';
import { Rule, FormInstance } from 'antd/lib/form';
import { VALIDATION } from 'constants/Constants';
interface Props {
    intro: string | undefined;
    form: FormInstance;
}
/**
 * Phần Mô tả trong phần thông tin chung
 */
function LecturerType(props: Props): JSX.Element {
    const { t } = useTranslation();
    const rules: Rule[] = [
        ({ getFieldValue }) => ({
            validator(rule, value) {
                if (!value) {
                    return Promise.resolve();
                }

                const name = getFieldValue('intro');

                if (name.trim().length > VALIDATION.MAX_QUIZ_DESC_LENGTH) {
                    return Promise.reject(
                        t('desc_course_length_max_invalidate', { max: 1000 })
                    );
                }

                return Promise.resolve();
            }
        })
    ];
    const handleChangeDesc = (event: any, editor: any) => {
        props.form.setFieldsValue({ intro: editor.getData() });
    };

    return (
        <Row className="mt-4">
            <Col sm={3}>
                <p>{t('description')}</p>
            </Col>
            <Col sm={9}>
                <div className="ckh-300">
                    <CKEditor onChange={handleChangeDesc} data={props.intro} />
                    <Form.Item rules={rules} name="intro">
                        <Input hidden />
                    </Form.Item>
                </div>
            </Col>
        </Row>
    );
}

export default React.memo(LecturerType);
