import {
    Checkbox,
    DatePicker,
    Form,
    Input,
    InputNumber,
    Select,
    Col,
    Row
} from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { FormInstance } from 'antd/lib/form';
import SelectCondition from 'components/lecturers-page-course/SelectCondition';
import { DATE_TIME_VIEW } from 'helper/DateTimeHelper';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Module from 'types/Module';
import moment from 'moment';

/**
 * Phần cài đặt chung
 * @param props
 */

interface Props {
    timeOpen?: string;
    timeClose?: string;
    modules: Module[];
    conditions: number[];
    form: FormInstance;
    enableTimeOpen?: boolean;
    enableTimeClose?: boolean;
    onChangeEnableDate: (name: string) => (e: CheckboxChangeEvent) => void;
}

function SetupExam(props: Props): JSX.Element {
    const { modules } = props;

    const { t } = useTranslation();

    useEffect(() => {
        props?.form &&
            props.form.setFieldsValue({
                conditions: props?.conditions?.join(',')
            });
    }, [props.conditions]);

    const onUpdateConditions = (module: Module | undefined) => {
        if (module && module?.id) {
            props?.form.setFieldsValue({ conditions: [module?.id].join(',') });
        } else {
            props?.form.setFieldsValue({ conditions: [].join(',') });
        }
    };
    return (
        <>
            <Form.Item label={t('required_success')} name="conditions">
                <SelectCondition
                    modules={modules}
                    onUpdate={onUpdateConditions}
                    conditions={props?.conditions}
                    key={props?.conditions.join(',')}
                />
            </Form.Item>
            <Form.Item label={t('amount_exam')} name="attempts">
                <InputNumber min={0} style={{ width: '100%' }} />
            </Form.Item>
            <Form.Item label={t('targets_exam')} name="gradePass">
                <InputNumber style={{ width: '100%' }} min={0} max={100} />
            </Form.Item>
            <Form.Item label={t('operation_time_setting')} name="timeOpen">
                <Checkbox
                    checked={props.enableTimeOpen}
                    onChange={props.onChangeEnableDate('timeOpen')}
                >
                    <span style={{ fontWeight: 'bold' }}>{t('time_open')}</span>
                </Checkbox>
                <DatePicker
                    allowClear={false}
                    defaultValue={
                        props?.form.getFieldValue('timeOpen') || moment()
                    }
                    format={DATE_TIME_VIEW}
                    showTime
                    disabled={!props.enableTimeOpen}
                    onChange={(value, dateString) =>
                        props?.form.setFieldsValue({
                            timeOpen: value
                        })
                    }
                    key={props?.form.getFieldValue('timeOpen')}
                />
            </Form.Item>
            <Row>
                <Col sm={6}></Col>
                <Col sm={16}>
                    <Form.Item
                        style={{}}
                        name="timeClose"
                        rules={[
                            ({ getFieldValue }) => ({
                                validator(rule, value) {
                                    if (!props.enableTimeClose) {
                                        return Promise.resolve();
                                    } else {
                                        const timeOpen = getFieldValue(
                                            'timeOpen'
                                        );
                                        if (
                                            !value ||
                                            (timeOpen && timeOpen >= value)
                                        ) {
                                            return Promise.reject(
                                                t(
                                                    'date_end_must_greater_than_start'
                                                )
                                            );
                                        }
                                    }
                                    return Promise.resolve();
                                }
                            })
                        ]}
                    >
                        <Checkbox
                            checked={props.enableTimeClose}
                            onChange={props.onChangeEnableDate('timeClose')}
                        >
                            <span style={{ fontWeight: 'bold' }}>
                                {t('time_close')}
                            </span>
                        </Checkbox>
                        <DatePicker
                            allowClear={false}
                            defaultValue={
                                props?.form.getFieldValue('timeClose') ||
                                moment()
                            }
                            format={DATE_TIME_VIEW}
                            showTime
                            disabled={!props.enableTimeClose}
                            onChange={(value, dateString) =>
                                props?.form.setFieldsValue({
                                    timeClose: value
                                })
                            }
                            key={props?.form.getFieldValue('timeClose')}
                        />
                    </Form.Item>
                </Col>
            </Row>
        </>
    );
}

export default React.memo(SetupExam);
