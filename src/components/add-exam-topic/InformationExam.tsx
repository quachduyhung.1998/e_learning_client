import { Form, Input } from 'antd';
import { Rule } from 'antd/lib/form';
import { VALIDATION } from 'constants/Constants';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Col, Row } from 'reactstrap';

/**
 * Phần thông tin chung
 * @param props
 */
function InformationExam(props: { name?: string }): JSX.Element {
    const { t } = useTranslation();
    const rules: Rule[] = [
        {
            required: true,
            message: t('this_is_required')
        },
        ({ getFieldValue }) => ({
            validator(rule, value) {
                if (!value) {
                    return Promise.resolve();
                }

                const name = getFieldValue('name');

                if (
                    name.trim().length < VALIDATION.MIN_QUIZ_NAME_LENGTH ||
                    name.trim().length > VALIDATION.MAX_QUIZ_NAME_LENGTH
                ) {
                    return Promise.reject(t('max_quiz_length'));
                }

                return Promise.resolve();
            }
        })
    ];
    return (
        <Row>
            <Col sm="3">
                <label htmlFor="">
                    {t('name_test')} <span className="text-danger">*</span>:
                </label>
            </Col>
            <Col>
                <Form.Item rules={rules} name="name">
                    <Input autoFocus type="text" className="form-control" />
                </Form.Item>
            </Col>
        </Row>
    );
}

export default React.memo(InformationExam);
