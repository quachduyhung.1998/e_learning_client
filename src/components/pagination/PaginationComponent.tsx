import React from 'react';
import { useTranslation } from 'react-i18next';
import ReactPaginate from 'react-paginate';
import './PaginationComponent.scss';
interface Props {
    pageCount: number; // Số lượng page
    pageRangeDisplayed: number; // Số lượng page muốn hiển thị ra
    initialPage: number; // Page khởi tao ban đầu để focus
    marginPagesDisplayed: number; // Cách bao nhiêu page hiển thị ...
    onPageChange: (selectedItem: { selected: number }) => void; // Lăng nghe sự kiện thay đổi page
}
function PaginationComponent(props: Props): JSX.Element {
    const { t } = useTranslation();

    return (
        <div className="pagination mt-3 d-flex justify-content-end">
            {props?.pageCount > 0 ? (
                <ReactPaginate
                    previousLabel={t('previous')}
                    nextLabel={t('next')}
                    breakLabel={'...'}
                    pageCount={props.pageCount}
                    initialPage={props.initialPage}
                    marginPagesDisplayed={props.marginPagesDisplayed}
                    pageRangeDisplayed={props.pageRangeDisplayed}
                    onPageChange={props.onPageChange}
                    containerClassName={'pagination'}
                    activeClassName={'active'}
                    pageClassName={'pageClassName'}
                    previousClassName={'previousClassName'}
                    nextClassName={'nextClassName'}
                    breakClassName="breakClassName"
                />
            ) : (
                ''
            )}
        </div>
    );
}

export default PaginationComponent;
