import React from 'react';
import { Typography, Divider } from 'antd';
import './TitleComponent.scss';

interface Props {
    title?: string;
    color?: string;
    className?: string;
}

function TitleComponent(props: Props): JSX.Element {
    return (
        <div className={`title-component-wrapper ${props.className}`}>
            <Typography.Title className="title" style={{ color: props.color }}>
                {props.title}
            </Typography.Title>
            <div className="divider" style={{ backgroundColor: props.color }} />
        </div>
    );
}

export default TitleComponent;
