import ItemCourse from 'components/list-course/ItemCourse';
import React from 'react';
import { Course } from 'types/Course';
import './SliderCourse.less';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';
import { LeftOutlined, RightOutlined } from '@ant-design/icons';
import constPaginate from 'constants/Paginate';

interface Props {
    data: Course[];
}

function SliderCourse(props: Props): JSX.Element {
    function SampleNextArrow(props: any) {
        const { className, style, onClick } = props;
        return (
            <span className={className} onClick={onClick}>
                <RightOutlined />
            </span>
        );
    }

    function SamplePrevArrow(props: any) {
        const { className, style, onClick } = props;
        return (
            <div className={className} onClick={onClick}>
                <LeftOutlined />
            </div>
        );
    }
    const settings = {
        dots: false,
        infinite: false,
        slidesToShow: constPaginate.SLIDER_TO_SHOW,
        slidesToScroll: 1,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };
    return (
        <Slider {...settings}>
            {props.data.map((course: Course) => {
                return <ItemCourse key={course.id} course={course} />;
            })}
        </Slider>
    );
}

export default SliderCourse;
