import { Button, Col, Form, Input, Pagination, Row } from 'antd';
import MainEmpty from 'components/shares/MainEmpty';
import TitleComponent from 'components/text/TitleComponent';
import constPaginate from 'constants/Paginate';
import { TIME_OUT_SEARCH } from 'constants/Search';
import constStyle from 'constants/Style';
import useGetCourseInJoin from 'hooks/course/useGetCourseInJoin';
import useGetCourseRecommendation from 'hooks/course/useGetCourseRecommendation';
import useQuery from 'hooks/route/useQuery';
import useFetchCategories from 'hooks/useFetchCategories';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { Category } from 'types/Category';
import { Course } from 'types/Course';
import { ListCourseData } from 'types/responses';
import ItemCourse from './ItemCourse';
import SelectTree from 'components/shares/SelectTree';

interface Props {
    typeList: string;
    onBack?: () => void;
    limit?: number;
    getPageFromParams?: boolean;
}
const { Search } = Input;
const limitDefault = 8;

const ListCourse = (props: Props) => {
    const { t } = useTranslation();
    const history = useHistory();
    const query = useQuery();
    const pages = query.get('page');
    const [search, setSearch] = useState<string>('');
    const [typeList, setTypeList] = useState<string>(props.typeList);
    const [cateIds, setCateIds] = useState<number[]>([]);
    const [filter, setFilter] = useState({
        page:
            props?.getPageFromParams && pages
                ? Number(pages)
                : constPaginate.PAGE_DEFAULT,
        limit: props.limit || limitDefault,
        categoryId: cateIds,
        search
    });
    const [form] = Form.useForm();
    const [onFetchCategories, resFetchCategories] = useFetchCategories();

    const [resGetCourses, setResGetCourses] = useState<ListCourseData>();
    const { resGetCourseInJoin, onGetCourseInJoin } = useGetCourseInJoin();
    const {
        resGetCourseRecommendation,
        onGetCourseRecommendation
    } = useGetCourseRecommendation();

    useEffect(() => {
        onFetchCategories();
    }, []);

    const onSearching = (text: string) => {
        setSearch(text);
    };

    useEffect(() => {
        setFilter({
            ...filter,
            search
        });
    }, [search]);

    useEffect(() => {
        setTypeList(props.typeList);
        setFilter({
            ...filter,
            page:
                props?.getPageFromParams && pages
                    ? Number(pages)
                    : constPaginate.PAGE_DEFAULT,
            limit: props.limit || limitDefault
        });
    }, [props.typeList]);

    const onChangeFilter = (page: number = constPaginate.PAGE_DEFAULT) => {
        const values = form.getFieldsValue();
        if (props?.getPageFromParams) {
            history.push({
                search: `?page=${page + 1}`
            });
        }
        setFilter({
            page: page + 1,
            limit: props.limit || limitDefault,
            categoryId: cateIds || [],
            search: values?.search || ''
        });
    };

    useEffect(() => {
        getListCourse();
    }, [filter]);

    const getListCourse = () => {
        switch (typeList) {
            case 'my_course':
                onGetCourseInJoin(filter);
                break;
            default:
                onGetCourseRecommendation(filter);
                break;
        }
    };

    useEffect(() => {
        switch (typeList) {
            case 'my_course':
                setResGetCourses(resGetCourseInJoin);
                break;
            case 'recommendation_course':
                setResGetCourses(resGetCourseRecommendation);
                break;
            default:
                setResGetCourses(undefined);
                break;
        }
    }, [resGetCourseInJoin, resGetCourseRecommendation]);

    const timeOutRef = useRef<number>(0);

    const onChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        timeOutRef.current && clearTimeout(timeOutRef.current);
        timeOutRef.current = setTimeout(() => {
            onChangeFilter(0);
        }, TIME_OUT_SEARCH);
    };
    const onChangeSelectedCate = (cates: any) => {
        const cateIds: number[] = [];
        if (cates && cates.length) {
            cates.map((cate: any) => {
                cateIds.push(cate.value);
            });

            setCateIds(cateIds);
            setFilter({
                ...filter,
                categoryId: cateIds
            });
        } else {
            setCateIds([]);
            setFilter({
                ...filter,
                categoryId: []
            });
        }
    };
    return (
        <>
            <Row gutter={[8, 8]}>
                <Col md={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
                    <Row justify="space-between">
                        <Col>
                            <TitleComponent
                                color={constStyle.COLOR_MAIN}
                                title={t(props.typeList)}
                            />
                        </Col>
                        <Col>
                            <Button onClick={() => history.push('/')}>
                                {t('back')}
                            </Button>
                        </Col>
                    </Row>
                </Col>
                <Col md={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
                    <Form form={form}>
                        <Row style={{ width: '100%' }} gutter={[8, 8]}>
                            <Col
                                md={{ span: 12 }}
                                sm={{ span: 24 }}
                                xs={{ span: 24 }}
                            >
                                <Form.Item name="categoryId">
                                    <SelectTree
                                        data={resFetchCategories}
                                        keyData={{
                                            value: 'id',
                                            title: 'name',
                                            children: 'children'
                                        }}
                                        placeholder={t('theme')}
                                        showSearch
                                        multiple
                                        onChange={onChangeSelectedCate}
                                        treeCheckStrictly
                                        allowClear
                                        treeCheckable
                                    />
                                </Form.Item>
                            </Col>
                            <Col
                                md={{ span: 12 }}
                                sm={{ span: 24 }}
                                xs={{ span: 24 }}
                            >
                                <Form.Item name="search">
                                    <Search
                                        placeholder={t('search')}
                                        onSearch={onSearching}
                                        onChange={onChangeSearch}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </Row>
            <Row>
                {resGetCourses?.results?.map((course: Course) => {
                    return (
                        <Col key={course.id} md={{ span: 6 }} sm={{ span: 8 }}>
                            <ItemCourse course={course} />
                        </Col>
                    );
                })}
            </Row>
            {!resGetCourses?.results?.length && <MainEmpty />}

            <Row>
                <Col
                    span={24}
                    style={{ textAlign: 'right', marginTop: '15px' }}
                >
                    <Pagination
                        hideOnSinglePage={true}
                        current={filter?.page}
                        pageSize={filter?.limit}
                        total={
                            resGetCourses?.total ||
                            (resGetCourses?.totalPage || 0) * filter?.limit
                        }
                        onChange={(page: number, pageSize?: number) =>
                            onChangeFilter(page - 1)
                        }
                    />
                </Col>
            </Row>
        </>
    );
};

export default ListCourse;
