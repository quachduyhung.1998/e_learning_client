import { Button, Col, Progress, Row, Tooltip, Typography } from 'antd';
import constStyle from 'constants/Style';
import getImageServer from 'helper/ImageHelper';
import { getUrl, subString } from 'helper/String';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import {
    STUDENT_COURSE_CONTENT,
    STUDENT_COURSE_DETAIL
} from 'routes/web/paths';
import { Course } from 'types/Course';
interface Props {
    course: Course;
}

const { Title } = Typography;

const ProgressCourse = (props: { course: Course }): JSX.Element => {
    const { t } = useTranslation();
    const history = useHistory();
    if (props.course.percent_progress) {
        const progressCourse =
            props.course.percent_progress >= 100
                ? 100
                : props.course.percent_progress;
        return (
            <>
                <p
                    style={{
                        paddingRight: '5px',
                        textAlign: 'right',
                        margin: '0px',
                        color:
                            progressCourse === 100
                                ? constStyle.COLOR_MAIN_4
                                : constStyle.COLOR_MAIN_2
                    }}
                >
                    {progressCourse === 100 ? t('done') : progressCourse + '%'}
                </p>
                <Progress
                    percent={progressCourse}
                    status={progressCourse === 100 ? 'success' : 'exception'}
                    showInfo={false}
                    style={{
                        top: '-0.75em'
                    }}
                />
            </>
        );
    } else {
        return (
            <Button
                onClick={() =>
                    history.push(
                        getUrl(STUDENT_COURSE_CONTENT, {
                            courseId: props.course.id
                        })
                    )
                }
                type="primary"
                shape="round"
                style={{ top: '0.5em' }}
            >
                {t('start_learn')}
            </Button>
        );
    }
};

const ItemCourse = (props: Props): JSX.Element => {
    const { t } = useTranslation();
    const history = useHistory();

    const onClickItem = () => {
        if (props.course.completion) {
            history.push(
                getUrl(STUDENT_COURSE_CONTENT, {
                    courseId: props.course.id
                })
            );
        } else {
            history.push(`${STUDENT_COURSE_DETAIL}/${props.course.id}`);
        }
    };
    return (
        <div
            style={{
                margin: '1em',
                border: '1px solid #f0f0f0'
            }}
        >
            <Row justify="space-between">
                <Col
                    span={24}
                    style={{ textAlign: 'center', padding: '0.5em' }}
                    onClick={onClickItem}
                    className="cursor-pointer"
                >
                    <div style={{ height: '150px' }}>
                        <img
                            style={{
                                width: 'auto',
                                height: '150px',
                                maxWidth: '100%'
                            }}
                            src={props.course.image}
                            alt=""
                        />
                    </div>
                </Col>
                <Col
                    span={24}
                    style={{
                        textAlign: 'center',
                        height: '6.5em'
                    }}
                >
                    <div onClick={onClickItem} className="cursor-pointer">
                        <Title
                            level={4}
                            style={{
                                color: constStyle.COLOR_MAIN,
                                height: '2.8em',
                                paddingRight: '0.5em',
                                paddingLeft: '0.5em'
                            }}
                            ellipsis={{ rows: 2, expandable: false }}
                        >
                            <Tooltip
                                placement="topLeft"
                                title={props.course.name}
                            >
                                <span>
                                    {subString(props.course.name, 100, '...')}{' '}
                                    {/* Nên dùng lodash truncate? */}
                                </span>
                            </Tooltip>
                        </Title>
                    </div>
                    <ProgressCourse course={props.course} />
                </Col>
            </Row>
        </div>
    );
};

export default ItemCourse;
