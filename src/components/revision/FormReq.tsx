import { Button, Form, InputNumber, Row, Space } from 'antd';
import { Store } from 'antd/lib/form/interface';
import EditableTagGroup from 'components/shares/EditableTagGroup';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import useCreateRevision from 'hooks/revision/useCreateRevision';
import useFetchCategories from 'hooks/useFetchCategories';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { REVISION_START } from 'routes/web/paths';
import SelectTree from 'components/shares/SelectTree';
import ModalError from 'components/shares/modal/ModalError';

interface Props {
    onCancel?: () => void;
}

const FormReq = (props: Props): JSX.Element => {
    const { t } = useTranslation();
    const history = useHistory();
    const [cateIds, setCateIds] = useState<number[]>([]);
    const {
        resCreateRevision,
        onCreateRevision,
        createRevisionError
    } = useCreateRevision();
    const [onFetchCategories, resFetchCategories] = useFetchCategories();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (createRevisionError) {
            setShowMessageError(true);
        }
    }, [createRevisionError]);
    useEffect(() => {
        onFetchCategories();
    }, []);

    const validateForm = {
        categoryIds: [
            {
                required: true,
                message: t('categoryId_is_required')
            }
        ],
        numberQuestion: [
            {
                required: true,
                message: t('number_question_is_required')
            }
        ],
        timeMinute: [
            {
                required: true,
                message: t('time_minute_is_required')
            }
        ],
        content: [
            {
                required: true,
                message: t('content_is_required')
            }
        ]
    };

    const [form] = Form.useForm();

    const onFinish = (values: Store) => {
        onCreateRevision({
            categoryIds: cateIds ? cateIds : [],
            conceptName: values.content.toString(),
            totalQuestion: values.numberQuestion,
            maxDuration: values.timeMinute
        });
    };

    useEffect(() => {
        if (resCreateRevision) {
            LocalStorageHelper.saveObject(
                StorageKey.REVISION_INFO,
                resCreateRevision
            );
            LocalStorageHelper.remove(StorageKey.CACHE_FORM_REVISION_REQ);
            if (history.location.pathname === REVISION_START) {
                window.location.reload();
            } else {
                history.push(REVISION_START);
                props?.onCancel && props?.onCancel();
            }
        }
    }, [resCreateRevision]);
    const onChangeSelectedCate = (cates: any) => {
        const cateIds: number[] = [];
        if (cates && cates.length) {
            cates.map((cate: any) => {
                cateIds.push(cate.value);
            });
            setCateIds(cateIds);
        }
    };

    const formCache = LocalStorageHelper.getObject(
        StorageKey.CACHE_FORM_REVISION_REQ
    );

    return (
        <div>
            <Form
                onValuesChange={(values) =>
                    LocalStorageHelper.saveObject(
                        StorageKey.CACHE_FORM_REVISION_REQ,
                        form.getFieldsValue()
                    )
                }
                onFinish={onFinish}
                labelCol={{ md: { span: 8 }, sm: { span: 24 } }}
                labelAlign="left"
                form={form}
                hideRequiredMark={true}
                initialValues={formCache}
            >
                <Form.Item label={<>{t('theme')}</>} name="categoryIds">
                    <SelectTree
                        data={resFetchCategories}
                        keyData={{
                            value: 'id',
                            title: 'name',
                            children: 'children'
                        }}
                        placeholder={t('please_select')}
                        showSearch
                        multiple
                        onChange={onChangeSelectedCate}
                        treeCheckStrictly
                        allowClear
                    />
                </Form.Item>
                <Form.Item
                    label={
                        <>
                            {t('content')}{' '}
                            <span className="required-dot">*</span>
                        </>
                    }
                    name="content"
                    validateTrigger={['submit']}
                    rules={validateForm.content}
                >
                    <EditableTagGroup
                        maxLengthItem={50}
                        maxItem={10}
                        tags={formCache.content}
                    />
                </Form.Item>
                <Form.Item
                    label={
                        <>
                            {t('number_of_questions')}{' '}
                            <span className="required-dot">*</span>
                        </>
                    }
                    name="numberQuestion"
                    rules={validateForm.numberQuestion}
                >
                    <InputNumber
                        min={1}
                        style={{ width: '100%' }}
                        precision={0}
                    />
                </Form.Item>
                <Form.Item
                    label={
                        <>
                            {t('time_minute')}{' '}
                            <span className="required-dot">*</span>
                        </>
                    }
                    name="timeMinute"
                    rules={validateForm.timeMinute}
                >
                    <InputNumber
                        min={1}
                        style={{ width: '100%' }}
                        precision={0}
                    />
                </Form.Item>
                <Row justify="center">
                    <Space>
                        {props?.onCancel && (
                            <Button
                                onClick={() => {
                                    props?.onCancel && props?.onCancel();
                                }}
                            >
                                {t('cancel')}
                            </Button>
                        )}
                        <Button htmlType="submit" type="primary">
                            {t('start')}
                        </Button>
                    </Space>
                </Row>
            </Form>
            {showMessageError && (
                <ModalError
                    messageError={createRevisionError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
};

export default FormReq;
