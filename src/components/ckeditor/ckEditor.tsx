// @ts-ignore
import CKEditor5 from '@ckeditor/ckeditor5-react';
// @ts-ignore
import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
// @ts-ignore
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
// @ts-ignore
import SimpleUploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/simpleuploadadapter';
// @ts-ignore
import Autoformat from '@ckeditor/ckeditor5-autoformat/src/autoformat';
// @ts-ignore
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
// @ts-ignore
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
// @ts-ignore
import BlockQuote from '@ckeditor/ckeditor5-block-quote/src/blockquote';
// @ts-ignore
import Heading from '@ckeditor/ckeditor5-heading/src/heading';
// @ts-ignore
import Image from '@ckeditor/ckeditor5-image/src/image';
// @ts-ignore
import ImageCaption from '@ckeditor/ckeditor5-image/src/imagecaption';
// @ts-ignore
import ImageStyle from '@ckeditor/ckeditor5-image/src/imagestyle';
// @ts-ignore
import ImageToolbar from '@ckeditor/ckeditor5-image/src/imagetoolbar';
// @ts-ignore
import ImageTextAlternative from '@ckeditor/ckeditor5-image/src/imagetextalternative';
// @ts-ignore
import ImageUpload from '@ckeditor/ckeditor5-image/src/imageupload';
// @ts-ignore
import Link from '@ckeditor/ckeditor5-link/src/link';
// @ts-ignore
import List from '@ckeditor/ckeditor5-list/src/list';
// @ts-ignore
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
// @ts-ignore
import Indent from '@ckeditor/ckeditor5-indent/src/indent';
// @ts-ignore
import Table from '@ckeditor/ckeditor5-table/src/table';
// @ts-ignore
import TableToolbar from '@ckeditor/ckeditor5-table/src/tabletoolbar';
// @ts-ignore
import MediaEmbed from '@ckeditor/ckeditor5-media-embed/src/mediaembed';
// @ts-ignore
import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment';

import { default as React } from 'react';
import LocalStorageHelper, { StorageKey } from 'helper/LocalStorageHelper';
import { Session } from 'types/Session';
import Endpoint from 'network/Endpoint';

interface Props {
    data?: string;
    disabled?: boolean;
    onChange: any;
}

function CKEditor(props: Props): JSX.Element {
    return (
        <CKEditor5
            editor={ClassicEditor}
            onChange={props.onChange}
            data={props.data}
            disabled={props.disabled || false}
            config={{
                plugins: [
                    Essentials,
                    SimpleUploadAdapter,
                    Autoformat,
                    Bold,
                    Italic,
                    BlockQuote,
                    Heading,
                    Image,
                    ImageCaption,
                    ImageStyle,
                    ImageTextAlternative,
                    ImageToolbar,
                    ImageUpload,
                    Link,
                    List,
                    Paragraph,
                    Indent,
                    Table,
                    TableToolbar,
                    MediaEmbed,
                    Alignment
                ],
                toolbar: [
                    'heading',
                    '|',
                    'bold',
                    'italic',
                    'link',
                    'bulletedList',
                    'numberedList',
                    '|',
                    'alignment',
                    'indent',
                    'outdent',
                    '|',
                    'imageUpload',
                    'blockQuote',
                    'insertTable',
                    'mediaEmbed',
                    'undo',
                    'redo'
                ],
                image: {
                    toolbar: [
                        'imageStyle:full',
                        'imageStyle:side',
                        '|',
                        'imageTextAlternative'
                    ]
                },
                table: {
                    contentToolbar: [
                        'tableColumn',
                        'tableRow',
                        'mergeTableCells'
                    ]
                },
                simpleUpload: {
                    uploadUrl: `${
                        process.env.REACT_APP_API_DOMAIN + Endpoint.UPLOAD
                    }?adapter=ckeditor`,
                    headers: {
                        Authorization: `Bearer ${(function (): string {
                            const sessionStr: string =
                                LocalStorageHelper.get(
                                    StorageKey.USER_SESSION
                                ) || '';
                            const session: Session = JSON.parse(sessionStr);
                            const token = session.idToken;
                            return token;
                        })()}`
                    }
                }
            }}
        />
    );
}

export default CKEditor;
