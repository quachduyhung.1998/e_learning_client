import { Input, Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Col, FormGroup, Row } from 'reactstrap';

interface props {
    onShowModal: (status: boolean) => void;
    onSearchListStudent: (text: string) => void;
}
/**
 * Show form search
 * @param props
 */
function Search(props: props): JSX.Element {
    const { t } = useTranslation();
    const { Search } = Input;
    const handleAddNew = () => {
        props.onShowModal(true);
    };
    return (
        <div>
            <Row className="mt-4">
                <Col sm="4">
                    <FormGroup>
                        <Search
                            className="box-search"
                            placeholder={t('search_student')}
                            onSearch={(value) =>
                                props.onSearchListStudent(value)
                            }
                            // style={{ width: 200 }}
                        />
                    </FormGroup>
                </Col>
                {/* <Col sm="8">
                    <Button
                        type="primary"
                        className="float-right btn-add-student"
                        onClick={handleAddNew}
                        style={{ borderRadius: '20px' }}
                    >
                        <svg
                            className="text-white"
                            width="11.549"
                            height="11.562"
                            viewBox="0 0 11.549 11.562"
                            style={{ marginRight: '10px' }}
                        >
                            <path
                                style={{
                                    fill: 'white',
                                    verticalAlign: 'initial'
                                }}
                                d="M8.76,2.973a.962.962,0,0,0-.948.976V7.8H3.962a.962.962,0,1,0,0,1.925h3.85v3.85a.962.962,0,1,0,1.925,0V9.722h3.85a.962.962,0,1,0,0-1.925H9.737V3.948a.962.962,0,0,0-.976-.976Z"
                                transform="translate(-3 -2.972)"
                            />
                        </svg>
                        {t('add_student')}
                    </Button>
                </Col> */}
            </Row>
        </div>
    );
}

export default Search;
