import {
    Button,
    Dropdown,
    Menu,
    Progress,
    Table,
    Tooltip,
    Pagination
} from 'antd';
import { ColumnsType } from 'antd/lib/table';
import PaginationComponent from 'components/pagination/PaginationComponent';
import Confirm from 'components/shares/modal/Confirm';
import { getUrl, subString } from 'helper/String';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { COURSE_QUIZ_RESULT } from 'routes/web/paths';
import { CollectionType } from 'types/Base';
import { User } from 'types/User';
import './List.scss';
interface Data {
    _id: number;
    student_id: string;
    student_group_id: string;
}

interface props {
    listUser: User[];
    limit: number;
    page: number;
    total: number;
    totalPage: number;
    onPageChange: (page: number) => void;
    onRemoveCourseStudent: (
        userId: string,
        collectionType: string
    ) => void | undefined;
    courseId: number;
}
interface Options {
    data: any;
}

function List(props: props): JSX.Element {
    const { t } = useTranslation();
    const [pageCurrent, setPageCurrent] = useState(props.page || 1);
    useEffect(() => {
        setPageCurrent(props.page);
    }, [props.page]);
    const history = useTranslation();
    const { listUser } = props;
    const [visible, setVisible] = useState(false);
    const onConfirmDelete = () => {
        setVisible(false);
        if (object) {
            props.onRemoveCourseStudent(object._id, object.collectionType);
        }
    };
    const [object, setObject] = useState<{
        _id: string;
        collectionType: string;
    }>();
    const [menu] = useState(() => (option: Options) => {
        return (
            <Menu>
                <Menu.Item>
                    <Link
                        to={{
                            pathname: `/teacher/course/${props?.courseId}/student/quiz-result/over-view`,
                            search: `userId=${option?.data?.user_id}`
                        }}
                    >
                        Xem lịch sử
                    </Link>
                </Menu.Item>
            </Menu>
        );
    });

    const [columns, setColumns] = useState<ColumnsType<User>>();

    useEffect(() => {
        setColumns([
            {
                title: t('num_order'),
                align: 'center',
                dataIndex: 'id',
                render: (text: any, record: any, index: any) => {
                    return index + 1;
                }
            },
            {
                title: t('full_name'),
                render: (record: any) => {
                    return (
                        <Tooltip placement="topLeft" title={record?.name}>
                            <>{subString(record?.name, 30, '...')}</>
                        </Tooltip>
                    );
                }
            },
            {
                title: t('email'),
                dataIndex: 'email',
                // eslint-disable-next-line react/display-name
                render: (mail: string) => {
                    return (
                        <Tooltip placement="topLeft" title={mail}>
                            <a href={'mailto:' + mail}>
                                {subString(mail, 30, '...')}
                            </a>
                        </Tooltip>
                    );
                }
            },
            {
                title: t('result'),
                align: 'center',

                // eslint-disable-next-line react/display-name
                render: (record: any) => {
                    return (
                        <>
                            <Progress
                                strokeColor={{
                                    from: '#108ee9',
                                    to: '#87d068'
                                }}
                                percent={record.progress || 0}
                                status="active"
                            />
                        </>
                    );
                }
            },
            {
                title: t('action'),
                dataIndex: 'id',
                align: 'center',
                // eslint-disable-next-line react/display-name
                render: (id: string, record: User) => {
                    return (
                        <>
                            <Dropdown
                                overlay={() =>
                                    menu({
                                        data: record
                                    })
                                }
                                placement="bottomCenter"
                            >
                                <Button>{t('action')}</Button>
                            </Dropdown>
                        </>
                    );
                }
            }
        ]);
    }, [listUser]);

    return (
        <div className="list-student-of-course">
            <Table
                rowKey={(user: User) => user._id}
                columns={columns}
                dataSource={listUser || []}
                pagination={false}
                bordered
            />

            {props.total > props.limit && (
                <div className="d-flex justify-content-end mt-3">
                    <Pagination
                        defaultCurrent={props.page | 0}
                        defaultPageSize={props.limit | 10}
                        total={props.total}
                        onChange={props.onPageChange}
                    ></Pagination>
                </div>
            )}

            <Confirm
                title={t('delete_stutent')}
                content={t('confirm_delete')}
                onVisible={visible}
                onOk={onConfirmDelete}
                onCancel={() => setVisible(false)}
            ></Confirm>
        </div>
    );
}

export default List;
