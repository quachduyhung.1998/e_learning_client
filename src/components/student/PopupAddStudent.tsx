import { LoadingOutlined } from '@ant-design/icons';
import { Col, Input, Modal, Row, Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import ButtonAdd from 'components/shares/button/ButtonAdd';
import { subString } from 'helper/String';
import useCourseAssignStudent from 'hooks/student/useCourseAssignStudent';
import useFetchSearchUnerolled from 'hooks/student/useFetchSearchUnerolled';
import filter from 'lodash/filter';
import map from 'lodash/map';
import takeWhile from 'lodash/takeWhile';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { CollectionType } from 'types/Base';
import { User } from 'types/User';
import useUpdateInteractiveCourse from 'hooks/course/useUpdateInteractiveCourse';
import ModalError from 'components/shares/modal/ModalError';
import useListUserPermission from 'hooks/user/useListUserPermission';
// import './List.scss';
const { Search } = Input;

interface props {
    modal: boolean;
    onHiddenModal: (status: boolean) => void;
    classId?: string;
    onReloadListUser: () => void;
}

/**
 * Form thêm mới học viên
 * @param props
 */
function PopupAddStudent(props: props): JSX.Element {
    const {
        resUpdateInteractive,
        onUpdateInteractiveCourse,
        updateInteractiveCourseError
    } = useUpdateInteractiveCourse();
    const { t } = useTranslation();
    const [selectedRowKeys, setSelectedRowKeys] = useState<any[]>([]);
    const [selectedUserIds, setSelectedUserIds] = useState<string[]>([]);
    const [selectedGroupIds, setSelectedGroupIds] = useState<string[]>([]);
    const [disable, setDisable] = useState(true);
    const [listUser, setListUser] = useState<User[]>([]);
    const [total, setTotal] = useState<number>(0);
    const [params, setParams] = useState({
        text: '',
        classId: props.classId,
        limit: 6,
        page: 1,
        mergeData: false
    });

    const [onFetchUsers, users, loadingList] = useListUserPermission();
    const pageRef = useRef(1);
    useEffect(() => {
        onFetchUsers({ roleId: 0 });
    }, []);

    const [
        onAssignStudent,
        resAssignStudent,
        loading,
        courseAssignStudentError
    ] = useCourseAssignStudent();

    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (courseAssignStudentError || updateInteractiveCourseError) {
            setShowMessageError(true);
        }
    }, [courseAssignStudentError, updateInteractiveCourseError]);
    useEffect(() => {
        const node = document.querySelector('#table-add-user .ant-table-body');
        if (node) {
            node.removeEventListener('scroll', () => {});
            node.addEventListener('scroll', () => {
                const perc =
                    (node.scrollTop / (node.scrollHeight - node.clientHeight)) *
                    100;
                if (perc >= 100) {
                    onLoadMore();
                }
            });
        }

        return node?.removeEventListener('scroll', () => {});
    }, [props.modal]);

    const onLoadMore = () => {
        const cOffset = total / params.limit;
        const cOffsetUpward = Math.ceil(cOffset);
        const nOffset = pageRef.current + 1;
        if (cOffsetUpward >= nOffset) {
            setParams({
                ...params,
                page: nOffset,
                mergeData: true
            });
        }
    };

    const [columns, setColumns] = useState<ColumnsType<User>>();

    useEffect(() => {
        setColumns([
            {
                title: t('num_order'),
                align: 'center',
                dataIndex: 'id',
                render: (text: any, record: User, index: any) => {
                    return index + 1;
                }
            },
            {
                title: t('full_name'),
                render: (record: User) => {
                    return (
                        <Tooltip placement="topLeft" title={record?.name}>
                            <>{subString(record?.name, 30, '...')}</>
                        </Tooltip>
                    );
                }
            },
            {
                title: 'Email',
                dataIndex: 'email',
                // eslint-disable-next-line react/display-name
                render: (email: string) => {
                    return (
                        <Tooltip placement="topLeft" title={email}>
                            <a href={'mailto:' + email}>
                                {subString(email, 30, '...')}
                            </a>
                        </Tooltip>
                    );
                }
            }
        ]);
    }, [users]);

    // đóng modal lại
    const toggle = () => {
        props.onHiddenModal(false);
        setSelectedUserIds([]);
        setSelectedGroupIds([]);
        setDisable(true);
        setParams({ ...params, page: 1, text: '', mergeData: false });
    };
    /**
     * Xử lý thêm mới người dùng
     * hoặc nhóm vào danh sách khoá học
     */
    const handleAddUser = () => {
        onAssignStudent({
            class_id: Number(props.classId),
            user_id: JSON.stringify(selectedRowKeys)
        });
        toggle();
    };

    useEffect(() => {
        if (loading === false) {
            setParams({ ...params, text: '', mergeData: false });
            props.onReloadListUser();
        }
    }, [loading]);

    /**
     * Tìm kiếm người dùng trong bảng
     * @param e
     */
    const handleSearch = (value: string) => {
        setParams({ ...params, text: value, mergeData: false, page: 1 });
    };

    const onSelectChange = (selectedRowKeys: any[], selectedRows: any[]) => {
        setSelectedRowKeys(selectedRowKeys);
        // setSelectedRowKeys(selectedRowKeys);
        // if (selectedRowKeys && selectedRowKeys.length) {
        //     setDisable(false);
        // } else {
        //     setDisable(true);
        // }
    };

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    };
    return (
        <div id="popup-student">
            <Modal
                visible={props.modal}
                onOk={toggle}
                onCancel={toggle}
                title={t('add_student')}
                footer={null}
                width={900}
                forceRender={true}
            >
                <Row gutter={[8, 8]} justify="space-between">
                    <Col span={12}>
                        <Search
                            className="box-search"
                            placeholder={t('search_student')}
                            onSearch={(value: string) => handleSearch(value)}
                        />
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <ButtonAdd
                            hidden={selectedRowKeys?.length === 0}
                            onClick={handleAddUser}
                        />
                    </Col>
                </Row>
                <Row className="list-student-of-course">
                    <Col>
                        <Row
                            style={{ background: 'white' }}
                            gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
                        >
                            <Col>
                                <Table
                                    id="table-add-user"
                                    rowSelection={rowSelection}
                                    rowKey="id"
                                    columns={columns}
                                    dataSource={users || []}
                                    scroll={{ x: true, y: 300 }}
                                    pagination={false}
                                    bordered
                                />
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Modal>
            {showMessageError && (
                <ModalError
                    messageError={
                        courseAssignStudentError || updateInteractiveCourseError
                    }
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
}

export default PopupAddStudent;
