import { Button, Dropdown, Menu, Select, Col, Row } from 'antd';
import Confirm from 'components/shares/modal/Confirm';
import { getUrl } from 'helper/String';
import useDeleteQuestion from 'hooks/question/useDeleteQuestion';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useParams } from 'react-router-dom';
import { Table } from 'reactstrap';
import { CREATE_QUESTION, DETAIL_QUESTION } from 'routes/web/paths';
import { DetailQuestion } from 'types/Question';
import './scss/ListQuestion.scss';
import ModalError from 'components/shares/modal/ModalError';
import useGetQuestionsOfQuiz from 'hooks/question/useGetQuestionsOfQuiz';
import { paths } from 'routes';
import { category_course, onKeyPressByRegex } from 'helper/Utils';
import { PlusOutlined } from '@ant-design/icons';

interface Props {
    questions?: DetailQuestion[];
    onReloadQuestion?: () => void;
    actions?: boolean;
}

interface Params {
    courseId: string;
    quizId: string;
}
interface Options {
    data: DetailQuestion;
}

function ListQuestion(props: Props): JSX.Element {
    const { t } = useTranslation();
    const params: Params = useParams();
    const history = useHistory();
    const { questions } = props;
    const [category, setCategory] = useState(1);

    const {
        resGetQuestionsNewPage,
        resGetQuestionsOfQuiz,
        setResGetQuestionsOfQuiz,
        resTotalQuestion,
        onGetQuestionsOfQuiz
    } = useGetQuestionsOfQuiz();

    const [
        resQuestion,
        onDeleteQuestion,
        loading,
        messageError
    ] = useDeleteQuestion();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    const onReloadQuestion = () => {
        onGetQuestionsOfQuiz({ category: category });
    };

    useEffect(() => {
        if (!loading) {
            setObject(undefined);
            setVisible(false);
            onReloadQuestion();
        }
        if (messageError) {
            setShowMessageError(true);
        }
    }, [loading, messageError, category]);
    const [menu] = useState(() => (option: Options) => {
        return (
            <Menu>
                <Menu.Item>
                    <Link
                        to={{
                            pathname: DETAIL_QUESTION,
                            search: `id=${option.data.id}`
                        }}
                    >
                        {t('edit')}
                    </Link>
                </Menu.Item>
                <Menu.Item>
                    <Link
                        to="#"
                        onClick={(e) => {
                            e.preventDefault();
                            setVisible(true);
                            setObject({
                                courseId: Number(params.courseId),
                                quizId: Number(params.quizId),
                                questionId: option.data.id,
                                id: option.data.id.toString()
                            });
                        }}
                    >
                        {t('delete')}
                    </Link>
                </Menu.Item>
            </Menu>
        );
    });
    const [visible, setVisible] = useState(false);
    const [object, setObject] = useState<{
        courseId: number;
        quizId: number;
        questionId: number;
        id?: string;
    }>();
    /**
     * Xóa Câu hỏi
     */
    const onOkDeleteConfirm = () => {
        setVisible(false);
        if (object) onDeleteQuestion(object);
    };
    return (
        <>
            <Row justify="space-between">
                <Col sm={6} className="my-2">
                    <Select
                        onChange={(value) => setCategory(Number(value))}
                        className="w-100 "
                    >
                        {category_course.map((item) => {
                            return (
                                <Select.Option value={item.id} key={item.id}>
                                    {item.name}
                                </Select.Option>
                            );
                        })}
                    </Select>
                </Col>
                <Col sm={4} className="my-2">
                    <Button
                        shape="round"
                        icon={<PlusOutlined />}
                        type="primary"
                        onClick={() => history.push(CREATE_QUESTION)}
                    >
                        {t('add_question')}
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col sm={24}>
                    <Table bordered>
                        <thead>
                            <tr className="text-center">
                                <th style={{ width: '10%' }}>
                                    {t('num_order')}
                                </th>
                                <th>{t('quiz_name')}</th>
                                {(props.actions === undefined ||
                                    props.actions) && <th>{t('action')}</th>}
                            </tr>
                        </thead>
                        <tbody>
                            {resGetQuestionsNewPage?.map(
                                (item: DetailQuestion, index: number) => {
                                    return (
                                        <tr key={item.id} className="">
                                            <th
                                                scope="row"
                                                className="text-center"
                                            >
                                                {index + 1}
                                            </th>
                                            <td>{item.name}</td>
                                            {(props.actions === undefined ||
                                                props.actions) && (
                                                <td
                                                    style={{ width: '20%' }}
                                                    className="text-center"
                                                >
                                                    <Dropdown
                                                        overlay={() =>
                                                            menu({ data: item })
                                                        }
                                                        placement="bottomCenter"
                                                    >
                                                        <Button>
                                                            {t('action')}
                                                        </Button>
                                                    </Dropdown>
                                                </td>
                                            )}
                                        </tr>
                                    );
                                }
                            )}
                        </tbody>
                    </Table>
                </Col>
            </Row>
            {showMessageError && (
                <ModalError
                    messageError={messageError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
            <Confirm
                title={`${t('delete_question')}?`}
                content={t('delete_confirmation_content', { name: 'câu hỏi' })}
                onVisible={visible}
                onOk={onOkDeleteConfirm}
                onCancel={() => setVisible(false)}
            />
        </>
    );
}

export default React.memo(ListQuestion);
