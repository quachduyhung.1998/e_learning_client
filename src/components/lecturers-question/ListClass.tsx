import { Button, Col, Dropdown, Menu, Row, Select } from 'antd';
import Confirm from 'components/shares/modal/Confirm';
import ModalError from 'components/shares/modal/ModalError';
import PopupAddStudent from 'components/student/PopupAddStudent';
import { category_course } from 'helper/Utils';
import useFetchListClass from 'hooks/class/useFetchListClass';
import useDeleteQuestion from 'hooks/question/useDeleteQuestion';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useParams } from 'react-router-dom';
import { Table } from 'reactstrap';
import { ACTION_ADD_CLASS } from 'routes/web/paths';
import { DetailQuestion } from 'types/Question';
import './scss/ListQuestion.scss';

interface Props {
    questions?: DetailQuestion[];
    onReloadQuestion?: () => void;
    actions?: boolean;
}

interface Params {
    courseId: string;
    quizId: string;
}
interface Options {
    data: DetailQuestion;
}

function ListClass(props: Props): JSX.Element {
    const { t } = useTranslation();
    const params: Params = useParams();
    const history = useHistory();
    const { questions } = props;
    const [category, setCategory] = useState(1);

    const {
        listClass,
        loading: loadingListClass,
        onFetchListClass
    } = useFetchListClass();

    const [
        resQuestion,
        onDeleteQuestion,
        loading,
        messageError
    ] = useDeleteQuestion();
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    const onReloadQuestion = () => {
        onFetchListClass({});
    };

    useEffect(() => {
        if (!loading) {
            setObject(undefined);
            setVisible(false);
            onReloadQuestion();
        }
        if (messageError) {
            setShowMessageError(true);
        }
    }, [loading, messageError, category]);
    const [menu] = useState(() => (option: Options) => {
        return (
            <Menu>
                <Menu.Item>
                    <span
                        onClick={() => {
                            setCourseId(option.data.id);
                            toggleModalAddStudent();
                        }}
                    >
                        {t('add_student')}
                    </span>
                </Menu.Item>
            </Menu>
        );
    });
    const [visible, setVisible] = useState(false);
    const [object, setObject] = useState<{
        courseId: number;
        quizId: number;
        questionId: number;
        id?: string;
    }>();
    /**
     * Xóa Câu hỏi
     */
    const onOkDeleteConfirm = () => {
        setVisible(false);
        if (object) onDeleteQuestion(object);
    };
    const [visibleModalAddStudent, setVisibleModalAddStudent] = useState(false);
    const [courseId, setCourseId] = useState<any>();
    const toggleModalAddStudent = () => {
        setVisibleModalAddStudent(!visibleModalAddStudent);
    };
    return (
        <>
            <PopupAddStudent
                onReloadListUser={() => {}}
                classId={courseId}
                modal={visibleModalAddStudent}
                onHiddenModal={toggleModalAddStudent}
            />
            <Row justify="space-between">
                <Col sm={6} className="my-2">
                    <Select
                        onChange={(value) => setCategory(Number(value))}
                        className="w-100 "
                    >
                        {category_course.map((item) => {
                            return (
                                <Select.Option value={item.id} key={item.id}>
                                    {item.name}
                                </Select.Option>
                            );
                        })}
                    </Select>
                </Col>
                {/* <Col sm={4} className="my-2">
                    <Button
                        shape="round"
                        icon={<PlusOutlined />}
                        type="primary"
                        onClick={() =>
                            history.push({ pathname: CLASS, hash: '#create' })
                        }
                    >
                        {t('add_class')}
                    </Button>
                </Col> */}
            </Row>
            <Row>
                <Col sm={24}>
                    <Table bordered>
                        <thead>
                            <tr className="text-center">
                                <th style={{ width: '10%' }}>
                                    {t('num_order')}
                                </th>
                                <th>{t('class_name')}</th>
                                {(props.actions === undefined ||
                                    props.actions) && <th>{t('action')}</th>}
                            </tr>
                        </thead>
                        <tbody>
                            {listClass?.map((item: any, index: number) => {
                                return (
                                    <tr key={item.id} className="">
                                        <th scope="row" className="text-center">
                                            {index + 1}
                                        </th>
                                        <td>
                                            <span
                                                onClick={() =>
                                                    history.push({
                                                        pathname: '/post',
                                                        search: `classId=${item?.id}`
                                                    })
                                                }
                                            >
                                                {item.name}
                                            </span>
                                        </td>
                                        {(props.actions === undefined ||
                                            props.actions) && (
                                            <td
                                                style={{ width: '20%' }}
                                                className="text-center"
                                            >
                                                <Dropdown
                                                    overlay={() =>
                                                        menu({ data: item })
                                                    }
                                                    placement="bottomCenter"
                                                >
                                                    <Button>
                                                        {t('action')}
                                                    </Button>
                                                </Dropdown>
                                            </td>
                                        )}
                                    </tr>
                                );
                            })}
                        </tbody>
                    </Table>
                </Col>
            </Row>
            {showMessageError && (
                <ModalError
                    messageError={messageError}
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
            <Confirm
                title={`${t('delete_question')}?`}
                content={t('delete_confirmation_content', { name: 'câu hỏi' })}
                onVisible={visible}
                onOk={onOkDeleteConfirm}
                onCancel={() => setVisible(false)}
            />
        </>
    );
}

export default React.memo(ListClass);
