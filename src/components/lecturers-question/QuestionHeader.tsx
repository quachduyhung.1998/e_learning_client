import ButtonCreate from 'components/shares/button/ButtonCreate';
import { getUrl } from 'helper/String';
import React, { MouseEvent } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useParams } from 'react-router-dom';
import { Col, Row } from 'reactstrap';
import { CREATE_QUESTION } from 'routes/web/paths';
import images from 'images';

interface Params {
    courseId: string;
    quizId: string;
}

function QuestionHeader(): JSX.Element {
    const { t } = useTranslation();
    const history = useHistory();
    const params: Params = useParams();

    const handleRedirectCreate = (
        e?: React.MouseEvent<HTMLElement, globalThis.MouseEvent>
    ) => {
        e?.preventDefault();
        history.push(
            `${getUrl(CREATE_QUESTION, {
                courseId: params.courseId,
                quizId: params.quizId
            })}`
        );
    };

    return (
        <Row>
            <Col>
                <div className="d-flex">
                    <h3 className="mr-auto"></h3>
                    <div className="p-2 list-action">
                        <ButtonCreate
                            title={t('add_question')}
                            onClick={handleRedirectCreate}
                            icon={images.ic_plus}
                        />
                    </div>
                </div>
            </Col>
        </Row>
    );
}

export default React.memo(QuestionHeader);
