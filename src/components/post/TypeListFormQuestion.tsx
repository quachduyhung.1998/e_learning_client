import React, { useEffect, useState } from 'react';
import { Form, Space, Radio, Checkbox, Button } from 'antd';
import { QuestionTypeAnswer } from 'types/Question';
import { CloseCircleOutlined, PlusOutlined } from '@ant-design/icons';
function TypeListFormQuestion(props: any): JSX.Element {
    const {
        form,
        checkType,
        CKEditor,
        t,
        onAnswerDetail,
        selectQuestionType
    } = props;
    const handleChangeAnswerCorrect = (
        e: any,
        id: number,
        type: number,
        count: number
    ) => {
        if (type === QuestionTypeAnswer.ONE_ANSWER) {
            for (let index = 0; index < count; index++) {
                datas[index].answerCorrect = 0;
            }
        }
        datas[id].answerCorrect = e.target.checked ? 1 : 0;
        setDatas([...datas]);
    };
    /**
     * Chỉnh sửa ckeditor
     * @param _event : sự kiện
     * @param editor : lấy dữ liệu
     * @param id : truyền id
     */
    const handleChangeAnswers = (_event: any, editor: any, id: number) => {
        datas[id].answers = editor.getData();
        setDatas([...datas]);
    };
    const [datas, setDatas] = useState<
        { answerCorrect: number; answers: string | undefined }[]
    >([]);

    useEffect(() => {
        const newData: {
            answerCorrect: number;
            answers: string | undefined;
        }[] = [...datas];
        if (selectQuestionType) {
            for (let index = 0; index < newData.length; index++) {
                newData[index].answerCorrect = 0;
            }
        }
        setDatas(newData);
    }, [checkType]);

    useEffect(() => {
        const newData = [...datas];
        form.setFieldsValue({ datas: newData });
    }, [datas]);

    useEffect(() => {
        if (onAnswerDetail && onAnswerDetail.answers) {
            const arr: { answerCorrect: number; answers: string }[] = [];
            if (selectQuestionType) {
                for (
                    let index = 0;
                    index < onAnswerDetail.answers.length;
                    index++
                ) {
                    arr.push({
                        answerCorrect: 0,
                        answers: onAnswerDetail.answers[index]
                    });
                }
            } else {
                for (
                    let index = 0;
                    index < onAnswerDetail.answers.length;
                    index++
                ) {
                    arr.push({
                        answerCorrect: onAnswerDetail.answerCorrect[index],
                        answers: onAnswerDetail.answers[index]
                    });
                }
            }
            setDatas(arr);
        } else {
            setDatas([
                { answerCorrect: 0, answers: '' },
                { answerCorrect: 0, answers: '' }
            ]);
        }
    }, [onAnswerDetail]);

    return (
        <Form.List name="datas">
            {(fields, { add, remove }) => {
                return (
                    <div>
                        <Form.Item>
                            {fields.map((field, index) => (
                                <div key={field.key}>
                                    <Space
                                        align="start"
                                        style={{ marginBottom: '8px' }}
                                    >
                                        <Form.Item
                                            name={[field.name, 'answerCorrect']}
                                        >
                                            {checkType === 1 ? (
                                                <Radio
                                                    onChange={(e) =>
                                                        handleChangeAnswerCorrect(
                                                            e,
                                                            field.name,
                                                            checkType,
                                                            fields.length
                                                        )
                                                    }
                                                    checked={
                                                        datas[field.name]
                                                            ? datas[field.name]
                                                                  .answerCorrect ===
                                                              1
                                                                ? true
                                                                : false
                                                            : false
                                                    }
                                                />
                                            ) : (
                                                <Checkbox
                                                    onChange={(e) =>
                                                        handleChangeAnswerCorrect(
                                                            e,
                                                            index,
                                                            checkType,
                                                            fields.length
                                                        )
                                                    }
                                                    checked={
                                                        datas[field.name]
                                                            ? datas[field.name]
                                                                  .answerCorrect ===
                                                              1
                                                                ? true
                                                                : false
                                                            : false
                                                    }
                                                />
                                            )}
                                        </Form.Item>
                                        <Form.Item
                                            className="ckh-200"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: t('required', {
                                                        name: t('answer')
                                                    })
                                                },
                                                {
                                                    transform: (
                                                        value: string
                                                    ) => {
                                                        const el = window.document.createElement(
                                                            'div'
                                                        );
                                                        el.innerHTML = value;
                                                        return el.textContent;
                                                    },
                                                    max: 1000,
                                                    message: t(
                                                        'answer_content_cannot_1000_characters'
                                                    )
                                                }
                                            ]}
                                            name={[field.name, 'answers']}
                                        >
                                            <CKEditor
                                                onChange={(
                                                    _event: any,
                                                    editor: any
                                                ) => {
                                                    return handleChangeAnswers(
                                                        _event,
                                                        editor,
                                                        index
                                                    );
                                                }}
                                                data={
                                                    datas[field.name]
                                                        ? datas[field.name]
                                                              .answers
                                                        : ''
                                                }
                                            />
                                        </Form.Item>

                                        {fields.length > 2 ? (
                                            <CloseCircleOutlined
                                                style={{ color: 'red' }}
                                                className="dynamic-delete-button"
                                                onClick={() => {
                                                    remove(field.name);
                                                    const dataAnswers = datas.filter(
                                                        (i, k) =>
                                                            k !== field.name
                                                    );
                                                    setDatas(dataAnswers);
                                                }}
                                            />
                                        ) : null}
                                    </Space>
                                </div>
                            ))}
                        </Form.Item>

                        <Form.Item>
                            <Button
                                type="primary"
                                onClick={() => {
                                    add();
                                    datas.push({
                                        answers: undefined,
                                        answerCorrect: 0
                                    });
                                    setDatas([...datas]);
                                }}
                            >
                                {t('add_answer')}
                            </Button>
                        </Form.Item>
                    </div>
                );
            }}
        </Form.List>
    );
}
export default TypeListFormQuestion;
