import React, { useEffect, useState } from 'react';
import { Radio, Col, Row, Form, Input, Space } from 'antd';
import { AnswerCorrect } from '../../types/Question';

function TypeTrueFalseQuestion(props: any) {
    const { form, onAnswerDetail, selectQuestionType, checkType, t } = props;
    useEffect(() => {
        if (onAnswerDetail) {
            if (onAnswerDetail && onAnswerDetail.answers) {
                const arr: { answerCorrect: number; answers: string }[] = [];
                for (
                    let index = 0;
                    index < onAnswerDetail.answers.length;
                    index++
                ) {
                    arr.push({
                        answerCorrect: onAnswerDetail.answerCorrect[index],
                        answers: onAnswerDetail.answers[index]
                    });
                }
                setDatas(arr);
            }
        } else {
            setDatas([
                {
                    answerCorrect: 0,
                    answers: 'Đúng'
                },
                {
                    answerCorrect: 0,
                    answers: 'Sai'
                }
            ]);
        }
    }, []);
    useEffect(() => {
        if (selectQuestionType) {
            setDatas([
                {
                    answerCorrect: 0,
                    answers: 'Đúng'
                },
                {
                    answerCorrect: 0,
                    answers: 'Sai'
                }
            ]);
        }
    }, [checkType]);
    const [datas, setDatas] = useState<
        { answerCorrect: number; answers: string | undefined }[]
    >([]);
    useEffect(() => {
        form.setFieldsValue({ datas: datas });
    }, [datas]);

    const handleAnswerCorrect = (number: number) => {
        const datas = form.getFieldValue('datas');
        if (number === 0) {
            datas[number].answerCorrect = 1;
            datas[1].answerCorrect = 0;
        } else {
            datas[number].answerCorrect = 1;
            datas[0].answerCorrect = 0;
        }
        setDatas([...datas]);
    };
    return (
        <>
            <Row>
                <Col className="gutter-row" span={12}>
                    <Space align="start" style={{ marginBottom: '8px' }}>
                        <Form.Item
                            name={['datas', 0, 'answerCorrect']}
                            initialValue={1}
                        >
                            <Radio
                                onChange={() => handleAnswerCorrect(0)}
                                checked={
                                    datas[0]
                                        ? datas[0].answerCorrect ===
                                          AnswerCorrect.CORRECT
                                            ? true
                                            : false
                                        : false
                                }
                            />
                        </Form.Item>
                        <Form.Item
                            name={['datas', 0, 'answers']}
                            initialValue="Đúng"
                        >
                            <Input
                                placeholder="Đúng"
                                disabled
                                style={{ background: 'white', color: 'black' }}
                            />
                        </Form.Item>
                    </Space>
                </Col>
                <Col className="gutter-row" span={12}>
                    <Space align="start" style={{ marginBottom: '8px' }}>
                        <Form.Item
                            name={['datas', 1, 'answerCorrect']}
                            initialValue={0}
                        >
                            <Radio
                                onChange={() => handleAnswerCorrect(1)}
                                checked={
                                    datas[1]
                                        ? datas[1].answerCorrect ===
                                          AnswerCorrect.CORRECT
                                            ? true
                                            : false
                                        : false
                                }
                            />
                        </Form.Item>
                        <Form.Item
                            name={['datas', 1, 'answers']}
                            initialValue={t('false')}
                        >
                            <Input
                                disabled
                                placeholder={t('false')}
                                style={{ background: 'white', color: 'black' }}
                            />
                        </Form.Item>
                    </Space>
                </Col>
            </Row>
        </>
    );
}

export default TypeTrueFalseQuestion;
