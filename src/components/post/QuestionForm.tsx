import CKEditor from 'components/ckeditor/ckEditor';
import {
    Button,
    Col,
    Form,
    Input,
    Row,
    Select,
    message,
    notification
} from 'antd';
import { Store } from 'antd/lib/form/interface';
import React, { MouseEvent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import usePostQuestion from '../../hooks/question/usePostQuestion';
import { DetailQuestion, QuestionTypeAnswer } from '../../types/Question';
import TypeListFormQuestion from './TypeListFormQuestion';
import usePutQuestion from 'hooks/question/usePutQuestion';
import { property, map } from 'lodash';
import TypeTrueFalseQuestion from './TypeTrueFalseQuestion';
import images from 'images';
import LabelRequired from 'components/shares/LabelRequired';
import TitleComponent from 'components/text/TitleComponent';
import constStyle from 'constants/Style';
import ModalError from 'components/shares/modal/ModalError';
import { category_course, onKeyPressByRegex } from 'helper/Utils';
interface Props {
    question?: DetailQuestion;
    courseId?: string;
    quizId?: string;
}
interface Data {
    id?: number;
    name: string;
    answers: string;
    type_question: number;
    course_category_id: number;
    mark: number | string;
}
function QuestionForm(props: Props): JSX.Element {
    const { t } = useTranslation();
    const { question, courseId, quizId } = props;
    const handleChangeContent = (event: any, editor: any) => {
        form.setFieldsValue({ questionText: editor.getData() });
    };

    const [form] = Form.useForm();
    const validateForm = {
        name: [
            {
                required: true,
                message: t('required', { name: t('quiz_name') })
            },
            {
                max: 500,
                message: t('question_name_up_to_500_characters')
            }
        ],
        type_question: [
            {
                required: true,
                message: t('required', { name: t('type_question') })
            }
        ],
        subject: [
            {
                required: true,
                message: t('required', { name: t('subject') })
            }
        ],
        point: [
            {
                required: true,
                message: t('required', { name: t('point') })
            }
        ],
        content: [
            {
                required: true,
                message: t('required', { name: t('content') })
            },
            {
                transform: (value: string) => {
                    const el = window.document.createElement('div');
                    el.innerHTML = value;
                    return el.textContent;
                },
                max: 2000,
                message: t('question_content_cannot_2000_characters')
            }
        ]
    };
    const [
        onPostQuestion,
        resPostQuestion,
        loadingPostQuestion,
        postQuestionError
    ] = usePostQuestion();
    const [
        onPutQuestion,
        resPutQuestion,
        loadingPutQuestion,
        putQuetionError
    ] = usePutQuestion();
    const [messageError, setMessageError] = useState<string>('');
    const [showMessageError, setShowMessageError] = useState(false);
    const handleCancelMessage = () => {
        setShowMessageError(false);
    };
    useEffect(() => {
        if (messageError || postQuestionError || putQuetionError) {
            setShowMessageError(true);
        }
    }, [messageError, postQuestionError, putQuetionError]);
    const [questionAnswer, setQuestionAnswer] = useState<
        any | { answerCorrect: number[]; answer: string[] }
    >();
    useEffect(() => {
        if (question) {
            form.setFieldsValue({
                name: question.name.trim(),
                type: question.type_question,
                questionText: question.questionText,
                point: question.mark,
                course_category_id: question.course_category_id
            });
            SetType(Number(question.type_question));
            const answerTemp:
                | any
                | { answerCorrect: number[]; answer: string[] } = {};
            const answerCorrect: number[] = [];
            const answer: number[] = [];
            question.answers?.forEach((item: any) => {
                answerCorrect?.push(Number(item.right));
                answer?.push(item.key);
            });

            setQuestionAnswer({
                answerCorrect: answerCorrect,
                answer: answer
            });
        } else {
            form.setFieldsValue({
                name: undefined,
                type: checkType,
                questionText: undefined
            });
        }
    }, [question]);

    /**
     * Xử lý lưu dữ liệu form
     * @param value
     */
    const onSubmit = (value: Store) => {
        if (value.datas === undefined) {
            value.datas = form.getFieldValue('datas');
        }
        if (value.point == 0) {
            return notification.error({ message: 'Số điểm phải lớn hơn 0' });
        }
        const answerCorrect: number[] = map(
            value.datas,
            property('answerCorrect')
        );
        if (answerCorrect) {
            const arrAnswerCorrect = answerCorrect.filter(
                (answer) => answer === 1
            );
            if (arrAnswerCorrect.length === 0) {
                setMessageError(`${t('choose_answer_correct')}`);
                return;
            }
            if (value.type === 0 && arrAnswerCorrect.length < 2) {
                setMessageError(
                    `${t(
                        'choose_at_least_2_answers_for_this_type_of_question'
                    )}`
                );
                return;
            }
        }
        const answer: string[] = map(value.datas, property('answers'));
        const answerQuestion: any[] = [];
        for (const key in answerCorrect) {
            answerQuestion.push({
                key: answer[key],
                right: answerCorrect[key]
            });
        }
        const data: Data = {
            name: value.name,
            answers: JSON.stringify(answerQuestion),
            type_question: checkType,
            mark: value.point,
            course_category_id: value.course_category_id
        };

        if (question) {
            data.id = question.id;
            onPutQuestion(data);
        } else {
            onPostQuestion(data);
        }
    };
    const history = useHistory();

    const [checkType, SetType] = useState(0);
    const [selectQuestionType, setSelectQuestionType] = useState(false);
    /**
     * Chọn kiểu : 1 => chọn một , 0 => chọn nhiều , 2 => chọn đúng sai
     * @param value
     */
    const handleSelectType = (value: number) => {
        SetType(value);
        setSelectQuestionType(true);
    };
    /**
     * Trở về trang danh sách
     * @param e
     */
    const handleCancel = (e: MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        history.goBack();
    };
    const showHtml = () => {
        if (checkType === QuestionTypeAnswer.TRUE_FALSE) {
            return (
                <TypeTrueFalseQuestion
                    form={form}
                    onAnswerDetail={{
                        answerCorrect: question?.answerCorrect,
                        answers: question?.answer
                    }}
                    checkType={checkType}
                    selectQuestionType={selectQuestionType}
                    t={t}
                />
            );
        } else {
            return (
                <TypeListFormQuestion
                    form={form}
                    checkType={checkType}
                    CKEditor={CKEditor}
                    t={t}
                    onAnswerDetail={{
                        answerCorrect: questionAnswer?.answerCorrect,
                        answers: questionAnswer?.answer
                    }}
                    selectQuestionType={selectQuestionType}
                />
            );
        }
    };
    return (
        <div className="question-form bg-white">
            <Form
                form={form}
                layout="horizontal"
                onFinish={onSubmit}
                onFinishFailed={(err) => err}
                hideRequiredMark={true}
            >
                <Row style={{ marginBottom: '20px' }}>
                    <Col sm={24}>
                        <Row className="d-flex" justify="space-between">
                            <TitleComponent
                                title={t('add_question')}
                                color={constStyle.COLOR_MAIN}
                            />
                            <div className="list-action">
                                <Button
                                    className="style-btn"
                                    style={{ marginRight: '10px' }}
                                    type="default"
                                    onClick={handleCancel}
                                >
                                    <img src={images.ic_down} alt="" />
                                    {t('back')}
                                </Button>
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    className="style-btn"
                                >
                                    <img src={images.ic_upload} alt="" />
                                    {question ? t('update') : t('create')}
                                </Button>
                            </div>
                        </Row>
                    </Col>
                </Row>
                <Form.Item
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 20 }}
                    label={<LabelRequired label={t('quiz_name')} />}
                    name="name"
                    labelAlign="left"
                    rules={validateForm.name}
                >
                    <Input
                        onBlur={(e) => {
                            form.setFieldsValue({
                                name: e.target.value.trim()
                            });
                        }}
                    />
                </Form.Item>
                <Form.Item
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 20 }}
                    label={<LabelRequired label={t('point')} />}
                    name="point"
                    labelAlign="left"
                    rules={validateForm.point}
                >
                    <Input onKeyPressCapture={onKeyPressByRegex} />
                </Form.Item>
                <Form.Item
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 20 }}
                    label={<LabelRequired label={t('subject')} />}
                    name="course_category_id"
                    labelAlign="left"
                    rules={validateForm.subject}
                >
                    <Select>
                        {category_course.map((item) => {
                            return (
                                <Select.Option value={item.id} key={item.id}>
                                    {item.name}
                                </Select.Option>
                            );
                        })}
                    </Select>
                </Form.Item>
                <Form.Item
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 20 }}
                    label={<LabelRequired label={t('type_question')} />}
                    name="type"
                    labelAlign="left"
                    rules={validateForm.type_question}
                >
                    <Select onChange={handleSelectType}>
                        <Select.Option value={QuestionTypeAnswer.ONE_ANSWER}>
                            {t('choose_one')}
                        </Select.Option>
                        <Select.Option value={QuestionTypeAnswer.MULTI_ANSWERS}>
                            {t('choose_multi')}
                        </Select.Option>
                        <Select.Option value={QuestionTypeAnswer.TRUE_FALSE}>
                            {t('choose_true_false')}
                        </Select.Option>
                    </Select>
                </Form.Item>
                {/* <Form.Item
                    name="questionText"
                    className="ckh-200"
                    label={<LabelRequired label={t('content')} />}
                    rules={validateForm.content}
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 20 }}
                    labelAlign="left"
                >
                    <CKEditor
                        onChange={handleChangeContent}
                        data={question?.questionText}
                    />
                </Form.Item> */}
                <Form.Item
                    label={<LabelRequired label={t('answer')} />}
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 20 }}
                    labelAlign="left"
                    required={true}
                >
                    {showHtml()}
                </Form.Item>
            </Form>
            {showMessageError && (
                <ModalError
                    messageError={
                        messageError || postQuestionError || putQuetionError
                    }
                    handleCancel={handleCancelMessage}
                ></ModalError>
            )}
        </div>
    );
}

export default React.memo(QuestionForm);
