import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import LocalStorageHelper from 'helper/LocalStorageHelper';

type SliceState = {
    id: string | null;
    name: string | null;
    token: string | null;
    refreshToken: string | null;
    userRoles: string[];
};

const userRoles = LocalStorageHelper.getUserStorage()?.userRoles || [];

const initialState: SliceState = {
    id: null,
    name: null,
    token: null,
    refreshToken: null,
    userRoles: userRoles
};

interface Auth {
    token: string;
    id: string;
    name: string;
    refreshToken: string;
    userRoles: string[];
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        login: (state, action: PayloadAction<{ auth: Auth }>) => {
            state.id = action.payload.auth.id;
            state.name = action.payload.auth.name;
            state.token = action.payload.auth.token;
            state.refreshToken = action.payload.auth.refreshToken;
        },
        updateUserRoles: (state, action: PayloadAction<Auth['userRoles']>) => {
            state.userRoles = action.payload;
        }
    }
});

const { reducer, actions } = authSlice;
export const { login, updateUserRoles } = actions;
export default reducer;
