sudo mkdir -p /var/www/adaptive-learning-web
cd /var/www/adaptive-learning-web/
sudo rm -rf node_modules build
sudo tar -xf ~/build.tar.gz -C .
[ ! -f .env ] && sudo cp .env.$1 .env
rm ~/build.tar.gz
